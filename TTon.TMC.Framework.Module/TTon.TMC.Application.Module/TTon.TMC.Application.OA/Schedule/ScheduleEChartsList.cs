﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.OA.Schedule
{
    public class ScheduleEChartsList
    {
        public string id { get; set; }

        public string scheduleName { get; set; }

        public string startTime { get; set; }

        public string endTime { get; set; }

        public string createUser { get; set; }

        public string createTime { get; set; }

        public string roomName { get; set; }

        public string services { get; set; }

        public string peopleCount { get; set; }

        public string equipment { get; set; }
    }
}
