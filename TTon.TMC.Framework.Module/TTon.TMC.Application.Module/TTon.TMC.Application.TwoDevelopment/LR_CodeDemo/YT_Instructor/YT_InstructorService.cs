﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Instructor.Model;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using TTon.TMC.Application.Base.AuthorizeModule;
using TTon.TMC.Application.Base.OrganizationModule;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.VX;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 16:01
    /// 描 述：配置指导书
    /// </summary>
    public class YT_InstructorService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据(web)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_InstructorEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                YT_Supplier.Name as Supplier_Id,YT_PartsTypes.Parts_Name AS Parts_Id,YT_MaintenanceTypes.Name AS MaintenanceType,
                t.SketchDate,
                t.Number,
                t.WorkHours,
                t.IssueDescription,
                t.IssuePic,
                t.Pick_Scope,
                t.Pick_Prepare,
                t.Pick_Step,
                t.Pick_Pic,
                t.Pick_Demand,
                t.Pick_Project,
                t.Remark,
                t.Remark_Pic,
                t.Name,
                t.Parts_Num
                ");
                strSql.Append("  FROM YT_Instructor t LEFT JOIN YT_Supplier ON YT_Supplier.Id=t.Supplier_Id  LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id=t.Parts_Id  LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id=t.MaintenanceType ");
                strSql.Append("  WHERE 1=1  And t.IsDelete=0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Supplier_Id"].IsEmpty())
                {
                    dp.Add("Supplier_Id", "%" + queryParam["Supplier_Id"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Supplier_Id Like @Supplier_Id ");
                }
                if (!queryParam["Parts_Id"].IsEmpty())
                {
                    dp.Add("Parts_Id", "%" + queryParam["Parts_Id"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Parts_Id Like @Parts_Id ");
                }
                return this.BaseRepository().FindList<YT_InstructorEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取页面显示列表数据(api)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_InstructorEntity> GetPageListbyAPI(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                YT_Supplier.Name as Supplier_Id,YT_PartsTypes.Parts_Name AS Parts_Id,YT_MaintenanceTypes.Name AS MaintenanceType,
                t.SketchDate,
                t.Number,
                t.WorkHours,
                t.IssueDescription,
                t.IssuePic,
                t.Pick_Scope,
                t.Pick_Prepare,
                t.Pick_Step,
                t.Pick_Pic,
                t.Pick_Demand,
                t.Pick_Project,
                t.Remark,
                t.Remark_Pic,
                t.Name
                ");
                strSql.Append("  FROM YT_Instructor t LEFT JOIN YT_Supplier ON YT_Supplier.Id=t.Supplier_Id  LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id=t.Parts_Id  LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id=t.MaintenanceType ");
                strSql.Append("  WHERE 1=1  And t.IsDelete=0 ");

                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryJson.IsEmpty())
                {

                    strSql.Append(" AND t.Name Like" + "'%" + queryJson + "%'");
                }

                return this.BaseRepository().FindList<YT_InstructorEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }



        /// <summary>
        /// 获取YT_Instructor表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_InstructorEntity GetYT_InstructorEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_InstructorEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取指导书列表
        /// </summary>
        /// <param name="SupplierId"></param>
        /// <param name="PartId"></param>
        /// <param name="maintenanceId"></param>
        /// <returns></returns>
        public List<InstructorListModel> GetInstructorList(string SupplierId, string PartId, string MaintenanceId)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT
	                Id AS id,
	                Name AS name
                FROM
	                YT_Instructor
                WHERE
	                1 = 1
                AND IsDelete = 0
                AND Supplier_Id = '" + SupplierId + @"'
                AND Parts_Id =  '" + PartId + @"'
                AND MaintenanceType = '" + MaintenanceId + @"'
                ORDER BY
	                CreateDate DESC");
            List<InstructorListModel> list = new RepositoryFactory().BaseRepository().FindList<InstructorListModel>(sql.ToString()).AsList();
            return list;
        }


        /// <summary>
        /// 指导书详情
        /// </summary>
        /// <param name="keyvalue"></param>
        /// <returns></returns>
        public InstructorDetailModel GetInstructorDetail(string keyvalue)
        {
            try
            {
                InstructorDetailModel model = new InstructorDetailModel();

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                            t.Id AS Id,
	                            YT_Supplier.Name AS Supplier,
	                            YT_PartsTypes.Parts_Name AS Parts,
	                            t.Parts_Num AS PartsNum,
	                            YT_MaintenanceTypes.Name AS MaintenanceType,
	                            t.SketchDate AS SketchDate,
	                            t.Number AS Number,
	                            t.WorkHours AS WorkHours,
	                            t.IssueDescription AS IssueDescription,
	                            t.Pick_Scope AS Pick_Scope,
	                            t.Pick_Prepare AS Pick_Prepare,
	                            t.Pick_Step AS Pick_Step,
	                            t.Pick_Demand AS Pick_Demand,
	                            t.Pick_Project AS Pick_Project,
	                            t.Remark AS Remark,
	                            CONVERT (
		                            VARCHAR (100),
		                            t.CreateDate,
		                            120
	                            ) AS CreateDate,
	                            Base_User.F_RealName AS CreateUser,
	                            t.Name AS Name
                            FROM
	                            YT_Instructor t
                            LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.Parts_Id
                            LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintenanceType
                            LEFT JOIN Base_User ON Base_User.F_UserId = t.CreateUserId
                            WHERE
	                            t.Id = '" + keyvalue + @"'
                            AND t.IsDelete = 0");
                List<InstructorDetailModel> list = new RepositoryFactory().BaseRepository().FindList<InstructorDetailModel>(sql.ToString()).AsList();
                model = list[0];

                //图片
                StringBuilder ImgSql = new StringBuilder();
                ImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Instructor ON YT_Instructor.IssuePic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Instructor.Id = '" + keyvalue + @"'");
                List<Image> ImgList = new RepositoryFactory().BaseRepository().FindList<Image>(ImgSql.ToString()).AsList();
                if (ImgList.Count != 0)
                {
                    foreach (var item in ImgList)
                    {
                        String a = item.Img_Url.Substring(item.Img_Url.LastIndexOf("yt_new/") + 7);
                        item.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.IssuePic = ImgList;

                //图片
                StringBuilder Img_PickSql = new StringBuilder();
                Img_PickSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Instructor ON YT_Instructor.Pick_Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Instructor.Id = '" + keyvalue + @"'");
                List<Image> Img_PickList = new RepositoryFactory().BaseRepository().FindList<Image>(Img_PickSql.ToString()).AsList();
                if (Img_PickList.Count != 0)
                {
                    foreach (var item in Img_PickList)
                    {
                        String a = item.Img_Url.Substring(item.Img_Url.LastIndexOf("yt_new/") + 7);
                        item.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.Pick_Pic = Img_PickList;

                //图片
                StringBuilder Img_RemarkSql = new StringBuilder();
                Img_RemarkSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Instructor ON YT_Instructor.Remark_Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Instructor.Id = '" + keyvalue + @"'");
                List<Image> Img_RemarkList = new RepositoryFactory().BaseRepository().FindList<Image>(Img_RemarkSql.ToString()).AsList();
                if (Img_RemarkList.Count != 0)
                {
                    foreach (var item in Img_RemarkList)
                    {
                        String a = item.Img_Url.Substring(item.Img_Url.LastIndexOf("fileAnnexes/") + 12);
                        item.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.Remark_Pic = Img_RemarkList;

                return model;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                YT_InstructorEntity entity = GetYT_InstructorEntity(keyValue);
                entity.IsDelete = 1;

                this.BaseRepository().Update<YT_InstructorEntity>(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_InstructorEntity entity)
        {
            try
            {
                YT_PartsTypesEntity Parts = new YT_PartsTypesService().GetYT_PartsTypesEntity(entity.Parts_Id);
                YT_MaintenanceTypesEntity maintenance = new YT_MaintenanceTypesService().GetYT_MaintenanceTypesEntity(entity.MaintenanceType);
                entity.Name = Parts.Parts_Name + "-" + maintenance.Name + "指导书";
                entity.Parts_Num = string.IsNullOrEmpty(Parts.Parts_Number) ? "" : Parts.Parts_Number;
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();

                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 新增指导书、维修类型、订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        /// <param name="orderEntity"></param>
        public void SaveSupplierOrOrder(YT_InstructorEntity instructorEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity, YT_OrderEntity orderEntity)
        {

            //判断零件是否存在
            YT_PartsTypesEntity partsTypesEntity = new YT_PartsTypesEntity();
            if (orderEntity.PartsNumber != null && orderEntity.PartsNumber != "")
            {
                partsTypesEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(d => d.Parts_Number == orderEntity.PartsNumber && d.IsDelete == 0);
                if (partsTypesEntity == null)
                {
                    partsTypesEntity = new YT_PartsTypesEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Parts_Name = orderEntity.PartsType,
                        Parts_Number = orderEntity.PartsNumber,
                        CreateDate = DateTime.Now,
                        IsDelete = 0
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_PartsTypesEntity>(partsTypesEntity);
                }
                orderEntity.PartsNumber = partsTypesEntity.Parts_Number;
                orderEntity.PartsType = partsTypesEntity.Id;
            }
            else
            {
                orderEntity.PartsNumber = "";
                orderEntity.PartsType = "";
            }
            
            //判断其类型是否存在
            string maintenacnceTypeId = "";
            string maintenacnceTypeName = "";
            List<YT_MaintenanceTypesEntity> yT_MaintenanceTypesList = new RepositoryFactory().BaseRepository().FindList<YT_MaintenanceTypesEntity>(d => d.Name.Replace(" ", "") == maintenanceTypesEntity.Name.Replace(" ", "")).AsList();
            if (yT_MaintenanceTypesList.Count == 0)
            {
                YT_MaintenanceTypesEntity yT_MaintenanceTypesEntity = new YT_MaintenanceTypesEntity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = maintenanceTypesEntity.Name,
                    IsDelete = 0,
                    CreateDate = DateTime.Now
                };
                new RepositoryFactory().BaseRepository().Insert<YT_MaintenanceTypesEntity>(yT_MaintenanceTypesEntity);

                maintenacnceTypeId = yT_MaintenanceTypesEntity.Id;
                maintenacnceTypeName = yT_MaintenanceTypesEntity.Name;
            }
            else
            {
                maintenacnceTypeId = yT_MaintenanceTypesList[0].Id;
                maintenacnceTypeName = yT_MaintenanceTypesList[0].Name;
            }


            //关联表
            if (orderEntity.PartsNumber != null && orderEntity.PartsNumber != "")
            {
                YT_SupplierOrPartsEntity yT_SupplierOrPartsEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierOrPartsEntity>(d => d.Supplier_Id == orderEntity.Supplier_Id && d.Parts_Id == partsTypesEntity.Id);
                if (yT_SupplierOrPartsEntity == null)
                {
                    yT_SupplierOrPartsEntity = new YT_SupplierOrPartsEntity()
                    {
                        Supplier_Id = orderEntity.Supplier_Id,
                        Parts_Id = orderEntity.PartsType,
                        Id = Guid.NewGuid().ToString(),
                        CreateDate = DateTime.Now,
                        IsDelete = 0,
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_SupplierOrPartsEntity>(yT_SupplierOrPartsEntity);
                }
            }

            instructorEntity.Supplier_Id = orderEntity.Supplier_Id;
            instructorEntity.Parts_Id = orderEntity.PartsType;
            instructorEntity.Parts_Num = orderEntity.PartsNumber;
            instructorEntity.MaintenanceType = maintenacnceTypeId;
            if (orderEntity.PartsNumber != null && orderEntity.PartsNumber != "")
            {
                instructorEntity.Name = partsTypesEntity.Parts_Name + "-" + maintenacnceTypeName + "指导书";
                instructorEntity.Parts_Num = string.IsNullOrEmpty(partsTypesEntity.Parts_Number) ? "" : partsTypesEntity.Parts_Number;
            }
            else
            {
                instructorEntity.Name = maintenacnceTypeName + "指导书";
                instructorEntity.Parts_Num = "";
            }
            instructorEntity.Id = Guid.NewGuid().ToString();
            instructorEntity.CreateDate = DateTime.Now;
            instructorEntity.IsDelete = 0;
            new RepositoryFactory().BaseRepository().Insert<YT_InstructorEntity>(instructorEntity);


            orderEntity.Instructor = instructorEntity.Id;
            orderEntity.MaintainType = maintenacnceTypeId;
            if (!string.IsNullOrEmpty(orderEntity.Date))
            {
                orderEntity.Date = Convert.ToDateTime(orderEntity.Date).ToString("yyyy-MM-dd HH:ss");
            }
            orderEntity.Order_Status = orderEntity.Area == 1 ? 6 : 2;
            new YT_OrderService().SaveEntity("", orderEntity);


        }

        /// <summary>
        /// 导入订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="supplierEntity"></param>
        /// <param name="orderUserEntity"></param>
        /// <param name="partsTypesEntity"></param>
        /// <param name="orderEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        public void ImportOrder(YT_InstructorEntity instructorEntity, YT_SupplierEntity supplierEntity, YT_OrderUserEntity orderUserEntity, YT_PartsTypesEntity partsTypesEntity, YT_OrderEntity orderEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity)
        {
            try
            {
                UserEntity userInfo = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_RealName == orderEntity.Name);
                string userId = "";
                if (userInfo != null)
                {
                    userId = userInfo.F_UserId;
                }
                #region 判断零件是否存在
                YT_PartsTypesEntity partsTypes = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(d => d.Parts_Number == partsTypesEntity.Parts_Number && d.Parts_Name == partsTypesEntity.Parts_Name);
                if (partsTypes == null)
                {
                    partsTypes = new YT_PartsTypesEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Parts_Name = partsTypesEntity.Parts_Name,
                        Parts_Number = partsTypesEntity.Parts_Number,
                        CreateDate = DateTime.Now,
                        CreateUserId = userId,
                        IsDelete = 0
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_PartsTypesEntity>(partsTypes);
                }
                orderEntity.PartsType = partsTypes.Id;
                #endregion

                #region 判断维修类型是否存在
                YT_MaintenanceTypesEntity maintenanceTypes = new RepositoryFactory().BaseRepository().FindEntity<YT_MaintenanceTypesEntity>(d => d.Name == maintenanceTypesEntity.Name);
                if (maintenanceTypes == null)
                {
                    maintenanceTypes = new YT_MaintenanceTypesEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = maintenanceTypesEntity.Name,
                        IsDelete = 0,
                        CreateDate = DateTime.Now,
                        CreateUserId = userId
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_MaintenanceTypesEntity>(maintenanceTypes);
                }
                orderEntity.MaintainType = maintenanceTypes.Id;
                #endregion

                #region 判断供应商是否存在
                YT_SupplierEntity supplier = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(d => d.Name == supplierEntity.Name);
                if (supplier == null)
                {
                    supplier = new YT_SupplierEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = supplierEntity.Name,
                        IsDelete = 0,
                        CreateDate = DateTime.Now,
                        Compellation = "",
                        Phone = "",
                        Code = "",
                        CreateUserId = userId
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_SupplierEntity>(supplier);
                }
                orderEntity.Supplier_Id = supplier.Id;
                #endregion

                #region 判断指导书是否存在
                string instructorName = partsTypes.Parts_Name + "-" + maintenanceTypes.Name + "指导书";
                YT_InstructorEntity instructor = new RepositoryFactory().BaseRepository().FindEntity<YT_InstructorEntity>(d => d.Name == instructorName);
                if (instructor == null)
                {
                    instructor = new YT_InstructorEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = instructorName,
                        IsDelete = 0,
                        CreateDate = DateTime.Now,
                        Supplier_Id = supplier.Id,
                        Parts_Id = partsTypes.Id,
                        MaintenanceType = maintenanceTypes.Id,
                        Parts_Num = partsTypes.Parts_Number,
                        IssueDescription = instructorEntity.IssueDescription,
                        Pick_Step = instructorEntity.Pick_Step,
                        CreateUserId = userId
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_InstructorEntity>(instructor);
                }
                orderEntity.Instructor = instructor.Id;
                #endregion

                #region 判断关联表是否存在
                YT_SupplierOrPartsEntity yT_SupplierOrPartsEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierOrPartsEntity>(d => d.Supplier_Id == supplier.Id && d.Parts_Id == partsTypes.Id);
                if (yT_SupplierOrPartsEntity == null)
                {
                    yT_SupplierOrPartsEntity = new YT_SupplierOrPartsEntity()
                    {
                        Supplier_Id = supplier.Id,
                        Parts_Id = partsTypes.Id,
                        Id = Guid.NewGuid().ToString(),
                        CreateDate = DateTime.Now,
                        IsDelete = 0,
                        CreateUserId = userId
                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_SupplierOrPartsEntity>(yT_SupplierOrPartsEntity);
                }
                #endregion

                #region 保存订单
                if (!string.IsNullOrEmpty(orderEntity.Date))
                {
                    orderEntity.Date = Convert.ToDateTime(orderEntity.Date).ToString("yyyy-MM-dd HH:mm");
                }
                if (!string.IsNullOrEmpty(orderEntity.SuccessDate))
                {
                    orderEntity.SuccessDate = Convert.ToDateTime(orderEntity.SuccessDate).ToString("yyyy-MM-dd HH:mm");
                }
                orderEntity.SupplierStatus = 1;
                orderEntity.Order_Status = 5;
                orderEntity.IsDelete = 0;
                orderEntity.CreateDate = DateTime.Now;
                orderEntity.CreateUserId = userId;
                string str = GenerateRandom(1);
                Random rd = new Random();
                int num = rd.Next(0, 10);
                orderEntity.OrderNumber = "YTX" + supplier.Name.Replace(" ", "") + DateTime.Now.ToString("yyMMddHHmm") + str + num;
                orderEntity.Id = Guid.NewGuid().ToString();
                new RepositoryFactory().BaseRepository().Insert<YT_OrderEntity>(orderEntity);
                #endregion

                #region 判断任务是否存在
                UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_UserId == "18aa3ea8-021c-4364-b2d3-a0ee41a92f49");
                if (userEntity != null)
                {
                    YT_OrderUserEntity orderUser = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderUserEntity>(d => d.Order_Id == orderEntity.Id);
                    if (orderUser == null)
                    {
                        orderUser = new YT_OrderUserEntity()
                        {
                            Id = Guid.NewGuid().ToString(),
                            Order_Id = orderEntity.Id,
                            Order_UserId = userEntity.F_UserId,
                            PartsType = partsTypes.Id,
                            IsApprover = 1,
                            CreateDate = DateTime.Now,
                            IsDelete = 0,
                            Accomplish = orderEntity.SuccessDate,
                            All = orderUserEntity.Success + orderUserEntity.Error,
                            Success = orderUserEntity.Success,
                            Error = orderUserEntity.Error,
                            CreateUserId = userId
                        };
                        new RepositoryFactory().BaseRepository().Insert<YT_OrderUserEntity>(orderUser);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static char[] constant = {
                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'
        };
        public static string GenerateRandom(int Length)
        {
            System.Text.StringBuilder newRandom = new System.Text.StringBuilder(26);
            Random rd = new Random();
            for (int i = 0; i < Length; i++)
            {
                newRandom.Append(constant[rd.Next(26)]);
            }
            return newRandom.ToString();
        }


        /// <summary>
        /// 新增指导书、维修类型、修改订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        /// <param name="orderEntity"></param>
        public void UpdateSupplierOrOrder(YT_InstructorEntity instructorEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity, YT_OrderEntity orderEntity)
        {

            YT_OrderEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(orderEntity.Id);


            //判断其类型是否存在
            string maintenacnceTypeId = "";
            string maintenacnceTypeName = "";
            List<YT_MaintenanceTypesEntity> yT_MaintenanceTypesList = new RepositoryFactory().BaseRepository().FindList<YT_MaintenanceTypesEntity>(d => d.Name.Replace(" ", "") == maintenanceTypesEntity.Name.Replace(" ", "")).AsList();
            if (yT_MaintenanceTypesList.Count == 0)
            {
                YT_MaintenanceTypesEntity yT_MaintenanceTypesEntity = new YT_MaintenanceTypesEntity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = maintenanceTypesEntity.Name,
                    IsDelete = 0,
                    CreateDate = DateTime.Now
                };
                new RepositoryFactory().BaseRepository().Insert<YT_MaintenanceTypesEntity>(yT_MaintenanceTypesEntity);

                maintenacnceTypeId = yT_MaintenanceTypesEntity.Id;
                maintenacnceTypeName = yT_MaintenanceTypesEntity.Name;
            }
            else
            {
                maintenacnceTypeId = yT_MaintenanceTypesList[0].Id;
                maintenacnceTypeName = yT_MaintenanceTypesList[0].Name;
            }

            if (string.IsNullOrEmpty(orderEntity.Instructor))
            {
                ///创建指导书
                YT_PartsTypesEntity Parts = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(entity.PartsType);
                instructorEntity.MaintenanceType = maintenacnceTypeId;

                instructorEntity.Supplier_Id = entity.Supplier_Id;
                instructorEntity.Parts_Id = entity.PartsType;
                instructorEntity.Name = Parts.Parts_Name + "-" + maintenacnceTypeName + "指导书";
                instructorEntity.Parts_Num = entity.PartsNumber;

                instructorEntity.Id = Guid.NewGuid().ToString();
                instructorEntity.CreateDate = DateTime.Now;
                instructorEntity.IsDelete = 0;
                instructorEntity.CreateUserId = orderEntity.ModifyUserId;
                new RepositoryFactory().BaseRepository().Insert<YT_InstructorEntity>(instructorEntity);
                orderEntity.Instructor = instructorEntity.Id;
            }

            entity.Order_Status = entity.Area == 1 ? 6 : 2;
            entity.Count = orderEntity.Count;
            entity.MaintainType = maintenacnceTypeId;
            entity.Instructor = orderEntity.Instructor;
            entity.ModifyUserId = orderEntity.ModifyUserId;
            new YT_OrderService().SaveEntity(orderEntity.Id, entity);
        }

        /// <summary>
        /// 获取WX 用户权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IdentityModel getIdentity(string userId)
        {

            IdentityModel model = new IdentityModel();
            //获取质检部经理
            string QualityManager = Config.GetValue("QualityManager");
            //获取供应商
            string Supplier = Config.GetValue("Supplier");
            //质检部
            string SQE = Config.GetValue("Quality");

            UserRelationEntity isQualityManager = new RepositoryFactory().BaseRepository().FindEntity<UserRelationEntity>(d => d.F_UserId == userId && d.F_ObjectId == QualityManager);
            if (isQualityManager == null)
            {
                model.Manager = 0;
            }
            else
            {
                model.Manager = 1;
            }

            UserRelationEntity isQuality = new RepositoryFactory().BaseRepository().FindEntity<UserRelationEntity>(d => d.F_UserId == userId && d.F_ObjectId == SQE);
            if (isQuality == null)
            {
                model.SQE = 0;
            }
            else
            {
                model.SQE = 1;
            }

            UserRelationEntity isSupplier = new RepositoryFactory().BaseRepository().FindEntity<UserRelationEntity>(d => d.F_UserId == userId && d.F_ObjectId == Supplier);
            if (isSupplier == null)
            {
                model.Supplier = 0;
                model.SupplierId = "";
                model.SupplierName = "";
            }
            else
            {
                model.Supplier = 1;
                UserEntity user = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(userId);
                StringBuilder strsql = new StringBuilder();
                strsql.Append("SELECT * FROM YT_Supplier where  YT_Supplier.Compellation LIKE '%" + user.F_RealName + "%'");
                List<YT_SupplierEntity> yT_SupplierEntities = new RepositoryFactory().BaseRepository().FindList<YT_SupplierEntity>(strsql.ToString()).AsList();
                model.SupplierId = yT_SupplierEntities.Count == 0 ? "" : yT_SupplierEntities[0].Id;
                model.SupplierName = yT_SupplierEntities.Count == 0 ? "" : yT_SupplierEntities[0].Name;
            }


            return model;
        }
        #endregion

        /// <summary>
        /// WX
        /// </summary>
        /// <returns></returns>
        public string getAccessToken()
        {
            string access_token = string.Empty;
            string appid = Config.GetValue("WeChatAppId");
            string secret = Config.GetValue("WeChatSecret");
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&APPID=" + appid + "&secret=" + secret;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxAccessTokenModel jsonResult = JsonConvert.DeserializeObject<WxAccessTokenModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            access_token = jsonResult.access_token;
            return access_token;
        }
        /// <summary>
        /// WX
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="body"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        static string pushMassage(string openid, string body, string access_token)
        {
            //第二步组装推送数进行推送
            string url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.ContentType = "application/json";

            byte[] buffer = encoding.GetBytes(body);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

    }
}
