﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.TwoDevelopment.Common
{
    public class PushModel
    {
        
        public string id { get; set; }

        public string title { get; set; }

        public string content { get; set; }
        /// <summary>
        /// 0:报修提交
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 状态 0 提交 1 预估 2 同意  3 拒绝  4 指派 5 完成
        /// </summary>
        public int status { get; set; }

    }
}