﻿using TTon.TMC.Util;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using System.Web;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Statistics;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_OrderSuperaddition;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-22 09:53
    /// 描 述：订单报修
    /// </summary>
    public interface YT_OrderIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_OrderEntity> GetPageList(Pagination pagination, string queryJson);

        List<GetListModel> GetList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取YT_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        YT_OrderEntity GetYT_OrderEntity(string keyValue);

        /// <summary>
        /// 获取YT_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        OrderModel GetYT_OrderSqlEntity(string keyValue);

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, YT_OrderEntity entity);

        /// <summary>
        /// 预估时间接口
        /// </summary>
        List<OrderTaskListModel> GetOrderList(OrderListParam param);
        bool estimateOrder(string estimate_date, string order_id, string user_id);

        /// <summary>
        /// 订单审批
        /// </summary>
        /// <param name="order_Id"></param>
        /// <param name="user_Id"></param>
        /// <returns></returns>
        bool approvalOrder(string order_id, string user_id, int status);

        /// <summary>
        /// 分配人员列表
        /// </summary>
        /// <returns></returns>
         List<OrderUserModel> userList(Pagination pagination, string queryJson);
        List<YT_OrderInventoryEntity> GetOrderInventoryList(string orderid);

        /// <summary>
        /// 指派人员
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <param name="userList"></param>
        /// <returns></returns>
        bool designate(string order_id, string user_id, List<string> userList,string all,string accomplish,int category);
        /// <summary>
        /// 订单完成
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="task_pic"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        int submitOrder(string order_id, string task_pic, string user_id, string estimate_date, string success_remark,string person,string count,string thread_time,string lineedge_time,string success_date,string sortingWay, string frequency,
            string workers,string workday, string weekend,string statutoryHoliday,string mealTimes,string start_date,string badProductDescribe,string workpieceRatio);


        /// <summary>
        /// 获取已分配、未分配数量
        /// </summary>
        /// <param name="order_id"></param>
        /// <returns></returns>
        OrderPartsNumberModel getOrderPartsNumber(string order_id);

        /// <summary>
        /// 提交任务接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool submitTask(string task_id, string order_id, int success, int error, int unfinished);
        List<OrderTaskListModel> GetOrderTaskList(GetOrderListParam param);
        OrderTaskListModel GetOrderDetail(string keyValue);
        HomeStatisticsModel GetHomeStatistics(OrderListParam param);
        List<StatisticsListModel> GetOrderListToWx(GetOrderListParam param);
        OrderTaskListModel GetTaskDetail(string keyValue);
        bool WxApproveOrder(ApproveOrderParams model);
        string ExportOrderList(HttpContext context, string startTime, string endTime, string start, string end, string name, string supplier, string orderNumber, string partsNumber);
        OrderEchartModel GetOrderEcharts(string startTime, string endTime);
        bool SaveSuperadditionForm(string keyValue, YT_OrderSuperadditionEntity entity);
        bool VerifyOrder(string keyValue);
        List<OrderTaskListModel> GetUnfinishedOrderList(OrderListParam param);
        List<OrderTaskListModel> GetUnfinishedTaskList(OrderListParam param);
        List<OrderTaskListModel> GetTodayOrderList(OrderListParam param);
        List<OrderTaskListModel> GetTodayTaskList(OrderListParam param);
        List<OrderTaskListModel> GetInventoryOrderList(OrderListParam param);
        int CheckOrderInventory(string userId);
        int CheckOrderManage(string userId);
        int CheckSquadLeader(string userId);
        OrderEchartModel GetOrderEchartsToAnalysis(string startTime, string endTime, string supplier);
        StatisticsInfoModel GetStatisticsInfo();
        List<UserFinishPartTypesModel> GetUserPartTypesInfo(OrderListParam param);
        MaintenanceCountStatisticsModel MaintenanceCountStatistics(string userId);
        int SaveAbnormal(string orderId,string userId,string content,string img);

        List<AbnormalListModel> GetAbnormalList(GetAbnormalListParams param);

        AbnormalInfoModel GetAbnormalInfo(string keyValue);

        int AbnormalApprove(AbnormalApproveParams param);
        List<UserFinishEchartsModel> GetOrderEchartsToIdentity(string startTime, string endTime,int type);
        List<OrderEchartsToAModel> GetOrderEchartsToA(string startTime, string endTime, string supplier);
        OrderEchartModel OrderEchartsToAnalysis(string startTime, string endTime, string supplier);
        OrderWorkHoursModel getOrderWorkHours(OrderWorkHoursParams param);
        int test(string date);
        #endregion

    }
}
