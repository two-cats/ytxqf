﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-04-23 21:45
 * 描  述：能源表基本信息
 */
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                learun.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/EnergyMeter/EnergyClockInfo/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/EnergyMeter/EnergyClockInfo/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/EnergyMeter/EnergyClockInfo/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/EnergyMeter/EnergyClockInfo/GetPageList',
                headData: [
                    { label: "能源表ID", name: "Id", width: 160, align: "left"},
                    { label: "关联设备ID", name: "AssetID", width: 160, align: "left"},
                    { label: "能源表名称", name: "Name", width: 160, align: "left"},
                    { label: "能源表状态", name: "Status", width: 160, align: "left"},
                    { label: "能源表所在位置", name: "Location", width: 160, align: "left"},
                    { label: "能源表描述", name: "Summary", width: 160, align: "left"},
                    { label: "更新时间", name: "UpdateTime", width: 160, align: "left"},
                ],
                mainId:'Id',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
