﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class ResponseDailyLogModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 今日工作总结
        /// </summary>
        public string TodayContent { get; set; }

        /// <summary>
        /// 明日工作计划
        /// </summary>
        public string TomorrowPlan { get; set; }

        /// <summary>
        /// 今日心得体会
        /// </summary>
        public string TodaySummary { get; set; }
    }
}