/**
 * Crayola colors in JSON format
 * from: https://gist.github.com/jjdelc/1868136
 */

var maintenance
$(function () {
    $.ajax({
        type: "Get",
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        //url: 'http://ytxqf.api.ttonservice.com/v1.0/Autocompleter/maintenance',
        url: 'http://ytxqf.api.ttonservice.com/v1.0/Autocompleter/maintenance',
        dataType: "json",
        success: function (response) {
            if (response.data != null) {
                maintenance = response.data;

                $('#maintenance2').autocompleter({
                    // marker for autocomplete matches
                    highlightMatches: true,

                    // object to local or url to remote search
                    source: maintenance,

                    // custom template
                    template: '{{ label }}',

                    // show hint
                    hint: false,

                    // abort source if empty field
                    empty: false,

                    // max results
                    limit: 10,

                    callback: function (value, index, selected) {
                    
                        //if (selected) {
                        //    $('#partsName').val(selected.hex);
                        //}
                    }
                });
            }
        }
    }) 
});
