﻿using System.Web;
using System.Web.Mvc;

namespace TTon.TMC.Application.WX
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
