﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class RespDepartmentModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public IList<RespUserModel> Users { get; set; }

        public int Count { get; set; }
    }

    public class RespUserModel
    {
        public string Id { get; set; }

        public string LoginName { get; set; }

        public string RealName { get; set; }

    }
}