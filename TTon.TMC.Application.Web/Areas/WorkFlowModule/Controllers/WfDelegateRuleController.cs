﻿using TTon.TMC.Application.WorkFlow;
using System.Web.Mvc;

namespace TTon.TMC.Application.Web.Areas.WorkFlowModule.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.17
    /// 描 述：工作委托
    /// </summary>
    public class WfDelegateRuleController : MvcControllerBase
    {
        private WfDelegateRuleIBLL wfDelegateRuleIBLL = new WfDelegateRuleBLL();

        #region 视图功能
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion
    }
}