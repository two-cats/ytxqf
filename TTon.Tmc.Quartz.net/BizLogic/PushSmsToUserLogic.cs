﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.Tmc.Util;
using TTon.TMC.Quartz.net.Model;

namespace ESteward.QuartzService.BizLogic
{
    public class PushSmsToUserLogic
    {
        public void pushSmsToUser()
        {
            #region 短信
            string _strWorkingDayAM1 = "06:40";
            string _strWorkingDayAM2 = "07:00";
            //string _strWorkingDayPM1 = "18:40";
            //string _strWorkingDayPM2 = "19:00";
            TimeSpan dspWorkingDayAM1 = DateTime.Parse(_strWorkingDayAM1).TimeOfDay;
            TimeSpan dspWorkingDayAM2 = DateTime.Parse(_strWorkingDayAM2).TimeOfDay;
            //TimeSpan dspWorkingDayPM1 = DateTime.Parse(_strWorkingDayPM1).TimeOfDay;
            //TimeSpan dspWorkingDayPM2 = DateTime.Parse(_strWorkingDayPM2).TimeOfDay;

            string startt = "";
            string endt = "";

            DateTime t1 = DateTime.Now;

            TimeSpan dspNow = t1.TimeOfDay;
            //早上6:50
            if (dspNow > dspWorkingDayAM1 && dspNow < dspWorkingDayAM2)
            {
                DateTime thisTime = DateTime.Now.AddDays(-1);
                startt = thisTime.ToString("yyyy-MM-dd") + " 19:00";
                endt = DateTime.Now.ToString("yyyy-MM-dd") + " 7:00";

                string assistant = Config.GetValue("Assistant");
                StringBuilder Sql = new StringBuilder();
                Sql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + assistant + "' AND F_Category = 2");
                List<Base_User> userList = DB_Pla.Context.FromSql(Sql.ToString()).ToList<Base_User>();
                if (userList.Count > 0)
                {
                    foreach (var item in userList)
                    {
                        String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
                        String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
                        String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
                        String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
                        IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
                        //初始化ascClient,暂时不支持多region（请勿修改）
                        DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
                        IAcsClient acsClient = new DefaultAcsClient(profile);
                        SendSmsRequest request = new SendSmsRequest();
                        //手机号
                        request.PhoneNumbers = item.F_Mobile;
                        //必填:短信签名-可在短信控制台中找到
                        request.SignName = "亚泰新";
                        //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                        request.TemplateCode = "SMS_147970978";
                        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                        request.TemplateParam = "{\"starttime\":\"" + startt + "\",\"endtime\":\"" + endt + "\"}";
                        //请求失败这里会抛ClientException异常
                        SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                    }
                }
            }       
            #endregion
        }
    }
}
