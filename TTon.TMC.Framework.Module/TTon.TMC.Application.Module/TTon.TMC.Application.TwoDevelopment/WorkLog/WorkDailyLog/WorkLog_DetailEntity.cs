﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace TTon.TMC.Application.TwoDevelopment.WorkLog
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:25
    /// 描 述：工作日志
    /// </summary>
    public class WorkLog_DetailEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        [Column("USERID")]
        public string UserId { get; set; }
        /// <summary>
        /// SubmitTime
        /// </summary>
        [Column("SUBMITTIME")]
        public DateTime? SubmitTime { get; set; }
        /// <summary>
        /// TodayLog
        /// </summary>
        [Column("TODAYLOG")]
        public string TodayLog { get; set; }
        /// <summary>
        /// TomorrowLog
        /// </summary>
        [Column("TOMORROWLOG")]
        public string TomorrowLog { get; set; }
        /// <summary>
        /// LogContent
        /// </summary>
        [Column("LOGCONTENT")]
        public string LogContent { get; set; }
        /// <summary>
        /// Flag
        /// </summary>
        [Column("FLAG")]
        public int? Flag { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion


        #region 扩展字段

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public string UserAvatar { get; set; }

        [NotMapped]
        public IList<WorkLog_CommentEntity> CommentList { get; set; }

        #endregion
    }
}

