﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using TTon.TMC.Application.WebApi._2._0.Models;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/Instructor")]
    public class InstructorController:BaseApiController
    {
        #region 模块对象
        private YT_InstructorIBLL InstructorBll = new YT_InstructorBLL();
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [Route("GetPageList")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage GetPageList(queryJson queryJson)
        {
            try
            {
                Pagination paginationobj = queryJson.pagination;
                var data = InstructorBll.GetPageListbyAPI(paginationobj, queryJson.querystr);
                var jsonData = new
                {
                    rows = data,
                    total = paginationobj.total,
                    page = paginationobj.page,
                    records = paginationobj.records
                };
                return Render(HttpStatusCode.OK, "success", jsonData);
               
            }

            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 添加指导书
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [Route("Add")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage Add(string keyValue, YT_InstructorEntity Instructor)
        {
            try
            {
                InstructorBll.SaveEntity(keyValue, Instructor);
                return Render(HttpStatusCode.OK, "success", "保存成功！");

            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 添加指导书和订单
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [Route("Add/or")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage AddOr(SupplierOrOrderModel model)
        {
            try
            {
                InstructorBll.SaveSupplierOrOrder(model.instructorEntity,model.maintenanceTypesEntity,model.orderEntity);
                return Render(HttpStatusCode.OK, "success", "保存成功！");

            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }


        /// <summary>
        /// 添加指导书和订单
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [Route("update/or")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage UpdateSupplierOrOrder(SupplierOrOrderModel model)
        {
            try
            {
                InstructorBll.UpdateSupplierOrOrder(model.instructorEntity, model.maintenanceTypesEntity, model.orderEntity);
                return Render(HttpStatusCode.OK, "success", "保存成功！");

            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 指导书详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [Route("detail")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetInstructorDetail(string keyValue)
        {
            try
            {
                var data = InstructorBll.GetInstructorDetail(keyValue);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取WX 当前用户的权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("Identity")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetIdentity(string userId) {
            try
            {
                var data = InstructorBll.getIdentity(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }
        #endregion




    }
}