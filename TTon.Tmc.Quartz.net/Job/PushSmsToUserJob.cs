﻿using ESteward.QuartzService.BizLogic;
using Quartz;
using System;
using Common.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESteward.QuartzService.Job
{
    public class PushSmsToUserJob : IJob
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(PushSmsToUserJob));

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("开始执行PushSmsToUserJob@" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                PushSmsToUserLogic pushSmsToUserLogic = new PushSmsToUserLogic();
                pushSmsToUserLogic.pushSmsToUser();
                Console.WriteLine("成功执行PushSmsToUserJob" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch (Exception ex)
            {
                //_logger.Error(ex);
                Console.WriteLine("ERR:" + ex.Message);
            }
        }
    }
}
