﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.TaskManager
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class Task_FileInfoEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 工单Id
        /// </summary>
        [Column("TASKID")]
        public string TaskId { get; set; }
        /// <summary>
        /// 工单文件地址
        /// </summary>
        [Column("FILEPATH")]
        public string FilePath { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
        [Column("FILETYPE")]
        public string FileType { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        [Column("UPLOADTIME")]
        public DateTime? UploadTime { get; set; }
        /// <summary>
        /// 文件状态,0:无效;1:有效;2:已删除
        /// </summary>
        [Column("STATUS")]
        public int? Status { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

