using TTon.TMC.Application.CRM;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2017-07-11 09:43
    /// 描 述：客户管理
    /// </summary>
    public class CrmCustomerMap : EntityTypeConfiguration<CrmCustomerEntity>
    {
        public CrmCustomerMap()
        {
            #region 表、主键
            //表
            this.ToTable("CRM_CUSTOMER");
            //主键
            this.HasKey(t => t.F_CustomerId);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

