﻿using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2019-02-19 14:40
    /// 描 述：YT_ManagerReport
    /// </summary>
    public class YT_ManagerReportMap : EntityTypeConfiguration<YT_ManagerReportEntity>
    {
        public YT_ManagerReportMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_MANAGERREPORT");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

