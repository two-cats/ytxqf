﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.VX;
using TTon.TMC.Application.WebApi._2._0.Models;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/wx")]
    public class WxAuthApiController : BaseApiController
    {
        private WeChatUserIBLL weChatUserBLL = new WeChatUserBLL();
        private static string WeChatAppId = Config.GetValue("WeChatAppId");
        private static string WeChatSecret = Config.GetValue("WeChatSecret");



        #region 获取数据
        /// <summary>
        /// 通过code换取网页授权access_token
        /// </summary>
        /// <returns></returns>
        [Route("accesstoke"), AcceptVerbs("GET")]
        public HttpResponseMessage getWxAccessToken(string code)
        {
            try
            {
                string appid = WeChatAppId;
                string secret = WeChatSecret;
                TwoDevelopment.LR_CodeDemo.VX.WxAccessTokenModel result = weChatUserBLL.getWxAccessToken(appid, secret, code);
                return Render(HttpStatusCode.OK, "success", result);
            }
            catch (Exception ex)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
        /// <summary>
        /// 拉取用户信息
        /// </summary>
        /// <returns></returns>
        [Route("userinfo"), AcceptVerbs("GET")]
        public HttpResponseMessage getWxUserInfo(string access_token, string openid)
        {
            try
            {
                TwoDevelopment.LR_CodeDemo.VX.WxUserInfoModel result = weChatUserBLL.getWxUserInfo(access_token, openid);
                return Render(HttpStatusCode.OK, "success", result);
            }
            catch (Exception ex)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
        #endregion
    }
}
