﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes
{
    public class PartsTypesAnalysisModel
    {
        public string name { get; set; }

        public string number { get; set; }

        public int success { get; set; }

        public int error { get; set; }
    }
}
