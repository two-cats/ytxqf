﻿using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Instructor.Model;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 16:01
    /// 描 述：配置指导书
    /// </summary>
    public class YT_InstructorBLL : YT_InstructorIBLL
    {
        private YT_InstructorService yT_InstructorService = new YT_InstructorService();

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据(web)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_InstructorEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_InstructorService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        /// <summary>
        /// 获取页面显示列表数据(api)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_InstructorEntity> GetPageListbyAPI(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_InstructorService.GetPageListbyAPI(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取YT_Instructor表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_InstructorEntity GetYT_InstructorEntity(string keyValue)
        {
            try
            {
                return yT_InstructorService.GetYT_InstructorEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取指导书列表
        /// </summary>
        /// <param name="SupplierId"></param>
        /// <param name="PartId"></param>
        /// <param name="maintenanceId"></param>
        /// <returns></returns>
        public List<InstructorListModel> GetInstructorList(string SupplierId, string PartId, string MaintenanceId)
        {
            try
            {
                return yT_InstructorService.GetInstructorList(SupplierId, PartId, MaintenanceId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 指导书详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public InstructorDetailModel GetInstructorDetail(string keyValue)
        {
            try
            {
                return yT_InstructorService.GetInstructorDetail(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                yT_InstructorService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_InstructorEntity entity)
        {
            try
            {
                yT_InstructorService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 新增指导书、维修类型、订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        /// <param name="orderEntity"></param>
        public void SaveSupplierOrOrder(YT_InstructorEntity instructorEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity, YT_OrderEntity orderEntity)
        {
            try
            {
                yT_InstructorService.SaveSupplierOrOrder(instructorEntity, maintenanceTypesEntity,orderEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 导入订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="supplierEntity"></param>
        /// <param name="orderUserEntity"></param>
        /// <param name="partsTypesEntity"></param>
        /// <param name="orderEntity"></param>
        public void ImportOrder(YT_InstructorEntity instructorEntity, YT_SupplierEntity supplierEntity, YT_OrderUserEntity orderUserEntity, YT_PartsTypesEntity partsTypesEntity, YT_OrderEntity orderEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity)
        {
            try
            {
                yT_InstructorService.ImportOrder(instructorEntity, supplierEntity, orderUserEntity, partsTypesEntity, orderEntity, maintenanceTypesEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 新增指导书、维修类型、修改订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        /// <param name="orderEntity"></param>
        public void UpdateSupplierOrOrder(YT_InstructorEntity instructorEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity, YT_OrderEntity orderEntity)
        {
            try
            {
                yT_InstructorService.UpdateSupplierOrOrder(instructorEntity, maintenanceTypesEntity, orderEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取WX 用户权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IdentityModel getIdentity(string userId) {
            try
            {
               return   yT_InstructorService.getIdentity(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
