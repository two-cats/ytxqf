﻿using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class queryJson
    {
        /// <summary>
        /// 查询内容
        /// </summary>
        public string querystr { get; set; }

        public Pagination pagination { get; set; }
    }
    
}