﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class StatisticsInfoModel
    {
        public string orderCount { get; set; }

        public string successCount { get; set; }

        public string unfinishedCount { get; set; }

        public string lastTime { get; set; }
    }
}
