﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class RequestDailyLogCommentModel
    {
        public string LogId { get; set; }

        public string UserId { get; set; }

        public int Star { get; set; }

        public string Comment { get; set; }
    }
}