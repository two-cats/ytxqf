﻿using Common.Logging;
using ESteward.QuartzService.BizLogic;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESteward.QuartzService.Job
{
    public class PushToSupplierOrderStatisticsJob : IJob
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(PushToSupplierOrderStatisticsJob));

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("开始执行PushToSupplierOrderStatisticsJob@" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                PushToSupplierOrderStatisticsLogic pushToSupplierOrderStatisticsLogic = new PushToSupplierOrderStatisticsLogic();
                pushToSupplierOrderStatisticsLogic.pushToSupplierOrderStatistics();
                Console.WriteLine("成功执行PushToSupplierOrderStatisticsJob" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch (Exception ex)
            {
                //_logger.Error(ex);
                Console.WriteLine("ERR:" + ex.Message);
            }
        }
    }
}
