﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 13:20
    /// 描 述：配置供应商零件
    /// </summary>
    public class YT_SupplierOrPartsEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// Supplier_Id
        /// </summary>
        [Column("SUPPLIER_ID")]
        public string Supplier_Id { get; set; }
        /// <summary>
        /// Parts_Id
        /// </summary>
        [Column("PARTS_ID")]
        public string Parts_Id { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// CreateUserId
        /// </summary>
        [Column("CREATEUSERID")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// ModifyUserId
        /// </summary>
        [Column("MODIFYUSERID")]
        public string ModifyUserId { get; set; }
        /// <summary>
        /// ModifyDate
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// IsDelete
        /// </summary>
        [Column("ISDELETE")]
        public int? IsDelete { get; set; }
        /// <summary>
        /// 维修类型
        /// </summary>
        [Column("MAINTENANCETYPE")]
        public string MaintenanceType { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreateDate = DateTime.Now;
            this.IsDelete = 0;
            this.CreateUserId = LoginUserInfo.Get().userId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
            this.ModifyDate = DateTime.Now;
            this.ModifyUserId = LoginUserInfo.Get().userId;
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

