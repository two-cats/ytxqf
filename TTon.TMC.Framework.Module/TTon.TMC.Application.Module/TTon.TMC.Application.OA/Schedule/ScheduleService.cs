﻿using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TTon.TMC.Application.OA.Schedule
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.07.11
    /// 描 述：日程管理
    /// </summary>
    public class ScheduleService : RepositoryFactory
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>返回列表</returns>
        public IEnumerable<ScheduleEntity> GetList()
        {
            try
            {
                return this.BaseRepository().FindList<ScheduleEntity>(t => t.F_DeleteMark != 1);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                    throw;
                else
                    throw ExceptionEx.ThrowServiceException(ex);
            }
        }


  
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<ScheduleEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT [F_ScheduleId]
                          ,[F_ScheduleName]
                          ,[F_ScheduleContent]
                          ,[F_Category]
                          ,[F_StartDate]
                          ,[F_StartTime]
                          ,[F_EndDate]
                          ,[F_EndTime]
                          ,[F_Early]
                          ,[F_IsMailAlert]
                          ,[F_IsMobileAlert]
                          ,[F_IsWeChatAlert]
                          ,[F_ScheduleState]
                          ,[F_SortCode]
                          ,[F_DeleteMark]
                          ,[F_EnabledMark]
                          ,[F_Description]
                          ,[F_CreateDate]
                          ,[F_CreateUserId]
                          ,[F_CreateUserName]
                          ,[F_ModifyDate]
                          ,[F_ModifyUserId]
                          ,[F_ModifyUserName]
                          ,[F_ProjectId]
                          ,[F_RoomId]
                          ,[F_Attendees]
                          ,[F_Services]
                          ,[F_Assign]
                          ,[F_Approval]
                          ,[F_Opinion]
                          ,[F_ApprovalUserID]
                      FROM [TMC_Framework_Base].[dbo].[OA_Schedule]
                ");
                strSql.Append("  WHERE 1=1 ");

                if (!string.IsNullOrEmpty(queryJson))
                {
                    string[] ss = queryJson.Split('#');
                    // 0- 未开始、1-进行中、2-已完成 
                    string sqlStr = string.Empty;
                    switch (ss[1])
                    {
                        //获取全部
                        case "0":
                            sqlStr = "AND (('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' <= F_StartDate) AND F_ScheduleState is null)";
                            strSql.Append(sqlStr);
                            break;
                        //获取当前时间在开始和结束之间
                        case "1":
                            sqlStr = "AND ((F_StartDate< '"+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' and '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' < F_EndDate) AND F_ScheduleState is null)";
                            strSql.Append(sqlStr);
                            break;
                        //获取当前时间在结束时间之后
                        case "2":
                            sqlStr = "AND (('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' >= F_EndDate) or F_ScheduleState = '0') ";
                            strSql.Append(sqlStr);
                            break;
                    }
                    if (!string.IsNullOrEmpty(ss[2]))
                    {
                        strSql.Append("AND F_Assign = '" + ss[2] + "'");
                    }
                    if (!string.IsNullOrEmpty(ss[0])) {
                        strSql.Append("AND F_ProjectId = '" + ss[0] + "'");
                    }
                    if (string.IsNullOrEmpty(ss[3]))
                    {
                        strSql.Append("AND F_Approval is null");
                    }
                    else {
                        strSql.Append("AND F_Approval = '" + ss[3] + "'");
                    }

                }

                return new RepositoryFactory().BaseRepository().FindList<ScheduleEntity>(strSql.ToString(), pagination);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取页面显示列表数据 ---- WX
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<ScheduleEntity> GetMeetingPageList(string userid,Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select top 100 percent T.* FROM (
                          SELECT [F_ScheduleId]
                          ,[F_ScheduleName]
                          ,[F_ScheduleContent]
                          ,[F_Category]
                          ,[F_StartDate]
                          ,[F_StartTime]
                          ,[F_EndDate]
                          ,[F_EndTime]
                          ,[F_Early]
                          ,[F_IsMailAlert]
                          ,[F_IsMobileAlert]
                          ,[F_IsWeChatAlert]
                          ,[F_ScheduleState]
                          ,[F_SortCode]
                          ,[F_DeleteMark]
                          ,[F_EnabledMark]
                          ,[F_Description]
                          ,[F_CreateDate]
                          ,[F_CreateUserId]
                          ,[F_CreateUserName]
                          ,[F_ModifyDate]
                          ,[F_ModifyUserId]
                          ,[F_ModifyUserName]
                          ,[F_ProjectId]
                          ,[F_RoomId]
                          ,[F_Attendees]
                          ,[F_Services]
                          ,[F_Assign]
                          ,[F_Approval]
                          ,[F_Opinion]
                          ,[F_ApprovalUserID]
                      FROM [TMC_Framework_Base].[dbo].[OA_Schedule]
                ");
                strSql.Append("  WHERE 1=1 AND F_CreateUserId = '"+userid+ "' AND (F_DeleteMark != '1' OR F_DeleteMark = '' OR F_DeleteMark IS NULL))T ");
                strSql.Append("  ORDER BY T.[F_StartDate] DESC");

                return new RepositoryFactory().BaseRepository().FindList<ScheduleEntity>(strSql.ToString(), pagination);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public ScheduleEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<ScheduleEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                    throw;
                else
                    throw ExceptionEx.ThrowServiceException(ex);
            }
        }
        #endregion

        #region 统计报表
        /// <summary>
        ///  会议状态统计报表
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public MeetingStatusChartsModel GetMeetingStatusEcharts(string startTime, string endTime)
        {
            MeetingStatusChartsModel model = new MeetingStatusChartsModel();
            DateTime now = DateTime.Now;
            DateTime meeting_Start = DateTime.Now;
            DateTime meeting_End = DateTime.Now;
            StringBuilder whereSql = new StringBuilder();
            if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                meeting_Start = now.AddMonths(-1);
                meeting_End = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            }
            else if (string.IsNullOrEmpty(startTime))
            {
                meeting_End = Convert.ToDateTime(endTime);
                meeting_Start = meeting_End.AddDays(-30);
            }
            else if (string.IsNullOrEmpty(endTime))
            {
                meeting_Start = Convert.ToDateTime(startTime);
                meeting_End = DateTime.Now;
            }
            else
            {
                meeting_Start = Convert.ToDateTime(startTime);
                meeting_End = Convert.ToDateTime(endTime);
            }
            //已同意
            StringBuilder asql = new StringBuilder();
            asql.Append(@"SELECT
                            *
                            FROM OA_Schedule
                            WHERE F_Approval = '0'
                            AND (F_DeleteMark != '1' OR F_DeleteMark = '' OR F_DeleteMark IS NULL)
                            AND F_ModifyDate between '" + meeting_Start + "' and '" + meeting_End + "'");
            List<ScheduleEntity> aList = (List<ScheduleEntity>)new RepositoryFactory().BaseRepository().FindList<ScheduleEntity>(asql.ToString());
            //已拒绝
            StringBuilder rsql = new StringBuilder();
            rsql.Append(@"SELECT
                            *
                            FROM OA_Schedule
                            WHERE F_Approval = '1'
                            AND (F_DeleteMark != '1' OR F_DeleteMark = '' OR F_DeleteMark IS NULL)
                            AND F_ModifyDate between '" + meeting_Start + "' and '" + meeting_End + "'");
            List<ScheduleEntity> rList = (List<ScheduleEntity>)new RepositoryFactory().BaseRepository().FindList<ScheduleEntity>(rsql.ToString());
            //已取消
            StringBuilder csql = new StringBuilder();
            csql.Append(@"SELECT
                            *
                            FROM OA_Schedule
                            WHERE (F_Approval = '' OR F_Approval IS NULL)
                            AND F_DeleteMark = '1'
                            AND F_ModifyDate between '" + meeting_Start + "' and '" + meeting_End + "'");
            List<ScheduleEntity> cList = (List<ScheduleEntity>)new RepositoryFactory().BaseRepository().FindList<ScheduleEntity>(csql.ToString());
            //已完成
            StringBuilder fsql = new StringBuilder();
            fsql.Append(@"SELECT
                            *
                            FROM OA_Schedule
                            WHERE F_Approval = '0'
                            AND (F_DeleteMark != '1' OR F_DeleteMark = '' OR F_DeleteMark IS NULL)
                            AND F_StartDate > '" + meeting_Start + @"'
                            AND F_EndDate < '" + meeting_End + "'");
            List<ScheduleEntity> fList = (List<ScheduleEntity>)new RepositoryFactory().BaseRepository().FindList<ScheduleEntity>(fsql.ToString());

            model.agreeCount = aList.Count;
            model.repulseCount = rList.Count;
            model.cancelCount = cList.Count;
            model.finishCount = fList.Count;

            return model;
        }


        public IEnumerable<ScheduleEChartsList> GetEChartsPageList(Pagination pagination, string queryJson, string startTime, string endTime, string status)
        {
            try
            {
                DateTime now = DateTime.Now;
                DateTime meeting_Start = DateTime.Now;
                DateTime meeting_End = DateTime.Now;
                if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    meeting_Start = now.AddMonths(-1);
                    meeting_End = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                }
                else if (string.IsNullOrEmpty(startTime))
                {
                    meeting_End = Convert.ToDateTime(endTime);
                    meeting_Start = meeting_End.AddDays(-30);
                }
                else if (string.IsNullOrEmpty(endTime))
                {
                    meeting_Start = Convert.ToDateTime(startTime);
                    meeting_End = DateTime.Now;
                }
                else
                {
                    meeting_Start = Convert.ToDateTime(startTime);
                    meeting_End = Convert.ToDateTime(endTime);
                }

                string whereSql = "";
                if (status == "1") //同意
                {
                    whereSql = "OA_Schedule.F_Approval = '0' AND(OA_Schedule.F_DeleteMark != '1' OR OA_Schedule.F_DeleteMark = '' OR OA_Schedule.F_DeleteMark IS NULL) AND OA_Schedule.F_ModifyDate between '" + meeting_Start + "' and '" + meeting_End + "'";
                }
                else if (status == "2") //拒绝
                {
                    whereSql = "OA_Schedule.F_Approval = '1' AND(OA_Schedule.F_DeleteMark != '1' OR OA_Schedule.F_DeleteMark = '' OR OA_Schedule.F_DeleteMark IS NULL) AND OA_Schedule.F_ModifyDate between '" + meeting_Start + "' and '" + meeting_End + "'";
                }
                else if (status == "3") //取消
                {
                    whereSql = "(OA_Schedule.F_Approval = '' OR OA_Schedule.F_Approval IS NULL) AND OA_Schedule.F_DeleteMark = '1' AND OA_Schedule.F_ModifyDate between '" + meeting_Start + "' and '" + meeting_End + "'";
                }
                else //完成
                {
                    whereSql = "OA_Schedule.F_Approval = '0' AND(OA_Schedule.F_DeleteMark != '1' OR OA_Schedule.F_DeleteMark = '' OR OA_Schedule.F_DeleteMark IS NULL) AND OA_Schedule.F_StartDate > '" + meeting_Start + "' AND OA_Schedule.F_EndDate< '" + meeting_End + "'";
                }

                StringBuilder strsql = new StringBuilder();
                strsql.Append(@"SELECT
	                            OA_Schedule.F_ScheduleId AS id,
	                            OA_Schedule.F_ScheduleName AS scheduleName,
	                            CONVERT (
		                            VARCHAR (100),
		                            OA_Schedule.F_StartDate,
		                            20
	                            ) AS startTime,
	                            CONVERT (
		                            VARCHAR (100),
		                            OA_Schedule.F_EndDate,
		                            20
	                            ) AS endTime,
	                            OA_Schedule.F_CreateUserName AS createUser,
	                            CONVERT (
		                            VARCHAR (100),
		                            OA_Schedule.F_CreateDate,
		                            23
	                            ) AS createTime,
	                            Base_DataItemDetail.F_ItemName AS roomName,
	                            F_Services AS services,
	                            F_PeopleCount AS peopleCount,
	                            F_Equipment AS equipment
                            FROM
	                            OA_Schedule
                            LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue = OA_Schedule.F_RoomId
                            WHERE " + whereSql + @"
                            ORDER BY createTime DESC");
                List<ScheduleEChartsList> list = (List<ScheduleEChartsList>)new RepositoryFactory().BaseRepository().FindList<ScheduleEChartsList>(strsql.ToString());
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<ScheduleEntity>(t => t.F_ScheduleId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                    throw;
                else
                    throw ExceptionEx.ThrowServiceException(ex);
            }
        }
        /// <summary>
        /// 删除数据 -- 取消会议
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void DeleteMeeting(string keyValue)
        {
            try
            {
                ScheduleEntity scheduleEntity = this.BaseRepository().FindEntity<ScheduleEntity>(t => t.F_ScheduleId == keyValue);
                scheduleEntity.F_DeleteMark = 1;
                scheduleEntity.Modify(keyValue);
                this.BaseRepository().Update(scheduleEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                    throw;
                else
                    throw ExceptionEx.ThrowServiceException(ex);
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, ScheduleEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                    throw;
                else
                    throw ExceptionEx.ThrowServiceException(ex);
            }
        }
        #endregion
    }
}
