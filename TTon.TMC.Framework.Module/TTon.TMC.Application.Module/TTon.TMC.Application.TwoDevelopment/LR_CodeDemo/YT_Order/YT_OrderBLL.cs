﻿using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using System.Web;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Statistics;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_OrderSuperaddition;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-22 09:53
    /// 描 述：订单报修
    /// </summary>
    public class YT_OrderBLL : YT_OrderIBLL
    {
        private YT_OrderService yT_OrderService = new YT_OrderService();

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_OrderEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_OrderService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public List<GetListModel> GetList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_OrderService.GetList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取YT_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_OrderEntity GetYT_OrderEntity(string keyValue)
        {
            try
            {
                return yT_OrderService.GetYT_OrderEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取YT_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrderModel GetYT_OrderSqlEntity(string keyValue)
        {
            try
            {
                return yT_OrderService.GetYT_OrderSqlEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetOrderList(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetOrderList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 验证是否为仓库管理员
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckOrderInventory(string userId)
        {
            try
            {
                return yT_OrderService.CheckOrderInventory(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 验证是否为经理
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckOrderManage(string userId)
        {
            try
            {
                return yT_OrderService.CheckOrderManage(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 验证是否为班长
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckSquadLeader(string userId)
        {
            try
            {
                return yT_OrderService.CheckSquadLeader(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 仓库管理员订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetInventoryOrderList(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetInventoryOrderList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<StatisticsListModel> GetOrderListToWx(GetOrderListParam param)
        {
            try
            {
                return yT_OrderService.GetOrderListToWx(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetOrderTaskList(GetOrderListParam param)
        {
            try
            {
                return yT_OrderService.GetOrderTaskList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 仓库管理员查看列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<YT_OrderInventoryEntity> GetOrderInventoryList(string orderid)
        {
            try
            {
                return yT_OrderService.GetOrderInventoryList(orderid);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 任务详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public OrderTaskListModel GetTaskDetail(string keyValue)
        {
            try
            {
                return yT_OrderService.GetTaskDetail(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        /// <summary>
        /// 订单详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public OrderTaskListModel GetOrderDetail(string keyValue)
        {
            try
            {
                return yT_OrderService.GetOrderDetail(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取未完成订单数量
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public HomeStatisticsModel GetHomeStatistics(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetHomeStatistics(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 未完成订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetUnfinishedOrderList(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetUnfinishedOrderList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 未完成任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetUnfinishedTaskList(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetUnfinishedTaskList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public MaintenanceCountStatisticsModel MaintenanceCountStatistics(string userId)
        {
            try
            {
                return yT_OrderService.MaintenanceCountStatistics(userId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 今日完成订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetTodayOrderList(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetTodayOrderList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 今日完成任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetTodayTaskList(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetTodayTaskList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<UserFinishPartTypesModel> GetUserPartTypesInfo(OrderListParam param)
        {
            try
            {
                return yT_OrderService.GetUserPartTypesInfo(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public OrderEchartModel GetOrderEchartsToAnalysis(string startTime, string endTime,string supplier)
        {
            try
            {
                return yT_OrderService.GetOrderEchartsToAnalysis(startTime, endTime,supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<OrderEchartsToAModel> GetOrderEchartsToA(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_OrderService.GetOrderEchartsToA(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public OrderEchartModel OrderEchartsToAnalysis(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_OrderService.OrderEchartsToAnalysis(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public OrderEchartModel GetOrderEcharts(string startTime, string endTime)
        {
            try
            {
                return yT_OrderService.GetOrderEcharts(startTime, endTime);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<UserFinishEchartsModel> GetOrderEchartsToIdentity(string startTime, string endTime,int type)
        {
            try
            {
                return yT_OrderService.GetOrderEchartsToIdentity(startTime, endTime,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public StatisticsInfoModel GetStatisticsInfo()
        {
            try
            {
                return yT_OrderService.GetStatisticsInfo();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                yT_OrderService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_OrderEntity entity)
        {
            try
            {
                yT_OrderService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool VerifyOrder(string keyValue)
        {
            try
            {
                return yT_OrderService.VerifyOrder(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public bool SaveSuperadditionForm(string keyValue, YT_OrderSuperadditionEntity entity)
        {
            try
            {
               return yT_OrderService.SaveSuperadditionForm(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 预估时间接口
        /// </summary>
        public bool estimateOrder(string estimate_date, string order_id, string user_id)
        {
            try
            {
               return  yT_OrderService.estimateOrder(estimate_date, order_id,user_id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 订单审批
        /// </summary>
        /// <param name="order_Id"></param>
        /// <param name="user_Id"></param>
        /// <returns></returns>
        public bool approvalOrder(string order_id, string user_id,int status)
        {
            try
            {
                return yT_OrderService.approvalOrder(order_id, user_id,status);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 提交任务接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool submitTask(string task_id, string order_id, int success, int error, int unfinished)
        {
            try
            {
               return   yT_OrderService.submitTask(task_id, order_id, success,error,unfinished);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 分配人员列表
        /// </summary>
        /// <returns></returns>
        public List<OrderUserModel> userList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_OrderService.userList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 指派人员
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <param name="userList"></param>
        /// <returns></returns>
        public bool designate(string order_id, string user_id, List<string> userList, string all, string accomplish,int category)
        {
            try
            {
                return yT_OrderService.designate(order_id, user_id,userList,all,accomplish, category);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取已分配、未分配数量
        /// </summary>
        /// <param name="order_id"></param>
        /// <returns></returns>
        public OrderPartsNumberModel getOrderPartsNumber(string order_id)
        {
            try
            {
                return yT_OrderService.getOrderPartsNumber(order_id);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 通过工作效率计算工时
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public OrderWorkHoursModel getOrderWorkHours(OrderWorkHoursParams param)
        {
            try
            {
                return yT_OrderService.getOrderWorkHours(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public int test(string date)
        {
            try
            {
                return yT_OrderService.IsHolidayByDate(date);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 订单完成
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="task_pic"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public int submitOrder(string order_id, string task_pic, string user_id, string estimate_date, string success_remark, string person, string count, string thread_time, string lineedge_time,string success_date, string sortingWay, string frequency,
            string workers, string workday, string weekend, string statutoryHoliday, string mealTimes,string start_date,string badProductDescribe,string workpieceRatio)
        {
            try
            {
                return yT_OrderService.submitOrder(order_id, task_pic, user_id,estimate_date,success_remark,person,count, thread_time, lineedge_time, success_date,sortingWay,frequency,workers,workday,weekend,statutoryHoliday,mealTimes,start_date, badProductDescribe, workpieceRatio);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 订单审批
        /// </summary>
        /// <param name="order_Id"></param>
        /// <param name="user_Id"></param>
        /// <returns></returns>
        public bool WxApproveOrder(ApproveOrderParams model)
        {
            try
            {
                return yT_OrderService.WxApproveOrder(model);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public int SaveAbnormal(string orderId, string userId, string content, string img)
        {
            try
            {
                return yT_OrderService.SaveAbnormal(orderId,userId,content,img);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<AbnormalListModel> GetAbnormalList(GetAbnormalListParams param)
        {
            try
            {
                return yT_OrderService.GetAbnormalList(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public AbnormalInfoModel GetAbnormalInfo(string keyValue)
        {
            try
            {
                return yT_OrderService.GetAbnormalInfo(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public int AbnormalApprove(AbnormalApproveParams param)
        {
            try
            {
                return yT_OrderService.AbnormalApprove(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region 导出
        /// <summary>
        /// 导出
        /// </summary>
        public string ExportOrderList(HttpContext context, string startTime, string endTime, string start, string end, string name, string supplier, string orderNumber, string partsNumber)
        {
            try
            {
                return yT_OrderService.ExportOrderList(context, startTime, endTime,start,end,name,supplier,orderNumber,partsNumber);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion
    }
}
