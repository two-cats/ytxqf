using TTon.TMC.Application.CRM;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2017-07-10 17:59
    /// 描 述：订单管理
    /// </summary>
    public class CRM_OrderMap : EntityTypeConfiguration<CrmOrderEntity>
    {
        public CRM_OrderMap()
        {
            #region 表、主键
            //表
            this.ToTable("CRM_ORDER");
            //主键
            this.HasKey(t => t.F_OrderId);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

