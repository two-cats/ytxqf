﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class ApproveOrderParams
    {
        public string orderId { get; set; }

        public int status { get; set; }
    }
}
