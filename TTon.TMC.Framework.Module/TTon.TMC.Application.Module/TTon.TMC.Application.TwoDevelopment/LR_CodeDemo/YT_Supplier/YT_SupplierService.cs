﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;
using TTon.TMC.Application.Base.OrganizationModule;
using TTon.TMC.Application.Base.AuthorizeModule;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 09:50
    /// 描 述：供应商配置
    /// </summary>
    public class YT_SupplierService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_SupplierEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Name,
                t.Remark,
                t.Compellation,
                t.Phone,
                t.Code,
                t.CreateDate,
                t.CreateUserId,
                t.ModifyDate
                ");
                strSql.Append("  FROM YT_Supplier t ");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                return this.BaseRepository().FindList<YT_SupplierEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public List<GetSupplierInfoModel> GetSupplierPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Name,
                t.Remark,
                t.Compellation,
                t.Phone,
                t.Code,
                t.CreateDate,
                t.CreateUserId,
                t.ModifyDate
                ");
                strSql.Append("  FROM YT_Supplier t ");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                List<GetSupplierInfoModel> list = this.BaseRepository().FindList<GetSupplierInfoModel>(strSql.ToString(), dp, pagination).AsList();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        int a = 0;
                        if (!string.IsNullOrEmpty(item.Compellation))
                        {
                            string[] array = item.Compellation.Split(',');
                            if (array.Length != 0)
                            {
                                foreach (var itemName in array)
                                {
                                    UserEntity userInfo = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(t => t.F_RealName == itemName);
                                    if (userInfo != null)
                                    {
                                        if (string.IsNullOrEmpty(userInfo.F_WeChat))
                                        {
                                            a = 1;
                                        }
                                    }
                                    else
                                    {
                                        a = 1;
                                    }
                                }
                            }
                        }
                        if (a == 1)
                        {
                            item.Check = "未验证";
                        }
                        else
                        {
                            item.Check = "已验证";
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_SupplierEntity> GetList()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Name,
                t.Remark,
                t.CreateDate,
                t.CreateUserId,
                t.ModifyDate,
                ISNULL(t.Code,'') AS Code
                ");
                strSql.Append("  FROM YT_Supplier t ");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0 Order by t.CreateDate desc ");
                return this.BaseRepository().FindList<YT_SupplierEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public List<SupplierListToTree> GetSupplierTree()
        {
            List<SupplierListToTree> list = new List<SupplierListToTree>();
            var strSql = new StringBuilder();
            strSql.Append(@"SELECT
	                Id AS id,
	                Name AS name
                FROM
	                YT_Supplier
                WHERE IsDelete = 0
                ORDER BY
	                CreateDate DESC ");
            DataTable dt = new RepositoryFactory().BaseRepository().FindTable(strSql.ToString());
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierListToTree model = new SupplierListToTree();
                    model.number = i;
                    model.Id = dt.Rows[i]["id"].ToString();
                    model.Name = dt.Rows[i]["name"].ToString();
                    list.Add(model);
                }
            }
            return list;
        }

        /// <summary>
        /// 获取YT_Supplier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_SupplierEntity GetYT_SupplierEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_SupplierEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取YT_Supplier表实体数据
        /// <param name="keyValue">编号</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<YT_SupplierEntity> SupplierEntityByid(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * FROM YT_Supplier WHERE Code = '" + keyValue + "' AND IsDelete ='0' ORDER BY CreateDate desc ");
                return this.BaseRepository().FindList<YT_SupplierEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public List<SupplierListToTree> GetSupplierList(string startTime, string endTime)
        {
            List<SupplierListToTree> list = new List<SupplierListToTree>();
            var strSql = new StringBuilder();
            strSql.Append(@"SELECT
	                Id AS id,
	                Name AS name
                FROM
	                YT_Supplier
                WHERE IsDelete = 0
                ORDER BY
	                CreateDate DESC ");
            DataTable dt = new RepositoryFactory().BaseRepository().FindTable(strSql.ToString());
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierListToTree model = new SupplierListToTree();
                    model.number = i;
                    model.Id = dt.Rows[i]["id"].ToString();
                    model.Name = dt.Rows[i]["name"].ToString();
                    list.Add(model);
                }
            }
            return list;
        }


        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                YT_SupplierEntity entity = this.BaseRepository().FindEntity<YT_SupplierEntity>(keyValue);
                entity.IsDelete = 1;
                this.BaseRepository().Update<YT_SupplierEntity>(entity);              
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_SupplierEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
