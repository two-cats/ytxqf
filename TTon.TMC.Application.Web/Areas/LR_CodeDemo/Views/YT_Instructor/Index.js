﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-08-21 16:01
 * 描  述：配置指导书
 */
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').lrMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 120, 400);
            $('#Supplier_Id').lrYT_SupplierSelect();
            $('#Parts_Id').lrYT_PartsTypesSelect();
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                learun.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/LR_CodeDemo/YT_Instructor/Form',
                    width: 1000,
                    height: 600,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/LR_CodeDemo/YT_Instructor/Form?keyValue=' + keyValue,
                        width: 1000,
                        height: 600,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/LR_CodeDemo/YT_Instructor/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').jfGrid({
                url: top.$.rootUrl + '/LR_CodeDemo/YT_Instructor/GetPageList',
                headData: [
                    { label: "名称", name: "Name", width: 200, align: "left" },
                    { label: "供应商", name: "Supplier_Id", width: 200, align: "left"},
                    { label: "零件", name: "Parts_Id", width: 200, align: "left" },
                    { label: "零件编号", name: "Parts_Num", width: 200, align: "left" },
                    { label: "维修类型", name: "MaintenanceType", width: 200, align: "left"},
                    //{ label: "绘图时间", name: "SketchDate", width: 250, align: "left"},
                    //{ label: "编号", name: "Number", width: 200, align: "left"},
                    //{ label: "工时要求", name: "WorkHours", width: 200, align: "left"},
                    { label: "缺陷描述", name: "IssueDescription", width: 300, align: "left"},                  
                    //{ label: "返修配件范围", name: "Pick_Scope", width: 300, align: "left"},
                    //{ label: "返修准备", name: "Pick_Prepare", width: 300, align: "left"},
                    { label: "挑选/返工步骤", name: "Pick_Step", width: 300, align: "left"},                  
                    { label: "挑选/返修要求", name: "Pick_Demand", width: 300, align: "left"},
                    //{ label: "返修方案", name: "Pick_Project", width: 300, align: "left"},
                    //{ label: "备注", name: "Remark", width: 300, align: "left"},             
                ],
                mainId:'Id',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
