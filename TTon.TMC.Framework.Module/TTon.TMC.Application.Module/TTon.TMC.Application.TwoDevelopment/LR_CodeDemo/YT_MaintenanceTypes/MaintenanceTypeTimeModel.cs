﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes
{
    public class MaintenanceTypeTimeModel
    {
        public string maintenanceType { get; set; }
        /// <summary>
        /// 区域
        /// </summary>
        public double aTime { get; set; }

        public int aCount { get; set; }

        public double aAverage { get; set; }
        /// <summary>
        /// 跟线
        /// </summary>
        public double tTime { get; set; }

        public int tCount { get; set; }

        public double tAverage { get; set; }
        /// <summary>
        /// 线边
        /// </summary>
        public double lTime { get; set; }

        public int lCount { get; set; }

        public double lAverage { get; set; }
    }
}
