﻿using TTon.TMC.Util;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_SupplierOrParts.ImportExcel;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 13:20
    /// 描 述：配置供应商零件
    /// </summary>
    public interface YT_SupplierOrPartsIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_SupplierOrPartsEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取YT_SupplierOrParts表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        YT_SupplierOrPartsEntity GetYT_SupplierOrPartsEntity(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, YT_SupplierOrPartsEntity entity);

        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="model"></param>
        void InsertImportExcel(ImportModel model);
        #endregion

    }
}
