﻿using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ScanInfo;
using System.Data;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-09-26 09:14
    /// 描 述：检测记录
    /// </summary>
    public class YT_ScanInfoBLL : YT_ScanInfoIBLL
    {
        private YT_ScanInfoService yT_ScanInfoService = new YT_ScanInfoService();

        #region 获取数据

        /// <summary>
        /// 获取列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<YT_ScanInfoEntity> GetList( string queryJson )
        {
            try
            {
                return yT_ScanInfoService.GetList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取列表分页数据
        /// <param name="pagination">分页参数</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<YT_ScanInfoEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_ScanInfoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_ScanInfoEntity GetEntity(string keyValue)
        {
            try
            {
                return yT_ScanInfoService.GetEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 验证次数
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool GetScanCount(string keyValue)
        {
            try
            {
                return yT_ScanInfoService.GetScanCount(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                yT_ScanInfoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_ScanInfoEntity entity)
        {
            try
            {
                yT_ScanInfoService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 扫描
        /// </summary>
        /// <param name="param"></param>
        public void AddScan(ScanParams param)
        {
            try
            {
                yT_ScanInfoService.AddScan(param);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion


        #region 导出
        /// <summary>
        /// 导出
        /// </summary>
        public void ExportScanList(string startTime, string endTime)
        {
            try
            {
                //取出数据源
                DataTable exportTable = yT_ScanInfoService.ExportScanList(startTime,endTime);
                //设置导出格式
                ExcelConfig excelconfig = new ExcelConfig();
                excelconfig.Title = "亚泰新工业检测记录";
                excelconfig.TitleFont = "微软雅黑";
                excelconfig.TitlePoint = 18;
                excelconfig.FileName = "检测记录.xls";
                excelconfig.IsAllSizeColumn = true;
                //每一列的设置,没有设置的列信息，系统将按datatable中的列名导出
                excelconfig.ColumnEntity = new List<ColumnModel>();
                excelconfig.ColumnEntity.Add(new ColumnModel() { Column = "primitivecode", ExcelColumn = "原始编码" });
                excelconfig.ColumnEntity.Add(new ColumnModel() { Column = "equipmentcode", ExcelColumn = "设备编码" });
                excelconfig.ColumnEntity.Add(new ColumnModel() { Column = "createuserid", ExcelColumn = "创建人" });
                excelconfig.ColumnEntity.Add(new ColumnModel() { Column = "createdate", ExcelColumn = "创建时间" });
                excelconfig.ColumnEntity.Add(new ColumnModel() { Column = "modifyuserid", ExcelColumn = "更新人" });
                excelconfig.ColumnEntity.Add(new ColumnModel() { Column = "modifydate", ExcelColumn = "更新时间" });
                //调用导出方法
                ExcelHelper.ExcelDownload(exportTable, excelconfig);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion
    }
}
