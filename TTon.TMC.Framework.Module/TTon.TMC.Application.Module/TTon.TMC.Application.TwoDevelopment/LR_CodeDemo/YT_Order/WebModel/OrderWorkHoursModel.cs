﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderWorkHoursModel
    {
        /// <summary>
        /// 库检耗时
        /// </summary>
        public string estimateDate { get; set; }
        /// <summary>
        /// 跟线耗时
        /// </summary>
        public string threadElapsedTime { get; set; }
        /// <summary>
        /// 线边耗时
        /// </summary>
        public string lineEdgeElapsedTime { get; set; }
        /// <summary>
        /// 工作日耗时
        /// </summary>
        public string workday { get; set; }
        /// <summary>
        /// 周末耗时
        /// </summary>
        public string weekend { get; set; }
        /// <summary>
        /// 法定假日耗时
        /// </summary>
        public string statutoryHoliday { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
        public string successDate { get; set; }
    }
}
