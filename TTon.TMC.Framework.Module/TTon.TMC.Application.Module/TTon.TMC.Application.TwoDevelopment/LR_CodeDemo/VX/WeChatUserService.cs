﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.VX
{
    class WeChatUserService
    {
        #region 微信授权登陆
       
        /// <summary>
        /// 通过code换取网页授权access_token
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="secret"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public WxAccessTokenModel getWxAccessToken(string appid, string secret, string code)
        {
            WxAccessTokenModel result = new WxAccessTokenModel();

            string url = "https://api.weixin.qq.com/sns/oauth2/access_token?" +
                          @"appid=" + appid +
                          @"&secret=" + secret +
                          @"&code=" + code +
                          @"&grant_type=authorization_code";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxAccessTokenModel jsonResult = JsonConvert.DeserializeObject<WxAccessTokenModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            if (jsonResult.errcode == 0)
            {
                result = jsonResult;
            }

            return result;
        }
        /// <summary>
        /// 拉取用户信息
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public WxUserInfoModel getWxUserInfo(string access_token, string openid)
        {
            WxUserInfoModel result = new WxUserInfoModel();

            string url = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token +
                          @"&openid=" + openid +
                          @"&lang=zh_CN";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxUserInfoModel jsonResult = JsonConvert.DeserializeObject<WxUserInfoModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            if (jsonResult.errcode == 0)
            {
                result = jsonResult;
            }

            return result;
        }
        #endregion
    }
}
