﻿$(function () {
    var vs = "";
    $.ajax({
        url: "/LR_CodeDemo/YT_Order/GetStatisticsInfo",
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            $("#d1 font").text(result.data.orderCount);
            $("#d2 font").text(result.data.successCount);
            $("#d3 font").text(result.data.unfinishedCount);
            $("#d4 font").text(result.data.lastTime);
        }
    })

    $.ajax({
        url: "/LR_CodeDemo/YT_Supplier/GetSupplierTree",
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                if (result.data[i].number == 0) {
                    html += "<option value='" + result.data[i].Id + "' selected = 'selected'>" + result.data[i].Name + "</opeion>";
                    vs = result.data[i].Id;
                }
                if (result.data[i].number != 0) {
                    html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Name + "</opeion>";
                }
            }
            $("#Supplier").html(html);
            $("#Supplier1").html(html);
            $("#Supplier6").html(html);
        }
    })

    $.ajax({
        url: "/LR_CodeDemo/YT_Supplier/GetSupplierList?startTime=&endTime=",
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                html += "<option value='" + result.data[i].Id + "' >" + result.data[i].Name + "</opeion>";
            }
            $("#Spiller").html(html);
        }
    })
    //$.ajax({
    //    url: "/LR_CodeDemo/YT_Supplier/GetSupplierTree",
    //    type: "GET",
    //    async: false,
    //    dataType: "JSON",
    //    success: function (result) {
    //        var html = "<option value=''>==请选择==</option>";
    //        for (var i = 0; i < result.data.length; i++) {
    //            html += "<option value='" + result.data[i].Id + "' >" + result.data[i].Name + "</opeion>";
    //            //vs = result.data[i].Id;
    //        }
    //        $("#Supplier5").html(html);
    //    }
    //})

    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsTypesTree?keyValue=" + vs,
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                html += "<option value='" + result.data[i].Id + "' >" + result.data[i].Name + "</opeion>";
            }
            $("#Part").html(html);
        }
    })

    //$.ajax({
    //    url: "/LR_CodeDemo/YT_MaintenanceTypes/GetMaintenanceTypesTree?keyValue=" + vb,
    //    type: "GET",
    //    async: false,
    //    dataType: "JSON",
    //    success: function (result) {
    //        var html = "<option value=''>==请选择==</option>";
    //        for (var i = 0; i < result.data.length; i++) {
    //            html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Name + "</opeion>";
    //            vc = result.data[i].Id;
    //        }
    //        $("#MaintenanceType").html(html);
    //    }
    //})

    $.ajax({
        url: "/LR_CodeDemo/YT_MaintenanceTypes/GetMaintenanceTypesTime?startTime=&endTime=&supplier=" + vs + "&part=",
        async: true, //同步执行
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.data.length == 0) {
                var newdiv = document.getElementById("EnergyECharts");
                newdiv.innerHTML = "<font size='5'>当前时间段内该供应商未维修零件</font>";
            }
            else {
                var name = [];
                var aTime = [];
                var aCount = [];
                var aAverage = [];
                var tTime = [];
                var tCount = [];
                var tAverage = [];
                var lTime = [];
                var lCount = [];
                var lAverage = [];
                for (var i = 0; i < data.data.length; i++) {
                    name.push(data.data[i].maintenanceType);
                    aTime.push(data.data[i].aTime);
                    aCount.push(data.data[i].aCount);
                    aAverage.push(data.data[i].aAverage);
                    tTime.push(data.data[i].tTime);
                    tCount.push(data.data[i].tCount);
                    tAverage.push(data.data[i].tAverage);
                    lTime.push(data.data[i].lTime);
                    lCount.push(data.data[i].lCount);
                    lAverage.push(data.data[i].lAverage);
                }

                var EnergyChat = echarts.init(document.getElementById('EnergyECharts'));
                EnergyOption = {
                    title: {
                        text: '维修类型完成数量与耗时统计',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                    },
                    legend: {
                        orient: 'horizontal',
                        x: 'center',
                        y: 'bottom',
                        data: ['区域挑选耗时（小时）', '区域挑选数量（件）', '跟线挑选耗时（小时）', '跟线挑选数量（件）', '线边挑选耗时（小时）', '线边挑选数量（件）']
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: name,
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: '',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}'
                            }
                        },
                        {
                            type: 'value',
                            name: '平均耗时',
                            min: 0,
                            axisLabel: {
                                formatter: '{value} 秒/件'
                            }
                        }
                    ],
                    series: [
                        {
                            name: '区域挑选耗时（小时）',
                            type: 'bar',
                            stack: '区域',
                            data: aTime
                        },
                        {
                            name: '区域挑选数量（件）',
                            type: 'bar',
                            stack: '区域',
                            data: aCount
                        },
                        {
                            name: '区域挑选平均耗时（秒）',
                            type: 'line',
                            yAxisIndex: 1,
                            data: aAverage
                        },
                        {
                            name: '跟线挑选耗时（小时）',
                            type: 'bar',
                            stack: '跟线',
                            data: tTime
                        },
                        {
                            name: '跟线挑选数量（件）',
                            type: 'bar',
                            stack: '跟线',
                            data: tCount
                        },
                        {
                            name: '跟线挑选平均耗时（秒）',
                            type: 'line',
                            yAxisIndex: 1,
                            data: tAverage
                        },
                        {
                            name: '线边挑选耗时（小时）',
                            type: 'bar',
                            stack: '线边',
                            data: lTime
                        },
                        {
                            name: '线边挑选数量（件）',
                            type: 'bar',
                            stack: '线边',
                            data: lCount
                        },
                        {
                            name: '线边挑选平均耗时（秒）',
                            type: 'line',
                            yAxisIndex: 1,
                            data: lAverage
                        }
                    ]
                };
                EnergyChat.setOption(EnergyOption);
            }
        }
    })

    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetUserFinishEcharts?startTime=&endTime=&supplier=" + vs,
        async: true, //同步执行
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.data.length == 0) {
                var newdiv = document.getElementById("meeting_evaluate");
                newdiv.innerHTML = "<font size='5'>当前时间段内该供应商未维修零件</font>";
            }
            else {
                var name = [];
                var count = [];
                for (var i = 0; i < data.data.length; i++) {
                    name.push(data.data[i].name);
                    count.push(data.data[i].count);
                }
                var Meeting_evaluate_Chat = echarts.init(document.getElementById('meeting_evaluate'));
                Meeting_evaluateOption = {
                    title: {
                        text: '员工完成零件数量统计',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                        formatter: function (params) {
                            var relVal = params[0].name;
                            for (var i = 0, l = params.length; i < l; i++) {
                                relVal += '<br/>' + params[i].value + "件";
                            }
                            return relVal;
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: name,
                        axisLabel: {
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 4;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }

                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName
                            }

                        }
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: count,
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#d48265'
                            }
                        },
                    }]
                };
                Meeting_evaluate_Chat.setOption(Meeting_evaluateOption);
            }
        }
    })

    $.ajax({
        url: "/LR_CodeDemo/YT_Order/GetOrderEcharts?startTime=&endTime=",
        async: true, //同步执行
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.data.OCount == 0 && data.data.SCount == 0 && data.data.QCount == 0) {
                var newdiv = document.getElementById("meeting_status");
                newdiv.innerHTML = "<font size='5'>当前时间段内未提交订单</font>";
            }
            else {
                //本部
                var oCount = data.data.OCount;
                //供应商
                var sCount = data.data.SCount;
                //SQE
                var qCount = data.data.QCount;
                var Meeting_Status_Chat = echarts.init(document.getElementById('meeting_status'));
                Meeting_StatusOption = {
                    title: {
                        text: '下单统计图表',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                        formatter: function (params) {
                            var relVal = params[0].name;
                            for (var i = 0, l = params.length; i < l; i++) {
                                relVal += '<br/>' + params[i].value + "单";
                            }
                            return relVal;
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: ['本部', '供应商', 'SQE']
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        type: 'bar',
                        data: [oCount, sCount, qCount],
                        itemStyle: {
                            normal: {
                                color: '#d48265'
                            }
                        },
                    }]
                };
                Meeting_Status_Chat.setOption(Meeting_StatusOption);
            }
        }
    })

    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsFinishAndNumEcharts?startTime=&endTime=&supplier=" + vs,
        async: true, //同步执行
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.length == 0) {
                var newdiv = document.getElementById("part_num_time");
                newdiv.innerHTML = "<font size='5'>当前时间段内该供应商未维修零件</font>";
            }
            else {
                var name = [];
                var aCount = [];
                var aTime = [];
                for (var i = 0; i < data.length; i++) {
                    name.push(data[i].partsTypes);
                    aCount.push(data[i].aCount);
                    aTime.push(data[i].aTime);
                }
                var part_num_time_Chat = echarts.init(document.getElementById('part_num_time'));
                part_num_timeOption = {
                    title: {
                        text: '零件完成数量与耗时统计',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                    },
                    legend: {
                        orient: 'horizontal',
                        x: 'center',
                        y: 'bottom',
                        data: ['耗时（小时）', '数量（件）']
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: name,
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: '',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}'
                            }
                        }
                    ],
                    series: [
                        {
                            name: '耗时（小时）',
                            type: 'bar',
                            data: aTime
                        },
                        {
                            name: '数量（件）',
                            type: 'bar',
                            data: aCount
                        },
                    ]
                };
                part_num_time_Chat.setOption(part_num_timeOption);
            }
        }
    })
})

function btn_Energy() {
    var vw = $('#Supplier  option:selected').val();
    chargeParts(vw);
    var start = $("#meeting_a_StartTime").val();
    var end = $("#meeting_a_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }

    var vr = $('#Part  option:selected').val();

    $.ajax({
        url: "/LR_CodeDemo/YT_MaintenanceTypes/GetMaintenanceTypesTime?startTime=" + start + "&endTime=" + end + "&supplier=" + vw + "&part=" + vr,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var name = [];
            var aTime = [];
            var aCount = [];
            var aAverage = [];
            var tTime = [];
            var tCount = [];
            var tAverage = [];
            var lTime = [];
            var lCount = [];
            var lAverage = [];
            for (var i = 0; i < data.data.length; i++) {
                name.push(data.data[i].maintenanceType);
                aTime.push(data.data[i].aTime);
                aCount.push(data.data[i].aCount);
                aAverage.push(data.data[i].aAverage);
                tTime.push(data.data[i].tTime);
                tCount.push(data.data[i].tCount);
                tAverage.push(data.data[i].tAverage);
                lTime.push(data.data[i].lTime);
                lCount.push(data.data[i].lCount);
                lAverage.push(data.data[i].lAverage);
            }

            var EnergyChat = echarts.init(document.getElementById('EnergyECharts'));
            EnergyOption = {
                title: {
                    text: '维修类型完成数量与耗时统计',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                },
                legend: {
                    orient: 'horizontal',
                    x: 'center',
                    y: 'bottom',
                    data: ['区域挑选耗时（小时）', '区域挑选数量（件）', '跟线挑选耗时（小时）', '跟线挑选数量（件）', '线边挑选耗时（小时）', '线边挑选数量（件）']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: name,
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '',
                        min: 0,
                        axisLabel: {
                            formatter: '{value}'
                        }
                    },
                    {
                        type: 'value',
                        name: '平均耗时',
                        min: 0,
                        axisLabel: {
                            formatter: '{value} 秒/件'
                        }
                    }
                ],
                series: [
                    {
                        name: '区域挑选耗时（小时）',
                        type: 'bar',
                        stack: '区域',
                        data: aTime
                    },
                    {
                        name: '区域挑选数量（件）',
                        type: 'bar',
                        stack: '区域',
                        data: aCount
                    },
                    {
                        name: '区域挑选平均耗时（秒）',
                        type: 'line',
                        yAxisIndex: 1,
                        data: aAverage
                    },
                    {
                        name: '跟线挑选耗时（小时）',
                        type: 'bar',
                        stack: '跟线',
                        data: tTime
                    },
                    {
                        name: '跟线挑选数量（件）',
                        type: 'bar',
                        stack: '跟线',
                        data: tCount
                    },
                    {
                        name: '跟线挑选平均耗时（秒）',
                        type: 'line',
                        yAxisIndex: 1,
                        data: tAverage
                    },
                    {
                        name: '线边挑选耗时（小时）',
                        type: 'bar',
                        stack: '线边',
                        data: lTime
                    },
                    {
                        name: '线边挑选数量（件）',
                        type: 'bar',
                        stack: '线边',
                        data: lCount
                    },
                    {
                        name: '线边挑选平均耗时（秒）',
                        type: 'line',
                        yAxisIndex: 1,
                        data: lAverage
                    }
                ]
            };
            EnergyChat.setOption(EnergyOption);
        }
    })
}

function btn_Energy_Parts() {
    var vw = $('#Supplier  option:selected').val();
    var vr = $('#Part  option:selected').val();
    var start = $("#meeting_a_StartTime").val();
    var end = $("#meeting_a_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }

    $.ajax({
        url: "/LR_CodeDemo/YT_MaintenanceTypes/GetMaintenanceTypesTime?startTime=" + start + "&endTime=" + end + "&supplier=" + vw + "&part=" + vr,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var name = [];
            var aTime = [];
            var aCount = [];
            var aAverage = [];
            var tTime = [];
            var tCount = [];
            var tAverage = [];
            var lTime = [];
            var lCount = [];
            var lAverage = [];
            for (var i = 0; i < data.data.length; i++) {
                name.push(data.data[i].maintenanceType);
                aTime.push(data.data[i].aTime);
                aCount.push(data.data[i].aCount);
                aAverage.push(data.data[i].aAverage);
                tTime.push(data.data[i].tTime);
                tCount.push(data.data[i].tCount);
                tAverage.push(data.data[i].tAverage);
                lTime.push(data.data[i].lTime);
                lCount.push(data.data[i].lCount);
                lAverage.push(data.data[i].lAverage);
            }

            var EnergyChat = echarts.init(document.getElementById('EnergyECharts'));
            EnergyOption = {
                title: {
                    text: '维修类型完成数量与耗时统计',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                },
                legend: {
                    orient: 'horizontal',
                    x: 'center',
                    y: 'bottom',
                    data: ['区域挑选耗时（小时）', '区域挑选数量（件）', '跟线挑选耗时（小时）', '跟线挑选数量（件）', '线边挑选耗时（小时）', '线边挑选数量（件）']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: name,
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '',
                        min: 0,
                        axisLabel: {
                            formatter: '{value}'
                        }
                    },
                    {
                        type: 'value',
                        name: '平均耗时',
                        min: 0,
                        axisLabel: {
                            formatter: '{value} 秒/件'
                        }
                    }
                ],
                series: [
                    {
                        name: '区域挑选耗时（小时）',
                        type: 'bar',
                        stack: '区域',
                        data: aTime
                    },
                    {
                        name: '区域挑选数量（件）',
                        type: 'bar',
                        stack: '区域',
                        data: aCount
                    },
                    {
                        name: '区域挑选平均耗时（秒）',
                        type: 'line',
                        yAxisIndex: 1,
                        data: aAverage
                    },
                    {
                        name: '跟线挑选耗时（小时）',
                        type: 'bar',
                        stack: '跟线',
                        data: tTime
                    },
                    {
                        name: '跟线挑选数量（件）',
                        type: 'bar',
                        stack: '跟线',
                        data: tCount
                    },
                    {
                        name: '跟线挑选平均耗时（秒）',
                        type: 'line',
                        yAxisIndex: 1,
                        data: tAverage
                    },
                    {
                        name: '线边挑选耗时（小时）',
                        type: 'bar',
                        stack: '线边',
                        data: lTime
                    },
                    {
                        name: '线边挑选数量（件）',
                        type: 'bar',
                        stack: '线边',
                        data: lCount
                    },
                    {
                        name: '线边挑选平均耗时（秒）',
                        type: 'line',
                        yAxisIndex: 1,
                        data: lAverage
                    }
                ]
            };
            EnergyChat.setOption(EnergyOption);
        }
    })
}

function chargeParts(vw) {
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsTypesTree?keyValue=" + vw,
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                html += "<option value='" + result.data[i].Id + "' >" + result.data[i].Name + "</opeion>";
            }
            $("#Part").html(html);
        }
    })
}

function btn_meeting_evaluate() {
    var vs = $('#Supplier1  option:selected').val();
    var start = $("#meeting_StartTime").val();
    var end = $("#meeting_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetUserFinishEcharts?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var name = [];
            var count = [];
            for (var i = 0; i < data.data.length; i++) {
                name.push(data.data[i].name);
                count.push(data.data[i].count);
            }
            var Meeting_evaluate_Chat = echarts.init(document.getElementById('meeting_evaluate'));
            Meeting_evaluateOption = {
                title: {
                    text: '员工完成零件数量统计',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                    formatter: function (params) {
                        var relVal = params[0].name;
                        for (var i = 0, l = params.length; i < l; i++) {
                            relVal += '<br/>' + params[i].value + "件";
                        }
                        return relVal;
                    }
                },
                xAxis: {
                    type: 'category',
                    data: name,
                    axisLabel: {
                        interval: 0,
                        formatter: function (params) {
                            var newParamsName = "";
                            var paramsNameNumber = params.length;
                            var provideNumber = 4;
                            var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                            if (paramsNameNumber > provideNumber) {
                                for (var p = 0; p < rowNumber; p++) {
                                    var tempStr = "";
                                    var start = p * provideNumber;
                                    var end = start + provideNumber;
                                    if (p == rowNumber - 1) {
                                        tempStr = params.substring(start, paramsNameNumber);
                                    } else {
                                        tempStr = params.substring(start, end) + "\n";
                                    }
                                    newParamsName += tempStr;
                                }

                            } else {
                                newParamsName = params;
                            }
                            return newParamsName
                        }

                    }
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: count,
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: '#d48265'
                        }
                    },
                }]
            };
            Meeting_evaluate_Chat.setOption(Meeting_evaluateOption);
        }
    })
}

function btn_meeting_status() {
    var id = $('#Identity  option:selected').val();
    var start = $("#meeting_status_StartTime").val();
    var end = $("#meeting_status_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }
    //GetSupplierList(start, end);
    if (id == 0) {
        $.ajax({
            url: "/LR_CodeDemo/YT_Order/GetOrderEcharts?startTime=" + start + "&endTime=" + end,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                //本部
                var oCount = data.data.OCount;
                //供应商
                var sCount = data.data.SCount;
                //SQE
                var qCount = data.data.QCount;
                var Meeting_Status_Chat = echarts.init(document.getElementById('meeting_status'));
                Meeting_StatusOption = {
                    title: {
                        text: '下单统计图表',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                        formatter: function (params) {
                            var relVal = params[0].name;
                            for (var i = 0, l = params.length; i < l; i++) {
                                relVal += '<br/>' + params[i].value + "单";
                            }
                            return relVal;
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: ['本部', '供应商', 'SQE']
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        type: 'bar',
                        data: [oCount, sCount, qCount],
                        itemStyle: {
                            normal: {
                                color: '#d48265'
                            }
                        },
                    }]
                };
                Meeting_Status_Chat.setOption(Meeting_StatusOption);
            }
        })
    }
    else {
        $.ajax({
            url: "/LR_CodeDemo/YT_Order/GetOrderEchartsToIdentity?startTime=" + start + "&endTime=" + end + "&type=" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                var name = [];
                var count = [];
                for (var i = 0; i < data.data.length; i++) {
                    name.push(data.data[i].name);
                    count.push(data.data[i].count);
                }
                var Meeting_Status_Chat = echarts.init(document.getElementById('meeting_status'));
                Meeting_StatusOption = {
                    title: {
                        text: '下单统计图表',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                        formatter: function (params) {
                            var relVal = params[0].name;
                            for (var i = 0, l = params.length; i < l; i++) {
                                relVal += '<br/>' + params[i].value + "单";
                            }
                            return relVal;
                        }
                    },
                    xAxis: {
                        type: 'category',
                        data: name,
                        axisLabel: {
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 4;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }

                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName
                            }

                        }
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: count,
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#d48265'
                            }
                        },
                    }]
                };
                Meeting_Status_Chat.setOption(Meeting_StatusOption);
            }
        })
    }
}

function btn_Energy_Spiller() {
    var su = $('#Spiller  option:selected').val();
    var start = $("#meeting_status_StartTime").val();
    var end = $("#meeting_status_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }

    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetUserFinishEchartsToSu?startTime=" + start + "&endTime=" + end + "&supplier=" + su,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var name = [];
            var count = [];
            for (var i = 0; i < data.data.length; i++) {
                name.push(data.data[i].name);
                count.push(data.data[i].count);
            }
            var Meeting_Status_Chat = echarts.init(document.getElementById('meeting_status'));
            Meeting_StatusOption = {
                title: {
                    text: '下单统计图表',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                    formatter: function (params) {
                        var relVal = params[0].name;
                        for (var i = 0, l = params.length; i < l; i++) {
                            relVal += '<br/>' + params[i].value + "单";
                        }
                        return relVal;
                    }
                },
                xAxis: {
                    type: 'category',
                    data: name,
                    axisLabel: {
                        interval: 0,
                        formatter: function (params) {
                            var newParamsName = "";
                            var paramsNameNumber = params.length;
                            var provideNumber = 4;
                            var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                            if (paramsNameNumber > provideNumber) {
                                for (var p = 0; p < rowNumber; p++) {
                                    var tempStr = "";
                                    var start = p * provideNumber;
                                    var end = start + provideNumber;
                                    if (p == rowNumber - 1) {
                                        tempStr = params.substring(start, paramsNameNumber);
                                    } else {
                                        tempStr = params.substring(start, end) + "\n";
                                    }
                                    newParamsName += tempStr;
                                }

                            } else {
                                newParamsName = params;
                            }
                            return newParamsName
                        }

                    }
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: count,
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: '#d48265'
                        }
                    },
                }]
            };
            Meeting_Status_Chat.setOption(Meeting_StatusOption);
        }
    })

}

function GetSupplierList(start, end) {
    $.ajax({
        url: "/LR_CodeDemo/YT_Supplier/GetSupplierList?startTime=" + start + "&endTime=" + end,
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                html += "<option value='" + result.data[i].Id + "' >" + result.data[i].Name + "</opeion>";
            }
            $("#Spiller").html(html);
        }
    })
}

function btn_part_num_time() {
    var vs = $('#Supplier6  option:selected').val();
    var start = $("#part_StartTime").val();
    var end = $("#part_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsFinishAndNumEcharts?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: true, //同步执行
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var name = [];
            var aCount = [];
            var aTime = [];
            for (var i = 0; i < data.length; i++) {
                name.push(data[i].partsTypes);
                aCount.push(data[i].aCount);
                aTime.push(data[i].aTime);
            }
            var part_num_time_Chat = echarts.init(document.getElementById('part_num_time'));
            part_num_timeOption = {
                title: {
                    text: '零件完成数量与耗时统计',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                },
                legend: {
                    orient: 'horizontal',
                    x: 'center',
                    y: 'bottom',
                    data: ['耗时（小时）', '数量（件）']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: name,
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '',
                        min: 0,
                        axisLabel: {
                            formatter: '{value}'
                        }
                    }
                ],
                series: [
                    {
                        name: '耗时（小时）',
                        type: 'bar',
                        data: aTime
                    },
                    {
                        name: '数量（件）',
                        type: 'bar',
                        data: aCount
                    },
                ]
            };
            part_num_time_Chat.setOption(part_num_timeOption);
        }
    })
}