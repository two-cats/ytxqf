﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-05-29 14:25
 * 描  述：工作日志
 */
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 查看
            $('#lr_view').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/WorkLog/WorkDailyLog/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/WorkLog/WorkDailyLog/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/WorkLog/WorkDailyLog/GetPageList',
                headData: [
                    //{ label: "Id", name: "Id", width: 160, align: "left"},
                    { label: "用户姓名", name: "UserName", width: 160, align: "left"},
                    { label: "日志日期", name: "SubmitTime", width: 160, align: "left"},
                    { label: "今日总结", name: "TodayLog", width: 160, align: "left"},
                    { label: "心得体会", name: "LogContent", width: 160, align: "left"},
                    { label: "明日计划", name: "TomorrowLog", width: 160, align: "left"},
                    { label: "状态", name: "Flag", width: 160, align: "left"},
                    { label: "更新时间", name: "UpdateTime", width: 160, align: "left"},
                ],
                mainId:'Id',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
