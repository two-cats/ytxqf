﻿using TTon.TMC.Application.OA.Schedule;
using TTon.TMC.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTon.TMC.Application.Base.SystemModule;

namespace TTon.TMC.Application.Web.Areas.OAModule.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.7.11
    /// 描 述：日程管理
    /// </summary>
    public class ScheduleController : MvcControllerBase
    {
        private ScheduleIBLL scheduleIBLL = new ScheduleBLL();
        #region 属性
        private DataItemService dataItemService = new DataItemService();
        #endregion

        #region 视图功能
        /// <summary>
        /// 管理页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ApprovalForm()
        {
            return View();
        }

        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CauseForm()
        {
            return View();
        }

        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EChartsIndex()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取会议列表
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetEChartsPageList(string pagination, string queryJson, string startTime, string endTime, string status)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = scheduleIBLL.GetEChartsPageList(paginationobj, queryJson, startTime, endTime, status);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取日程数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetList()
        {
            List<Hashtable> data = new List<Hashtable>();
            

            foreach (ScheduleEntity entity in scheduleIBLL.GetList().ToList())
            {
               
                Hashtable ht = new Hashtable();
                ht["id"] = entity.F_ScheduleId;
               string title = dataItemService.GetDetailListbyValue(entity.F_RoomId).FirstOrDefault().F_ItemName.ToString();

                ht["title"] = title;
                ht["end"] = (entity.F_EndDate.ToDate().ToString("yyyy-MM-dd") + " " + entity.F_EndTime.Substring(0, 2) + ":" + entity.F_EndTime.Substring(2, 2)).ToDate().ToString("yyyy-MM-dd HH:mm:ss");
                ht["start"] = (entity.F_StartDate.ToDate().ToString("yyyy-MM-dd") + " " + entity.F_StartTime.Substring(0, 2) + ":" + entity.F_StartTime.Substring(2, 2)).ToDate().ToString("yyyy-MM-dd HH:mm:ss");
                ht["allDay"] = false;
                data.Add(ht);
            }
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = scheduleIBLL.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormEntity(string keyValue)
        {
            var data = scheduleIBLL.GetEntity(keyValue);
            return Success(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            scheduleIBLL.DeleteEntity(keyValue);
            return Success("删除成功。");
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, ScheduleEntity entity)
        {
            scheduleIBLL.SaveForm(keyValue, entity);
            return Success("操作成功。");
        }
        #endregion

        #region 报表统计
        /// <summary>
        /// 会议状态统计
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMeetingStatusEcharts(string startTime, string endTime)
        {
            var data = scheduleIBLL.GetMeetingStatusEcharts(startTime, endTime);
            return Success(data);
        }


        #endregion
    }
}