﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class PushModel
    {
        
        public string Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
        /// <summary>
        /// 0:会议详细 1：会议审批 2：报修弹窗  3：报修通知
        /// </summary>
        public int type { get; set; }

        /// <summary>
        /// 状态 0 提交 1 审批同意 2 审批拒绝  3 指派  4 挑选物资 5 同意  6 拒绝  7结束
        /// </summary>
        public int status { get; set; }
        /// <summary>
        /// 权限 0 工人  1 工单负责人  2 经理
        /// </summary>
        public int jurisdiction { get; set; }

    }
}