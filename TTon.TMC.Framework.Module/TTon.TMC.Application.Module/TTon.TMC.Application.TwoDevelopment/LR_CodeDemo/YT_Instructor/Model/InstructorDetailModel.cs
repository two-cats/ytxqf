﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Instructor.Model
{
    public class InstructorDetailModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Supplier { get; set; }

        public string Parts { get; set; }

        public string PartsNum { get; set; }

        public string MaintenanceType { get; set; }

        public string SketchDate { get; set; }

        public string Number { get; set; }

        public string WorkHours { get; set; }

        public string IssueDescription { get; set; }

        public List<Image> IssuePic { get; set; }

        public string Pick_Scope { get; set; }

        public string Pick_Prepare { get; set; }

        public string Pick_Step { get; set; }

        public List<Image> Pick_Pic { get; set; }

        public string Pick_Demand { get; set; }

        public string Pick_Project { get; set; }

        public string Remark { get; set; }

        public List<Image> Remark_Pic { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateUser { get; set; }
    }

    public class Image
    {
        public string Img_Url { get; set; }
    }
}
