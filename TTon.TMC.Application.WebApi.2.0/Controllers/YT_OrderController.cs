﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using TTon.TMC.Application.WebApi._2._0.Models;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/order")]
    public class YT_OrderController : BaseApiController
    {
        private YT_OrderIBLL yT_OrderIBLL = new YT_OrderBLL();

        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        [Route("add")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage addOrder(YT_OrderEntity Entity)
        {
            try
            {
              
                Entity.Order_Status = 2;
                if (!string.IsNullOrEmpty(Entity.Date))
                {
                    Entity.Date = Convert.ToDateTime(Entity.Date).ToString("yyyy-MM-dd HH:ss");
                }
                yT_OrderIBLL.SaveEntity("",Entity);
                return Render(HttpStatusCode.OK, "success", new YT_OrderEntity() );
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }



        /// <summary>
        /// 创建订单基础信息
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        [Route("add/basic")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage addWXOrder(YT_OrderEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(entity.Date))
                {
                    entity.Date = Convert.ToDateTime(entity.Date).ToString("yyyy-MM-dd HH:ss");
                }
                new WX_Order_StatisticsService().SaveOrderEntity(entity);
                return Render(HttpStatusCode.OK, "success", new YT_OrderEntity());
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 订单获取库管历史列表
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        [Route("Inventory")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetOrderInventoryList(string orderid)
        {
            try
            {
                var data = yT_OrderIBLL.GetOrderInventoryList(orderid);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 库管提交给值班经理
        /// </summary>
        /// <returns></returns>
        [Route("update/inventory")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage OrderInventory(OrderInventoryModel model) {
            try{

                bool data = new WX_Order_StatisticsService().OrderInventory(model);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderInventoryModel());
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
                
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 预估时间
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("estimate")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage estimateOrder(estimateOrderModel model)
        {
            try
            {
                bool data=yT_OrderIBLL.estimateOrder(model.estimate_date,model.order_id,model.user_id);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new YT_OrderEntity());

                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "不能重复预估时间！。");
                }
              
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
        /// <summary>
        /// 订单审批
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("approval")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage approvalOrder(ExamineModel model)
        {
            try
            {
                bool data = yT_OrderIBLL.approvalOrder(model.order_id, model.user_id,model.status);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new YT_OrderEntity());

                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "不能重复审批！。");
                }

            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
        /// <summary>
        /// 分配人员列表
        /// </summary>
        /// <returns></returns>
        [Route("user")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage getOrderUser(queryJson queryJson)
        {
            try
            {
                var data = yT_OrderIBLL.userList(queryJson.pagination, queryJson.querystr);
                return Render(HttpStatusCode.OK, "success", data);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("list")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetOrderList(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetOrderList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 仓库管理员订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("check/inventory")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage CheckOrderInventory(string userId)
        {
            try
            {
                var data = yT_OrderIBLL.CheckOrderInventory(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 验证是否位经理
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("check/manage")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage CheckOrderManage(string userId)
        {
            try
            {
                var data = yT_OrderIBLL.CheckOrderManage(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 验证是否为班长
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("check/squadleader")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage CheckSquadLeader(string userId)
        {
            try
            {
                var data = yT_OrderIBLL.CheckSquadLeader(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 仓库管理员订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("inventory/list")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetInventoryOrderList(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetInventoryOrderList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("list/wx")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetOrderListToWx(GetOrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetOrderListToWx(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("task/list")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetOrderTaskList(GetOrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetOrderTaskList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 任务详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [Route("task/detail")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetTaskDetail(string keyValue)
        {
            try
            {
                var data = yT_OrderIBLL.GetTaskDetail(keyValue);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 订单详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [Route("detail")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetOrderDetail(string keyValue)
        {
            try
            {
                var data = yT_OrderIBLL.GetOrderDetail(keyValue);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// APP首页统计
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("statistics")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetHomeStatistics(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetHomeStatistics(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 员工完成零件统计
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("parttypes/finish")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUserPartTypesInfo(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetUserPartTypesInfo(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取未完成订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("unfinished/order")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnfinishedOrderList(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetUnfinishedOrderList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取未完成任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("unfinished/task")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetUnfinishedTaskList(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetUnfinishedTaskList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 维修统计
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("maintain/statistics")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage MaintenanceCountStatistics(string userId)
        {
            try
            {
                var data = yT_OrderIBLL.MaintenanceCountStatistics(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取今日完成订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("today/order")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetTodayOrderList(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetTodayOrderList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取今日完成任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("today/task")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetTodayTaskList(OrderListParam param)
        {
            try
            {
                var data = yT_OrderIBLL.GetTodayTaskList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 指派人员
        /// </summary>
        /// <returns></returns>
        [Route("designate")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage designate(OrderDesignateModel model) {
            try
            {
                bool data = yT_OrderIBLL.designate(model.order_id, model.user_id,model.userList,model.all,model.accomplish,model.category);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderDesignateModel());

                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "该订单状态不可指派");
                }           
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 获取零件未分配、已分配数量
        /// </summary>
        /// <param name="order_id"></param>
        /// <returns></returns>
        [Route("parts/number")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage getOrderNumer(string order_id)
        {
            try
            {
                var  data = yT_OrderIBLL.getOrderPartsNumber(order_id);
                if (data!=null)
                {
                    return Render(HttpStatusCode.OK, "success", data);

                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "订单数据异常！。");
                }


            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }


        /// <summary>
        /// 提交任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("task/submit")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage submitTask(SubmitModel model) {
            try
            {
                 var data= yT_OrderIBLL.submitTask(model.task_id,model.order_id,model.success,model.error,model.Unfinished);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderDesignateModel());
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "请填写正确的零件数量！。");
                }             
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 更改任务
        /// </summary>
        /// <returns></returns>
        [Route("task/update")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage UpdateTask(UpdateTaskModel model) {
            try
            {
                var data = new WX_Order_StatisticsService().UpdateTask(model);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderDesignateModel());
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        [Route("test")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage test(string date)
        {
            var data = yT_OrderIBLL.test(date);
            return Render(HttpStatusCode.OK, "success", data);
        }

        /// <summary>
        /// 通过工作效率计算工时
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("work/hours")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage getOrderWorkHours(OrderWorkHoursParams param)
        {
            try
            {
                var data = yT_OrderIBLL.getOrderWorkHours(param);
                if (data != null)
                {
                    return Render(HttpStatusCode.OK, "success", data);

                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "工时计算过程出现问题！");
                }


            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }


        /// <summary>
        /// 订单完成
        /// </summary>
        /// <returns></returns>
        [Route("submit")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage submitOrder(submitOrderModel model) {
            try
            {
                int data = yT_OrderIBLL.submitOrder(model.order_id, model.task_pic, model.user_id,model.estimate_date,model.success_remark,model.person,model.count,model.thread_time,model.lineedge_time,model.success_date,
                    model.sortingWay,model.frequency,model.workers,model.workday,model.weekend,model.statutoryHoliday,model.mealTimes,model.start_date,model.badProductDescribe,model.workpieceRatio);
                if (data == 0)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderDesignateModel());

                }
                else if(data == 1)
                {
                    return Render(HttpStatusCode.InternalServerError, "该订单未指派！。");
                }
                else if (data == 3)
                {
                    return Render(HttpStatusCode.InternalServerError, "该订单未提交给仓库管理员！。");
                }
                else if (data == 4)
                {
                    return Render(HttpStatusCode.InternalServerError, "结束时间不能小于开始时间！。");
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "该订单已完成，不能重复提交！。");
                }


            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
        /// <summary>
        /// 库管完成订单
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        [Route("inventory/submit")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage InventorySubmit(string order_id, string user_id)
        {
            try
            {
                bool data=new WX_Order_StatisticsService().ManageSubmit(order_id, user_id);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderDesignateModel());
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "该订单不能完成！");
                }


            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 库管完成订单
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        [Route("store/error")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage UpdateStorekeeper(string order_id, string user_id)
        {
            try
            {
                bool data = new WX_Order_StatisticsService().UpdateStorekeeper(order_id, user_id);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", new OrderDesignateModel());
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "订单无效！");
                }


            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// WX 订单统计
        /// </summary>
        /// <returns></returns>
        [Route("statistics/wx")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage order_Statistics(string supplier, string start, string end) {

            try
            {
                var  data = new WX_Order_StatisticsService().getWX_Order_Statistics(supplier,start,end);

              return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }


        /// <summary>
        /// 订单审批
        /// </summary>
        /// <returns></returns>
        [Route("wx/approve")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage WxApproveOrder(ApproveOrderParams model)
        {
            try
            {
                var data =  yT_OrderIBLL.WxApproveOrder(model);
                if (data)
                {
                    return Render(HttpStatusCode.OK, "success", "审批成功");
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "审批失败");
                }
            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 添加异常记录
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("add/abnormal")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage SaveAbnormal(SaveAbnormalParams param)
        {
            try
            {
                var data = yT_OrderIBLL.SaveAbnormal(param.orderId,param.userId,param.content,param.img);
                if (data == 0)
                {
                    return Render(HttpStatusCode.OK, "success", new object());
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "只有班长才能提交");
                }

            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 获取异常列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("abnormal/list")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetAbnormalList(GetAbnormalListParams param)
        {
            try
            {
                var data = yT_OrderIBLL.GetAbnormalList(param);
                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 获取异常详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [Route("abnormal/details")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetAbnormalInfo(string keyValue)
        {
            try
            {
                var data = yT_OrderIBLL.GetAbnormalInfo(keyValue);
                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

        /// <summary>
        /// 审批异常记录
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("abnormal/approve")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AbnormalApprove(AbnormalApproveParams param)
        {
            try
            {
                var data = yT_OrderIBLL.AbnormalApprove(param);
                if (data == 1)
                {
                    return Render(HttpStatusCode.InternalServerError, "只有仓库管理员有权限操作");
                }
                else if (data == 2)
                {
                    return Render(HttpStatusCode.InternalServerError, "该记录不存在");
                }
                else
                {
                    return Render(HttpStatusCode.OK, "success", data);
                }
            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
    }
}
