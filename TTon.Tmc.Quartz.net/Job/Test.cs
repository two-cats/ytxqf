﻿using log4net;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESteward.QuartzService.Job
{
    public class Test : IJob
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(Test));

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("开始执行Test@" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            
                Console.WriteLine("成功执行Test" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                Console.WriteLine("ERR:" + ex.Message);
            }
        }
    }
}
