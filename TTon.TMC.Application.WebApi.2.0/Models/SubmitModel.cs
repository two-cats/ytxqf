﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    /// <summary>
    /// 提交任务Model
    /// </summary>
   public class SubmitModel
    {
        /// <summary>
        /// 任务主键
        /// </summary>
        public string task_id { get; set; }

        /// <summary>
        /// 订单主键
        /// </summary>
        public string order_id { get; set; }


        /// <summary>
        /// 合格数量
        /// </summary>
        public int success { get; set; }
        /// <summary>
        /// 不合格数量
        /// </summary>
        public int error { get; set; }

        /// <summary>
        /// 未完成数量
        /// </summary>
        public int Unfinished { get; set; }

    }
}
