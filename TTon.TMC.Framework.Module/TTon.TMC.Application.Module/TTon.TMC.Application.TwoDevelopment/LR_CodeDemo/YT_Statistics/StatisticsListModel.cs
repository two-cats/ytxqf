﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Statistics
{
    public class StatisticsListModel
    {
        public string Id { get; set; }

        public string OrderNumber { get; set; }

        public string DrawingNumber { get; set; }

        public string Supplier { get; set; }

        public string PartsNumber { get; set; }

        public string PartsType { get; set; }

        public string BatchNumber { get; set; }

        public string IssueDescription { get; set; }
 
        public string RepairType { get; set; }
  
        public int? Sum { get; set; }

        public int? Qualified { get; set; }

        public int? Disqualified { get; set; }

        public string Unfinished { get; set; }

        public string CreateTime { get; set; }

        public string SuccessDate { get; set; }

        public string ParentId { get; set; }
    }
}
