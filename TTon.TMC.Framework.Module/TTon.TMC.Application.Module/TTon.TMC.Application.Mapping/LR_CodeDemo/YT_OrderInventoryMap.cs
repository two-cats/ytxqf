﻿using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-10-31 10:29
    /// 描 述：库管流程
    /// </summary>
    public class YT_OrderInventoryMap : EntityTypeConfiguration<YT_OrderInventoryEntity>
    {
        public YT_OrderInventoryMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_ORDERINVENTORY");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

