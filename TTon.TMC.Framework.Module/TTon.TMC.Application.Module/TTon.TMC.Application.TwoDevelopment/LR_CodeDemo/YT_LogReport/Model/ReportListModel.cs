﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_LogReport.Model
{
    public class ReportListModel
    {
        public string id { get; set; }

        public string name { get; set; }

        public string createuser { get; set; }

        public string heiruser { get; set; }

        public string heirstatus { get; set; }

        public string heirtime { get; set; }

        public string createtime { get; set; }

        public int everconfirmed { get; set; }

        public string createdate { get; set; }

        public List<OrderList> orderlist { get; set; }
    }

    public class OrderList
    {
        public string ordernumber { get; set; }

        public string surplus { get; set; }
    }
}
