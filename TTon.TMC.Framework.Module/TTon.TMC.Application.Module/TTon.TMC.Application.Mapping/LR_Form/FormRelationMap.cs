﻿using TTon.TMC.Application.Form;
using System.Data.Entity.ModelConfiguration;

namespace TTon.TMC.Application.Mapping
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.01
    /// 描 述：表单关联功能
    /// </summary>
    public class FormRelationMap : EntityTypeConfiguration<FormRelationEntity>
    {
        public FormRelationMap()
        {
            #region 表、主键
            //表
            this.ToTable("FORM_RELATION");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
