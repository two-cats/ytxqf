﻿using TTon.TMC.Application.IM;

namespace TTon.TMC.Application.IMServer
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.01
    /// 描 述：程序开始入口
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IMStart.Start();
        }
    }
}
