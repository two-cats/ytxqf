﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Quartz.net.Helper
{
    public class PushModel
    {
        
        public string id { get; set; }

        public string title { get; set; }

        public string content { get; set; }
        /// <summary>
        /// 0:会议详细 1：会议审批 2：报修弹窗  3：报修通知  4:维保任务
        /// </summary>
        public int type { get; set; }

    }
}