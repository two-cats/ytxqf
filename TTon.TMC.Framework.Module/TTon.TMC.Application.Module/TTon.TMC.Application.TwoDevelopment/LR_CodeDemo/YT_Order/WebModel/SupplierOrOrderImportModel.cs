﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class SupplierOrOrderImportModel
    {
        public YT_InstructorEntity instructorEntity { get; set; }

        public YT_SupplierEntity supplierEntity { get; set; }

        public YT_OrderUserEntity orderUserEntity { get; set; }

        public YT_PartsTypesEntity partsTypesEntity { get; set; }

        public YT_OrderEntity orderEntity { get; set; }

        public YT_MaintenanceTypesEntity maintenanceTypesEntity { get; set; }
    }
}
