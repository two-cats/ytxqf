﻿using TTon.TMC.Util;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace TTon.TMC.Application.Web.Areas.LR_CodeDemo.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 10:22
    /// 描 述：配置零件
    /// </summary>
    public class YT_PartsTypesController : MvcControllerBase
    {
        private YT_PartsTypesIBLL yT_PartsTypesIBLL = new YT_PartsTypesBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 零件列表
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPartsTypesTree(string keyValue)
        {
            var list = yT_PartsTypesIBLL.GetPartsTypesTree(keyValue);

            return Success(list);
        }


        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = yT_PartsTypesIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList()
        {
            var YT_PartsTypesData = yT_PartsTypesIBLL.GetList();
            return Success(YT_PartsTypesData);
        }
        /// <summary>
        /// 获取列表
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetListTree(string parentId)
        {
            var YT_PartsTypesData = yT_PartsTypesIBLL.GetListTree(parentId);
            return Success(YT_PartsTypesData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var YT_PartsTypesData = yT_PartsTypesIBLL.GetYT_PartsTypesEntity( keyValue );
            var jsonData = new {
                YT_PartsTypesData = YT_PartsTypesData,
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetEntity(string keyValue)
        {
            var YT_PartsTypesData = yT_PartsTypesIBLL.GetYT_PartsTypesEntity(keyValue);
            return Success(YT_PartsTypesData);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            yT_PartsTypesIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            YT_PartsTypesEntity entity = strEntity.ToObject<YT_PartsTypesEntity>();
            yT_PartsTypesIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }
        #endregion

        #region 首页图表
        /// <summary>
        /// 员工完成数量
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetUserFinishEcharts(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetUserFinishEcharts(startTime, endTime, supplier);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetUserFinishEchartsToSu(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetUserFinishEchartsToSu(startTime, endTime, supplier);
            return Success(data);
        }

        /// <summary>
        /// 本月（时段）各零件挑选数量折线图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult  GetPartsTypesEcharsModel(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetPartsTypesEcharsModel(startTime, endTime, supplier);
            return Content(data.ToJson());
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetNewPartsTypesToAnalysis(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetNewPartsTypesToAnalysis(startTime, endTime, supplier);
            return Content(data.ToJson());
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPartsTypesEcharsToAnalysis(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetPartsTypesEcharsToAnalysis(startTime, endTime, supplier);
            return Content(data.ToJson());
        }

        /// <summary>
        /// 本月（时段）零件挑选原因占比
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaintenanceTypesModel(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetMaintenanceTypesModel(startTime, endTime, supplier);
            return Content(data.ToJson());
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaintenanceTypesToAnalysis(string startTime, string endTime,string supplier)
        {
            var data = yT_PartsTypesIBLL.GetMaintenanceTypesToAnalysis(startTime, endTime, supplier);
            return Content(data.ToJson());
        }


        /// <summary>
        ///  供应商累计合格率（合格数量/总数）
        /// </summary>
        /// <param name="supplier"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPercentOfPass(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetPercentOfPass(startTime, endTime, supplier);
            return Content(data.ToJson());
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPercentToAnalysis(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetPercentToAnalysis(startTime, endTime, supplier);
            return Content(data.ToJson());
        }

        /// <summary>
        /// 零件完成数量与耗时统计
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="supplier"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPartsFinishAndNumEcharts(string startTime, string endTime, string supplier)
        {
            var data = yT_PartsTypesIBLL.GetPartsFinishAndNumEcharts(startTime, endTime, supplier);
            return Content(data.ToJson());
        }
        #endregion
    }
}
