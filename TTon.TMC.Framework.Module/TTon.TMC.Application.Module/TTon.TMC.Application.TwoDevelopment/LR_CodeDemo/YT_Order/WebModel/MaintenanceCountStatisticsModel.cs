﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class MaintenanceCountStatisticsModel
    {
        public int sum { get; set; }

        public string percentage { get; set; }

        public List<PartTypesCountModel> list { get; set; } 
    }

    public class PartTypesCountModel
    {
        /// <summary>
        /// 厂家（供应商）
        /// </summary>
        public string supplier { get; set; }
        /// <summary>
        /// 零件名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 零件数量
        /// </summary>
        public int count { get; set; }
    }

    public class UserFinishCountModel
    {
        public string id { get; set; }

        public string name { get; set; }

        public int count { get; set; }
    }
}
