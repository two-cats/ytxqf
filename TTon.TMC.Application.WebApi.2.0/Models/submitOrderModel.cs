﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    /// <summary>
    /// 订单完成Model
    /// </summary>
    public class submitOrderModel
    {
        public string order_id { get; set; }

        public string task_pic { get; set; }

        public string user_id { get; set; }

        public string estimate_date { get; set; }

        public string success_remark { get; set; }

        public string person { get; set; }

        public string count { get; set; }
        public string thread_time { get; set; }

        public string lineedge_time { get; set; }

        public string success_date { get; set; }

        public string sortingWay { get; set; }

        public string frequency { get; set; }

        public string workers { get; set; }

        public string workday { get; set; }

        public string weekend { get; set; }

        public string statutoryHoliday { get; set; }

        public string mealTimes { get; set; }

        public string start_date { get; set; }

        public string badProductDescribe { get; set; }
  
        public string workpieceRatio { get; set; }
    }
}