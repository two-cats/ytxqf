﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class GetListModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Date { get; set; }

        public string OrderNumber { get; set; }

        public string PartsNumber { get; set; }

        public string Supplier_Id { get; set; }

        public string PartsType { get; set; }

        public string MaintainType { get; set; }

        public string Instructor { get; set; }

        public string Remark { get; set; }

        public int Order_Status { get; set; }

        public string EstimateDate { get; set; }

        public string ThreadElapsedTime { get; set; }

        public string LineEdgeElapsedTime { get; set; }

        public string ElapsedTime { get; set; }

        public DateTime CreateDate { get; set; }

        public string StartDate { get; set; }

        public string SuccessDate { get; set; }

        public int Count { get; set; }

        public string Success_Remark { get; set; }

        public string F_RealName { get; set; }

        public int Success { get; set; }

        public int Error { get; set; }

        public string Unfinished { get; set; }

        public int SupplierStatus { get; set; }
    }
}
