﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Util;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_LogReport
{
    public class YT_LogReportEntity
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        /// <returns></returns>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// HeirUser
        /// </summary>
        /// <returns></returns>
        [Column("HEIRUSER")]
        public string HeirUser { get; set; }
        /// <summary>
        /// HeirStatus
        /// </summary>
        /// <returns></returns>
        [Column("HEIRSTATUS")]
        public int? HeirStatus { get; set; }
        /// <summary>
        /// HeirTime
        /// </summary>
        /// <returns></returns>
        [Column("HEIRTIME")]
        public DateTime? HeirTime { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        /// <returns></returns>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// CreateUser
        /// </summary>
        /// <returns></returns>
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
            this.HeirStatus = 0;
            this.CreateDate = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
            this.HeirTime = DateTime.Now;
        }
        #endregion
    }
}
