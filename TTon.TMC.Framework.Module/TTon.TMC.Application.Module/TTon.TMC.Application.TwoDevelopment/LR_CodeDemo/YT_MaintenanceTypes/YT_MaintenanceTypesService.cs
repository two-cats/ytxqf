﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;
using System.Linq;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 11:48
    /// 描 述：配置维修类型
    /// </summary>
    public class YT_MaintenanceTypesService : RepositoryFactory
    {
        #region 获取数据
        public List<SupplierListToTree> GetMaintenanceTypesTree(string keyValue)
        {
            List<SupplierListToTree> list = new List<SupplierListToTree>();
            var strSql = new StringBuilder();
            strSql.Append(@"SELECT DISTINCT
	                YT_MaintenanceTypes.Id AS id,
	                YT_MaintenanceTypes.Name AS name,
	                YT_MaintenanceTypes.CreateDate
                FROM
	                YT_MaintenanceTypes
                LEFT JOIN YT_Order ON YT_Order.MaintainType = YT_MaintenanceTypes.Id
                WHERE
	                YT_Order.PartsType = '"+keyValue+@"'
                ORDER BY
	                YT_MaintenanceTypes.CreateDate DESC ");
            DataTable dt = new RepositoryFactory().BaseRepository().FindTable(strSql.ToString());
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierListToTree model = new SupplierListToTree();
                    model.number = i;
                    model.Id = dt.Rows[i]["id"].ToString();
                    model.Name = dt.Rows[i]["name"].ToString();
                    list.Add(model);
                }
            }
            return list;
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_MaintenanceTypesEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Name,
                t.Remark,
                t.CreateDate,
                t.CreateUserId,
                t.ModifyDate
                ");
                strSql.Append("  FROM YT_MaintenanceTypes t ");
                strSql.Append("  WHERE 1=1  and t.IsDelete=0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                return this.BaseRepository().FindList<YT_MaintenanceTypesEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_MaintenanceTypesEntity> GetList()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Name,
                t.Remark,
                t.CreateDate,
                t.CreateUserId,
                t.ModifyDate
                ");
                strSql.Append("  FROM YT_MaintenanceTypes t ");
                strSql.Append("  WHERE 1=1  and t.IsDelete=0  order by t.CreateDate desc");
                return this.BaseRepository().FindList<YT_MaintenanceTypesEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <param name="Parts_Id"></param>
        /// <returns></returns>
        public IEnumerable<YT_MaintenanceTypesEntity> GetListTree()
        {
            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT YT_MaintenanceTypes.* FROM YT_MaintenanceTypes 
                            WHERE YT_MaintenanceTypes.IsDelete = 0
                            Order by YT_MaintenanceTypes.CreateDate desc ");

            return this.BaseRepository().FindList<YT_MaintenanceTypesEntity>(strsql.ToString());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <param name="Parts_Id"></param>
        /// <returns></returns>
        public IEnumerable<AutocmpModel> GetACmaintenance()
        {
            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT
	                Id as hex,Name as label
                FROM
	                YT_MaintenanceTypes 
                WHERE
	                YT_MaintenanceTypes.IsDelete = 0 
                ORDER BY
	                Name DESC");
            return this.BaseRepository().FindList<AutocmpModel>(strsql.ToString());
        }



        /// <summary>
        /// 获取YT_MaintenanceTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_MaintenanceTypesEntity GetYT_MaintenanceTypesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_MaintenanceTypesEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                YT_MaintenanceTypesEntity entity = GetYT_MaintenanceTypesEntity(keyValue);
                entity.IsDelete = 1;
                this.BaseRepository().Update<YT_MaintenanceTypesEntity>(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_MaintenanceTypesEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 首页图表
        /// <summary>
        /// 零件平均数
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<PartsTypesEchartModel> GetPartsTypesEcharts(string supplier)
        {
            try
            {
                List<PartsTypesEchartModel> list = new List<PartsTypesEchartModel>();

                #region 时间筛选
                DateTime now = DateTime.Now;
                DateTime last = DateTime.Now.AddMonths(-1);

                DateTime nowStartDate = now.AddDays(1 - now.Day).Date;
                DateTime nowEndDate = now.AddDays(1 - now.Day).Date.AddMonths(1).AddSeconds(-1);

                DateTime lastStartDate = last.AddDays(1 - last.Day).Date;
                DateTime lastEndDate = last.AddDays(1 - last.Day).Date.AddMonths(1).AddSeconds(-1);
                #endregion

                StringBuilder supplierSql = new StringBuilder();
                supplierSql.Append(@"SELECT DISTINCT
                             PartsType
                            FROM
                             YT_Order t
                            WHERE
                             t.CreateDate BETWEEN '" + lastStartDate + @"'
                            AND '" + nowEndDate + @"'
                            AND t.IsDelete = 0
                            AND t.Order_Status = 5
                            AND t.Supplier_Id = '" + supplier + "'");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(supplierSql.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string id = dt.Rows[i]["PartsType"].ToString();
                        PartsTypesEchartModel model = new PartsTypesEchartModel();
                        YT_PartsTypesEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(t => t.Id == id);
                        model.partsTypes = entity.Parts_Name;

                        StringBuilder consumeSql = new StringBuilder();
                        consumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL,
                                   ISNULL(EstimateDate, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.CreateDate BETWEEN '" + nowStartDate + @"'
                                AND '" + nowEndDate + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + "'");
                        DataTable consumedt = new RepositoryFactory().BaseRepository().FindTable(consumeSql.ToString());
                        int consume = consumedt.Rows[0]["EstimateDate"].ToInt() * 3600;

                        StringBuilder sumSql = new StringBuilder();
                        sumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.CreateDate BETWEEN '" + nowStartDate + @"'
                                AND '" + nowEndDate + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + "'");
                        DataTable sumdt = new RepositoryFactory().BaseRepository().FindTable(sumSql.ToString());
                        int sum = sumdt.Rows[0]["cou"].ToInt();
                        if (sum == 0)
                        {
                            model.nowMonth = 0.00;
                        }
                        else
                        {
                            double coun = Math.Round((float)(consume / sum), 2);
                            model.nowMonth = coun;
                        }

                        StringBuilder lastconsumeSql = new StringBuilder();
                        lastconsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL,
                                   ISNULL(EstimateDate, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.CreateDate BETWEEN '" + lastStartDate + @"'
                                AND '" + lastEndDate + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + "'");
                        DataTable lastconsumedt = new RepositoryFactory().BaseRepository().FindTable(lastconsumeSql.ToString());
                        int lastconsume = lastconsumedt.Rows[0]["EstimateDate"].ToInt() * 3600;

                        StringBuilder lastsumSql = new StringBuilder();
                        lastsumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.CreateDate BETWEEN '" + lastStartDate + @"'
                                AND '" + lastEndDate + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status != 3
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + "'");
                        DataTable lastsumdt = new RepositoryFactory().BaseRepository().FindTable(lastsumSql.ToString());
                        int lastsum = lastsumdt.Rows[0]["cou"].ToInt();
                        if (lastsum == 0)
                        {
                            model.lastMonth = 0.00;
                        }
                        else
                        {
                            double coun = Math.Round((float)(lastconsume / lastsum), 2);
                            model.lastMonth = coun;
                        }
                        list.Add(model);
                    }
                }
                else
                {
                    list = new List<PartsTypesEchartModel>();
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MaintenanceTypeTimeModel> GetMaintenanceTypesTime(string startTime, string endTime, string supplier, string part)
        {
            try
            {
                List<MaintenanceTypeTimeModel> list = new List<MaintenanceTypeTimeModel>();

                #region 时间筛选
                DateTime now = DateTime.Now;

                DateTime startDate = new DateTime();

                DateTime endDate = new DateTime();

                if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    startDate = now.AddMonths(-1);
                    endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                }
                else if (string.IsNullOrEmpty(startTime))
                {
                    endDate = Convert.ToDateTime(endTime);
                    startDate = endDate.AddDays(-30);
                }
                else if (string.IsNullOrEmpty(endTime))
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = DateTime.Now;
                }
                else
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = Convert.ToDateTime(endTime);
                }
                #endregion

                string startD = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT DISTINCT
	                    MaintainType
                    FROM
	                    YT_Order t
                    WHERE
	                    t.SuccessDate BETWEEN '" + startD + @"'
                    AND '" + endD + @"'
                    AND t.IsDelete = 0
                    AND t.Order_Status = 5
                    AND t.Supplier_Id = '" + supplier + @"'
                    ");
                if (!string.IsNullOrEmpty(part))
                {
                    sql.Append(" AND t.PartsType = '" + part + "' ");
                }
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string id = dt.Rows[i]["MaintainType"].ToString();
                        MaintenanceTypeTimeModel model = new MaintenanceTypeTimeModel();
                        YT_MaintenanceTypesEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_MaintenanceTypesEntity>(d => d.Id == id);
                        if (entity != null)
                        {
                            model.maintenanceType = entity.Name;

                            StringBuilder aConsumeSql = new StringBuilder();
                            aConsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(EstimateDate, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.MaintainType = '" + id + @"'
                                AND t.Area = 1");
                            DataTable aConsumedt = new RepositoryFactory().BaseRepository().FindTable(aConsumeSql.ToString());
                            double aConsume = aConsumedt.Rows[0]["EstimateDate"].ToDouble();
                            model.aTime = aConsume;

                            StringBuilder tConsumeSql = new StringBuilder();
                            tConsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(ThreadElapsedTime, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.MaintainType = '" + id + @"'
                                AND t.Thread = 1");
                            DataTable tConsumedt = new RepositoryFactory().BaseRepository().FindTable(tConsumeSql.ToString());
                            double tConsume = tConsumedt.Rows[0]["EstimateDate"].ToDouble();
                            model.tTime = tConsume;

                            StringBuilder lConsumeSql = new StringBuilder();
                            lConsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(LineEdgeElapsedTime, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.MaintainType = '" + id + @"'
                                AND t.LineEdge = 1");
                            DataTable lConsumedt = new RepositoryFactory().BaseRepository().FindTable(lConsumeSql.ToString());
                            double lConsume = lConsumedt.Rows[0]["EstimateDate"].ToDouble();
                            model.lTime = lConsume;

                            StringBuilder aSumSql = new StringBuilder();
                            aSumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.MaintainType = '" + id + @"'
                                AND t.Area = 1");
                            DataTable aSumdt = new RepositoryFactory().BaseRepository().FindTable(aSumSql.ToString());
                            int aSum = aSumdt.Rows[0]["cou"].ToInt();
                            model.aCount = aSum;
                            int a = 0;
                            if (aSum == 0)
                            {
                                a = 1;
                            }
                            else
                            {
                                float aa = (float)(aConsume * 3600) / aSum;
                                decimal aCoun = Math.Round(Convert.ToDecimal(aa), 1, MidpointRounding.AwayFromZero);
                                model.aAverage = Convert.ToDouble(aCoun);
                            }

                            StringBuilder tSumSql = new StringBuilder();
                            tSumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.MaintainType = '" + id + @"'
                                AND t.Thread = 1");
                            DataTable tSumdt = new RepositoryFactory().BaseRepository().FindTable(tSumSql.ToString());
                            int tSum = tSumdt.Rows[0]["cou"].ToInt();
                            model.tCount = tSum;
                            int t = 0;
                            if (tSum == 0)
                            {
                                t = 1;
                            }
                            else
                            {
                                float aa = (float)(tConsume * 3600) / tSum;
                                decimal tCoun = Math.Round(Convert.ToDecimal(aa), 1, MidpointRounding.AwayFromZero);
                                model.tAverage = Convert.ToDouble(tCoun);
                            }

                            StringBuilder lSumSql = new StringBuilder();
                            lSumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.MaintainType = '" + id + @"'
                                AND t.LineEdge = 1");
                            DataTable lSumdt = new RepositoryFactory().BaseRepository().FindTable(lSumSql.ToString());
                            int lSum = lSumdt.Rows[0]["cou"].ToInt();
                            model.lCount = lSum;
                            int l = 0;
                            if (lSum == 0)
                            {
                                l = 1;
                            }
                            else
                            {
                                float aa = (float)(lConsume * 3600) / lSum;
                                decimal lCoun = Math.Round(Convert.ToDecimal(aa), 1, MidpointRounding.AwayFromZero);
                                model.lAverage = Convert.ToDouble(lCoun);
                            }

                            list.Add(model);
                        }                     
                    }
                }

                return list;
            }
            catch(Exception e)
            {
                throw e;
            }
        }


        public List<MaintenanceTypeTimeToAModel> GetMaintenanceTypesTimeToAnalysis(string startTime, string endTime, string supplier)
        {
            List<MaintenanceTypeTimeToAModel> list = new List<MaintenanceTypeTimeToAModel>();

            //DateTime startDate = Convert.ToDateTime(startTime);
            //DateTime endDate = Convert.ToDateTime(endTime);

            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT DISTINCT
	                    MaintainType
                    FROM
	                    YT_Order t
                    WHERE
	                    t.SuccessDate BETWEEN '" + startTime + @"'
                    AND '" + endTime + @"'
                    AND t.IsDelete = 0
                    AND t.Supplier_Id = '" + supplier + @"'
                    ");
            DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string id = dt.Rows[i]["MaintainType"].ToString();
                    MaintenanceTypeTimeToAModel model = new MaintenanceTypeTimeToAModel();
                    YT_MaintenanceTypesEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_MaintenanceTypesEntity>(d => d.Id == id);
                    if (entity != null)
                    {
                        model.maintenanceType = entity.Name;

                        //时间
                        StringBuilder aconsumeSql = new StringBuilder();
                        aconsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                     DECIMAL(10, 2),
                                   ISNULL(EstimateDate, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startTime + @"'
                                AND '" + endTime + @"'
                                AND t.IsDelete = 0
                                AND t.MaintainType = '" + id + @"'");
                        DataTable aconsumedt = new RepositoryFactory().BaseRepository().FindTable(aconsumeSql.ToString());
                        double aconsume = aconsumedt.Rows[0]["EstimateDate"].ToDouble();

                        StringBuilder bconsumeSql = new StringBuilder();
                        bconsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                    DECIMAL(10, 2),
                                   ISNULL(ThreadElapsedTime, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startTime + @"'
                                AND '" + endTime + @"'
                                AND t.IsDelete = 0
                                AND t.MaintainType = '" + id + @"'");
                        DataTable bconsumedt = new RepositoryFactory().BaseRepository().FindTable(bconsumeSql.ToString());
                        double bconsume = bconsumedt.Rows[0]["EstimateDate"].ToDouble();

                        StringBuilder cconsumeSql = new StringBuilder();
                        cconsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(LineEdgeElapsedTime, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startTime + @"'
                                AND '" + endTime + @"'
                                AND t.IsDelete = 0
                                AND t.MaintainType = '" + id + @"'");
                        DataTable cconsumedt = new RepositoryFactory().BaseRepository().FindTable(cconsumeSql.ToString());
                        double cconsume = cconsumedt.Rows[0]["EstimateDate"].ToDouble();

                        //数量
                        StringBuilder sumSql = new StringBuilder();
                        sumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.SuccessDate BETWEEN '" + startTime + @"'
                                AND '" + endTime + @"'
                                AND t.IsDelete = 0
                                AND t.MaintainType = '" + id + @"'");
                        DataTable sumdt = new RepositoryFactory().BaseRepository().FindTable(sumSql.ToString());
                        int sum = sumdt.Rows[0]["cou"].ToInt();
                        if (sum > 0)
                        {
                            double a = aconsume + bconsume + cconsume;
                            double coun = Math.Round((float)(a * 3600 / sum), 2);
                            model.average = coun.ToString();

                            list.Add(model);
                        }
                    }
                }
            }

            if (list.Count> 0)
            {
                int sum3 = list.Sum(x => x.average.ToInt());

                int max = list[0].average.ToInt();
                for (int i = 1; i < list.Count; i++)
                {
                    if (list[i].average.ToInt() > max)
                    {
                        max = list[i].average.ToInt();
                    }
                }
                List<MaintenanceTypeTimeToAModel> maxList = list.Where(x => x.average == max.ToString()).ToList();
                if (maxList.Count > 0)
                {
                    maxList[0].num = "1";
                }
            }
            return list;
        }
        #endregion
    }
}
