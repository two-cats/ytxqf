﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-05-06 15:11
 * 描  述：任务单
 */
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                learun.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/TaskManager/TaskBillInfo/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/TaskManager/TaskBillInfo/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/TaskManager/TaskBillInfo/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/TaskManager/TaskBillInfo/GetPageList',
                headData: [
                    { label: "Id", name: "Id", width: 160, align: "left"},
                    { label: "任务标题", name: "Subject", width: 160, align: "left"},
                    { label: "任务分类", name: "Category", width: 160, align: "left"},
                    { label: "报修位置", name: "PlaceId", width: 160, align: "left"},
                    { label: "位置补充", name: "PlaceExtend", width: 160, align: "left"},
                    { label: "任务级别", name: "Level", width: 160, align: "left"},
                    { label: "任务描述", name: "Summary", width: 160, align: "left"},
                    { label: "任务状态、发起、确认、派发、完成、评论、回访", name: "Status", width: 160, align: "left"},
                    { label: "发起时间", name: "SubmitTime", width: 160, align: "left"},
                    { label: "确认时间", name: "ConfirmTime", width: 160, align: "left"},
                    { label: "派发时间", name: "ForwardTime", width: 160, align: "left"},
                    { label: "完成时间", name: "FinishTime", width: 160, align: "left"},
                    { label: "评论时间", name: "CommentTime", width: 160, align: "left"},
                    { label: "回访时间", name: "CallbackTime", width: 160, align: "left"},
                    { label: "工单物理状态，有效，无效，删除。", name: "PhysicalStatus", width: 160, align: "left"},
                    { label: "显示状态，进行中、已完成。", name: "DisplayStatus", width: 160, align: "left"},
                    { label: "工单文件地址", name: "FilePath", width: 160, align: "left"},
                    { label: "文件类型", name: "FileType", width: 160, align: "left"},
                    { label: "物料Id", name: "MaterialId", width: 160, align: "left"},
                    { label: "物料数量", name: "Count", width: 160, align: "left"},
                    { label: "备注", name: "Remark", width: 160, align: "left"},
                    { label: "指派人员", name: "UserId", width: 160, align: "left"},
                    { label: "是否是工单负责人", name: "IsMaster", width: 160, align: "left"},
                    { label: "任务角色", name: "TaskRole", width: 160, align: "left"},
                ],
                mainId:'Id',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
