﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes
{
   public class AutocmpModel
    {
        /// <summary>
        /// 提示内容
        /// </summary>
        public string hex { get; set; }
        /// <summary>
        /// 选择内容
        /// </summary>
        public string label { get; set; }
    }
}
