﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    /// <summary>
    /// 指派人员Model
    /// </summary>
    public class OrderDesignateModel
    {

        public string order_id { get; set; }

        public string user_id { get; set; }

        public List<string> userList { get; set; }

        public string all { get; set; }

        public string accomplish { get; set; }

        public int category { get; set; }
    }
}