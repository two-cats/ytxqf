﻿using TTon.TMC.Util;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Instructor.Model;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 16:01
    /// 描 述：配置指导书
    /// </summary>
    public interface YT_InstructorIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据(web)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_InstructorEntity> GetPageList(Pagination pagination, string queryJson);

        /// <summary>
        /// 获取页面显示列表数据(api)
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_InstructorEntity> GetPageListbyAPI(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取YT_Instructor表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        YT_InstructorEntity GetYT_InstructorEntity(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, YT_InstructorEntity entity);
        InstructorDetailModel GetInstructorDetail(string keyValue);
        List<InstructorListModel> GetInstructorList(string supplierId, string partId, string MaintenanceId);


        /// <summary>
        /// 新增指导书、维修类型、订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        /// <param name="orderEntity"></param>
        void SaveSupplierOrOrder(YT_InstructorEntity instructorEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity, YT_OrderEntity orderEntity);

        /// <summary>
        /// 新增指导书、维修类型、修改订单
        /// </summary>
        /// <param name="instructorEntity"></param>
        /// <param name="maintenanceTypesEntity"></param>
        /// <param name="orderEntity"></param>
        void UpdateSupplierOrOrder(YT_InstructorEntity instructorEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity, YT_OrderEntity orderEntity);

        /// <summary>
        /// 获取WX 用户权限
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IdentityModel getIdentity(string userId);
        void ImportOrder(YT_InstructorEntity instructorEntity, YT_SupplierEntity supplierEntity, YT_OrderUserEntity orderUserEntity, YT_PartsTypesEntity partsTypesEntity, YT_OrderEntity orderEntity, YT_MaintenanceTypesEntity maintenanceTypesEntity);
        #endregion

    }
}
