﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class HomeStatisticsModel
    {
        public int UnfinishedOrder { get; set; }

        public int UnfinishedTask { get; set; }

        public int TodayOrder { get; set; }

        public int TodayTask { get; set; }

        public int PartTypes { get; set; }
    }
}
