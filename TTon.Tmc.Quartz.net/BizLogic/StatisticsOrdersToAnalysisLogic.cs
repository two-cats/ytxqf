﻿using ESteward.QuartzService.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.Tmc.Util;
using TTon.Tmc.Util.Extension;
using TTon.TMC.Quartz.net.Model;

namespace ESteward.QuartzService.BizLogic
{
    public class StatisticsOrdersToAnalysisLogic
    {
        public void ordersToAnalysis()
        {
            #region 岗位筛选
            List<string> OUserId = new List<string>();
            List<string> SUserId = new List<string>();
            List<string> QUserId = new List<string>();

            #region 本部
            string Oobjects1 = Config.GetValue("OrderAndTaskManage");
            string Oobjects2 = Config.GetValue("OrderManage");
            string Oobjects3 = Config.GetValue("SquadLeader");
            StringBuilder OUserRelationSql = new StringBuilder();
            OUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Oobjects1 + "','" + Oobjects2 + "','" + Oobjects3 + "')  ");
            List<Base_UserRelation> OrelationEntity = DB_Pla.Context.FromSql(OUserRelationSql.ToString()).ToList<Base_UserRelation>();
            if (OrelationEntity.Count != 0)
            {
                for (int i = 0; i < OrelationEntity.Count; i++)
                {
                    Base_User userEntity = DB_Pla.Context.From<Base_User>().Where(d => d.F_UserId == OrelationEntity[i].F_UserId).ToFirst();
                    OUserId.Add(userEntity.F_UserId);
                }
            }
            string ouserid = "";
            foreach (var ostruid in OUserId)
            {
                ouserid += "'" + ostruid + "'" + ",";
            }
            if (ouserid != "")
            {
                ouserid = ouserid.Substring(0, ouserid.Length - 1);
            }
            #endregion

            #region 供应商
            string Sobjects = Config.GetValue("Supplier");
            StringBuilder SUserRelationSql = new StringBuilder();
            SUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + Sobjects + "'  ");
            List<Base_UserRelation> SrelationEntity = DB_Pla.Context.FromSql(SUserRelationSql.ToString()).ToList<Base_UserRelation>();
            if (SrelationEntity.Count != 0)
            {
                for (int i = 0; i < SrelationEntity.Count; i++)
                {
                    Base_User userEntity = DB_Pla.Context.From<Base_User>().Where(d => d.F_UserId == SrelationEntity[i].F_UserId).ToFirst();
                    OUserId.Add(userEntity.F_UserId);
                }
            }
            string suserid = "";
            foreach (var sstruid in SUserId)
            {
                suserid += "'" + sstruid + "'" + ",";
            }
            if (suserid != "")
            {
                suserid = suserid.Substring(0, suserid.Length - 1);
            }
            #endregion

            #region SQE
            string Qobjects1 = Config.GetValue("Quality");
            string Qobjects2 = Config.GetValue("QualityManager");
            StringBuilder QUserRelationSql = new StringBuilder();
            QUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Qobjects1 + "','" + Qobjects2 + "')  ");
            List<Base_UserRelation> QrelationEntity = DB_Pla.Context.FromSql(QUserRelationSql.ToString()).ToList<Base_UserRelation>();
            if (QrelationEntity.Count != 0)
            {
                for (int i = 0; i < QrelationEntity.Count; i++)
                {
                    Base_User userEntity = DB_Pla.Context.From<Base_User>().Where(d => d.F_UserId == QrelationEntity[i].F_UserId).ToFirst();
                    OUserId.Add(userEntity.F_UserId);
                }
            }

            string quserid = "";
            foreach (var qstruid in QUserId)
            {
                quserid += "'" + qstruid + "'" + ",";
            }
            if (quserid != "")
            {
                quserid = quserid.Substring(0, quserid.Length - 1);
            }
            #endregion
            #endregion

            #region 供应商筛选
            DateTime now = DateTime.Now;
            StringBuilder supplier = new StringBuilder();
            supplier.Append(@"SELECT
	                    *
                    FROM
	                    YT_Supplier
                    WHERE
	                    IsDelete = 0");
            List<YT_Supplier> supplierList = DB_Pla.Context.FromSql(supplier.ToString()).ToList<YT_Supplier>();
            #endregion

            if (supplierList.Count > 0)
            {
                //清空表数据
                StringBuilder sqlsss = new StringBuilder();
                sqlsss.Append(@"SELECT
	                        *
                        FROM
	                        YT_OrdersAnalysis");
                List<YT_OrdersAnalysis> sqlList = DB_Pla.Context.FromSql(sqlsss.ToString()).ToList<YT_OrdersAnalysis>();
                foreach (var itemId in sqlList)
                {
                    DB_Pla.Context.Delete<YT_OrdersAnalysis>(t => t.Id == itemId.Id);
                }

                StringBuilder sqlTime = new StringBuilder();
                sqlTime.Append(@"SELECT
	                    TOP 1 CreateDate
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    ORDER BY
	                    CreateDate ASC");
                DataTable stdt = DB_Pla.Context.FromSql(sqlTime.ToString()).ToDataTable();
                DateTime sTime = Convert.ToDateTime(stdt.Rows[0]["CreateDate"]);
                int ms = now.Month - sTime.Month;
                for (int i = 0; i < ms + 1; i++)
                {
                    string st = now.AddMonths(-i).ToString("yyyy-MM-01 00:00:00");
                    string et = Convert.ToDateTime(st).AddMonths(1).AddDays(-1).ToString();
                    foreach (var item in supplierList)
                    {
                        int OCount = 0;
                        if (ouserid.Trim() != "")
                        {
                            StringBuilder Osql = new StringBuilder();
                            Osql.Append(@"SELECT
	                            COUNT (Id) AS cou
                            FROM
	                            YT_Order
                            WHERE
	                            IsDelete = 0
                            AND Order_Status != 3
                            AND Supplier_Id = '" + item.Id + @"'
                            AND CreateDate BETWEEN '" + st + @"'
                            AND '" + et + @"'
                            AND CreateUserId IN (" + ouserid.Trim() + ")");
                            DataTable Odt = DB_Pla.Context.FromSql(Osql.ToString()).ToDataTable();
                            OCount = Odt.Rows[0]["cou"].ToInt();
                        }

                        int QCount = 0;
                        if (quserid.Trim() != "")
                        {
                            StringBuilder Qsql = new StringBuilder();
                            Qsql.Append(@"SELECT
	                            COUNT (Id) AS cou
                            FROM
	                            YT_Order
                            WHERE
	                            IsDelete = 0
                            AND Order_Status != 3
                            AND Supplier_Id = '" + item.Id + @"'
                            AND CreateDate BETWEEN '" + st + @"'
                            AND '" + et + @"'
                            AND CreateUserId IN (" + quserid.Trim() + ")");
                            DataTable Qdt = DB_Pla.Context.FromSql(Qsql.ToString()).ToDataTable();
                            QCount = Qdt.Rows[0]["cou"].ToInt();
                        }

                        int SCount = 0;
                        if (suserid.Trim() != "")
                        {
                            StringBuilder Ssql = new StringBuilder();
                            Ssql.Append(@"SELECT
	                            COUNT (Id) AS cou
                            FROM
	                            YT_Order
                            WHERE
	                            IsDelete = 0
                            AND Order_Status != 3
                            AND Supplier_Id = '" + item.Id + @"'
                            AND CreateDate BETWEEN '" + st + @"'
                            AND '" + et + @"'
                            AND CreateUserId IN (" + suserid.Trim() + ")");
                            DataTable Sdt = DB_Pla.Context.FromSql(Ssql.ToString()).ToDataTable();
                            SCount = Sdt.Rows[0]["cou"].ToInt();
                        }


                        StringBuilder strsql = new StringBuilder();
                        strsql.Append(@"SELECT
	                            CASE
                            WHEN SUM (YT_OrderUser.Success) IS NULL THEN
	                            0
                            ELSE
	                            SUM (YT_OrderUser.Success)
                            END AS success,
                             CASE
                            WHEN SUM (YT_OrderUser.Error) IS NULL THEN
	                            0
                            ELSE
	                            SUM (YT_OrderUser.Error)
                            END AS error
                            FROM
	                            YT_Order
                            LEFT JOIN YT_OrderUser ON YT_OrderUser.Order_Id = YT_Order.Id
                            WHERE
	                            YT_Order.IsDelete = 0
                            AND YT_Order.Supplier_Id = '" + item.Id + @"'
                            AND YT_Order.CreateDate BETWEEN '" + st + "' AND '" + et + "'");
                        DataTable strdt = DB_Pla.Context.FromSql(strsql.ToString()).ToDataTable();
                        int Success = strdt.Rows[0]["success"].ToInt();
                        int Error = strdt.Rows[0]["error"].ToInt();

                        YT_OrdersAnalysis ordersAnalysis = new YT_OrdersAnalysis
                        {
                            Id = Guid.NewGuid().ToString(),
                            Supplier = item.Id,
                            Time = Convert.ToDateTime(st).ToString("yyyy-MM"),
                            OCount = OCount,
                            QCount = QCount,
                            SCount = SCount,
                            Success = Success,
                            Error = Error,
                            CreateTime = DateTime.Now
                        };
                        DB_Pla.Context.Insert<YT_OrdersAnalysis>(ordersAnalysis);
                    }
                }
            }
        }
    }
}
