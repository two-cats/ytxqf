﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class MaintainStatisticsModel
    {
        /// <summary>
        /// 维修数量
        /// </summary>
        public string maintainCount { get; set; }
        /// <summary>
        /// 合格率
        /// </summary>
        public string percentOfPass { get; set; }
        /// <summary>
        /// 维修合格率统计
        /// </summary>
        public MaintainEchartModel MaintainEchart { get; set; }
    }

    public class MaintainEchartModel
    {
        /// <summary>
        /// 合格数量
        /// </summary>
        public string qualified { get; set; }
        /// <summary>
        /// 不合格数量
        /// </summary>
        public string unqualified { get; set; }
    }
}
