﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class MeetingQueryJson
    {
        public string userid { get; set; }
        /// <summary>
        /// 查询内容
        /// </summary>
        public string querystr { get; set; }

        public Pagination pagination { get; set; }
    }
}