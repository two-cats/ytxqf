﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-08-21 16:01
 * 描  述：配置指导书
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, learun) {
    "use strict";
    var selectedRow = learun.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#IssuePic').lrUploader();
            //$('#MaintenanceType').lrYT_MaintenanceTypesSelect();
            $('#Supplier_Id').lrYT_SupplierSelect().on('change', function () {
                var value = $('#Supplier_Id').lrselectGet();
                        $.ajax({
                            url: "/LR_CodeDemo/YT_PartsTypes/GetListTree?parentId=" + value,
                            type: "GET",
                            dataType: "JSON",
                            success: function (result) {

                                var html = "<option value=''>==请选择==</option>";
                                for (var i = 0; i < result.data.length; i++) {
                                    html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Parts_Name + "</opeion>";
                                }
                                $("#Parts_Id").html(html);
                            }
                        })                                
            });
            $('#MaintenanceType').lrYT_MaintenanceTypesSelect();
        },
        initData: function () {
            if (!!keyValue) {
                $.lrSetForm(top.$.rootUrl + '/LR_CodeDemo/YT_Instructor/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                        }
                        else {
                            $('[data-table="' + id + '"]').lrSetFormData(data[id]);
                            for (var i in data[id]) {
                                if (i =="Parts_Id") {
                                    if (data[id][i] != "" && data[id][i] != null) {
                                        $.ajax({
                                            url: "/LR_CodeDemo/YT_PartsTypes/GetEntity?keyValue=" + data[id][i],
                                            type: "GET",
                                            dataType: "JSON",
                                            success: function (result) {
                                                var html = "<option value=''>==请选择==</option>";
                                                html += "<option value='" + result.data.Id + "' selected>" + result.data.Parts_Name + "</opeion>";
                                                $("#Parts_Id").html(html);
                                            }
                                        })            
                                    }
                                }

                            }
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').lrValidform()) {
            return false;
        }

        var postData = {
            strEntity: JSON.stringify($('body').lrGetFormData())
        };
        $.lrSaveForm(top.$.rootUrl + '/LR_CodeDemo/YT_Instructor/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
