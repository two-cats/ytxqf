﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class ApprovalModel
    {
        /// <summary>
        /// 内容ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 审批人ID
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// 0：同意，1：拒绝
        /// </summary>
        public string Approval { get; set; }
        /// <summary>
        /// 审批意见
        /// </summary>
        public string Opinion { get; set; }


    }
}