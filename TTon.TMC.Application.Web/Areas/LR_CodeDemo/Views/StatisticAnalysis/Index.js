﻿$(function () {
    var vs = "";
    $.ajax({
        url: "/LR_CodeDemo/YT_Supplier/GetSupplierTree",
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Name + "</opeion>";
            }
            $("#Supplier").html(html);
        }
    })
})

var dname = "";
function btn_Energy() {
    var vs = $('#Supplier  option:selected').val();
    var start = $("#energyStart").val();
    var end = $("#energyEnd").val();
    if (start != "" && end == "") {
        return top.learun.alert.warning('请选择结束时间！');
    }
    if (start == "" && end != "") {
        return top.learun.alert.warning('请选择开始时间！');
    }
    if (start == "" && end == "") {
        return top.learun.alert.warning('请选择时间段！');
    }
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }
    var html = "";
    var name = $('#Supplier  option:selected').text() + "供应商";
    dname = name;
    var timeQuantum = $("#energyStart").val() + " ~ " + $("#energyEnd").val();
    var oCount = 0;
    var sCount = 0;
    var qCount = 0;
    var orderCount = 0;
    html += "<div id='title' style='text-align:center;width: 1100px;' class='col-xs-12'><font size='4'>阶段质量报告</font></div>";
    html += "<div id='title' style='text-align:center;width: 1100px;' class='col-xs-12'><font size='4'>- 呈送：" + name + "</font></div>";
    html += "<div style='margin-top:10px;margin-left: 60%;' class='col-xs-12'><font size='3'>" + timeQuantum + "</font></div>";
    html += "<div style='margin-top:10px;margin-left:10%;'><font size='3'>一、挑选/返工情况</font></div>";
    html += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>" + timeQuantum + " 数据统计。</font></div>";
    var html2 = "<TABLE  style='text-align:left; border: 1px solid #a0a0a0;width:50%;margin:auto' cellspacing='0'>";
    html2 += "<tr><td style='border: 1px solid #a0a0a0;'>时间&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>零件种类&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>零件总数量&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>合格率&nbsp;&nbsp;</td></tr>";
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetNewPartsTypesToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                html2 += "<tr><td style='border: 1px solid #a0a0a0;'>" + data[i].time + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].name + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].count + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].percent + "</td></tr>";
            }
        }
    })
    html2 += "</TABLE>";
    var html3 = "<TABLE  style='text-align:left; border: 1px solid #a0a0a0;width:50%;margin:auto' cellspacing='0'>";
    html3 += "<tr><td style='border: 1px solid #a0a0a0;'>类别&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>百分比&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>数量&nbsp;&nbsp;</td></tr>";
    var mCou = "";
    var perc = "";
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPercentToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var mCount = data.success + data.error;
            mCou = mCount;
            var ccc = data.success / mCount;
            var ddd = data.error / mCount;
            var aaa = ccc.toFixed(3);
            var eee = ddd.toFixed(3);
            var percentage = "";
            var percentage2 = "";
            if (Number(aaa * 100).toFixed(2) != "NaN") {
                percentage = Number(aaa * 100).toFixed(2) + "%";
            }
            else {
                percentage = 0.00 + "%";
            }
            perc = percentage;
            if (Number(eee * 100).toFixed(2) != "NaN") {
                percentage2 = Number(eee * 100).toFixed(2) + "%";
            }
            else {
                percentage2 = 0.00 + "%";
            }
            var PercentOfPass = echarts.init(document.getElementById('PercentOfPass'));
            var k = [];
            var j = {};
            j.value = data.success;
            j.name = '合格' + percentage + '(' + data.success + '个)';
            k.push(j);
            var p = {};
            p.value = data.error;
            p.name = '不合格' + percentage2 + '(' + data.error + '个)';
            k.push(p);
            html3 += "<tr><td style='border: 1px solid #a0a0a0;'>合格</td><td style='border: 1px solid #a0a0a0;'>" + percentage + "</td><td style='border: 1px solid #a0a0a0;'>" + data.success + "</td></tr>";
            html3 += "<tr><td style='border: 1px solid #a0a0a0;'>不合格</td><td style='border: 1px solid #a0a0a0;'>" + percentage2 + "</td><td style='border: 1px solid #a0a0a0;'>" + data.error + "</td></tr>";
            option = {
                //title: {
                //    text: '挑选/返工情况百分比统计图表',
                //    x: 'center'
                //},
                color: ['#91c7ae', '#ca8622'],
                series: [
                    {
                        name: '占总数百分比',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: k,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            PercentOfPass.setOption(option);
        }
    })
    html3 += "</TABLE>";
    html3 = "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>该时间段共挑选零件" + mCou + "件，合格率为" + perc + "</font></div>";
    var html4 = "<TABLE  style='text-align:left; border: 1px solid #a0a0a0;width:50%;margin:auto' cellspacing='0'>";
    html4 += "<tr><td style='border: 1px solid #a0a0a0;'>零件编号&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>零件名称&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>合格数量&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>不合格数量&nbsp;&nbsp;</td></tr>";
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsTypesEcharsToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var PartsTypesEchars = echarts.init(document.getElementById('Supplier_PartsType'));
            var name = [];
            var qualified = [];
            var unqualified = [];
            for (var i = 0; i < data.length; i++) {
                //name.push(data[i].number + '-' + data[i].name);
                name.push(data[i].number);
                qualified.push(data[i].success);
                unqualified.push(data[i].error);
                html4 += "<tr><td style='border: 1px solid #a0a0a0;'>" + data[i].number + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].name + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].success + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].error + "</td></tr>";
            }
            option = {
                tooltip: {
                    trigger: 'axis'
                },
                grid: {
                    top: '5%',
                    bottom: '40%'
                },
                xAxis: [
                    {
                        //type: 'category',
                        //data: name,
                        //axisPointer: {
                        //    type: 'shadow'
                        //}
                        data: name,
                        axisLabel: {
                            interval: 0,
                            formatter: function (value) {
                                return value.split("").join("\n");
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '合格数量',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#FFB90F'
                            }
                        },
                        //柱状图上方显示数字
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                textStyle: {
                                    fontsize: '1rem'
                                }
                            }
                        },
                        data: qualified
                    },
                    {
                        name: '不合格数量',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#FF3030'
                            }
                        },
                        //柱状图上方显示数字
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                textStyle: {
                                    fontsize: '1rem'
                                }
                            }
                        },
                        data: unqualified
                    }
                ]
            };
            PartsTypesEchars.setOption(option);
        }
    })
    html4 += "</TABLE>";
    html4 += "<div style='margin-top:10px;margin-left:10%;'><font size='3'>二、挑选/返工原因</font></div>";
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetMaintenanceTypesToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var MaintenanceTypesModel = echarts.init(document.getElementById('MaintenanceTypesModel'));
            var name = [];
            var value = [];
            for (var i = 0; i < data.list.length; i++) {
                name.push(data.list[i].name);
                var j = {};
                j.value = data.list[i].value;
                j.name = data.list[i].name;
                value.push(j);
            }
            html4 += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>" + timeQuantum + "挑选/返工零件原因“" + data.maxname + "”原因占比最大，达" + data.percent + "左右。 </font></div>";
            option = {
                //title: {
                //    text: '挑选/返工原因百分比统计图表',
                //    x: 'center'
                //},
                series: [
                    {
                        name: '本月零件挑选原因占比',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: value,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            MaintenanceTypesModel.setOption(option);
        }
    })
    var html5 = "<div style='margin-top:10px;margin-left:10%;'><font size='3'>三、挑选/返工耗时</font></div>";
    $.ajax({
        url: "/LR_CodeDemo/YT_MaintenanceTypes/GetMaintenanceTypesTimeToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false, //同步执行
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var name = [];
            var average = [];
            for (var i = 0; i < data.data.length; i++) {
                name.push(data.data[i].maintenanceType);
                average.push(data.data[i].average);
                if (data.data[i].num == "1") {
                    html5 += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>" + timeQuantum + "挑选/返工零件“" + data.data[i].maintenanceType + "”原因单体耗时最长，达" + data.data[i].average + "秒/个。 </font></div>";
                }
            }
            var EnergyChat = echarts.init(document.getElementById('EnergyECharts'));
            EnergyOption = {
                tooltip: {
                    trigger: 'axis',
                },
                grid: {
                    top: '5%',
                    bottom: '40%'
                },
                xAxis: [
                    {
                        data: name,
                        axisLabel: {
                            formatter: function (value) {
                                return value.split("").join("\n");
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '',
                        min: 0,
                        axisLabel: {
                            formatter: '{value}' + '秒'
                        }
                    }
                ],
                series: [{
                    data: average,
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            label: {
                                show: true, //开启显示
                                position: 'top', //在上方显示
                                itemStyle: {
                                    normal: {
                                        color: '#d48265'
                                    }
                                },
                            }
                        }
                    }
                }]
            };
            EnergyChat.setOption(EnergyOption);
        }
    })
    var html6 = "<div style='margin-top:10px;margin-left:10%;'><font size='3'>四、通过亚泰新“工业服务”软件提交订单数量统计</font></div>";
    $.ajax({
        url: "/LR_CodeDemo/YT_Order/OrderEchartsToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (data) {
            //本部
            oCount = data.data.OCount;
            //供应商
            sCount = data.data.SCount;
            //SQE
            qCount = data.data.QCount;
         
            var Meeting_Status_Chat = echarts.init(document.getElementById('meeting_status'));
            Meeting_StatusOption = {
                tooltip: {
                    trigger: 'axis',
                    formatter: function (params) {
                        var relVal = params[0].name;
                        for (var i = 0, l = params.length; i < l; i++) {
                            relVal += '<br/>' + params[i].value + "单";
                        }
                        return relVal;
                    }
                },
                xAxis: {
                    type: 'category',
                    data: ['亚泰新', '供应商', 'SQE']
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    type: 'bar',
                    data: [oCount, sCount, qCount],
                    itemStyle: {
                        normal: {
                            label: {
                                show: true, //开启显示
                                position: 'top', //在上方显示
                                itemStyle: {
                                    normal: {
                                        color: '#d48265'
                                    }
                                },
                            }
                        }
                    }
                }]
            };
            Meeting_Status_Chat.setOption(Meeting_StatusOption);
        }
    })
    $.ajax({
        url: "/LR_CodeDemo/YT_Order/GetOrderEchartsToA?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (data) {
            var time = [];
            var oCount = [];
            var qCount = [];
            var sCount = [];
            for (var i = 0; i < data.data.length; i++) {
                time.push(data.data[i].time);
                oCount.push(data.data[i].oCount);
                qCount.push(data.data[i].qCount);
                sCount.push(data.data[i].sCount);
            }
            var Meeting_Status_Mounts_Chat = echarts.init(document.getElementById('meeting_status_mounts'));
            Meeting_Status_MountsOption = {
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    orient: 'horizontal',
                    x: 'center',
                    y: 'top',
                    data: ['亚泰新', '供应商', 'SQE']
                },
                grid: {
                    bottom: '40%'
                },
                xAxis: [
                    {
                        data: time,
                        //axisLabel: {
                        //    formatter: function (value) {
                        //        return value.split("").join("\n");
                        //    }
                        //}
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '亚泰新',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true, //开启显示
                                    position: 'top', //在上方显示
                                }
                            }
                        },
                        data: oCount
                    },
                    {
                        name: '供应商',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true, //开启显示
                                    position: 'top', //在上方显示
                                }
                            }
                        },
                        data: sCount
                    },
                    {
                        name: 'SQE',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true, //开启显示
                                    position: 'top', //在上方显示
                                }
                            }
                        },
                        data: qCount
                    }
                ]
            };
            Meeting_Status_Mounts_Chat.setOption(Meeting_Status_MountsOption);
        }
    })

    $("#DIV").html(html);
    $("#DIV2").html(html2);
    $("#DIV3").html(html3);
    $("#DIV4").html(html4);
    $("#DIV5").html(html5);
    $("#DIV6").html(html6);
}

//$.ajax({
//    url: "/LR_CodeDemo/YT_Order/GetOrderEchartsToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
//    type: "GET",
//    async: false,
//    dataType: "JSON",
//    success: function (data) {
//        //本部
//        oCount = data.data.OCount;
//        //供应商
//        sCount = data.data.SCount;
//        //SQE
//        qCount = data.data.QCount;
//        //总数
//        orderCount = oCount + sCount + qCount;
//        html += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>本月共产生" + orderCount + "条订单，其中SQE提交" + qCount + "单，供应商提交" + sCount + "单，亚泰新提交" + oCount + "单。 </font></div>";
//        var Meeting_Status_Chat = echarts.init(document.getElementById('meeting_status'));
//        Meeting_StatusOption = {
//            tooltip: {
//                trigger: 'axis',
//                formatter: function (params) {
//                    var relVal = params[0].name;
//                    for (var i = 0, l = params.length; i < l; i++) {
//                        relVal += '<br/>' + params[i].value + "单";
//                    }
//                    return relVal;
//                }
//            },
//            xAxis: {
//                type: 'category',
//                data: ['亚泰新', '供应商', 'SQE']
//            },
//            yAxis: {
//                type: 'value'
//            },
//            series: [{
//                type: 'bar',
//                data: [oCount, sCount, qCount],
//                itemStyle: {
//                    normal: {
//                        label: {
//                            show: true, //开启显示
//                            position: 'top', //在上方显示
//                            textStyle: { //数值样式
//                                color: 'black',
//                                fontSize: 16
//                            }
//                        }
//                    }
//                }
//            }]
//        };
//        Meeting_Status_Chat.setOption(Meeting_StatusOption);
//    }
//})
//var html2 = "";
//html2 += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>二、挑选/返工情况 </font></div>";
//$.ajax({
//    url: "/LR_CodeDemo/YT_PartsTypes/GetPercentToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
//    async: false,
//    type: "GET",
//    dataType: "JSON",
//    success: function (data) {
//        var mCount = data.success + data.error;
//        var ccc = data.success / mCount;
//        var ddd = data.error / mCount;
//        var aaa = ccc.toFixed(3);
//        var eee = ddd.toFixed(3);
//        var percentage = aaa * 100 + "%";
//        var percentage2 = eee * 100 + "%";
//        html2 += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>累计维修" + mCount + "个零件，合格率为" + percentage +"。 </font></div>";
//        var PercentOfPass = echarts.init(document.getElementById('PercentOfPass'));
//        var k = [];
//        var j = {};
//        j.value = data.success;
//        j.name = '合格' + percentage + '(' + data.success +'个)';
//        k.push(j);
//        var p = {};
//        p.value = data.error;
//        p.name = '不合格' + percentage2 + '(' + data.error + '个)';
//        k.push(p);

//        option = {
//            //tooltip: {
//            //    trigger: 'item',
//            //    formatter: "{a} <br/>{b} : {c} ({d}%)"
//            //},
//            color: ['#91c7ae', '#ca8622'],
//            series: [
//                {
//                    name: '占总数百分比',
//                    type: 'pie',
//                    radius: '55%',
//                    center: ['50%', '60%'],
//                    data: k,
//                    itemStyle: {
//                        emphasis: {
//                            shadowBlur: 10,
//                            shadowOffsetX: 0,
//                            shadowColor: 'rgba(0, 0, 0, 0.5)'
//                        }
//                    }
//                }
//            ]
//        };
//        PercentOfPass.setOption(option);
//    }
//})
//var html3 = "<TABLE  style='text-align:left; border: 1px solid #a0a0a0;width:50%;margin:auto' cellspacing='0'>";
//html3 += "<tr><td style='border: 1px solid #a0a0a0;'>&nbsp;&nbsp;零件名称&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>&nbsp;&nbsp;合格数量&nbsp;&nbsp;</td><td style='border: 1px solid #a0a0a0;'>&nbsp;&nbsp;不合格数量&nbsp;&nbsp;</td></tr>";
//$.ajax({
//    url: "/LR_CodeDemo/YT_PartsTypes/GetPartsTypesEcharsToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
//    async: false,
//    type: "GET",
//    dataType: "JSON",
//    success: function (data) {
//        var PartsTypesEchars = echarts.init(document.getElementById('Supplier_PartsType'));
//        var name = [];
//        var qualified = [];
//        var unqualified = [];
//        for (var i = 0; i < data.length; i++) {
//            name.push(data[i].name);
//            qualified.push(data[i].success);
//            unqualified.push(data[i].error);
//            html3 += "<tr><td style='border: 1px solid #a0a0a0;'>" + data[i].name + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].success + "</td><td style='border: 1px solid #a0a0a0;'>" + data[i].error+"</td></tr>";
//        }
//        option = {
//            tooltip: {
//                trigger: 'axis'
//            },
//            xAxis: [
//                {
//                    type: 'category',
//                    data: name,
//                    axisPointer: {
//                        type: 'shadow'
//                    }
//                }
//            ],
//            yAxis: [
//                {
//                    type: 'value'
//                }
//            ],
//            series: [
//                {
//                    name: '合格数量',
//                    type: 'bar',
//                    itemStyle: {
//                        normal: {
//                            color: '#FFB90F'
//                        }
//                    },
//                    data: qualified
//                },
//                {
//                    name: '不合格数量',
//                    type: 'bar',
//                    itemStyle: {
//                        normal: {
//                            color: '#FF3030'
//                        }
//                    },
//                    data: unqualified
//                }
//            ]
//        };
//        PartsTypesEchars.setOption(option);
//    }
//})
//html3 += "</TABLE>";
//var html4 = "";
//html4 += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>三、挑选/返工原因 </font></div>";
//$.ajax({
//    url: "/LR_CodeDemo/YT_PartsTypes/GetMaintenanceTypesToAnalysis?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
//    async: false,
//    type: "GET",
//    dataType: "JSON",
//    success: function (data) {
//        var MaintenanceTypesModel = echarts.init(document.getElementById('MaintenanceTypesModel'));
//        var name = [];
//        var value = [];
//        for (var i = 0; i < data.list.length; i++) {
//            name.push(data.list[i].name);
//            var j = {};
//            j.value = data.list[i].value;
//            j.name = data.list[i].name;
//            value.push(j);
//        }
//        html4 += "<div style='margin-top:10px;margin-left:10%;' class='col-xs-12'><font size='3'>其中因" + data.maxname + "原因占比最多，达" + data.percent+"左右。 </font></div>";
//        option = {
//            //tooltip: {
//            //    trigger: 'item',
//            //    formatter: "{a} <br/>{b} : {c} ({d}%)"
//            //},
//            series: [
//                {
//                    name: '本月零件挑选原因占比',
//                    type: 'pie',
//                    radius: '55%',
//                    center: ['50%', '60%'],
//                    data: value,
//                    itemStyle: {
//                        emphasis: {
//                            shadowBlur: 10,
//                            shadowOffsetX: 0,
//                            shadowColor: 'rgba(0, 0, 0, 0.5)'
//                        }
//                    }
//                }
//            ]
//        };
//        MaintenanceTypesModel.setOption(option);
//    }
//})


function downloadresume() {
    html2canvas($("#pdfDom"), {
        onrendered: function (canvas) {
            var a = $('#lr_testDownload').attr('href', canvas.toDataURL()).attr('download', dname + '.png');
            a[0].click();
        }
    });
}