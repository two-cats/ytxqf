﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class SupplierOrOrderModel
    {
        public YT_InstructorEntity instructorEntity { get; set; }

        public YT_MaintenanceTypesEntity maintenanceTypesEntity { get; set; }

        public YT_OrderEntity orderEntity { get; set; }
    }
}