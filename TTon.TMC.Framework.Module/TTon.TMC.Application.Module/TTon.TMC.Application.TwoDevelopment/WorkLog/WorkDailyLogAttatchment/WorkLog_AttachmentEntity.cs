﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.WorkLog

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:30
    /// 描 述：工作日志附件
    /// </summary>
    public class WorkLog_AttachmentEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// LogId
        /// </summary>
        /// <returns></returns>
        [Column("LOGID")]
        public string LogId { get; set; }
        /// <summary>
        /// Url
        /// </summary>
        /// <returns></returns>
        [Column("URL")]
        public string Url { get; set; }
        /// <summary>
        /// AttachType
        /// </summary>
        /// <returns></returns>
        [Column("ATTACHTYPE")]
        public string AttachType { get; set; }
        /// <summary>
        /// UploadTime
        /// </summary>
        /// <returns></returns>
        [Column("UPLOADTIME")]
        public DateTime? UploadTime { get; set; }
        /// <summary>
        /// Flag
        /// </summary>
        /// <returns></returns>
        [Column("FLAG")]
        public int? Flag { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        /// <returns></returns>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

