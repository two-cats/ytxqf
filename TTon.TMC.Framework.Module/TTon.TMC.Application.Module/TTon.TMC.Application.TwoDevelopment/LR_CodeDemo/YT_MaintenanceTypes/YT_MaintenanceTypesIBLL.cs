﻿using TTon.TMC.Util;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 11:48
    /// 描 述：配置维修类型
    /// </summary>
    public interface YT_MaintenanceTypesIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_MaintenanceTypesEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取YT_MaintenanceTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        YT_MaintenanceTypesEntity GetYT_MaintenanceTypesEntity(string keyValue);

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_MaintenanceTypesEntity> GetList();
        List<SupplierListToTree> GetMaintenanceTypesTree(string keyValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <param name="Parts_Id"></param>
        /// <returns></returns>
        IEnumerable<YT_MaintenanceTypesEntity> GetListTree();



        IEnumerable<AutocmpModel> GetACmaintenance();
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, YT_MaintenanceTypesEntity entity);
        List<PartsTypesEchartModel> GetPartsTypesEcharts(string supplier);
        List<MaintenanceTypeTimeModel> GetMaintenanceTypesTime(string startTime, string endTime, string supplier, string part);
        List<MaintenanceTypeTimeToAModel> GetMaintenanceTypesTimeToAnalysis(string startTime, string endTime, string supplier);
        #endregion

    }
}
