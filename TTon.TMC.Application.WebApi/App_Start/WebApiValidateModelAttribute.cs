﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using TTon.TMC.Application.WebApi.Helper;
using TTon.TMC.Application.WebApi.Modules;

namespace TTon.TMC.Application.WebApi.App_Start
{
    public class WebApiValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.Method != HttpMethod.Post)
            {
                return;
            }

            if (!actionContext.ModelState.IsValid)
            {
                // 在响应体中返回验证错误
                var errors = new Dictionary<string, IEnumerable<string>>();
                foreach (KeyValuePair<string, ModelState> keyValue in actionContext.ModelState)
                {
                    errors[keyValue.Key] = keyValue.Value.Errors.Select(e => e.ErrorMessage);
                }

                HttpResponseMessage msg = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest);

                ResponseModel result = new ResponseModel();

                result.Code = (int)HttpStatusCode.BadRequest;
                result.Message = "failed";
                result.Data = errors;

                msg.Content = new StringContent(JsonConvert.SerializeObject(result, JsonHelper.settings));

                //actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);

                //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, result);

                actionContext.Response = msg;
            }
        }
    }
}