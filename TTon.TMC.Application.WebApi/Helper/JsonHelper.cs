﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi.Helper
{
    /// <summary>
    /// 序列化对象时，将序列化设置对象 settings 传入 SerializeObject(object value, JsonSerializerSettings settings) 方法
    /// </summary>
    public class JsonHelper
    {
        public static JsonSerializerSettings settings;

        static JsonHelper()
        {
            settings = new JsonSerializerSettings();
            JsonConvert.DefaultSettings = new Func<JsonSerializerSettings>(() =>
            {
                settings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
                settings.DateFormatString = "yyyy-MM-dd HH:mm:ss";

                settings.NullValueHandling = NullValueHandling.Ignore;

                settings.ContractResolver = new LowercaseContractResolver();
                return settings;
            });
        }
    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }
}