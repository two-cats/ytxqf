﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class LogReportResponseModel
    {
        public int finish { get; set; }

        public int unfinished { get; set; }

        public int qualified { get; set; }

        public int disqualified { get; set; }
    }
}
