﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-08-22 09:53
 * 描  述：订单报修
 */
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').lrdate({
                dfdata: [
                    { name: '今天', begin: function () { return learun.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return learun.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return learun.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return learun.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return learun.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return learun.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return learun.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return learun.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').lrMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_superaddition').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                $.ajax({
                    url: top.$.rootUrl + "/LR_CodeDemo/YT_Order/VerifyOrder?keyValue=" + keyValue,
                    type: "GET",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.info == "该订单已追加") {
                            alert("该订单已追加！");
                        }
                        if (data.info == "未追加") {
                            learun.layerForm({
                                id: 'form',
                                title: 'SQE判定追加',
                                url: top.$.rootUrl + '/LR_CodeDemo/YT_Order/SuperadditionForm?keyValue=' + keyValue,
                                width: 350,
                                height: 300,
                                callBack: function (id) {
                                    return top[id].acceptClick(refreshGirdData);
                                }
                            })
                        }
                    }
                })
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '详细',
                        url: top.$.rootUrl + '/LR_CodeDemo/YT_Order/Form?keyValue=' + keyValue,
                        width: 1000,
                        height: 800,
                        btn: null
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/LR_CodeDemo/YT_Order/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });

            // 导入
            $('#lr_import').on('click', function () {
                learun.layerForm({
                    id: 'form',
                    title: '上传文件',
                    url: top.$.rootUrl + '/LR_CodeDemo/YT_Order/ImportForm',
                    width: 600,
                    height: 400,
                    btn: null
                });

            });

            $('#lr_out').on('click', function () {
                var start = document.getElementById("start").value;
                var end = document.getElementById("end").value;
                var name = document.getElementById("Name").value;
                var supplier = document.getElementById("Supplier").value;
                var orderNumber = document.getElementById("OrderNumber").value;
                var partsNumber = document.getElementById("PartsNumber").value;
                $.ajax({
                    url: top.$.rootUrl + "/LR_CodeDemo/YT_Order/ExportOrderList?startTime=" + startTime + "&endTime=" + endTime + "&start=" + start + "&end=" + end + "&name=" + name + "&supplier=" + supplier + "&orderNumber=" + orderNumber + "&partsNumber=" + partsNumber,
                    type: "GET",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        window.open("http://ytxqf.web.ttonservice.com/Temporary/" + data.data.fileurl);
                    },
                    error: function (ex) {
                        alert("文件下载时出现错误");
                    }
                })
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').jfGrid({
                url: top.$.rootUrl + '/LR_CodeDemo/YT_Order/GetList',
                headData: [
                    { label: "姓名", name: "Name", width: 160, align: "left"},
                    { label: "手机号", name: "Phone", width: 160, align: "left"},
                    { label: "日期", name: "Date", width: 160, align: "left"},
                    { label: "订单编号", name: "OrderNumber", width: 160, align: "left"},
                    { label: "零件编号", name: "PartsNumber", width: 160, align: "left"},
                    { label: "供应商", name: "Supplier_Id", width: 160, align: "left"},
                    { label: "零件分类", name: "PartsType", width: 160, align: "left"},
                    { label: "维修类型", name: "MaintainType", width: 160, align: "left"},
                    { label: "指导书", name: "Instructor", width: 160, align: "left"},
                    {
                        label: "订单状态", name: "Order_Status", width: 160, align: "left",
                        formatter: function (cellvalue) {
                            if (cellvalue == "0") {
                                return "已提交";
                            } else if (cellvalue == "1") {
                                return "已预估";
                            } else if (cellvalue == "2") {
                                return "已提交";
                            } else if (cellvalue == "3") {
                                return "已拒绝";
                            } else if (cellvalue == "4") {
                                return "已指派";
                            } else if (cellvalue == "5") {
                                return "已完成";
                            } else if (cellvalue == "6") {
                                return "待分配";
                            } else if (cellvalue == "7") {
                                return "待确认";
                            }else if (cellvalue == "8") {
                                return "待下单";
                            } else {
                                return "";
                            }
                        }
                    },
                    { label: "区域挑选消耗工时", name: "EstimateDate", width: 160, align: "left" },
                    { label: "跟线挑选消耗工时", name: "ThreadElapsedTime", width: 160, align: "left" },
                    { label: "线边挑选消耗工时", name: "LineEdgeElapsedTime", width: 160, align: "left" },
                    { label: "总消耗工时", name: "ElapsedTime", width: 160, align: "left" },
                    { label: "合格数量", name: "Success", width: 160, align: "left" },
                    { label: "不合格数量", name: "Error", width: 160, align: "left" },
                    { label: "尚未挑选数量", name: "Unfinished", width: 160, align: "left" },
                    { label: "下单时间", name: "CreateDate", width: 160, align: "left" },
                    { label: "开始时间", name: "StartDate", width: 160, align: "left" },
                    { label: "完成时间", name: "SuccessDate", width: 160, align: "left" },
                    { label: "备注", name: "Remark", width: 160, align: "left" },
                ],
                mainId:'Id',
                reloadSelected: true,
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
