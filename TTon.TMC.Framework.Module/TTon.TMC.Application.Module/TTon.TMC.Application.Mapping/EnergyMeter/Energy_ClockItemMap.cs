﻿using TTon.TMC.Application.TwoDevelopment.EnergyMeter;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-04-24 11:38
    /// 描 述：能源表参数
    /// </summary>
    public class Energy_ClockItemMap : EntityTypeConfiguration<Energy_ClockItemEntity>
    {
        public Energy_ClockItemMap()
        {
            #region 表、主键
            //表
            this.ToTable("ENERGY_CLOCKITEM");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

