﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    /// <summary>
    /// 订单审批Model
    /// </summary>
    public class ExamineModel
    {
        public string order_id { get; set; }

        public string user_id { get; set; }
        public int status { get; set; }
    }
}