﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_OrderSuperaddition
{
    public class YT_OrderSuperadditionEntity
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// OrderId
        /// </summary>
        /// <returns></returns>
        [Column("ORDERID")]
        public string OrderId { get; set; }
        /// <summary>
        /// QualifiedCount
        /// </summary>
        /// <returns></returns>
        [Column("QUALIFIEDCOUNT")]
        public string QualifiedCount { get; set; }
        /// <summary>
        /// UnqualifiedCount
        /// </summary>
        /// <returns></returns>
        [Column("UNQUALIFIEDCOUNT")]
        public string UnqualifiedCount { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        [Column("CREATEDATE")]
        public DateTime CreateDate { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreateDate = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}
