﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.BaseModule.OrganizationModule.User
{
    /// <summary>
    /// 我的任务Model
    /// </summary>
    public class UserTaskResponseModel
    {
        /// <summary>
        /// 类型  0 报修 1 会务
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string createdate { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string content { get; set; }
    }
}
