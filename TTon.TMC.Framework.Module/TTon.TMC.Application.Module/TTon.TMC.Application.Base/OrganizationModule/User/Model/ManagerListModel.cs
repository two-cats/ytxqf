﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.BaseModule.OrganizationModule.User.Model
{
    public class ManagerListModel
    {
        public string user_id { get; set; }

        public string user_name { get; set; }
    }
}
