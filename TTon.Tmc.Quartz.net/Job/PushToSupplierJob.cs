﻿using Common.Logging;
using ESteward.QuartzService.BizLogic;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESteward.QuartzService.Job
{
    public class PushToSupplierJob : IJob
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(PushToSupplierJob));

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("开始执行PushToSupplierJob@" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                PushToSupplierLogic pushToSupplierLogic = new PushToSupplierLogic();
                pushToSupplierLogic.pushToSupplierLogic();
                Console.WriteLine("成功执行PushToSupplierJob" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch (Exception ex)
            {
                //_logger.Error(ex);
                Console.WriteLine("ERR:" + ex.Message);
            }
        }
    }
}
