﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTon.TMC.Application.WebApi._2._0.App_Start;
using TTon.TMC.Application.WebApi._2._0.Common;
using TTon.TMC.Application.WebApi._2._0.Helper;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [WebApiExceptionFilterAttribute]
    public class BaseApiController : ApiController
    {
        protected HttpResponseMessage Render(HttpStatusCode code, string message, object data)
        {
            HttpResponseMessage msg = Request.CreateResponse();
            ResponseModel model = new ResponseModel()
            {
                Message = message,
                Code = ((int)code),
                Data = data
            };
            msg.Content = new StringContent(JsonConvert.SerializeObject(model, JsonHelper.settings));
            return msg;
        }

        protected HttpResponseMessage Render(HttpStatusCode code, string message)
        {
            return Render(code, message, new object());
        }

        protected HttpResponseMessage Render(bool status, string message)
        {
            return Render(String.Empty, message);
        }

        protected HttpResponseMessage Render(string message, object data)
        {
            return Render(HttpStatusCode.OK, message, data);
        }
    }
}
