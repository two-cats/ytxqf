﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-09-29 23:03
    /// 描 述：订单统计
    /// </summary>
    public class YT_StatisticsEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        /// <returns></returns>
        [Column("ORDERNUMBER")]
        public string OrderNumber { get; set; }
        /// <summary>
        /// 图纸版本号
        /// </summary>
        /// <returns></returns>
        [Column("DRAWINGNUMBER")]
        public string DrawingNumber { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        /// <returns></returns>
        [Column("SUPPLIER")]
        public string Supplier { get; set; }
        /// <summary>
        /// 零件号
        /// </summary>
        /// <returns></returns>
        [Column("PARTSNUMBER")]
        public string PartsNumber { get; set; }
        /// <summary>
        /// 零件名称
        /// </summary>
        /// <returns></returns>
        [Column("PARTSTYPE")]
        public string PartsType { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        [Column("BATCHNUMBER")]
        public string BatchNumber { get; set; }
        /// <summary>
        /// 问题描述
        /// </summary>
        /// <returns></returns>
        [Column("ISSUEDESCRIPTION")]
        public string IssueDescription { get; set; }
        /// <summary>
        /// 临时措施
        /// </summary>
        /// <returns></returns>
        [Column("REPAIRTYPE")]
        public string RepairType { get; set; }
        /// <summary>
        /// 零件总数
        /// </summary>
        /// <returns></returns>
        [Column("SUM")]
        public int? Sum { get; set; }
        /// <summary>
        /// 合格数
        /// </summary>
        /// <returns></returns>
        [Column("QUALIFIED")]
        public int? Qualified { get; set; }
        /// <summary>
        /// Disqualified
        /// </summary>
        /// <returns></returns>
        [Column("DISQUALIFIED")]
        public int? Disqualified { get; set; }
        /// <summary>
        /// 未完成数量
        /// </summary>
        /// <returns></returns>
        [Column("UNFINISHED")]
        public int? Unfinished { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        /// <returns></returns>
        [Column("CREATETIME")]
        public string CreateTime { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
        /// <returns></returns>
        [Column("SUCCESSDATE")]
        public string SuccessDate { get; set; }
        /// <summary>
        /// 关联主键
        /// </summary>
        /// <returns></returns>
        [Column("PARENTID")]
        public string ParentId { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

