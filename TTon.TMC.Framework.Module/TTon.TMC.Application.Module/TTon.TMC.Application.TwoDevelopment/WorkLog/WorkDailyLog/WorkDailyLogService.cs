﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using TTon.TMC.Application.Base.OrganizationModule;

namespace TTon.TMC.Application.TwoDevelopment.WorkLog
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:25
    /// 描 述：工作日志
    /// </summary>
    public class WorkDailyLogService : RepositoryFactory
    {
        #region 获取数据


        public IEnumerable<WorkLog_DetailEntity> GetPageList(Pagination pagination,string userId,string scope)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.UserId,
                (SELECT F_RealName FROM Base_User where F_UserId=t.UserId) as UserName,
                (SELECT F_HeadIcon FROM Base_User where F_UserId=t.UserId) as UserAvatar,
                t.SubmitTime,
                t.TodayLog,
                t.LogContent,
                t.TomorrowLog,
                t.Flag,
                t.UpdateTime
                ");
                strSql.Append("  FROM WorkLog_Detail t ");
                strSql.Append("  WHERE 1=1 ");
                if(scope == "send")
                {
                    strSql.Append(" AND UserId = '" + userId + "'");
                }
                else if (scope == "approve")
                {
                    //发给上级审批
                    //获取当前用户的岗位
                    string sql = "SELECT F_ObjectId FROM Base_UserRelation WHERE F_UserId='" + userId + "' AND F_Category=2";
                    DataTable dataTable = BaseRepository().FindTable(sql);
                    string postIds = "";
                    if (dataTable != null)
                    {
                        foreach(DataRow dataRow in dataTable.Rows)
                        {
                            postIds += "'" + dataRow["F_ObjectId"].ToString() + "',";
                        }
                        if (!String.IsNullOrEmpty(postIds))
                        {
                            postIds = postIds.Substring(0, postIds.Length - 1);
                        }
                    }
                    if (!String.IsNullOrEmpty(postIds))
                    {
                        sql = String.Format(@"with postids(f_postid)
                                    as
                                    (
                                        select f_postid from base_post where f_parentid IN ({0})
                                        union all
                                        select t.f_postid from base_post t
                                        inner join postids as c on t.f_parentid = c.f_postid
                                    )
                                    select f_userid from Base_UserRelation where F_ObjectId in(select f_postid from postids) and F_Category = 2", postIds);

                        dataTable = BaseRepository().FindTable(sql);
                        string userIds = "";
                        if (dataTable != null)
                        {
                            foreach (DataRow row in dataTable.Rows)
                            {
                                userIds += "'" + row["f_userid"] + "',";
                            }
                            if (!String.IsNullOrEmpty(userIds))
                            {
                                userIds = userIds.Substring(0, userIds.Length - 1);
                            }
                        }
                        if (!String.IsNullOrEmpty(userIds))
                        {
                            strSql.Append(" AND t.UserId IN (");
                            strSql.Append(userIds);
                            strSql.Append(")");
                        }
                        else
                        {
                            strSql.Append(" AND 1=2");
                        }
                    }
                }
                else
                {
                    throw new Exception("SCOPE参数错误。");
                }
                //var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<WorkLog_DetailEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 日志评价
        /// </summary>
        /// <param name="logId">日志Id</param>
        /// <param name="userId">用户Id</param>
        /// <param name="star">评价星</param>
        /// <param name="comment">评价内容</param>
        /// <returns>成功/失败</returns>
        public bool SaveDailyLogComment(string logId,string userId,int star,string comment)
        {
            bool ret = false;
            try
            {
                WorkLog_CommentEntity entity = new WorkLog_CommentEntity();
                entity.Create();
                entity.LogId = logId;
                entity.CommentUserId = userId;
                entity.CommentStar = star;
                entity.Comment = comment;
                entity.CommentType = 1;   ///1:直接评价;2:点赞
                entity.CommentTime = DateTime.Now;
                entity.Flag = 0;
                entity.UpdateTime = DateTime.Now;

                int result = BaseRepository().Insert<WorkLog_CommentEntity>(entity);
                ret = result == 1;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return ret;
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<WorkLog_DetailEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                //获取当前登录用户信息
                UserInfo loginUser = LoginUserInfo.Get();
                //当前登录用户的岗位信息
                string postIds = loginUser.postIds;
                //d407aa68-a3ab-49b2-ac7f-2ec758fcf9a5

                //如果是超级管理员，则获取所有数据。
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.UserId,
                (SELECT F_RealName FROM Base_User where F_UserId=t.UserId) as UserName,
                t.SubmitTime,
                t.TodayLog,
                t.LogContent,
                t.TomorrowLog,
                t.Flag,
                t.UpdateTime
                ");
                strSql.Append("  FROM WorkLog_Detail t ");
                strSql.Append("  WHERE 1=1 ");
                
                //如果是非管理员，则获取自己所属的数据。
                if(loginUser.userId.ToLower() != "system")
                {
                    string sql = String.Format(@"with postids(f_postid)
                                    as
                                    (
                                        select f_postid from base_post where f_postid = '{0}'
                                        union all
                                        select t.f_postid from base_post t
                                        inner join postids as c on t.f_parentid = c.f_postid
                                    )
                                    select f_userid from Base_UserRelation where F_ObjectId in(select f_postid from postids) and F_Category = 2", postIds);

                    DataTable dataTable = BaseRepository().FindTable(sql);
                    string userIds = "";
                    if (dataTable != null)
                    {
                        foreach(DataRow row in dataTable.Rows)
                        {
                            userIds += "'" + row["f_userid"] + "',";
                        }
                        if (!String.IsNullOrEmpty(userIds))
                        {
                            userIds = userIds.Substring(0, userIds.Length - 1);
                        }
                    }
                    if (!String.IsNullOrEmpty(userIds))
                    {
                        strSql.Append(" AND t.UserId IN (");
                        strSql.Append(userIds);
                        strSql.Append(")");
                    }
                }
                
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<WorkLog_DetailEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取WorkLog_Detail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public WorkLog_DetailEntity GetWorkLog_DetailEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<WorkLog_DetailEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public WorkLog_DetailEntity GetDetail(string id)
        {
            try
            {
                string sql = @"select *,
                                    (select f_realname from base_user where f_userid=userid) as UserName,
                                    (select f_headicon from base_user where f_userid=userid) as UserAvatar 
                                    from worklog_detail 
                                   where id='" + id + "' ";
                var dp = new DynamicParameters(new { });
                WorkLog_DetailEntity entity = BaseRepository().FindEntity<WorkLog_DetailEntity>(sql, dp);
                if (entity != null)
                {
                    sql = @"select *,
                                    (select f_realname from base_user where f_userid=commentuserid) as CommentUserName,
                                    (select f_headicon from base_user where f_userid=commentuserid) as CommentUserAvatar 
                                    from worklog_comment 
                                   where logid='" + id + "' " +
                                   "order by commenttime desc";
                    IList<WorkLog_CommentEntity> list = (IList<WorkLog_CommentEntity>)BaseRepository().FindList<WorkLog_CommentEntity>(sql);
                    entity.CommentList = list;
                }
                return entity;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<WorkLog_DetailEntity>(t=>t.Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, WorkLog_DetailEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
