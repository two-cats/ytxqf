﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier
{
    public class SupplierListToTree
    {
        public int number { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
