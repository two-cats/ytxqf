﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.Base.OrganizationModule;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using TTon.TMC.Application.TwoDevelopment.Common;
using System.Net;
using System.IO;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.VX;
using Newtonsoft.Json;
using TTon.TMC.Application.Base.AuthorizeModule;
using System.Drawing;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Core;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using System.Threading.Tasks;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_LogReport;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ReportInfo;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_LogReport.Model;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Net.Mail;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PushMessage;
using System.Web;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Statistics;
using System.Threading;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_OrderSuperaddition;
using NPOI.HSSF.UserModel;
using System.Windows.Forms;
using HorizontalAlignment = NPOI.SS.UserModel.HorizontalAlignment;
using NPOI.HPSF;
using System.Runtime.InteropServices;
using NPOI.SS.Util;
using TTon.TMC.Application.Base.SystemModule;
using Microsoft.VisualBasic;
using Newtonsoft.Json.Linq;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-22 09:53
    /// 描 述：订单报修
    /// </summary>
    public class YT_OrderService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_OrderEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT 
                            t.Id,
                            t.Name,
                            t.Phone,
                            t.Date,
                            t.OrderNumber,
                            t.PartsNumber,
                            YT_Supplier.Name as Supplier_Id,
                            YT_PartsTypes.Parts_Name AS PartsType,
                            YT_MaintenanceTypes.Name AS MaintainType,
                            YT_Instructor.Name as Instructor,
                            t.Remark,
                            t.Order_Status,
                            t.EstimateDate,
                            t.CreateDate,
                            t.SuccessDate,
                            t.Count,
                            t.Success_Remark,
	                        Base_User.F_RealName
                        FROM YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id=t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id=t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id=t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id=t.InStructor
                        LEFT JOIN Base_User ON Base_User.F_UserId = t.CreateUserId
                        WHERE 1=1 
                            And t.IsDelete=0
                ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreateDate >= @startTime AND t.CreateDate <= @endTime ) ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["Supplier"].IsEmpty())
                {
                    dp.Add("Supplier", "%" + queryParam["Supplier"].ToString() + "%", DbType.String);
                    strSql.Append(" AND  YT_Supplier.Name Like @Supplier ");
                }
                if (!queryParam["CreateUser"].IsEmpty())
                {
                    dp.Add("CreateUser", "%" + queryParam["CreateUser"].ToString() + "%", DbType.String);
                    strSql.Append(" AND Base_User.F_RealName Like @CreateUser ");
                }
                return this.BaseRepository().FindList<YT_OrderEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public List<GetListModel> GetList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT 
                            t.Id,
                            t.Name,
                            t.Phone,
                            t.Date,
                            t.OrderNumber,
                            t.PartsNumber,
                            YT_Supplier.Name as Supplier_Id,
                            YT_PartsTypes.Parts_Name AS PartsType,
                            YT_MaintenanceTypes.Name AS MaintainType,
                            YT_Instructor.Name as Instructor,
                            t.Remark,
                            t.Order_Status,
                            ISNULL(t.ThreadElapsedTime, 0) AS ThreadElapsedTime,
                            ISNULL(t.LineEdgeElapsedTime, 0) AS LineEdgeElapsedTime,
                            ISNULL(t.EstimateDate, 0) AS EstimateDate,
                            ISNULL(
	                           CONVERT (
		                          FLOAT,
		                          ISNULL(t.ThreadElapsedTime, 0)
	                          ) + CONVERT (
		                          FLOAT,
		                          ISNULL(t.LineEdgeElapsedTime, 0)
	                          ) + CONVERT (
		                          FLOAT,
		                          ISNULL(t.EstimateDate, 0)
	                          ),
	                          0
                            ) AS ElapsedTime,
                            t.CreateDate,
                            t.StartDate,
                            t.SuccessDate,
                            t.Count,
                            t.Success_Remark,
	                        Base_User.F_RealName,
                            ISNULL(t.SupplierStatus, 0) AS SupplierStatus,
                            (SELECT SUM(YT_OrderUser.Success) FROM YT_OrderUser WHERE Order_Id = t.Id) AS Success,
	                        (SELECT SUM(YT_OrderUser.Error) FROM YT_OrderUser WHERE Order_Id = t.Id) AS Error,
                            (SELECT SUM(YT_OrderUser.[All] -YT_OrderUser.Success - YT_OrderUser.Error) FROM YT_OrderUser WHERE Order_Id = t.Id) AS Unfinished
                        FROM YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id=t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id=t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id=t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id=t.InStructor
                        LEFT JOIN Base_User ON Base_User.F_UserId = t.CreateUserId
                        WHERE 1=1 
                            And t.IsDelete=0
                ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                string status = "";
                if (queryParam["Status"].IsEmpty())
                {
                    status = "1";
                }
                else
                {
                    status = queryParam["Status"].ToString();
                }

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    string st = "";
                    string ed = "";
                    string start = "";
                    string end = "";
                    if (!queryParam["start"].IsEmpty() && !queryParam["end"].IsEmpty())
                    {
                        start = queryParam["start"].ToString();
                        end = queryParam["end"].ToString();
                    }
                    else if (!queryParam["start"].IsEmpty() && queryParam["end"].IsEmpty())
                    {
                        start = queryParam["start"].ToString();
                    }
                    else if (queryParam["start"].IsEmpty() && !queryParam["end"].IsEmpty())
                    {
                        end = queryParam["end"].ToString();
                    }
                    if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    {
                        st = Convert.ToDateTime(queryParam["StartTime"]).ToString("yyyy-MM-dd " + start);
                        ed = Convert.ToDateTime(queryParam["EndTime"]).ToString("yyyy-MM-dd " + end);
                    }
                    else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                    {
                        st = Convert.ToDateTime(queryParam["StartTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                        ed = Convert.ToDateTime(queryParam["EndTime"]).ToString("yyyy-MM-dd " + end);
                    }
                    else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                    {
                        st = Convert.ToDateTime(queryParam["StartTime"]).ToString("yyyy-MM-dd " + start);
                        ed = Convert.ToDateTime(queryParam["EndTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else
                    {
                        st = Convert.ToDateTime(queryParam["StartTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                        ed = Convert.ToDateTime(queryParam["EndTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                    }

                    if (status == "1")
                    {
                        strSql.Append(" AND (CONVERT(varchar(100), t.SuccessDate, 120) >= '" + st + @"' AND CONVERT(varchar(100), t.SuccessDate, 120) <= '" + ed + @"') AND t.SuccessDate is NOT NULL AND t.SuccessDate != '' ");
                    }
                    else
                    {
                         strSql.Append(" AND (CONVERT(varchar(100), t.CreateDate, 120) >= '" + st+ @"' AND CONVERT(varchar(100), t.CreateDate, 120) <= '" + ed+@"') ");
                    }
                    //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    //dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    //strSql.Append(" AND (t.SuccessDate >= @startTime AND t.SuccessDate <= @endTime ) AND t.SuccessDate is NOT NULL AND t.SuccessDate != '' ");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                if (!queryParam["OrderNumber"].IsEmpty())
                {
                    dp.Add("OrderNumber", "%" + queryParam["OrderNumber"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OrderNumber Like @OrderNumber ");
                }
                if (!queryParam["Supplier"].IsEmpty())
                {
                    dp.Add("Supplier", "%" + queryParam["Supplier"].ToString() + "%", DbType.String);
                    strSql.Append(" AND  YT_Supplier.Name Like @Supplier ");
                }
                if (!queryParam["PartsNumber"].IsEmpty())
                {
                    dp.Add("PartsNumber", "%" + queryParam["PartsNumber"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.PartsNumber Like @PartsNumber ");
                }

                if (status == "1")
                {
                    strSql.Append(" AND t.Order_Status = 5 ");
                    pagination.sidx = "t.SuccessDate";
                }
                else
                {
                    strSql.Append(" AND t.Order_Status != 3 AND t.Order_Status != 5 ");
                    pagination.sidx = "t.CreateDate";
                }

                pagination.sidx = "t.SuccessDate";
                pagination.sord = "DESC";
                List<GetListModel> list = this.BaseRepository().FindList<GetListModel>(strSql.ToString(), dp, pagination).AsList();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        item.Date = Convert.ToDateTime(item.Date).ToString("yyyy-MM-dd HH:mm");
                        string aa = "";
                        if (!string.IsNullOrEmpty(item.SuccessDate))
                        {
                            if (item.SuccessDate.Length == 20)
                            {
                                aa = item.SuccessDate.Substring(0, item.SuccessDate.Length - 4);
                            }
                            else if (item.SuccessDate.Length == 19)
                            {
                                aa = item.SuccessDate.Substring(0, item.SuccessDate.Length - 3);
                            }
                        }

                        if (!string.IsNullOrEmpty(item.StartDate))
                        {
                            if (item.StartDate.Length == 19)
                            {
                                item.StartDate = item.StartDate.Substring(0, item.StartDate.Length - 3);
                            }
                        }
        
                        item.SuccessDate = aa;
                        if (item.Unfinished.ToInt() < 0)
                        {
                            string a = item.Unfinished.ToString().Replace("-", string.Empty);
                            item.Unfinished = "超额" + a;
                        }
                        if (item.Order_Status == 0 && item.SupplierStatus == 0)
                        {
                            item.Order_Status = 8;
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 检查目录小红点
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public CheckMenuModel checkMenu(string userId)
        {
            CheckMenuModel model = new CheckMenuModel();

            #region 岗位权限
            List<string> UserId = new List<string>();
            string objects1 = Config.GetValue("OrderAndTaskManage");
            string objects2 = Config.GetValue("OrderManage");
            string objects3 = Config.GetValue("QualityManager");
            StringBuilder UserRelationSql = new StringBuilder();
            UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
            List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
            if (relationEntity.Count != 0)
            {
                for (int i = 0; i < relationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                    if (userEntity != null)
                    {
                        UserId.Add(userEntity.F_UserId);
                    }
                }
            }
            #endregion

            #region 检查任务
            string taskWhere = "";

             if (!UserId.Contains(userId))
            {
                taskWhere = " WHERE (YT_OrderUser.Order_UserId = '" + userId + "' OR YT_OrderUser.CreateUserId = '" + userId + "') AND YT_OrderUser.IsDelete = 0 ";
            }
            else
            {
                taskWhere = " WHERE YT_OrderUser.IsDelete = 0 ";
            }

            StringBuilder taskSql = new StringBuilder();
            taskSql.Append(@"SELECT
                        Top 1 
	                    ISNULL(YT_OrderUser.Id, '') AS TaskId
                        FROM
	                        YT_OrderUser
                        LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = YT_OrderUser.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        " + taskWhere + @"
                         AND YT_OrderUser.IsApprover = 0
                        ORDER BY YT_OrderUser.IsApprover ASC,YT_OrderUser.CreateDate DESC");
            DataTable tasklist = new RepositoryFactory().BaseRepository().FindTable(taskSql.ToString());
            if (tasklist.Rows.Count == 1)
            {
                model.task = 1;
            }
            else
            {
                model.task = 0;
            }
            #endregion

            #region 检查订单
            string orderWhere = "";
            if (!UserId.Contains(userId))
            {
                orderWhere = " WHERE (t.CreateUserId = '" + userId + "' OR t.ModifyUserId = '" + userId + "') AND t.IsDelete = 0 ";
            }
            else
            {
                orderWhere = " WHERE t.IsDelete = 0 ";
            }

            StringBuilder orderSql = new StringBuilder();
            orderSql.Append(@"SELECT
	                  	TOP 1 t.Id
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        " + orderWhere + @"
                        AND t.Order_Status != 5
	                    AND t.Order_Status != 3");
            DataTable orderList = this.BaseRepository().FindTable(orderSql.ToString());
            if (orderList.Rows.Count == 1)
            {
                model.order = 1;
            }
            else
            {
                model.order = 0;
            }
            #endregion

            #region 检查库存分配
            string orderInventory = Config.GetValue("OrderInventory");
            StringBuilder checksql = new StringBuilder();
            checksql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId= '" + orderInventory + "' AND  Base_UserRelation.F_UserId = '"+userId+"' ");
            List<UserRelationEntity> userRelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(checksql.ToString()).ToList();
            if (userRelationEntity.Count == 0)
            {
                model.allot = 0;
            }
            else
            {
                StringBuilder allotSql = new StringBuilder();
                allotSql.Append(@"SELECT TOP 1
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        WHERE t.IsDelete = 0
                        AND t.Order_Status != 7
                        AND t.Order_Status != 5
                        AND t.Order_Status != 3
                        AND t.Order_Status != 0
                        AND t.Area = 1
                        ORDER BY t.CreateDate DESC");
                DataTable allotList = this.BaseRepository().FindTable(allotSql.ToString());
                if (allotList.Rows.Count == 1)
                {
                    model.allot = 1;
                }
                else
                {
                    model.allot = 0;
                }
            }
            #endregion

            #region 检查异常记录
            List<string> UserIds = new List<string>();
            string manage = Config.GetValue("OrderAndTaskManage");
            string inventory = Config.GetValue("OrderInventory");
            StringBuilder UserSql = new StringBuilder();
            UserSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + manage + "','" + inventory + "') ");
            List<UserRelationEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserSql.ToString()).AsList();
            if (userList.Count != 0)
            {
                for (int i = 0; i < userList.Count; i++)
                {
                    UserEntity user = this.BaseRepository().FindEntity<UserEntity>(userList[i].F_UserId);
                    if (user != null)
                    {
                        UserIds.Add(user.F_UserId);
                    }
                }
            }
            if (!UserIds.Contains(userId))
            {
                model.abnormal = 0;
            }
            else
            {
                StringBuilder abnormalSql = new StringBuilder();
                abnormalSql.Append(@"SELECT
	                    TOP 1 t.Id AS id
                    FROM
	                    YT_AbnormalRemind t
                    LEFT JOIN Base_User ON Base_User.F_UserId = t.CreateUserId
                    LEFT JOIN YT_Order ON YT_Order.Id = t.OrderId
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                    WHERE
	                    t.IsDelete = 0
                    AND t.Status = 0");
                DataTable abnormalList = this.BaseRepository().FindTable(abnormalSql.ToString());
                if (abnormalList.Rows.Count == 1)
                {
                    model.abnormal = 1;
                }
                else
                {
                    model.abnormal = 0;
                }
            }
            #endregion

            #region 检查交班日志
            StringBuilder journalSql = new StringBuilder();
            journalSql.Append(@"SELECT
	                    t.Id AS id,
	                    t.Name AS name,
	                    h.F_RealName AS heiruser,
	                    c.F_RealName AS createuser,
	                    t.HeirStatus AS heirstatus,
	                    ISNULL(CONVERT (VARCHAR(100), t.HeirTime, 23),'') AS heirtime,
	                    CONVERT (
		                    VARCHAR (100),
		                    t.CreateDate,
		                    23
	                    ) AS createtime,
	                    CASE
                    WHEN t.HeirUser = '" + userId + @"'
                    AND t.HeirStatus = '0' THEN
	                    1
                    ELSE
	                    0
                    END everconfirmed
                    FROM
	                    YT_LogReport t
                    LEFT JOIN Base_User h ON h.F_UserId = t.HeirUser
                    LEFT JOIN Base_User c ON c.F_UserId = t.CreateUser
                    ORDER BY
	                    t.CreateDate DESC");
            List<ReportListModel> journalList = new RepositoryFactory().BaseRepository().FindList<ReportListModel>(journalSql.ToString()).AsList();
            if (journalList.Count > 0)
            {
                List<ReportListModel> newList = journalList.Where(d => d.everconfirmed == 0).ToList();
                if (newList.Count > 0)
                {
                    model.journal = 1;
                }
                else
                {
                    model.journal = 0;
                }
            }
            else
            {
                model.journal = 0;
            }
            #endregion

            return model;
        }

        /// <summary>
        /// 获取YT_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_OrderEntity GetYT_OrderEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_OrderEntity>(keyValue);
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message);
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取YT_Order表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrderModel GetYT_OrderSqlEntity(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Name,
                t.Phone,
                t.Date,
                t.OrderNumber,
                t.PartsNumber,
                t.Batch_Number,
                 YT_Supplier.Name as Supplier_Id,YT_PartsTypes.Parts_Name AS PartsType,YT_MaintenanceTypes.Name AS MaintainType,
                YT_Instructor.Name as Instructor,
                t.Remark,
                t.Order_Status,
                 ISNULL(t.ThreadElapsedTime, 0) AS ThreadElapsedTime,
                 ISNULL(t.LineEdgeElapsedTime, 0) AS LineEdgeElapsedTime,
                 ISNULL(t.EstimateDate, 0) AS EstimateDate,
                 ISNULL(
	                CONVERT (
		                 FLOAT,
		                 ISNULL(t.ThreadElapsedTime, 0)
	                 ) + CONVERT (
		                 FLOAT,
		                 ISNULL(t.LineEdgeElapsedTime, 0)
	                 ) + CONVERT (
		                  FLOAT,
		                  ISNULL(t.EstimateDate, 0)
	                  ),
	                  0
                ) AS ElapsedTime,
                t.CreateDate,t.SuccessDate,t.Count,t.Success_Remark,t.CreateUserId,
	            YT_OrderSuperaddition.QualifiedCount,
	            YT_OrderSuperaddition.UnqualifiedCount,
	            YT_OrderSuperaddition.Remark AS SRemark,
	            YT_OrderSuperaddition.CreateDate AS SCreateDate
                ");
                strSql.Append("  FROM YT_Order t  LEFT JOIN YT_Supplier ON YT_Supplier.Id=t.Supplier_Id  LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id=t.PartsType  LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id=t.MaintainType LEFT JOIN YT_Instructor ON YT_Instructor.Id=t.InStructor LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0  And t.Id='" + keyValue + "'");

                List<OrderModel> list = new RepositoryFactory().BaseRepository().FindList<OrderModel>(strSql.ToString()).AsList();
                StringBuilder strSqlUser = new StringBuilder();
                strSqlUser.Append("select Base_User.* from Base_User  where Base_User.F_UserId='" + list[0].CreateUserId + "'");
                List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strSqlUser.ToString()).AsList();
                if (userList.Count != 0)
                {
                    foreach (var item in userList)
                    {
                        list[0].Designated += item.F_RealName + ",";

                    }
                }

                StringBuilder ImgSql = new StringBuilder();
                ImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Order ON YT_Order.Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Order.Id = '" + list[0].Id + @"'
                            ORDER BY F_CreateDate ASC");
                List<ImageUrl> ImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(ImgSql.ToString()).AsList();
                if (ImgList.Count != 0)
                {
                    foreach (var itemlist in ImgList)
                    {
                        String a = itemlist.Img_Url.Substring(itemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                        itemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                list[0].pic = ImgList;


                StringBuilder EndImgSql = new StringBuilder();
                EndImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Order ON YT_Order.Task_Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Order.Id = '" + list[0].Id + @"'
                            ORDER BY F_CreateDate ASC");
                List<ImageUrl> EndImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(EndImgSql.ToString()).AsList();
                if (EndImgList.Count != 0)
                {
                    foreach (var enditemlist in EndImgList)
                    {
                        String a = enditemlist.Img_Url.Substring(enditemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                        enditemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                list[0].task_pic = EndImgList;
                return list[0];
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 验证是否为仓库管理员
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckOrderInventory(string userId)
        {
            List<string> UserId = new List<string>();
            string objects1 = Config.GetValue("OrderInventory");
            StringBuilder UserRelationSql = new StringBuilder();
            UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "')  ");
            List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
            if (relationEntity.Count != 0)
            {
                for (int i = 0; i < relationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                    UserId.Add(userEntity.F_UserId);
                }
            }

            if (UserId.Contains(userId))
            {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// 验证是否为经理
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckOrderManage(string userId)
        {
            List<string> UserId = new List<string>();
            string objects1 = Config.GetValue("OrderAndTaskManage");
            StringBuilder UserRelationSql = new StringBuilder();
            UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "')  ");
            List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
            if (relationEntity.Count != 0)
            {
                for (int i = 0; i < relationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                    UserId.Add(userEntity.F_UserId);
                }
            }

            if (UserId.Contains(userId))
            {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// 验证是否为班长
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckSquadLeader(string userId)
        {
            List<string> UserId = new List<string>();
            string objects1 = Config.GetValue("SquadLeader");
            StringBuilder UserRelationSql = new StringBuilder();
            UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "')  ");
            List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
            if (relationEntity.Count != 0)
            {
                for (int i = 0; i < relationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                    UserId.Add(userEntity.F_UserId);
                }
            }

            if (UserId.Contains(userId))
            {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// 仓库管理员订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetInventoryOrderList(OrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                StringBuilder sql = new StringBuilder();
                #region  零件确认
                if (param.type == "0")
                {
                    sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        WHERE t.IsDelete = 0
                        AND t.Order_Status != 7
                        AND t.Order_Status != 5
                        AND t.Order_Status != 3
                        AND t.Order_Status != 0
                        AND t.Area = 1");
                }
                #endregion

                #region 订单确认
                if (param.type == "1")
                {
                    sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        WHERE t.IsDelete = 0
                        AND t.Order_Status = 7");
                }
                #endregion
                param.pagination.sidx = "CreateDate";
                param.pagination.sord = "desc";
                list = this.BaseRepository().FindList<OrderTaskListModel>(sql.ToString(), param.pagination).AsList();

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetOrderList(OrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }

                if (UserId.Contains(param.userId) && !string.IsNullOrEmpty(param.querystr))
                {
                    where = " WHERE (t.OrderNumber LIKE '%" + param.querystr + "%' OR YT_Supplier.Name LIKE '%" + param.querystr + "%' OR t.PartsNumber LIKE '%" + param.querystr + "%') AND t.IsDelete = 0 ";
                }
                else if (!UserId.Contains(param.userId) && string.IsNullOrEmpty(param.querystr))
                {
                    where = " WHERE (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND t.IsDelete = 0 ";
                }
                else if (!UserId.Contains(param.userId) && !string.IsNullOrEmpty(param.querystr))
                {
                    where = " WHERE (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND (t.OrderNumber LIKE '%" + param.querystr + "%' OR YT_Supplier.Name LIKE '%" + param.querystr + "%' OR t.PartsNumber LIKE '%" + param.querystr + "%') AND t.IsDelete = 0 ";
                }
                else
                {
                    where = " WHERE t.IsDelete = 0 ";
                }
                #endregion

                StringBuilder sql = new StringBuilder();
                #region  未完成
                if (param.type == "0")
                {
                    sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        " + where + @"
                        AND t.Order_Status != 5
	                    AND t.Order_Status != 3");
                }
                #endregion

                #region 已完成
                if (param.type == "1")
                {
                    sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        " + where + @"
                        AND t.Order_Status = 5");
                }
                #endregion

                #region 搜索
                if (param.type == "2")
                {
                    if (string.IsNullOrEmpty(param.querystr))
                    {
                        return new List<OrderTaskListModel>();
                    }
                    sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        " + where + @"
	                    AND t.Order_Status != 3");
                }
                #endregion
                param.pagination.sidx = "CreateDate";
                param.pagination.sord = "desc";
                list = this.BaseRepository().FindList<OrderTaskListModel>(sql.ToString(), param.pagination).AsList();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        if (item.Order_Status == 0 && item.SupplierStatus == 0)
                        {
                            item.Order_Status = 8;
                        }
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 订单列表 -- 微信
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<StatisticsListModel> GetOrderListToWx(GetOrderListParam param)
        {
            try
            {
                List<StatisticsListModel> list = new List<StatisticsListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                string objects4 = Config.GetValue("Quality");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "','" + objects4 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }

                if (UserId.Contains(param.userId) && !string.IsNullOrEmpty(param.querystr))
                {
                    where = " (t.OrderNumber LIKE '%" + param.querystr + "%' OR t.PartsNumber LIKE '%" + param.querystr + "%') AND ";
                }
                else if (!UserId.Contains(param.userId) && string.IsNullOrEmpty(param.querystr))
                {
                    where = " (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND ";
                }
                else if (!UserId.Contains(param.userId) && !string.IsNullOrEmpty(param.querystr))
                {
                    where = " (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND (t.OrderNumber LIKE '%" + param.querystr + "%' OR t.PartsNumber LIKE '%" + param.querystr + "%') AND ";
                }
                else
                {
                    where = " ";
                }

                if (!String.IsNullOrEmpty(param.startTime) && !String.IsNullOrEmpty(param.endTime))
                {
                    where += " t.CreateDate BETWEEN '" + Convert.ToDateTime(param.startTime) + "' AND '" + Convert.ToDateTime(param.endTime) + "' AND t.Supplier_Id = '" + param.supplier + "' ";
                }
                if (String.IsNullOrEmpty(param.startTime) && !String.IsNullOrEmpty(param.endTime))
                {
                    where += " t.CreateDate <= '" + Convert.ToDateTime(param.endTime) + "' AND t.Supplier_Id = '" + param.supplier + "' ";
                }
                if (!String.IsNullOrEmpty(param.startTime) && String.IsNullOrEmpty(param.endTime))
                {
                    where += " t.CreateDate >= '" + Convert.ToDateTime(param.startTime) + "' AND t.Supplier_Id = '" + param.supplier + "' ";
                }
                if (String.IsNullOrEmpty(param.startTime) && String.IsNullOrEmpty(param.endTime))
                {
                    where += " t.Supplier_Id = '" + param.supplier + "' ";
                }
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                            t.Id,
	                            t.OrderNumber AS OrderNumber,
	                            ISNULL(t.DrawingNumber, '') AS DrawingNumber,
	                            YT_Supplier.Name AS Supplier,
	                            t.PartsNumber AS PartsNumber,
	                            YT_PartsTypes.Parts_Name AS PartsType,
	                            t.Batch_Number AS BatchNumber,
	                            (
		                            SELECT
			                            ISNULL(SUM(Error + Success), 0)
		                            FROM
			                            YT_OrderUser
		                            WHERE
			                            Order_Id = t.Id
		                            AND IsDelete = 0
	                            ) AS [Sum],
	                            CASE
                            WHEN t.RepairType = '0' THEN
	                            '挑选'
                            WHEN t.RepairType = '1' THEN
	                            '返修'
                            ELSE
	                            ''
                            END AS RepairType,
                             ISNULL(
	                            YT_Instructor.IssueDescription,
	                            ''
                            ) AS IssueDescription,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (YT_OrderUser.Success),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Qualified,
                             (
	                            SELECT
		                            ISNULL(SUM(YT_OrderUser.Error), 0)
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Disqualified,
                             CONVERT (
	                            VARCHAR (100),
	                            t.CreateDate,
	                            120
                            ) AS CreateTime,
                             ISNULL(t.SuccessDate, '') AS SuccessDate,
                             (
	                            SELECT
		                            ISNULL(
			                            t.[Count] - SUM (Error + Success),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Unfinished,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (
				                            YT_OrderUser.Error + YT_OrderUser.Success
			                            ),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Finish
                            FROM
	                            YT_Order t
                            LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                            LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                            WHERE " + where + @"
                           AND t.IsDelete = 0
                           AND t.Order_Status != 3
                           AND t.Order_Status != 0
                           ORDER BY t.CreateDate DESC");
                list = new RepositoryFactory().BaseRepository().FindList<StatisticsListModel>(sql.ToString()).AsList();
                if (list.Count > 0)
                {
                    foreach (var Item in list)
                    {
                        if (Item.Unfinished.ToInt() < 0)
                        {
                            string a = Item.Unfinished.ToString().Replace("-", string.Empty);
                            Item.Unfinished = "超额" + a;
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetOrderTaskList(GetOrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }

                if (UserId.Contains(param.userId) && !string.IsNullOrEmpty(param.querystr))
                {
                    where = " WHERE t.OrderNumber LIKE '%" + param.querystr + "%' AND YT_OrderUser.IsDelete = 0 ";
                }
                else if (!UserId.Contains(param.userId) && string.IsNullOrEmpty(param.querystr))
                {
                    where = " WHERE (YT_OrderUser.Order_UserId = '" + param.userId + "' OR YT_OrderUser.CreateUserId = '" + param.userId + "') AND YT_OrderUser.IsDelete = 0 ";
                }
                else if (!UserId.Contains(param.userId) && !string.IsNullOrEmpty(param.querystr))
                {
                    where = " WHERE (YT_OrderUser.Order_UserId = '" + param.userId + "' OR YT_OrderUser.CreateUserId = '" + param.userId + "') AND t.OrderNumber LIKE '%" + param.querystr + "%' AND YT_OrderUser.IsDelete = 0 ";
                }
                else
                {
                    where = " WHERE YT_OrderUser.IsDelete = 0 ";
                }
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        CONVERT (VARCHAR(100), t.[Date], 23) AS DATE,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(YT_OrderUser.Accomplish, '') AS Accomplish,
	                        CONVERT (
		                        VARCHAR (100),
		                        YT_OrderUser.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(t.SuccessDate, '') AS SuccessDate,
	                        ISNULL(YT_OrderUser.Id, '') AS TaskId,
	                        ISNULL(YT_OrderUser.IsApprover, 0) AS IsApprover
                        FROM
	                        YT_OrderUser
                        LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = YT_OrderUser.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        " + where + @"
                        ");
                //param.pagination.sidx = "IsApprover";  //ORDER BY IsApprover ASC,CreateDate DESC
                //param.pagination.sord = "desc";
                param.pagination.sidx = "IsApprover,CreateDate";
                param.pagination.sord = "DESC";
                list = new RepositoryFactory().BaseRepository().FindList<OrderTaskListModel>(sql.ToString(),param.pagination).AsList();
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 仓库管理员查看列表
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public List<YT_OrderInventoryEntity> GetOrderInventoryList(string orderid)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT
	                    YT_OrderInventory.Id,
	                    YT_OrderInventory.Order_Id,
	                    ISNULL(YT_OrderInventory.Batch_Number,'') AS Batch_Number,
	                    Base_User.F_RealName AS User_Id,
	                    YT_OrderInventory.[Count],
	                    CONVERT (
		                    VARCHAR (100),
		                    YT_OrderInventory.CreateTime,
		                    120
	                    ) AS CreateTime,
	                    YT_OrderInventory.CreateUserId
                    FROM
	                    YT_OrderInventory
                    LEFT JOIN Base_User ON Base_User.F_UserId = YT_OrderInventory.User_Id
                    WHERE
	                    YT_OrderInventory.Order_Id = '" + orderid + @"'
                    ORDER BY
	                    CreateTime DESC");
            List<YT_OrderInventoryEntity> result = this.BaseRepository().FindList<YT_OrderInventoryEntity>(sql.ToString()).AsList();
            return result;
        }

        /// <summary>
        /// 任务详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public OrderTaskListModel GetTaskDetail(string keyValue)
        {
            try
            {
                OrderTaskListModel model = new OrderTaskListModel();

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        t.Name,
	                        t.Phone,
	                        CONVERT (VARCHAR(100), t.[Date], 23) AS DATE,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Id, '') AS SupplierId,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(YT_PartsTypes.Id, '') AS PartsTypeId,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        ISNULL(YT_Instructor.Id, '') AS InstructorId,
	                        ISNULL(YT_Instructor.Name, '') AS InstructorName,
	                        ISNULL(t.Remark, '') AS Remark,
	                        t.Order_Status,
	                        ISNULL(t.EstimateDate, 0) AS EstimateDate,
	                        ISNULL(t.ThreadElapsedTime, 0) AS ThreadElapsedTime,
	                        ISNULL(t.LineEdgeElapsedTime, 0) AS LineEdgeElapsedTime,
	                        ISNULL(
		                        CONVERT (
			                        FLOAT,
			                        ISNULL(t.ThreadElapsedTime, 0)
		                        ) + CONVERT (
			                        FLOAT,
			                        ISNULL(t.LineEdgeElapsedTime, 0)
		                        ) + CONVERT (
			                        FLOAT,
			                        ISNULL(t.EstimateDate, 0)
		                        ),
		                        0
	                        ) AS ElapsedTime,
	                        CONVERT (
		                        VARCHAR (100),
		                        YT_OrderUser.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.SuccessDate, '') AS SuccessDate,
	                        ISNULL(t.Success_Remark, '') AS SuccessRemark,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        ISNULL(t.Person, 0) AS person,
	                        ISNULL(t.Batch_Number, '') AS BatchNumber,
	                        ISNULL(YT_OrderUser.Id, '') AS TaskId,
	                        ISNULL(YT_OrderUser.IsApprover, 0) AS IsApprover,
	                        ISNULL(YT_OrderUser.Success, 0) AS Success,
	                        ISNULL(YT_OrderUser.Error, 0) AS Error,
	                        ISNULL(YT_OrderUser.[All], 0) AS [All],
	                        ISNULL(
		                        YT_OrderUser.[All] - YT_OrderUser.Success - YT_OrderUser.Error,
		                        0
	                        ) AS Unfinished,
	                        ISNULL(YT_OrderUser.Accomplish, '') AS Accomplish,
	                        ISNULL(t.DrawingNumber, '') AS DrawingNumber,
	                        ISNULL(t.Pesponsible, '') AS Pesponsible,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         ISNULL(m.F_RealName, '') AS Notifier,
                         ISNULL(t.WxPushStatus, '') AS WxPushStatus,
                         ISNULL(t.WxPushCount, '') AS WxPushCount,
                         ISNULL(t.SmsPushStatus, '') AS SmsPushStatus,
                         ISNULL(t.SmsPushCount, '') AS SmsPushCount,
                         ISNULL(YT_OrderUser.Category, 1) AS Category
                        FROM
	                        YT_OrderUser
                        LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = YT_OrderUser.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        WHERE YT_OrderUser.Id = '" + keyValue + "'");
                List<OrderTaskListModel> list = new RepositoryFactory().BaseRepository().FindList<OrderTaskListModel>(sql.ToString()).AsList();
                if (list.Count > 0)
                {
                    foreach (var newItem in list)
                    {
                        if (newItem.Unfinished.ToInt() < 0)
                        {
                            string a = newItem.Unfinished.ToString().Replace("-", string.Empty);
                            newItem.Unfinished = "超额" + a;
                        }
                    }
                }
                model = list[0];

                StringBuilder ImgSql = new StringBuilder();
                ImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Order ON YT_Order.Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Order.Id = '" + model.Id + @"'
                            ORDER BY F_CreateDate ASC");
                List<ImageUrl> ImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(ImgSql.ToString()).AsList();
                if (ImgList.Count != 0)
                {
                    foreach (var itemlist in ImgList)
                    {
                        String a = itemlist.Img_Url.Substring(itemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                        itemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.Pic = ImgList;

                StringBuilder EndImgSql = new StringBuilder();
                EndImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Order ON YT_Order.Task_Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Order.Id = '" + model.Id + @"'
                            ORDER BY F_CreateDate ASC");
                List<ImageUrl> EndImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(EndImgSql.ToString()).AsList();
                if (EndImgList.Count != 0)
                {
                    foreach (var enditemlist in EndImgList)
                    {
                        String a = enditemlist.Img_Url.Substring(enditemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                        enditemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.TaskPic = EndImgList;

                return model;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 订单详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public OrderTaskListModel GetOrderDetail(string keyValue)
        {
            try
            {
                OrderTaskListModel model = new OrderTaskListModel();

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        t.Name,
	                        t.Phone,
	                        CONVERT (VARCHAR(100), t.[Date], 23) AS DATE,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Id, '') AS SupplierId,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(YT_PartsTypes.Id, '') AS PartsTypeId,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        ISNULL(YT_Instructor.Id, '') AS InstructorId,
	                        ISNULL(YT_Instructor.Name, '') AS InstructorName,
	                        ISNULL(t.Remark, '') AS Remark,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(c.F_Mobile, '') AS CreateUserPhone,
	                        ISNULL(m.F_RealName, '') AS ModifyUser,
	                        ISNULL(m.F_Mobile, '') AS ModifyUserPhone,
	                        ISNULL(t.SuccessDate, '') AS SuccessDate,
	                        ISNULL(t.StartDate, '') AS StartDate,
	                        ISNULL(t.Success_Remark, '') AS SuccessRemark,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        ISNULL(t.Person, 0) AS person,
	                        ISNULL(t.Batch_Number, '') AS BatchNumber,
	                        ISNULL(t.DrawingNumber, '') AS DrawingNumber,
	                        ISNULL(t.Pesponsible, '') AS Pesponsible,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         ISNULL(m.F_RealName, '') AS Notifier,
                         ISNULL(t.WxPushStatus, '') AS WxPushStatus,
                         ISNULL(t.WxPushCount, '') AS WxPushCount,
                         ISNULL(t.SmsPushStatus, '') AS SmsPushStatus,
                         ISNULL(t.SmsPushCount, '') AS SmsPushCount,
                         ISNULL(
	                        (
		                        SELECT
			                        TOP 1 Base_User.F_RealName
		                        FROM
			                        Base_User
		                        LEFT JOIN YT_OrderProcess ON YT_OrderProcess.CreateUser = Base_User.F_UserId
		                        WHERE
			                        YT_OrderProcess.Order_Status = '5'
		                        AND YT_OrderProcess.Order_Id = t.Id
		                        ORDER BY
			                        CreateDate DESC
	                        ),
	                        ''
                        ) AS UserName,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus,
                         ISNULL(
	                        (
		                        SELECT
			                        SUM (
				                        YT_OrderUser.Success + YT_OrderUser.Error
			                        )
		                        FROM
			                        YT_OrderUser
		                        WHERE
			                        Order_Id = t.Id
		                        AND IsDelete = 0
	                        ),
	                        0
                        ) AS [ALL],
                         ISNULL(
	                        (
		                        SELECT
			                        SUM (YT_OrderUser.Success)
		                        FROM
			                        YT_OrderUser
		                        WHERE
			                        Order_Id = t.Id
		                        AND IsDelete = 0
	                        ),
	                        0
                        ) AS Success,
                         ISNULL(
	                        (
		                        SELECT
			                        SUM (YT_OrderUser.Error)
		                        FROM
			                        YT_OrderUser
		                        WHERE
			                        Order_Id = t.Id
		                        AND IsDelete = 0
	                        ),
	                        0
                        ) AS Error,
                         ISNULL(
	                        (
		                        SELECT
			                        t.[Count] - SUM (
				                        YT_OrderUser.Success + YT_OrderUser.Error
			                        )
		                        FROM
			                        YT_OrderUser
		                        WHERE
			                        Order_Id = t.Id
		                        AND IsDelete = 0
	                        ),
	                        0
                        ) AS Unfinished,
                         ISNULL(
	                        YT_OrderSuperaddition.QualifiedCount,
	                        ''
                        ) AS QualifiedCount,
                         ISNULL(
	                        YT_OrderSuperaddition.UnqualifiedCount,
	                        ''
                        ) AS UnqualifiedCount,
                         ISNULL(
	                        YT_OrderSuperaddition.Remark,
	                        ''
                        ) AS SRemark,
                         ISNULL(
	                        CONVERT (
		                        VARCHAR (100),
		                        YT_OrderSuperaddition.CreateDate,
		                        120
	                        ),
	                        ''
                        ) AS SCreateDate,
                         ISNULL(t.ThreadElapsedTime, 0) AS ThreadElapsedTime,
                         ISNULL(t.LineEdgeElapsedTime, 0) AS LineEdgeElapsedTime,
                         ISNULL(t.EstimateDate, 0) AS EstimateDate,
                         ISNULL(
	                        CONVERT (
		                        FLOAT,
		                        ISNULL(t.ThreadElapsedTime, 0)
	                        ) + CONVERT (
		                        FLOAT,
		                        ISNULL(t.LineEdgeElapsedTime, 0)
	                        ) + CONVERT (
		                        FLOAT,
		                        ISNULL(t.EstimateDate, 0)
	                        ),
	                        0
                        ) AS ElapsedTime,
                        ISNULL(t.AllotCount,0) AS AllotCount
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        WHERE t.Id = '" + keyValue + "'");
                List<OrderTaskListModel> list = new RepositoryFactory().BaseRepository().FindList<OrderTaskListModel>(sql.ToString()).AsList();
                if (list.Count > 0)
                {
                    foreach (var newItem in list)
                    {
                        if (newItem.Unfinished.ToInt() < 0)
                        {
                            string a = newItem.Unfinished.ToString().Replace("-", string.Empty);
                            newItem.Unfinished = "超额" + a;
                        }
                        if (newItem.Order_Status == 0 && newItem.SupplierStatus == 0)
                        {
                            newItem.Order_Status = 8;
                        }
                    }
                }
                model = list[0];

                StringBuilder ImgSql = new StringBuilder();
                ImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Order ON YT_Order.Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Order.Id = '" + model.Id + @"'
                            ORDER BY F_CreateDate ASC");
                List<ImageUrl> ImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(ImgSql.ToString()).AsList();
                if (ImgList.Count != 0)
                {
                    foreach (var itemlist in ImgList)
                    {
                        String a = itemlist.Img_Url.Substring(itemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                        itemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.Pic = ImgList;

                StringBuilder EndImgSql = new StringBuilder();
                EndImgSql.Append(@"SELECT F_FilePath  as Img_Url
                            FROM Base_AnnexesFile
                            LEFT JOIN YT_Order ON YT_Order.Task_Pic = Base_AnnexesFile.F_FolderId
                            WHERE YT_Order.Id = '" + model.Id + @"'
                            ORDER BY F_CreateDate ASC");
                List<ImageUrl> EndImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(EndImgSql.ToString()).AsList();
                if (EndImgList.Count != 0)
                {
                    foreach (var enditemlist in EndImgList)
                    {
                        String a = enditemlist.Img_Url.Substring(enditemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                        enditemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                    }
                }
                model.TaskPic = EndImgList;

                StringBuilder abnormalSql = new StringBuilder();
                abnormalSql.Append(@"SELECT
	                    t.Id AS id,
	                    t.Content AS content
                    FROM
	                    YT_AbnormalRemind t
                    WHERE
	                    t.OrderId = '" + model.Id + @"'
                    AND Status = 1");
                List<AbnormalInfoModel> abnormal = new RepositoryFactory().BaseRepository().FindList<AbnormalInfoModel>(abnormalSql.ToString()).AsList();
                model.abnormalList = abnormal;

                return model;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取首页订单统计
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public HomeStatisticsModel GetHomeStatistics(OrderListParam param)
        {
            try
            {
                HomeStatisticsModel model = new HomeStatisticsModel();

                #region 检索条件
                string owhere = "";
                string twhere = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }

                if (!UserId.Contains(param.userId))
                {
                    owhere = " WHERE (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND t.IsDelete = 0 ";
                    twhere = " WHERE (YT_OrderUser.Order_UserId = '" + param.userId + "' OR YT_OrderUser.CreateUserId = '" + param.userId + "') AND YT_OrderUser.IsDelete = 0 ";
                }
                else
                {
                    owhere = " WHERE t.IsDelete = 0 ";
                    twhere = " WHERE YT_OrderUser.IsDelete = 0 ";
                }

                string startTime = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
                string endTime = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
                #endregion

                StringBuilder UnfinishedOrderSql = new StringBuilder();
                UnfinishedOrderSql.Append(@"SELECT
	                        COUNT (Id) AS cou
                        FROM
	                        YT_Order t
                        " + owhere + @"
                        AND t.Order_Status != 5
	                    AND t.Order_Status != 3");
                DataTable uodt = new RepositoryFactory().BaseRepository().FindTable(UnfinishedOrderSql.ToString());
                model.UnfinishedOrder = uodt.Rows[0]["cou"].ToInt();

                StringBuilder UnfinishedTaskSql = new StringBuilder();
                UnfinishedTaskSql.Append(@"SELECT
	                        COUNT (Id) AS cou
                        FROM
	                        YT_OrderUser
                        " + twhere + @"
                        AND YT_OrderUser.IsApprover = 0 ");
                DataTable utdt = new RepositoryFactory().BaseRepository().FindTable(UnfinishedTaskSql.ToString());
                model.UnfinishedTask = utdt.Rows[0]["cou"].ToInt();

                StringBuilder TodayOrderSql = new StringBuilder();
                TodayOrderSql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order t 
                    " + owhere + @"
                    AND t.SuccessDate BETWEEN '" + startTime + "' AND '" + endTime + @"'
                    AND t.Order_Status = 5");
                DataTable todt = new RepositoryFactory().BaseRepository().FindTable(TodayOrderSql.ToString());
                model.TodayOrder = todt.Rows[0]["cou"].ToInt();

                StringBuilder TodayTaskSql = new StringBuilder();
                TodayTaskSql.Append(@"SELECT
	                        COUNT (Id) AS cou
                        FROM
	                        YT_OrderUser
                        " + twhere + @"
                    AND YT_OrderUser.Accomplish BETWEEN '" + startTime + "' AND '" + endTime + @"'
                    AND YT_OrderUser.IsApprover = 1");
                DataTable ttdt = new RepositoryFactory().BaseRepository().FindTable(TodayTaskSql.ToString());
                model.TodayTask = ttdt.Rows[0]["cou"].ToInt();

                StringBuilder PartTypesSql = new StringBuilder();
                PartTypesSql.Append(@"SELECT
	                    SUM (
		                    ISNULL(Success, 0) + ISNULL(Error, 0)
	                    ) AS cou
                        FROM
	                        YT_OrderUser
                        " + twhere + @"
                    AND YT_OrderUser.Accomplish BETWEEN '" + startTime + "' AND '" + endTime + @"'
                    AND YT_OrderUser.IsApprover = 1");
                DataTable ptdt = new RepositoryFactory().BaseRepository().FindTable(PartTypesSql.ToString());
                model.PartTypes = ptdt.Rows[0]["cou"].ToInt();
                return model;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取今日完成任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetTodayTaskList(OrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }
                if (!UserId.Contains(param.userId))
                {
                    where = " WHERE (YT_OrderUser.Order_UserId = '" + param.userId + "' OR YT_OrderUser.CreateUserId = '" + param.userId + "') AND YT_OrderUser.IsDelete = 0 ";
                }
                else
                {
                    where = " WHERE YT_OrderUser.IsDelete = 0 ";
                }
                string startTime = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
                string endTime = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        CONVERT (VARCHAR(100), t.[Date], 23) AS DATE,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(YT_OrderUser.Accomplish, '') AS Accomplish,
	                        CONVERT (
		                        VARCHAR (100),
		                        YT_OrderUser.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(t.SuccessDate, '') AS SuccessDate,
	                        ISNULL(YT_OrderUser.Id, '') AS TaskId,
	                        ISNULL(YT_OrderUser.IsApprover, 0) AS IsApprover
                        FROM
	                        YT_OrderUser
                        LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = YT_OrderUser.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        " + where + @"
                    AND YT_OrderUser.Accomplish BETWEEN '" + startTime + "' AND '" + endTime + @"'
                    AND YT_OrderUser.IsApprover = 1");
                param.pagination.sidx = "CreateDate";
                param.pagination.sord = "desc";
                list = this.BaseRepository().FindList<OrderTaskListModel>(sql.ToString(), param.pagination).AsList();
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取未完成任务列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetUnfinishedTaskList(OrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }
                if (!UserId.Contains(param.userId))
                {
                    where = " WHERE (YT_OrderUser.Order_UserId = '" + param.userId + "' OR YT_OrderUser.CreateUserId = '" + param.userId + "') AND YT_OrderUser.IsDelete = 0 ";
                }
                else
                {
                    where = " WHERE YT_OrderUser.IsDelete = 0 ";
                }
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        CONVERT (VARCHAR(100), t.[Date], 23) AS DATE,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(YT_OrderUser.Accomplish, '') AS Accomplish,
	                        CONVERT (
		                        VARCHAR (100),
		                        YT_OrderUser.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(t.SuccessDate, '') AS SuccessDate,
	                        ISNULL(YT_OrderUser.Id, '') AS TaskId,
	                        ISNULL(YT_OrderUser.IsApprover, 0) AS IsApprover
                        FROM
	                        YT_OrderUser
                        LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = YT_OrderUser.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        " + where + @"
                        AND YT_OrderUser.IsApprover = 0 ");
                param.pagination.sidx = "CreateDate";
                param.pagination.sord = "desc";
                list = this.BaseRepository().FindList<OrderTaskListModel>(sql.ToString(), param.pagination).AsList();
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 今日完成订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetTodayOrderList(OrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }
                if (!UserId.Contains(param.userId))
                {
                    where = " WHERE (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND t.IsDelete = 0 ";
                }
                else
                {
                    where = " WHERE t.IsDelete = 0 ";
                }
                string startTime = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
                string endTime = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        " + where + @"
                        AND t.SuccessDate BETWEEN '" + startTime + "' AND '" + endTime + @"'
                        AND t.Order_Status = 5");
                param.pagination.sidx = "CreateDate";
                param.pagination.sord = "desc";
                list = this.BaseRepository().FindList<OrderTaskListModel>(sql.ToString(), param.pagination).AsList();

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取未完成订单列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OrderTaskListModel> GetUnfinishedOrderList(OrderListParam param)
        {
            try
            {
                List<OrderTaskListModel> list = new List<OrderTaskListModel>();

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }
                if (!UserId.Contains(param.userId))
                {
                    where = " WHERE (t.CreateUserId = '" + param.userId + "' OR t.ModifyUserId = '" + param.userId + "') AND t.IsDelete = 0 ";
                }
                else
                {
                    where = " WHERE t.IsDelete = 0 ";
                }
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                        t.Id,
	                        t.OrderNumber,
	                        t.PartsNumber,
	                        ISNULL(YT_Supplier.Name, '') AS SupplierName,
	                        ISNULL(
		                        YT_PartsTypes.Parts_Name,
		                        ''
	                        ) AS PartsTypeName,
	                        ISNULL(t.[Count], 0) AS PartsCount,
	                        ISNULL(
		                        YT_MaintenanceTypes.Name,
		                        ''
	                        ) AS MaintainType,
	                        t.Order_Status,
	                        CONVERT (
		                        VARCHAR (100),
		                        t.CreateDate,
		                        23
	                        ) AS CreateDate,
	                        ISNULL(c.F_RealName, '') AS CreateUser,
	                        ISNULL(t.Thread, 0) AS Thread,
	                        ISNULL(t.Area, 0) AS Area,
	                        ISNULL(t.LineEdge, 0) AS LineEdge,
	                        CASE
                        WHEN t.RepairType = '0' THEN
	                        '挑选'
                        WHEN t.RepairType = '1' THEN
	                        '返修'
                        ELSE
	                        ''
                        END AS RepairType,
                         CASE
                        WHEN t.ModifyUserId = ''
                        OR t.ModifyUserId IS NULL THEN
	                        '0'
                        ELSE
	                        '1'
                        END AS Category,
                         ISNULL(t.SupplierStatus, 0) AS SupplierStatus
                        FROM
	                        YT_Order t
                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                        LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                        LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                        LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId
                        LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                        LEFT JOIN YT_OrderSuperaddition ON YT_OrderSuperaddition.OrderId = t.Id
                        " + where + @"
                        AND t.Order_Status != 5
	                    AND t.Order_Status != 3");
                param.pagination.sidx = "CreateDate";
                param.pagination.sord = "desc";
                list = this.BaseRepository().FindList<OrderTaskListModel>(sql.ToString(), param.pagination).AsList();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        if (item.Order_Status == 0 && item.SupplierStatus == 0)
                        {
                            item.Order_Status = 8;
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 本周完成零件统计
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<UserFinishPartTypesModel> GetUserPartTypesInfo(OrderListParam param)
        {
            try
            {
                List<UserFinishPartTypesModel> list = new List<UserFinishPartTypesModel>();

                #region 时间筛选
                DateTime dateTime = DateTime.Now;

                int weeknow = Convert.ToInt32(dateTime.DayOfWeek);
                //因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。
                weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
                int daydiff = (-1) * weeknow;
                //本周第一天
                string firstDay = dateTime.AddDays(daydiff).ToString("yyyy-MM-dd 00:00:00");
                string lastDay = Convert.ToDateTime(firstDay).AddDays(6).ToString("yyyy-MM-dd 23:59:59");
                #endregion

                #region 检索条件
                string where = "";

                List<string> UserId = new List<string>();
                string objects1 = Config.GetValue("OrderAndTaskManage");
                string objects2 = Config.GetValue("OrderManage");
                string objects3 = Config.GetValue("QualityManager");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + objects1 + "','" + objects2 + "','" + objects3 + "')  ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }
                if (!UserId.Contains(param.userId))
                {
                    where = " AND (YT_OrderUser.Order_UserId = '" + param.userId + "' OR YT_OrderUser.CreateUserId = '" + param.userId + "') ";
                }
                #endregion

                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT DISTINCT
	                    YT_PartsTypes.Id AS id,
	                    YT_PartsTypes.Parts_Name AS name
                    FROM
	                    YT_OrderUser
                    LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                    AND YT_PartsTypes.Parts_Number = YT_Order.PartsNumber
                    WHERE
	                    YT_Order.IsDelete = 0
                    AND YT_OrderUser.IsDelete = 0
                    AND YT_PartsTypes.IsDelete = 0
                    " + where + @"
                    AND YT_OrderUser.Accomplish BETWEEN '" + firstDay + @"'
                    AND '" + lastDay + "'");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserFinishPartTypesModel model = new UserFinishPartTypesModel();
                        model.name = dt.Rows[i]["name"].ToString();

                        StringBuilder csql = new StringBuilder();
                        csql.Append(@"SELECT
	                            SUM (
		                            ISNULL(YT_OrderUser.Success, 0) + ISNULL(YT_OrderUser.Error, 0)
	                            ) AS cou
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                            AND YT_PartsTypes.Parts_Number = YT_Order.PartsNumber
                            WHERE
	                            YT_Order.IsDelete = 0
                            AND YT_OrderUser.IsDelete = 0
                            AND YT_PartsTypes.IsDelete = 0
                            AND YT_OrderUser.PartsType = '" + dt.Rows[i]["id"].ToString() + @"'
                            " + where + @"
                            AND YT_OrderUser.Accomplish BETWEEN '" + firstDay + @"'
                            AND '" + lastDay + "'");
                        DataTable cdt = new RepositoryFactory().BaseRepository().FindTable(csql.ToString());
                        model.count = cdt.Rows[0]["cou"].ToInt();
                        list.Add(model);
                    }
                }
                else
                {
                    list = new List<UserFinishPartTypesModel>();
                }

                int sum = list.Sum(x => x.count.ToInt());
                foreach (var itemCount in list)
                {
                    if (sum != 0)
                    {
                        System.Decimal ftemp = (System.Decimal)itemCount.count.ToInt() / sum;
                        itemCount.percentage = Convert.ToDecimal(ftemp).ToString("f2");
                    }
                    else
                    {
                        itemCount.percentage = "0.00";
                    }

                }

                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取报告
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public YT_OrderStatisticsEntity GetStatistics(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_OrderStatisticsEntity>(keyValue);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 获取报告列表
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public List<StatisticsListModel> GetStatisticsList(string keyValue)
        {
            try
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                *
                FROM
	                YT_Statistics
                WHERE ParentId = '" + keyValue + @"'
                ORDER BY
	                CreateTime DESC");
                List<StatisticsListModel> list = new RepositoryFactory().BaseRepository().FindList<StatisticsListModel>(sql.ToString()).AsList();
                if (list.Count > 0)
                {
                    foreach (var newItem in list)
                    {
                        if (newItem.Unfinished.ToInt() < 0)
                        {
                            string a = newItem.Unfinished.ToString().Replace("-", string.Empty);
                            newItem.Unfinished = "超额" + a;
                        }
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 维修统计
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public MaintenanceCountStatisticsModel MaintenanceCountStatistics(string userId)
        {
            try
            {
                MaintenanceCountStatisticsModel model = new MaintenanceCountStatisticsModel();
                List<PartTypesCountModel> list = new List<PartTypesCountModel>();
                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT DISTINCT
	                    Base_User.F_UserId AS id,
	                    Base_User.F_RealName AS name
                    FROM
	                    YT_OrderUser
                    LEFT JOIN Base_User ON Base_User.F_UserId = YT_OrderUser.Order_UserId
                    LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                    AND YT_PartsTypes.Parts_Number = YT_Order.PartsNumber
                    WHERE
	                    YT_OrderUser.IsApprover = 1
                    AND YT_Order.IsDelete = 0
                    AND YT_OrderUser.IsDelete = 0");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
                if (dt.Rows.Count > 0)
                {
                    List<UserFinishCountModel> userList = new List<UserFinishCountModel>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserFinishCountModel userModel = new UserFinishCountModel();
                        StringBuilder csql = new StringBuilder();
                        csql.Append(@"SELECT
	                            SUM (
		                            ISNULL(Success, 0) + ISNULL(Error, 0)
	                            ) AS cou
                            FROM
	                            YT_OrderUser
                            LEFT JOIN Base_User ON Base_User.F_UserId = YT_OrderUser.Order_UserId
                            LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                            AND YT_PartsTypes.Parts_Number = YT_Order.PartsNumber
                            WHERE
	                            YT_OrderUser.IsApprover = 1
                            AND YT_Order.IsDelete = 0
                            AND YT_OrderUser.IsDelete = 0
                            AND YT_OrderUser.Order_UserId = '" + dt.Rows[i]["id"].ToString() + "'");
                        DataTable cdt = new RepositoryFactory().BaseRepository().FindTable(csql.ToString());
                        userModel.id = dt.Rows[i]["id"].ToString();
                        userModel.name = dt.Rows[i]["name"].ToString();
                        userModel.count = cdt.Rows[0]["cou"].ToInt();
                        userList.Add(userModel);

                        if (dt.Rows[i]["id"].ToString() == userId)
                        {
                            model.sum = cdt.Rows[0]["cou"].ToInt();
                        }
                    }
                    if (model.sum != 0)
                    {
                        int count = userList.Count;
                        int a = 0;
                        foreach (var item in userList)
                        {
                            if (item.id != userId)
                            {
                                if (model.sum > item.count)
                                {
                                    a += 1;
                                }
                            }
                        }
                        System.Decimal ftemp = (System.Decimal)a / count;
                        model.percentage = Convert.ToDecimal(ftemp * 100).ToString("f2") + "%";

                        StringBuilder msql = new StringBuilder();
                        msql.Append(@"SELECT DISTINCT
                              YT_PartsTypes.Id AS id,
	                          YT_PartsTypes.Parts_Name AS name,
	                          YT_Supplier.Name AS supplier
                            FROM
	                            YT_OrderUser
                            LEFT JOIN Base_User ON Base_User.F_UserId = YT_OrderUser.Order_UserId
                            LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                            AND YT_PartsTypes.Parts_Number = YT_Order.PartsNumber
                            LEFT JOIN YT_SupplierOrParts ON YT_SupplierOrParts.Parts_Id = YT_PartsTypes.Id
                            LEFT JOIN YT_Supplier ON YT_Supplier.Id = YT_SupplierOrParts.Supplier_Id
                            WHERE
	                            YT_OrderUser.IsApprover = 1
                            AND YT_Order.IsDelete = 0
                            AND YT_OrderUser.IsDelete = 0
                            AND YT_OrderUser.Order_UserId = '" + userId + "'");
                        DataTable mdt = new RepositoryFactory().BaseRepository().FindTable(msql.ToString());
                        if (mdt.Rows.Count > 0)
                        {
                            for (int f = 0; f < mdt.Rows.Count; f++)
                            {
                                PartTypesCountModel partTypes = new PartTypesCountModel();
                                partTypes.name = mdt.Rows[f]["name"].ToString();
                                partTypes.supplier = mdt.Rows[f]["supplier"].ToString();
                                StringBuilder tsql = new StringBuilder();
                                tsql.Append(@"SELECT
	                                    SUM (
		                                    ISNULL(Success, 0) + ISNULL(Error, 0)
	                                    ) AS cou
                                    FROM
	                                    YT_OrderUser
                                    LEFT JOIN Base_User ON Base_User.F_UserId = YT_OrderUser.Order_UserId
                                    LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                                    AND YT_PartsTypes.Parts_Number = YT_Order.PartsNumber
                                    WHERE
	                                    YT_OrderUser.IsApprover = 1
                                    AND YT_Order.IsDelete = 0
                                    AND YT_OrderUser.IsDelete = 0
                                    AND YT_OrderUser.Order_UserId = '" + userId + @"'
                                    AND YT_OrderUser.PartsType = '" + mdt.Rows[f]["id"].ToString() + "'");
                                DataTable tdt = new RepositoryFactory().BaseRepository().FindTable(tsql.ToString());
                                partTypes.count = tdt.Rows[0]["cou"].ToInt();
                                list.Add(partTypes);
                            }
                            model.list = list;
                        }
                    }
                    else
                    {
                        model.percentage = "0.00%";
                        model.list = new List<PartTypesCountModel>();
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                //YT_OrderEntity entity = GetYT_OrderEntity(keyValue);
                YT_OrderEntity entity = this.BaseRepository().FindEntity<YT_OrderEntity>(keyValue);
                List<YT_OrderUserEntity> list = new RepositoryFactory().BaseRepository().FindList<YT_OrderUserEntity>(t => t.Order_Id == entity.Id).AsList();
                foreach (var item in list)
                {
                    item.IsDelete = 1;
                    this.BaseRepository().Update<YT_OrderUserEntity>(item);
                }

                entity.IsDelete = 1;
                this.BaseRepository().Update<YT_OrderEntity>(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public static void WriteLog(string strLog)
        {
            string sFilePath = "C:\\" + DateTime.Now.ToString("yyyyMM");
            string sFileName = "rizhi" + DateTime.Now.ToString("dd") + ".log";
            sFileName = sFilePath + "\\" + sFileName; //文件的绝对路径
            if (!Directory.Exists(sFilePath))//验证路径是否存在
            {
                Directory.CreateDirectory(sFilePath);
                //不存在则创建
            }
            FileStream fs;
            StreamWriter sw;
            if (File.Exists(sFileName))
            //验证文件是否存在，有则追加，无则创建
            {
                fs = new FileStream(sFileName, FileMode.Append, FileAccess.Write);
            }
            else
            {
                fs = new FileStream(sFileName, FileMode.Create, FileAccess.Write);
            }
            sw = new StreamWriter(fs);
            sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "   ---   " + strLog);
            sw.Close();
            fs.Close();
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_OrderEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);

                    //获取库管
                    string OrderInventory = Config.GetValue("OrderInventory");
                    ///获取当天值班经理
                    StringBuilder strsql = new StringBuilder();

                    //strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + orderManage + "'");
                    if (entity.Area == 1)
                    {
                        strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + OrderInventory + "'");
                    }
                    else
                    {
                        strsql.Append(@"select Base_User.* from Base_User
                            WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
                    }

                    List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).AsList();
                    if (userList.Count != 0)
                    {
                        List<string> clientList = new List<string>();
                        foreach (var item in userList)
                        {
                            if (!string.IsNullOrEmpty(item.F_ClientId))
                            {
                                clientList.Add(item.F_ClientId);
                            }
                        }

                        PushModel push = new PushModel()
                        {
                            id = entity.Id,
                            title = entity.Area == 0 ? "您有一条新的订单消息请查收" : "请完善库存数量",
                            content = entity.Name + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "提交订单",
                            status = entity.Area == 0 ? 2 : 6,
                            type = 0

                        };
                        if (clientList.Count != 0)
                        {
                            Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                        }
                    }
                }
                else
                {
                    YT_SupplierEntity supplierEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(entity.Supplier_Id);
                    entity.OrderNumber = "YTX" + supplierEntity.Name.Replace(" ", "") + DateTime.Now.ToString("yyMMddHHmm");
                    YT_OrderEntity order = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(t => t.OrderNumber == entity.OrderNumber);
                    if (order != null)
                    {
                        throw new Exception("订单重复提交");
                    }
                    entity.Create();
                    this.BaseRepository().Insert(entity);

                    //订单流程
                    YT_OrderProcessEntity OrderProcessEntity = new YT_OrderProcessEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Order_Status = entity.Area == 1 ? 6 : 2,
                        Order_Id = entity.Id,
                        CreateDate = DateTime.Now,
                        CreateUser = entity.CreateUserId,

                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(OrderProcessEntity);
                    //获取报修工程师
                    string orderManage = Config.GetValue("OrderManage");
                    //获取供应商
                    string Supplier = Config.GetValue("Supplier");
                    //获取质检部经理
                    string Quality = Config.GetValue("QualityManager");
                    //获取库管
                    string OrderInventory = Config.GetValue("OrderInventory");

                    ///获取当天值班经理
                    StringBuilder strsql = new StringBuilder();

                    if (entity.Area == 1)
                    {
                        strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + OrderInventory + "'");
                    }
                    else
                    {
                        strsql.Append(@"select Base_User.* from Base_User
                            WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
                    }



                    List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).AsList();
                    if (userList.Count != 0)
                    {
                        List<string> clientList = new List<string>();
                        foreach (var item in userList)
                        {
                            if (!string.IsNullOrEmpty(item.F_ClientId))
                            {
                                clientList.Add(item.F_ClientId);
                            }
                        }

                        PushModel push = new PushModel()
                        {
                            id = entity.Id,
                            title = entity.Area == 0 ? "您有一条新的订单消息请查收" : "请完善库存数量",
                            content = entity.Name + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "提交订单",
                            status = entity.Area == 0 ? 2 : 6,
                            type = 0

                        };
                        if (clientList.Count != 0)
                        {
                            Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool VerifyOrder(string keyValue)
        {
            try
            {
                YT_OrderSuperadditionEntity orderSuperadditionEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderSuperadditionEntity>(t => t.OrderId == keyValue);
                if (orderSuperadditionEntity != null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存追加
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool SaveSuperadditionForm(string keyValue, YT_OrderSuperadditionEntity entity)
        {
            try
            {
                YT_OrderSuperadditionEntity orderSuperadditionEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderSuperadditionEntity>(t => t.OrderId == keyValue);
                if (orderSuperadditionEntity != null)
                {
                    return false;
                }
                entity.OrderId = keyValue;
                entity.Create();
                this.BaseRepository().Insert(entity);
                return true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 预估时间接口
        /// </summary>
        public bool estimateOrder(string estimate_date, string order_id, string user_id)
        {

            YT_OrderEntity entity = GetYT_OrderEntity(order_id);
            if (entity.Order_Status != 0)
            {
                return false;
            }
            entity.EstimateDate = estimate_date;
            entity.Order_Status = 1;
            this.BaseRepository().Update<YT_OrderEntity>(entity);
            //订单流程表
            YT_OrderProcessEntity orderProcessEntity = new YT_OrderProcessEntity()
            {
                Id = Guid.NewGuid().ToString(),
                Order_Id = order_id,
                CreateDate = DateTime.Now,
                CreateUser = user_id,
                Order_Status = 1
            };
            new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(orderProcessEntity);

            UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(user_id);

            //WX模板推送
            //openid
            UserEntity vxue = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(entity.CreateUserId);
            string openid = vxue.F_WeChat;
            //填写推送内容
            WX_MessageModel vxmessage = new WX_MessageModel();
            List<Datalist> ldl = new List<Datalist>();
            vxmessage.touser = openid;
            vxmessage.template_id = "4CW0JMaJcFdziqg6ysoYIfoL51VWSx7Za0cZYp_vUfk";
            vxmessage.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
            vxmessage.topcolor = "#FF0000";
            vxmessage.data = new Datalist()
            {
                first = new item()
                {
                    value = "您好！你提交的工单已经开始受理",
                    color = ""
                },

                keyword1 = new item()
                {
                    value = userEntity.F_RealName,
                    color = ""
                },
                keyword2 = new item()
                {
                    value = userEntity.F_Mobile,
                    color = ""
                },
                keyword3 = new item()
                {
                    value = "工程师预估" + estimate_date + "小时完成",
                    color = ""
                },
                remark = new item()
                {
                    value = "您可以点击详情查看。",
                    color = ""
                }

            };
            string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
            //获取access_token
            string access_token = getAccessToken();
            pushMassage(openid, jsonvxmessage, access_token);
            return true;
        }

        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="order_Id"></param>
        /// <param name="user_Id"></param>
        /// <returns></returns>
        public bool approvalOrder(string order_id, string user_id, int status)
        {
            YT_OrderEntity entity = GetYT_OrderEntity(order_id);
            if (entity.Order_Status != 1)
            {
                return false;
            }
            if (status == 0)
            {
                entity.Order_Status = 2;
                new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(entity);
                //流程
                YT_OrderProcessEntity orderProcessEntity = new YT_OrderProcessEntity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Order_Id = order_id,
                    CreateDate = DateTime.Now,
                    CreateUser = user_id,
                    Order_Status = 2
                };
                new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(orderProcessEntity);
                //获取报修工程师
                string orderManage = Config.GetValue("OrderManage");
                if (!string.IsNullOrEmpty(orderManage))
                {
                    StringBuilder strsql = new StringBuilder();
                    strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + orderManage + "'");
                    List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).AsList();
                    List<string> clientList = new List<string>();
                    foreach (var item in userList)
                    {
                        if (!string.IsNullOrEmpty(item.F_ClientId))
                        {
                            clientList.Add(item.F_ClientId);
                        }
                    }
                    PushModel push = new PushModel()
                    {
                        id = entity.Id,
                        title = "您有一条新的订单消息请查收",
                        content = entity.Name + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "同意",
                        status = 2,
                        type = 0

                    };
                    if (clientList.Count != 0)
                    {
                        GeTuiHelper.PushMessageToList(clientList, push);
                    }
                }

            }
            else
            {
                entity.Order_Status = 3;
                new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(entity);
                //流程
                YT_OrderProcessEntity orderProcessEntity = new YT_OrderProcessEntity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Order_Id = order_id,
                    CreateDate = DateTime.Now,
                    CreateUser = user_id,
                    Order_Status = 3
                };
                new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(orderProcessEntity);
                //获取报修工程师
                string orderManage = Config.GetValue("OrderManage");
                if (!string.IsNullOrEmpty(orderManage))
                {
                    StringBuilder strsql = new StringBuilder();
                    strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + orderManage + "'");
                    List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).AsList();
                    List<string> clientList = new List<string>();
                    foreach (var item in userList)
                    {
                        if (!string.IsNullOrEmpty(item.F_ClientId))
                        {
                            clientList.Add(item.F_ClientId);
                        }
                    }
                    PushModel push = new PushModel()
                    {
                        id = entity.Id,
                        title = "您有一条新的订单消息请查收",
                        content = entity.Name + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "拒绝",
                        status = 3,
                        type = 0

                    };
                    if (clientList.Count != 0)
                    {
                        GeTuiHelper.PushMessageToList(clientList, push);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 分配人员列表
        /// </summary>
        /// <returns></returns>
        public List<OrderUserModel> userList(Pagination pagination, string queryJson)
        {
            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT * FROM (SELECT Base_User.F_UserId AS user_id,Base_User.F_RealName AS user_name,
                                                                 Base_Company.F_FullName AS company,
                                                  CASE WHEN(SELECT COUNT(*) 
																                            FROM YT_OrderUser
																                            WHERE YT_OrderUser.IsDelete = 0 AND YT_OrderUser.IsApprover = 0
																			                            AND YT_OrderUser.Order_UserId = Base_User.F_UserId) = 0 THEN 0 ELSE 1 END AS status
							                            FROM  Base_User
							                            LEFT JOIN Base_Company ON Base_Company.F_CompanyId = Base_User.F_CompanyId
							                            where Base_User.F_DeleteMark = 0 AND Base_Company.F_CompanyId='53298b7a-404c-4337-aa7f-80b2a4ca6681')t
                            WHERE t.user_id!='system' ");
            if (!string.IsNullOrEmpty(queryJson))
            {
                strsql.Append(" and  t.user_name like '%" + queryJson + "%'");
            }
            pagination.sidx = "status";
            pagination.sord = "asc";
            List<OrderUserModel> list = new RepositoryFactory().BaseRepository().FindList<OrderUserModel>(strsql.ToString(), pagination).AsList();
            foreach (var item in list)
            {
                StringBuilder strsqluser = new StringBuilder();
                strsqluser.Append(@"SELECT top 3 YT_PartsTypes.Parts_Name,count(*) as count
                                    FROM YT_OrderUser
                                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_OrderUser.PartsType
                                    WHERE YT_OrderUser.Order_UserId = '" + item.user_id + @"'
                                    GROUP BY YT_PartsTypes.Parts_Name ORDER BY count DESC ");
                List<OrderUserGoodModel> goodsList = new RepositoryFactory().BaseRepository().FindList<OrderUserGoodModel>(strsqluser.ToString()).AsList();
                if (goodsList.Count != 0)
                {
                    string goods = "";
                    for (int i = 0; i < goodsList.Count; i++)
                    {
                        if (i == goodsList.Count - 1)
                        {
                            goods += goodsList[i].Parts_Name;
                        }
                        else
                        {
                            goods += goodsList[i].Parts_Name + " | ";
                        }
                    }
                    item.good = goods;
                }
                else
                {
                    item.good = "";
                }
            }
            return list;
        }

        /// <summary>
        /// 指派人员
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <param name="userList"></param>
        /// <returns></returns>
        public bool designate(string order_id, string user_id, List<string> userList, string all, string accomplish, int category)
        {
            YT_OrderEntity entity = GetYT_OrderEntity(order_id);
            if (entity.Order_Status == 0 || entity.Order_Status == 1 || entity.Order_Status == 3 || entity.Order_Status == 5 || entity.Order_Status == 6)
            {
                return false;
            }
            entity.Order_Status = 4;
            new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(entity);

            StringBuilder orderUserSql = new StringBuilder();
            orderUserSql.Append(@"SELECT SUM(CASE WHEN t.nosubmit IS NULL THEN 0 ELSE t.nosubmit END+CASE WHEN t.yessubmit IS NULL THEN 0 ELSE t.yessubmit END) AS [ALL]
                                    FROM (SELECT  CASE WHEN YT_OrderUser.IsApprover=0 THEN SUM(YT_OrderUser.[All]) END AS nosubmit,
				                                    CASE WHEN YT_OrderUser.IsApprover=1 THEN SUM(YT_OrderUser.Success+YT_OrderUser.Error) END AS yessubmit
                                    FROM YT_OrderUser
                                    WHERE YT_OrderUser.Order_Id='" + order_id + @"'
                                    GROUP BY YT_OrderUser.IsApprover)t");
            List<YT_OrderUserEntity> orderUserAll = new RepositoryFactory().BaseRepository().FindList<YT_OrderUserEntity>(orderUserSql.ToString()).AsList();
            //int parts_Count = userList.Count * (string.IsNullOrEmpty(all) ? 0 : Convert.ToInt32(all));
            //if (parts_Count+orderUserAll[0].All>entity.Count)
            //{
            //    return false;
            //}
            //订单流程
            YT_OrderProcessEntity orderProcessEntity = new YT_OrderProcessEntity()
            {
                Id = Guid.NewGuid().ToString(),
                Order_Id = order_id,
                Order_Status = 4,
                CreateUser = user_id,
                CreateDate = DateTime.Now
            };
            new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(orderProcessEntity);

            //人员配置
            if (userList.Count != 0)
            {
                foreach (var item in userList)
                {
                    UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(item);


                    YT_OrderUserEntity orderUserEntity = new YT_OrderUserEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Order_Id = order_id,
                        Order_UserId = item,
                        PartsType = entity.PartsType,
                        CreateDate = DateTime.Now,
                        IsDelete = 0,
                        All = string.IsNullOrEmpty(all) ? 0 : Convert.ToInt32(all),
                        Accomplish = accomplish,
                        IsApprover = 0,
                        Category = category

                    };
                    new RepositoryFactory().BaseRepository().Insert<YT_OrderUserEntity>(orderUserEntity);


                    if (!string.IsNullOrEmpty(userEntity.F_ClientId))
                    {
                        UserEntity userManage = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(user_id);
                        PushModel push = new PushModel()
                        {
                            id = orderUserEntity.Id,
                            title = "您有一条新的任务消息请查收",
                            content = userManage.F_RealName + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "给您指派了任务",
                            status = 4,
                            type = 1

                        };
                        List<string> cliendList = new List<string>();
                        cliendList.Add(userEntity.F_ClientId);
                        Task.Run(() => { GeTuiHelper.PushMessageToList(cliendList, push); });

                    }

                }

            }

            return true;
        }

        /// <summary>
        /// 通过工作效率计算工时
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public OrderWorkHoursModel getOrderWorkHours(OrderWorkHoursParams param)
        {
            OrderWorkHoursModel model = new OrderWorkHoursModel();
            if (string.IsNullOrEmpty(param.startDate))
            {
                throw new Exception("请输入工作开始时间！");
            }
            if (string.IsNullOrEmpty(param.count))
            {
                throw new Exception("请输入零件挑选总数！");
            }
            if (string.IsNullOrEmpty(param.workpieceRatio))
            {
                throw new Exception("请输入工作效率！");
            }
            if (string.IsNullOrEmpty(param.workers))
            {
                throw new Exception("请输入作业人数！");
            }
            YT_OrderEntity orderEntity = this.BaseRepository().FindEntity<YT_OrderEntity>(t => t.Id == param.orderId);
            if (orderEntity == null)
            {
                throw new Exception("该订单不存在！");
            }

            decimal allcount = 0;
            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT
	            ISNULL(SUM (
		            YT_OrderUser.Success + YT_OrderUser.Error
	            ),0) AS ac
                FROM
	                YT_OrderUser
                WHERE
	                Order_Id = '" + orderEntity.Id+@"'
                AND IsDelete = 0");
            DataTable dt = this.BaseRepository().FindTable(sql.ToString());
            if (dt.Rows.Count > 0)
            {
                allcount = dt.Rows[0]["ac"].ToDecimal();
            }

            DateTime startTime = Convert.ToDateTime(param.startDate);
            int people = param.workers.ToInt();
            decimal count = allcount;
            decimal workpieceRatio = param.workpieceRatio.ToDecimal();
            decimal workhour = count / (workpieceRatio * people);
            //总计工时
            decimal hour = Math.Round(workhour, 2, MidpointRounding.AwayFromZero);
            //相隔天数
            DateTime endTime = startTime.AddHours(hour.ToDouble());
   
            TimeSpan span = endTime.Subtract(startTime);
            int days = span.Days;
            if (span.Days == 0)
            {
                if (startTime.ToString("yyyy-MM-dd") != endTime.ToString("yyyy-MM-dd"))
                {
                    days = 1;
                }
            }
            //else
            //{
            //    days = span.Days + 1;
            //}

            DateTime successTime = endTime;
            decimal workday = 0;
            decimal weekend = 0;

            // 区域挑选
            if (orderEntity.Area == 1)
            {
                if (days == 0)
                {
                    if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 05:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 05:30:00")) <= successTime)
                    {
                        successTime = successTime.AddHours(0.5);
                    }
                    if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 11:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 11:30:00")) <= successTime)
                    {
                        successTime = successTime.AddHours(0.5);
                    }
                    if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 17:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 17:30:00")) <= successTime)
                    {
                        successTime = successTime.AddHours(0.5);
                    }
                    if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:30:00")) <= successTime)
                    {
                        successTime = successTime.AddHours(0.5);
                    }

                    var day = startTime.DayOfWeek;
                    if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                    {
                        weekend = hour.ToDecimal() * people;
                    }
                    else
                    {
                        workday = hour.ToDecimal() * people;
                    }
                }
                else
                {
                    for (int i = 0; i <= days; i++)
                    {
                        double a = 0;
                        if (i == 0)
                        {
                            if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 05:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 05:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 11:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 11:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 17:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 17:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            if (startTime <= Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:00:00")) && Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            var day = startTime.DayOfWeek;
                            DateTime ed = Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:59:59"));
                            TimeSpan ts = ed.Subtract(startTime);
                            double hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                            {
                                weekend += (hours - a).ToDecimal() * people;
                            }
                            else
                            {
                                workday += (hours - a).ToDecimal() * people;
                            }
                        }
                        else
                        {
                            DateTime std = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                            if (std <= Convert.ToDateTime(std.ToString("yyyy-MM-dd 05:00:00")) && Convert.ToDateTime(std.ToString("yyyy-MM-dd 05:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            if (std <= Convert.ToDateTime(std.ToString("yyyy-MM-dd 11:00:00")) && Convert.ToDateTime(std.ToString("yyyy-MM-dd 11:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            if (std <= Convert.ToDateTime(std.ToString("yyyy-MM-dd 17:00:00")) && Convert.ToDateTime(std.ToString("yyyy-MM-dd 17:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }
                            if (std <= Convert.ToDateTime(std.ToString("yyyy-MM-dd 23:00:00")) && Convert.ToDateTime(std.ToString("yyyy-MM-dd 23:30:00")) <= successTime)
                            {
                                successTime = successTime.AddHours(0.5);
                                a += 0.5;
                            }

                            var day = startTime.AddDays(i).DayOfWeek;

                            double hours = 0;
                            //判断是否为最后一天
                            if (Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 23:59:59")) >= successTime)
                            {
                                DateTime sd = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                                DateTime ed = successTime;
                                TimeSpan ts = ed.Subtract(sd);
                                 hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                DateTime sd = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                                DateTime ed = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 23:59:59"));
                                TimeSpan ts = ed.Subtract(sd);
                                hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            }
                            if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                            {
                                weekend += (hours - a).ToDecimal() * people;
                            }
                            else
                            {
                                workday += (hours - a).ToDecimal() * people;
                            }
                        }
                    }
                }
                model.estimateDate = (hour * people).ToString();
                model.threadElapsedTime = "0";
                model.lineEdgeElapsedTime = "0";
            }

            // 跟线挑选
            if (orderEntity.Thread == 1)
            {
                if (days == 0)
                {
                    var day = startTime.DayOfWeek;
                    if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                    {
                        weekend = hour.ToDecimal() * people;
                    }
                    else
                    {
                        workday = hour.ToDecimal() * people;
                    }
                }
                else
                {
                    for (int i = 0; i <= days; i++)
                    {
                        if (i == 0)
                        {
                            var day = startTime.DayOfWeek;
                            DateTime ed = Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:59:59"));
                            TimeSpan ts = ed.Subtract(startTime);
                            double hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                            {
                                weekend += hours.ToDecimal() * people;
                            }
                            else
                            {
                                workday += hours.ToDecimal() * people;
                            }
                        }
                        else
                        {
                            var day = startTime.AddDays(i).DayOfWeek;

                            double hours = 0;
                            //判断是否为最后一天
                            if (Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 23:59:59")) >= successTime)
                            {
                                DateTime sd = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                                DateTime ed = successTime;
                                TimeSpan ts = ed.Subtract(sd);
                                hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                DateTime sd = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                                DateTime ed = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 23:59:59"));
                                TimeSpan ts = ed.Subtract(sd);
                                hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            }
                            if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                            {
                                weekend += hours.ToDecimal() * people;
                            }
                            else
                            {
                                workday += hours.ToDecimal() * people;
                            }
                        }
                    }
                }
                model.estimateDate = "0";
                model.threadElapsedTime = (hour * people).ToString();
                model.lineEdgeElapsedTime = "0";
            }

            // 线边挑选
            if (orderEntity.LineEdge == 1)
            {
                if (days == 0)
                {
                    var day = startTime.DayOfWeek;
                    if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                    {
                        weekend = hour.ToDecimal() * people;
                    }
                    else
                    {
                        workday = hour.ToDecimal() * people;
                    }
                }
                else
                {
                    for (int i = 0; i <= days; i++)
                    {
                        if (i == 0)
                        {
                            var day = startTime.DayOfWeek;
                            DateTime ed = Convert.ToDateTime(startTime.ToString("yyyy-MM-dd 23:59:59"));
                            TimeSpan ts = ed.Subtract(startTime);
                            double hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                            {
                                weekend += hours.ToDecimal() * people;
                            }
                            else
                            {
                                workday += hours.ToDecimal() * people;
                            }
                        }
                        else
                        {
                            var day = startTime.AddDays(i).DayOfWeek;

                            double hours = 0;
                            //判断是否为最后一天
                            if (Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 23:59:59")) >= successTime)
                            {
                                DateTime sd = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                                DateTime ed = successTime;
                                TimeSpan ts = ed.Subtract(sd);
                                hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                DateTime sd = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 00:00:00"));
                                DateTime ed = Convert.ToDateTime(startTime.AddDays(i).ToString("yyyy-MM-dd 23:59:59"));
                                TimeSpan ts = ed.Subtract(sd);
                                hours = Math.Round(ts.TotalHours, 2, MidpointRounding.AwayFromZero);
                            }
                            if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                            {
                                weekend += hours.ToDecimal() * people;
                            }
                            else
                            {
                                workday += hours.ToDecimal() * people;
                            }
                        }
                    }
                }

                model.estimateDate = "0";
                model.threadElapsedTime = "0";
                model.lineEdgeElapsedTime = (hour * people).ToString();
            }

            model.successDate = successTime.ToString("yyyy-MM-dd HH:mm");
            model.workday = workday.ToString();
            model.weekend = weekend.ToString();
            model.statutoryHoliday = "0";
            return model;
        }

        /// <summary>
        /// 判断是否是工作日、周末、法定假日
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
         public int IsHolidayByDate(string date)
         {
            DateTime datettt = Convert.ToDateTime(date);
            var isHoliday = 0;
            System.Net.WebClient WebClientObj = new System.Net.WebClient();
            var PostVars = new System.Collections.Specialized.NameValueCollection
            {
               { "d", datettt.ToString("yyyyMMdd") }//参数
            };
             try
            {
                 var day = datettt.DayOfWeek;

                //判断是否为周末
                if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                {
                    return 1;
                }


                 //0为工作日，1为周末，2为法定节假日
                 var byteResult = WebClientObj.UploadValues("http://tool.bitefu.net/jiari/", "POST", PostVars);//请求地址,传参方式,参数集合
                 var result = Encoding.UTF8.GetString(byteResult);//获取返回值
                 if (result == "1" )
                {
                    isHoliday = 1;
                }
                if (result == "2")
                {
                    isHoliday = 2;
                }
                if (result == "0")
                {
                    isHoliday = 0;
                }
             }
             catch
             {
                 isHoliday = 3;
             }
            return isHoliday;
            }

        /// <summary>
        /// 订单完成
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="task_pic"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public int submitOrder(string order_id, string task_pic, string user_id, string estimate_date, string success_remark, string person, string count, string thread_time, string lineedge_time, string success_date, string sortingWay, string frequency,
            string workers, string workday, string weekend, string statutoryHoliday, string mealTimes,string start_date,string badProductDescribe,string workpieceRatio)
        {

            YT_OrderEntity entity = GetYT_OrderEntity(order_id);
            if (entity.Area == 0)
            {
                if (entity.Order_Status != 4)
                {
                    return 1;
                }
            }


            if (entity.Order_Status == 5)
            {
                return 2;
            }
            //分配总数
            entity.WorkpieceRatio = workpieceRatio;
            entity.AllotCount = entity.Count.ToString();
            entity.BadProductDescribe = badProductDescribe;
            entity.Order_Status = entity.Area == 0 ? 5 : 7;
            entity.Task_Pic = task_pic;
            //entity.EstimateDate = estimate_date;
            entity.Success_Remark = success_remark;
            entity.Person = person.ToInt();
            //entity.Count = count.ToInt();
            if (!string.IsNullOrEmpty(start_date) && !string.IsNullOrEmpty(success_date))
            {
                if (Convert.ToDateTime(start_date) > Convert.ToDateTime(success_date))
                {
                    return 4;
                }
            }
            entity.StartDate = start_date == null ? DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") : Convert.ToDateTime(start_date).ToString("yyyy-MM-dd HH:mm:ss");
            entity.SuccessDate = success_date == null ? DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") : Convert.ToDateTime(success_date).ToString("yyyy-MM-dd HH:mm:ss");
            entity.EstimateDate = estimate_date;
            entity.ThreadElapsedTime = thread_time;
            entity.LineEdgeElapsedTime = lineedge_time;
            entity.SortingWay = sortingWay;
            entity.Frequency = frequency;
            entity.Workers = workers;
            entity.Workday = workday;
            entity.Weekend = weekend;
            entity.StatutoryHoliday = statutoryHoliday;
            entity.MealTimes = mealTimes;
            new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(entity);
            //流程
            YT_OrderProcessEntity orderProcessEntity = new YT_OrderProcessEntity()
            {
                Id = Guid.NewGuid().ToString(),
                Order_Id = order_id,
                Order_Status = entity.Area == 0 ? 5 : 7,
                CreateDate = DateTime.Now,
                CreateUser = user_id
            };
            new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(orderProcessEntity);

            #region WX模板推送
            UserEntity userManage = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(user_id);

            if (entity.Area == 1)
            {

                string OrderAndTaskManage = Config.GetValue("OrderInventory");
                List<UserRelationEntity> userRelations = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(d => d.F_ObjectId == OrderAndTaskManage).ToList();
                if (userRelations.Count != 0)
                {
                    List<string> clientList = new List<string>();
                    foreach (var item in userRelations)
                    {
                        UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_UserId == item.F_UserId);
                        if (!string.IsNullOrEmpty(userEntity.F_ClientId))
                        {
                            clientList.Add(userEntity.F_ClientId);
                        }

                    }
                    PushModel push = new PushModel()
                    {
                        id = order_id,
                        title = "订单确认通知",
                        content = userManage.F_RealName + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "提交订单,请确认！。",
                        status = 7,
                        type = 4

                    };
                    if (clientList.Count != 0)
                    {
                        Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                    }


                }
                return 0;
            }
            #region 推送给供应商
            YT_SupplierEntity supplierEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(entity.Supplier_Id);
            if (supplierEntity != null)
            {
                string[] array = supplierEntity.Compellation.Split(',');
                if (array.Length != 0)
                {
                    foreach (var item in array)
                    {
                        UserEntity user = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_RealName == item);
                        if (!string.IsNullOrEmpty(user.F_WeChat))
                        {
                            string open = user.F_WeChat;
                            //填写推送内容
                            WX_MessageModel message = new WX_MessageModel();
                            List<Datalist> ldls = new List<Datalist>();
                            message.touser = open;
                            message.template_id = "tdZxgehDK_ze9WbXo_oRG66fO2-STMARy5H67QgXMLw";
                            message.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
                            message.topcolor = "#FF0000";
                            message.data = new Datalist()
                            {
                                first = new item()
                                {
                                    value = "您好，已维修完成",
                                    color = ""
                                },

                                keyword1 = new item()
                                {
                                    value = userManage.F_RealName,
                                    color = ""
                                },
                                keyword2 = new item()
                                {
                                    value = entity.SuccessDate,
                                    color = ""
                                },
                                remark = new item()
                                {
                                    value = "请查收。",
                                    color = ""
                                }

                            };
                            string jsonvx = JsonConvert.SerializeObject(message);
                            //获取access_token
                            string token = getAccessToken();
                            pushMassage(open, jsonvx, token);
                        }
                    }
                }
            }
            #endregion

            #region 推送给创建人
            if (!String.IsNullOrEmpty(entity.CreateUserId))
            {
                UserEntity vxue = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(entity.CreateUserId);
                string openid = vxue.F_WeChat;
                //填写推送内容
                WX_MessageModel vxmessage = new WX_MessageModel();
                List<Datalist> ldl = new List<Datalist>();
                vxmessage.touser = openid;
                vxmessage.template_id = "tdZxgehDK_ze9WbXo_oRG66fO2-STMARy5H67QgXMLw";
                vxmessage.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
                vxmessage.topcolor = "#FF0000";
                vxmessage.data = new Datalist()
                {
                    first = new item()
                    {
                        value = "您好，已维修完成",
                        color = ""
                    },

                    keyword1 = new item()
                    {
                        value = userManage.F_RealName,
                        color = ""
                    },
                    keyword2 = new item()
                    {
                        value = entity.SuccessDate,
                        color = ""
                    },
                    remark = new item()
                    {
                        value = "请查收。",
                        color = ""
                    }

                };
                string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
                //获取access_token
                string access_token = getAccessToken();
                pushMassage(openid, jsonvxmessage, access_token);
            }
            #endregion

            #region 推送给SQE
            if (entity.Pesponsible != null)
            {
                UserEntity pes = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(t => t.F_RealName == entity.Pesponsible);
                if (pes != null)
                {
                    string openId = pes.F_WeChat;
                    //填写推送内容
                    WX_MessageModel vxmess = new WX_MessageModel();
                    List<Datalist> ldlss = new List<Datalist>();
                    vxmess.touser = openId;
                    vxmess.template_id = "tdZxgehDK_ze9WbXo_oRG66fO2-STMARy5H67QgXMLw";
                    vxmess.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
                    vxmess.topcolor = "#FF0000";
                    vxmess.data = new Datalist()
                    {
                        first = new item()
                        {
                            value = "您好，已维修完成",
                            color = ""
                        },

                        keyword1 = new item()
                        {
                            value = userManage.F_RealName,
                            color = ""
                        },
                        keyword2 = new item()
                        {
                            value = entity.SuccessDate,
                            color = ""
                        },
                        remark = new item()
                        {
                            value = "请查收。",
                            color = ""
                        }

                    };
                    string json = JsonConvert.SerializeObject(vxmess);
                    //获取access_token
                    string accesstoken = getAccessToken();
                    pushMassage(openId, json, accesstoken);
                }
            }
            #endregion

            #endregion
            return 0;
        }


        /// <summary>
        /// 获取已分配、未分配数量
        /// </summary>
        /// <param name="order_id"></param>
        /// <returns></returns>
        public OrderPartsNumberModel getOrderPartsNumber(string order_id)
        {
            OrderPartsNumberModel model = new OrderPartsNumberModel();
            StringBuilder orderUserSql = new StringBuilder();
            orderUserSql.Append(@"SELECT SUM(CASE WHEN t.nosubmit IS NULL THEN 0 ELSE t.nosubmit END+CASE WHEN t.yessubmit IS NULL THEN 0 ELSE t.yessubmit END) AS [ALL]
                                    FROM (SELECT  CASE WHEN YT_OrderUser.IsApprover=0 THEN SUM(YT_OrderUser.[All]) END AS nosubmit,
				                                    CASE WHEN YT_OrderUser.IsApprover=1 THEN SUM(YT_OrderUser.Success+YT_OrderUser.Error) END AS yessubmit
                                    FROM YT_OrderUser
                                    WHERE YT_OrderUser.Order_Id='" + order_id + @"'
                                    GROUP BY YT_OrderUser.IsApprover)t");
            List<YT_OrderUserEntity> orderUserAll = new RepositoryFactory().BaseRepository().FindList<YT_OrderUserEntity>(orderUserSql.ToString()).AsList();
            model.allocated = orderUserAll[0].All == null ? 0 : orderUserAll[0].All.ToInt();
            YT_OrderEntity Entity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(order_id);
            model.unabsorbed = Entity.Count.ToInt() - model.allocated;
            return model;
        }

        /// <summary>
        /// 提交任务接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool submitTask(string task_id, string order_id, int success, int error, int unfinished)
        {


            YT_OrderUserEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderUserEntity>(task_id);

            if (entity.All != 0 && entity.All != null)
            {
                //if (entity.All != success + error + unfinished)
                //{
                //    return false;
                //}
                entity.IsApprover = 1;
                entity.Success = success;
                entity.Error = error;
                entity.Unfinished = entity.All.ToInt() - success - error;
                new RepositoryFactory().BaseRepository().Update<YT_OrderUserEntity>(entity);
            }
            else
            {
                entity.IsApprover = 1;
                entity.Success = success;
                entity.Error = error;
                entity.Unfinished = 0;
                entity.All = success + error;
                new RepositoryFactory().BaseRepository().Update<YT_OrderUserEntity>(entity);
            }


            StringBuilder successSql = new StringBuilder();
            successSql.Append(@"SELECT  CASE WHEN  SUM(YT_OrderUser.Success+YT_OrderUser.Error) IS NULL THEN 0 ELSE SUM(YT_OrderUser.Success+YT_OrderUser.Error) END AS Success
                                FROM YT_OrderUser
                                WHERE YT_OrderUser.IsApprover = 1 AND  YT_OrderUser.Order_Id = '" + entity.Order_Id + "' ");
            List<YT_OrderUserEntity> list = new RepositoryFactory().BaseRepository().FindList<YT_OrderUserEntity>(successSql.ToString()).AsList();
            if (list.Count != 0)
            {
                YT_OrderEntity orderEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(entity.Order_Id);
                if (orderEntity.Count == list[0].Success)
                {
                    orderEntity.Order_Status = 4;
                    new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(orderEntity);
                }
            }
            ///获取当天值班经理
            StringBuilder strsql = new StringBuilder();

            //strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + orderManage + "'");

            strsql.Append(@"select Base_User.* from Base_User
                            WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
            List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).AsList();
            if (userList.Count != 0)
            {
                List<string> clientList = new List<string>();
                foreach (var item in userList)
                {
                    if (!string.IsNullOrEmpty(item.F_ClientId))
                    {
                        clientList.Add(item.F_ClientId);
                    }
                }
                UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(entity.Order_UserId);
                PushModel push = new PushModel()
                {
                    id = entity.Order_Id,
                    title = "完成任务通知",
                    content = userEntity.F_RealName + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "提交任务",
                    status = 1,
                    type = 2

                };
                if (clientList.Count != 0)
                {
                    Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                }
            }
            return true;
        }
        /// <summary>
        /// WX
        /// </summary>
        /// <returns></returns>
        public string getAccessToken()
        {
            string access_token = string.Empty;
            string appid = Config.GetValue("WeChatAppId");
            string secret = Config.GetValue("WeChatSecret");
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&APPID=" + appid + "&secret=" + secret;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxAccessTokenModel jsonResult = JsonConvert.DeserializeObject<WxAccessTokenModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            access_token = jsonResult.access_token;
            return access_token;
        }
        /// <summary>
        /// WX
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="body"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        static string pushMassage(string openid, string body, string access_token)
        {
            //第二步组装推送数进行推送
            string url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.ContentType = "application/json";

            byte[] buffer = encoding.GetBytes(body);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        /// <summary>
        /// WX 订单审批
        /// </summary>
        /// <param name="model"></param>
        public bool WxApproveOrder(ApproveOrderParams model)
        {
            try
            {
                if (!String.IsNullOrEmpty(model.orderId))
                {
                    YT_OrderEntity orderEntity = this.BaseRepository().FindEntity<YT_OrderEntity>(model.orderId);
                    if (orderEntity == null)
                    {
                        return false;
                    }
                    if (model.status == 1)
                    {
                        orderEntity.Order_Status = 2;
                    }
                    if (model.status == 2)
                    {
                        orderEntity.Order_Status = 3;
                    }
                    orderEntity.SupplierStatus = model.status;
                    new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(orderEntity);
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 添加异常
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="content"></param>
        /// <param name="img"></param>
        public int SaveAbnormal(string orderId, string userId, string content, string img)
        {
            try
            {
                #region 权限判断
                List<string> UserId = new List<string>();
                string objects = Config.GetValue("SquadLeader");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + objects + "' ");
                List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                if (relationEntity.Count != 0)
                {
                    for (int i = 0; i < relationEntity.Count; i++)
                    {
                        UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                        UserId.Add(userEntity.F_UserId);
                    }
                }

                if (!UserId.Contains(userId))
                {
                    return 1;
                }
                #endregion

                YT_AbnormalRemindEntity entity = new YT_AbnormalRemindEntity();
                entity.Id = Guid.NewGuid().ToString();
                entity.OrderId = orderId;
                entity.Content = content;
                entity.Img = img;
                entity.CreateDate = DateTime.Now;
                entity.CreateUserId = userId;
                entity.IsDelete = 0;
                entity.Status = 0;
                new RepositoryFactory().BaseRepository().Insert<YT_AbnormalRemindEntity>(entity);

                #region 推送
                List<string> clientList = new List<string>();
                string manage = Config.GetValue("OrderAndTaskManage");
                string inventory = Config.GetValue("OrderInventory");
                StringBuilder UserSql = new StringBuilder();
                UserSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + manage + "','" + inventory + "') ");
                List<UserRelationEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserSql.ToString()).AsList();
                if (userList.Count != 0)
                {
                    for (int i = 0; i < userList.Count; i++)
                    {
                        UserEntity user = this.BaseRepository().FindEntity<UserEntity>(userList[i].F_UserId);
                        clientList.Add(user.F_ClientId);
                    }
                }

                PushModel push = new PushModel()
                {
                    id = entity.Id,
                    title = "异常记录通知",
                    content = "您有一条新的异常记录。",
                    status = 8,
                    type = 6
                };
                if (clientList.Count != 0)
                {
                    Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                }
                #endregion

                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 获取异常记录列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<AbnormalListModel> GetAbnormalList(GetAbnormalListParams param)
        {
            #region 筛选
            List<string> UserId = new List<string>();
            string manage = Config.GetValue("OrderAndTaskManage");
            string inventory = Config.GetValue("OrderInventory");
            StringBuilder UserSql = new StringBuilder();
            UserSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + manage + "','" + inventory + "') ");
            List<UserRelationEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserSql.ToString()).AsList();
            if (userList.Count != 0)
            {
                for (int i = 0; i < userList.Count; i++)
                {
                    UserEntity user = this.BaseRepository().FindEntity<UserEntity>(userList[i].F_UserId);
                    UserId.Add(user.F_UserId);
                }
            }
            if (!UserId.Contains(param.userId))
            {
                return new List<AbnormalListModel>();
            }
            #endregion

            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT
	                    t.Id AS id,
	                    t.Status AS status,
	                    CONVERT (
		                    VARCHAR (100),
		                    t.CreateDate,
		                    120
	                    ) AS createTime,
	                    Base_User.F_RealName AS userName,
	                    YT_Order.OrderNumber AS orderNumber,
	                    YT_PartsTypes.Parts_Name AS partName
                    FROM
	                    YT_AbnormalRemind t
                    LEFT JOIN Base_User ON Base_User.F_UserId = t.CreateUserId
                    LEFT JOIN YT_Order ON YT_Order.Id = t.OrderId
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                    WHERE
	                    t.IsDelete = 0");
            param.pagination.sidx = "createTime";
            param.pagination.sord = "DESC";
            return this.BaseRepository().FindList<AbnormalListModel>(sql.ToString(), param.pagination).AsList();
        }

        /// <summary>
        /// 获取异常详情
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public AbnormalInfoModel GetAbnormalInfo(string keyValue)
        {
            AbnormalInfoModel model = new AbnormalInfoModel();

            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT
	                t.Id AS id,
	                t.Content AS content
                FROM
	                YT_AbnormalRemind t
                WHERE
	                t.Id = '" + keyValue + "'");
            List<AbnormalInfoModel> list = new RepositoryFactory().BaseRepository().FindList<AbnormalInfoModel>(sql.ToString()).AsList();
            model = list[0];

            StringBuilder ImgSql = new StringBuilder();
            ImgSql.Append(@"SELECT
	                F_FilePath AS Img_Url
                FROM
	                Base_AnnexesFile
                LEFT JOIN YT_AbnormalRemind ON YT_AbnormalRemind.Img = Base_AnnexesFile.F_FolderId
                WHERE
	                YT_AbnormalRemind.Id = '" + model.id + @"'
                ORDER BY
	                Base_AnnexesFile.F_CreateDate ASC");
            List<ImageUrl> ImgList = new RepositoryFactory().BaseRepository().FindList<ImageUrl>(ImgSql.ToString()).AsList();
            if (ImgList.Count != 0)
            {
                foreach (var itemlist in ImgList)
                {
                    String a = itemlist.Img_Url.Substring(itemlist.Img_Url.LastIndexOf("yt_new/") + 7);
                    itemlist.Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                }
            }
            model.pic = ImgList;

            return model;
        }

        /// <summary>
        /// 异常审批
        /// </summary>
        /// <param name="param"></param>
        public int AbnormalApprove(AbnormalApproveParams param)
        {
            #region 权限判断
            List<string> UserId = new List<string>();
            string objects = Config.GetValue("OrderInventory");
            StringBuilder UserRelationSql = new StringBuilder();
            UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + objects + "' ");
            List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
            if (relationEntity.Count != 0)
            {
                for (int i = 0; i < relationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                    UserId.Add(userEntity.F_UserId);
                }
            }

            if (!UserId.Contains(param.userId))
            {
                return 1;
            }
            #endregion

            YT_AbnormalRemindEntity entity = this.BaseRepository().FindEntity<YT_AbnormalRemindEntity>(param.id);
            if (entity == null)
            {
                return 2;
            }
            entity.ModifyDate = DateTime.Now;
            entity.ModifyUserId = param.userId;
            entity.Status = param.status;
            new RepositoryFactory().BaseRepository().Update<YT_AbnormalRemindEntity>(entity);

            return 0;
        }



        /// <summary>
        /// 发送日志报告
        /// </summary>
        /// <param name="StartTime"></param>
        /// <param name="EndTime"></param>
        /// <param name="UserId"></param>
        /// <param name="HeirUser"></param>
        /// <param name="MattersAttention"></param>
        /// <returns></returns>
        public LogReportResponseModel SendMail(string StartTime, string EndTime, string UserId, string HeirUser,string MattersAttention)
        {
            try
            {
                LogReportResponseModel model = new LogReportResponseModel();
                DateTime dtStart = Convert.ToDateTime(StartTime.Replace("20%", " "));
                DateTime dtEnd = Convert.ToDateTime(EndTime.Replace("20%", " "));

                if (!String.IsNullOrEmpty(HeirUser))
                {
                    List<string> UserIds = new List<string>();
                    string objects = Config.GetValue("SquadLeader");
                    StringBuilder UserRelationSql = new StringBuilder();
                    UserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + objects + "' ");
                    List<UserRelationEntity> relationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(UserRelationSql.ToString()).AsList();
                    if (relationEntity.Count != 0)
                    {
                        for (int i = 0; i < relationEntity.Count; i++)
                        {
                            UserEntity userinfo = this.BaseRepository().FindEntity<UserEntity>(relationEntity[i].F_UserId);
                            if (userinfo != null)
                            {
                                UserIds.Add(userinfo.F_UserId);
                            }
                        }
                    }

                    if (!UserIds.Contains(UserId))
                    {
                        throw new Exception("交班日志只支持班长生成。");
                    }

                    YT_LogReportEntity logEntity = new YT_LogReportEntity();
                    logEntity.Create();
                    logEntity.Name = DateTime.Now.ToString("d") + "交班日志";
                    logEntity.HeirUser = HeirUser;
                    logEntity.CreateUser = UserId;
                    this.BaseRepository().Insert(logEntity);

                    UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_UserId == UserId);

                    StringBuilder sql = new StringBuilder();
                    sql.Append(@"SELECT
	                            t.OrderNumber AS OrderNumber,
	                            ISNULL(t.DrawingNumber, '') AS DrawingNumber,
	                            YT_Supplier.Name AS Supplier,
	                            t.PartsNumber AS PartsNumber,
	                            YT_PartsTypes.Parts_Name AS PartsType,
	                            t.Batch_Number AS BatchNumber,
	                            ISNULL(t.[Count], 0) AS cou,
	                            (
		                            SELECT
			                            ISNULL(
				                            SUM (
					                            YT_OrderUser.Success + YT_OrderUser.Error
				                            ),
				                            0
			                            )
		                            FROM
			                            YT_OrderUser
		                            WHERE
			                            Order_Id = t.Id
		                            AND IsDelete = 0
	                            ) AS [Sum],
	                            CASE
                            WHEN t.RepairType = '0' THEN
	                            '挑选'
                            WHEN t.RepairType = '1' THEN
	                            '返修'
                            ELSE
	                            ''
                            END AS RepairType,
                             ISNULL(
	                            YT_Instructor.IssueDescription,
	                            ''
                            ) AS IssueDescription,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (YT_OrderUser.Success),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Qualified,
                             (
	                            SELECT
		                            ISNULL(SUM(YT_OrderUser.Error), 0)
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Disqualified,
                             CONVERT (
	                            VARCHAR (100),
	                            t.CreateDate,
	                            120
                            ) AS CreateTime,
                             ISNULL(t.SuccessDate, '') AS SuccessDate,
                             (
	                            SELECT
		                            ISNULL(
			                            t.[Count] - SUM (
				                            YT_OrderUser.Success + YT_OrderUser.Error
			                            ),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Unfinished,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (
				                            YT_OrderUser.Error + YT_OrderUser.Success
			                            ),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Finish,
                             ISNULL(
	                            YT_MaintenanceTypes.Name,
	                            ''
                            ) AS MaintainType,
                            ISNULL(t.StartDate,'') AS StartDate,
                            ISNULL(t.AllotCount,'') AS AllotCount,
                            ISNULL(t.Person,'') AS Person,
                            ISNULL(t.BadProductDescribe,'') AS BadProductDescribe,
                            ISNULL(t.EstimateDate,0) AS EstimateDate,
                            ISNULL(t.ThreadElapsedTime,0) AS ThreadElapsedTime,
                            ISNULL(t.LineEdgeElapsedTime,0) AS LineEdgeElapsedTime,
                            ISNULL(t.Workday,0) AS Workday,
                            ISNULL(t.Weekend,0) AS Weekend,
                            ISNULL(t.StatutoryHoliday,0) AS StatutoryHoliday,
                            ISNULL(t.SortingWay,'') AS SortingWay
                            FROM
	                            YT_Order t
                            LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                            LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                            LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                            WHERE
	                            t.CreateDate BETWEEN '" + dtStart + @"'
                            AND '" + dtEnd + @"'
                            AND t.IsDelete = 0
                            AND t.Order_Status != 3
                            AND t.Order_Status != 0
                            ORDER BY YT_Supplier.Name");
                    DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        int cou = dt.Compute("sum(cou)", "TRUE").ToInt();
                        int sum = dt.Compute("sum(Sum)", "TRUE").ToInt();
                        model.finish = dt.Compute("sum(Finish)", "TRUE").ToInt();
                        model.unfinished = dt.Compute("sum(Unfinished)", "TRUE").ToInt();
                        model.qualified = dt.Compute("sum(Qualified)", "TRUE").ToInt();
                        model.disqualified = dt.Compute("sum(Disqualified)", "TRUE").ToInt();

                        //#region 生成EXCEL + 发送邮件
                        //// 文件名称
                        //string fileName = DateTime.Now.ToString("yyMMddHHmm") + "亚泰新推送吉利日报.xlsx";
                        //// 文件路径
                        //string filePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data\\" + "ExcelFiles/" + fileName);

                        ////判断是否有目录        
                        //string directoryName = Path.GetDirectoryName(filePath);
                        //if (!Directory.Exists(directoryName))
                        //{
                        //    Directory.CreateDirectory(directoryName);
                        //}

                        //// 定义 Workbook 对象
                        //XSSFWorkbook myWorkBook;
                        //// 定义 Worksheet 对象
                        //XSSFSheet myWorkSheet;
                        //try
                        //{
                        //    // 模板文件
                        //    string templetFilePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data/亚泰新推送吉利日报模板.xlsx");
                        //    // 用 FileStream 打开 Excel 模板文件
                        //    using (FileStream file = new FileStream(templetFilePath, FileMode.Open, FileAccess.ReadWrite))
                        //    {
                        //        // 获取 Workbook 对象
                        //        myWorkBook = new XSSFWorkbook(file);
                        //        // 获取 Worksheet 对象
                        //        myWorkSheet = myWorkBook.GetSheet("日报") as XSSFSheet;

                        //        ICellStyle style = myWorkBook.CreateCellStyle() as XSSFCellStyle;

                        //        style.Alignment = HorizontalAlignment.Center;  // 设置单元格居中
                        //        myWorkSheet.GetRow(2).CreateCell(2).SetCellValue(userEntity.F_RealName);
                        //        myWorkSheet.GetRow(2).CreateCell(5).SetCellValue(dtStart.ToString("f").ToString() + "至" + dtEnd.ToString("f").ToString());
                        //        int starRow = 4;
                        //        for (int i = 0; i < dt.Rows.Count; i++)
                        //        {
                        //            DataRow en = dt.Rows[i];
                        //            int rowNumber = starRow + i;
                        //            int nums = i + 1;
                        //            myWorkSheet.CreateRow(rowNumber).CreateCell(0).SetCellValue(nums);  //序号
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(1).SetCellValue(en[12].ToString());  //开始时间
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(2).SetCellValue(en[13].ToString());  //完成时间
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(3).SetCellValue(en[0].ToString());  //订单号
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(4).SetCellValue(en[1].ToString());  //图纸版本号
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(5).SetCellValue(en[2].ToString());  //供应商名称
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(6).SetCellValue(en[3].ToString());  //零件号
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(7).SetCellValue(en[4].ToString());  //零件名称
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(8).SetCellValue(en[5].ToString());  //批次号
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(9).SetCellValue(en[16].ToString());  //维修类型
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(10).SetCellValue(en[9].ToString());  //实际缺陷
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(11).SetCellValue(en[8].ToString());  //临时措施
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(12).SetCellValue(en[6].ToString());  //下单数量
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(13).SetCellValue(en[7].ToString());  //挑选零件数
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(14).SetCellValue(en[10].ToString());  //良品数量
                        //            myWorkSheet.GetRow(rowNumber).CreateCell(15).SetCellValue(en[11].ToString());  //不良品数量
                        //            if (en[14].ToInt() < 0)
                        //            {
                        //                string d = en[14].ToString().Replace("-", string.Empty);
                        //                myWorkSheet.GetRow(rowNumber).CreateCell(16).SetCellValue("异常数量" + d);  //待挑选数量
                        //            }
                        //            else
                        //            {
                        //                myWorkSheet.GetRow(rowNumber).CreateCell(16).SetCellValue(en[14].ToString());  //待挑选数量
                        //            }
                        //        }
                        //        int endRow = dt.Rows.Count + 4;
                        //        myWorkSheet.CreateRow(endRow).CreateCell(0).SetCellValue("合计");
                        //        myWorkSheet.GetRow(endRow).CreateCell(1).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(2).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(3).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(4).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(5).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(6).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(7).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(8).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(9).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(10).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(11).SetCellValue("");
                        //        myWorkSheet.GetRow(endRow).CreateCell(12).SetCellValue(cou);
                        //        myWorkSheet.GetRow(endRow).CreateCell(13).SetCellValue(sum);
                        //        myWorkSheet.GetRow(endRow).CreateCell(14).SetCellValue(model.qualified);
                        //        myWorkSheet.GetRow(endRow).CreateCell(15).SetCellValue(model.disqualified);
                        //        if (model.unfinished < 0)
                        //        {
                        //            string d = model.unfinished.ToString().Replace("-", string.Empty);
                        //            myWorkSheet.GetRow(endRow).CreateCell(16).SetCellValue("异常数量" + model.unfinished);
                        //        }
                        //        else
                        //        {
                        //            myWorkSheet.GetRow(endRow).CreateCell(16).SetCellValue(model.unfinished);
                        //        }
                        //        //生成文件
                        //        FileStream newfile = new FileStream(filePath, FileMode.Create);
                        //        myWorkBook.Write(newfile);
                        //        newfile.Close();
                        //        file.Close();
                        //    }

                        //    #region 发送邮件
                        //    MailMessage mymail = new MailMessage();
                        //    //为该电子邮件添加附件   附件的路径     
                        //    mymail.Attachments.Add(new Attachment(filePath));
                        //    //发件人地址 
                        //    mymail.From = new MailAddress("it-service@yataixin.com.cn");
                        //    //收件人地址
                        //    mymail.To.Add(new MailAddress("yunfei.li@geely.com"));
                        //    mymail.To.Add(new MailAddress("Shuo.Fan@geely.com"));
                        //    mymail.To.Add(new MailAddress("Jinye.Zhao@geely.com"));
                        //    mymail.To.Add(new MailAddress("hulibin@geely.com"));
                        //    mymail.To.Add(new MailAddress("xiang.jin@geely.com"));
                        //    mymail.To.Add(new MailAddress("Zhiyong.Tu@geely.com"));
                        //    mymail.To.Add(new MailAddress("Zhen.Xu01@geely.com"));
                        //    mymail.To.Add(new MailAddress("jun.yang05@geely.com"));
                        //    mymail.To.Add(new MailAddress("Wanshun.Zhao@geely.com"));
                        //    mymail.To.Add(new MailAddress("Ling.li08@geely.com"));
                        //    //邮件主题
                        //    mymail.Subject = "亚泰新推送吉利日报";
                        //    //邮件标题编码
                        //    mymail.SubjectEncoding = System.Text.Encoding.UTF8;
                        //    //发送邮件的内容
                        //    mymail.Body = "";
                        //    //邮件内容编码
                        //    mymail.BodyEncoding = System.Text.Encoding.UTF8;
                        //    //是否是HTML邮件
                        //    mymail.IsBodyHtml = false;
                        //    //邮件优先级
                        //    mymail.Priority = MailPriority.High;
                        //    //创建一个邮件服务器类  
                        //    SmtpClient myclient = new SmtpClient();
                        //    //邮箱服务器地址，不同的邮箱不同
                        //    myclient.Host = "smtp.263.net";
                        //    //SMTP服务端口s
                        //    myclient.Port = 587;
                        //    myclient.EnableSsl = true;
                        //    //验证登录   @"输入有效的邮箱名, "*"输入有效的密码
                        //    myclient.Credentials = new NetworkCredential("it-service@yataixin.com.cn", "pass@word");
                        //    myclient.Send(mymail);
                        //    #endregion
                        //}
                        //catch (Exception e)
                        //{
                        //    WriteLog(e.Message);
                        //    throw new Exception("邮件发送失败。");
                        //}
                        //#endregion

                        #region 保存数据
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            YT_ReportInfoEntity infoEntity = new YT_ReportInfoEntity();
                            infoEntity.Create();
                            infoEntity.ParentId = logEntity.Id;
                            infoEntity.OrderNumber = dt.Rows[i]["OrderNumber"].ToString();
                            infoEntity.Supplier = dt.Rows[i]["Supplier"].ToString();
                            infoEntity.PartsNumber = dt.Rows[i]["PartsNumber"].ToString();
                            infoEntity.PartsType = dt.Rows[i]["PartsType"].ToString();
                            infoEntity.BatchNumber = dt.Rows[i]["BatchNumber"].ToString();
                            infoEntity.Sum = dt.Rows[i]["Sum"].ToInt();
                            infoEntity.Qualified = dt.Rows[i]["Qualified"].ToInt();
                            infoEntity.Disqualified = dt.Rows[i]["Disqualified"].ToInt();
                            infoEntity.Finish = dt.Rows[i]["Finish"].ToInt();
                            infoEntity.Unfinished = dt.Rows[i]["Unfinished"].ToInt();
                            infoEntity.CreateUser = UserId;
                            string sd = dt.Rows[i]["StartDate"].ToString();
                            if (!string.IsNullOrEmpty(sd))
                            {
                                infoEntity.StartDate = sd;
                            }
                            string ed = dt.Rows[i]["SuccessDate"].ToString();
                            if (!string.IsNullOrEmpty(ed))
                            {
                                infoEntity.SuccessDate = ed;
                            }
                            infoEntity.AllotCount = dt.Rows[i]["AllotCount"].ToString();
                            infoEntity.MaintainType = dt.Rows[i]["MaintainType"].ToString();
                            infoEntity.RepairType = dt.Rows[i]["SortingWay"].ToString();
                            infoEntity.Person = dt.Rows[i]["Person"].ToString();
                            decimal estime = dt.Rows[i]["EstimateDate"].ToDecimal();
                            decimal thtime = dt.Rows[i]["ThreadElapsedTime"].ToDecimal();
                            decimal litime = dt.Rows[i]["LineEdgeElapsedTime"].ToDecimal();
                            //decimal wotime = dt.Rows[i]["Workday"].ToDecimal();
                            //decimal wetime = dt.Rows[i]["Weekend"].ToDecimal();
                            //decimal sttime = dt.Rows[i]["StatutoryHoliday"].ToDecimal();
                            infoEntity.WorkHours = (estime + thtime + litime).ToString();
                            infoEntity.BadProductDescribe = dt.Rows[i]["BadProductDescribe"].ToString();
                            infoEntity.MattersAttention = MattersAttention;
                            infoEntity.Count = dt.Rows[i]["cou"].ToString();
                            this.BaseRepository().Insert(infoEntity);
                        }
                        #endregion
                    }
                    //else
                    //{
                    //    #region 生成EXCEL + 发送邮件

                    //    // 文件名称
                    //    string fileName = DateTime.Now.ToString("yyMMddHHmm") + "亚泰新推送吉利日报.xlsx";
                    //    // 文件路径
                    //    string filePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data\\" + "ExcelFiles/" + fileName);

                    //    //判断是否有目录        
                    //    string directoryName = Path.GetDirectoryName(filePath);
                    //    if (!Directory.Exists(directoryName))
                    //    {
                    //        Directory.CreateDirectory(directoryName);
                    //    }

                    //    // 定义 Workbook 对象
                    //    XSSFWorkbook myWorkBook;
                    //    // 定义 Worksheet 对象
                    //    XSSFSheet myWorkSheet;
                    //    try
                    //    {
                    //        // 模板文件
                    //        string templetFilePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data/亚泰新推送吉利日报模板.xlsx");
                    //        // 用 FileStream 打开 Excel 模板文件
                    //        using (FileStream file = new FileStream(templetFilePath, FileMode.Open, FileAccess.ReadWrite))
                    //        {
                    //            // 获取 Workbook 对象
                    //            myWorkBook = new XSSFWorkbook(file);
                    //            // 获取 Worksheet 对象
                    //            myWorkSheet = myWorkBook.GetSheet("日报") as XSSFSheet;

                    //            ICellStyle style = myWorkBook.CreateCellStyle() as XSSFCellStyle;

                    //            style.Alignment = HorizontalAlignment.Center;  // 设置单元格居中
                    //            myWorkSheet.GetRow(2).CreateCell(2).SetCellValue(userEntity.F_RealName);
                    //            myWorkSheet.GetRow(2).CreateCell(5).SetCellValue(dtStart.ToString("f").ToString() + "至" + dtEnd.ToString("f").ToString());
                    //            int starRow = 4;
                    //            for (int i = 0; i < dt.Rows.Count; i++)
                    //            {
                    //                DataRow en = dt.Rows[i];
                    //                int rowNumber = starRow + i;
                    //                int nums = i + 1;
                    //                myWorkSheet.CreateRow(rowNumber).CreateCell(0).SetCellValue(nums);
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(1).SetCellValue(en[0].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(2).SetCellValue(en[1].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(3).SetCellValue(en[2].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(4).SetCellValue(en[3].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(5).SetCellValue(en[4].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(6).SetCellValue(en[5].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(7).SetCellValue(en[6].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(8).SetCellValue(en[7].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(9).SetCellValue(en[8].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(10).SetCellValue(en[9].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(11).SetCellValue(en[10].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(12).SetCellValue(en[13].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(13).SetCellValue(en[11].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(14).SetCellValue(en[12].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(15).SetCellValue(en[14].ToString());
                    //                myWorkSheet.GetRow(rowNumber).CreateCell(16).SetCellValue(en[16].ToString());
                    //            }
                    //            int endRow = dt.Rows.Count + 4;
                    //            myWorkSheet.CreateRow(endRow).CreateCell(0).SetCellValue("合计");
                    //            myWorkSheet.GetRow(endRow).CreateCell(1).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(2).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(3).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(4).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(5).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(6).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(7).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(8).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(9).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(10).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(11).SetCellValue("");
                    //            myWorkSheet.GetRow(endRow).CreateCell(12).SetCellValue(0);
                    //            myWorkSheet.GetRow(endRow).CreateCell(13).SetCellValue(0);
                    //            myWorkSheet.GetRow(endRow).CreateCell(14).SetCellValue(0);
                    //            myWorkSheet.GetRow(endRow).CreateCell(15).SetCellValue(0);
                    //            myWorkSheet.GetRow(endRow).CreateCell(16).SetCellValue(0);


                    //            //生成文件
                    //            FileStream newfile = new FileStream(filePath, FileMode.Create);
                    //            myWorkBook.Write(newfile);
                    //            newfile.Close();
                    //            file.Close();
                    //        }

                    //        #region 发送邮件
                    //        MailMessage mymail = new MailMessage();
                    //        //为该电子邮件添加附件   附件的路径     
                    //        mymail.Attachments.Add(new Attachment(filePath));
                    //        //发件人地址 
                    //        mymail.From = new MailAddress("it-service@yataixin.com.cn");
                    //        //收件人地址
                    //        mymail.To.Add(new MailAddress("yunfei.li@geely.com"));
                    //        mymail.To.Add(new MailAddress("Shuo.Fan@geely.com"));
                    //        mymail.To.Add(new MailAddress("Jinye.Zhao@geely.com"));
                    //        mymail.To.Add(new MailAddress("hulibin@geely.com"));
                    //        mymail.To.Add(new MailAddress("xiang.jin@geely.com"));
                    //        mymail.To.Add(new MailAddress("Zhiyong.Tu@geely.com"));
                    //        mymail.To.Add(new MailAddress("Zhen.Xu01@geely.com"));
                    //        mymail.To.Add(new MailAddress("jun.yang05@geely.com"));
                    //        mymail.To.Add(new MailAddress("Wanshun.Zhao@geely.com"));
                    //        mymail.To.Add(new MailAddress("Ling.li08@geely.com"));
                    //        //邮件主题
                    //        mymail.Subject = "亚泰新推送吉利日报";
                    //        //邮件标题编码
                    //        mymail.SubjectEncoding = System.Text.Encoding.UTF8;
                    //        //发送邮件的内容
                    //        mymail.Body = "";
                    //        //邮件内容编码
                    //        mymail.BodyEncoding = System.Text.Encoding.UTF8;
                    //        //是否是HTML邮件
                    //        mymail.IsBodyHtml = false;
                    //        //邮件优先级
                    //        mymail.Priority = MailPriority.High;
                    //        //创建一个邮件服务器类  
                    //        SmtpClient myclient = new SmtpClient();
                    //        //邮箱服务器地址，不同的邮箱不同
                    //        myclient.Host = "smtp.263.net";
                    //        //SMTP服务端口s
                    //        myclient.Port = 587;
                    //        myclient.EnableSsl = true;
                    //        //验证登录   @"输入有效的邮箱名, "*"输入有效的密码
                    //        myclient.Credentials = new NetworkCredential("it-service@yataixin.com.cn", "pass@word");
                    //        myclient.Send(mymail);
                    //        #endregion
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        throw new Exception("邮件发送失败。");
                    //    }
                    //    #endregion
                    //}
                }
                else
                {
                    UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_UserId == UserId);

                    StringBuilder sql = new StringBuilder();
                    sql.Append(@"SELECT
	                            t.OrderNumber AS OrderNumber,
	                            ISNULL(t.DrawingNumber, '') AS DrawingNumber,
	                            YT_Supplier.Name AS Supplier,
	                            t.PartsNumber AS PartsNumber,
	                            YT_PartsTypes.Parts_Name AS PartsType,
	                            t.Batch_Number AS BatchNumber,
	                            ISNULL(t.[Count], 0) AS cou,
	                            (
		                            SELECT
			                            ISNULL(
				                            SUM (
					                            YT_OrderUser.Success + YT_OrderUser.Error
				                            ),
				                            0
			                            )
		                            FROM
			                            YT_OrderUser
		                            WHERE
			                            Order_Id = t.Id
		                            AND IsDelete = 0
	                            ) AS [Sum],
	                            CASE
                            WHEN t.RepairType = '0' THEN
	                            '挑选'
                            WHEN t.RepairType = '1' THEN
	                            '返修'
                            ELSE
	                            ''
                            END AS RepairType,
                             ISNULL(
	                            YT_Instructor.IssueDescription,
	                            ''
                            ) AS IssueDescription,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (YT_OrderUser.Success),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Qualified,
                             (
	                            SELECT
		                            ISNULL(SUM(YT_OrderUser.Error), 0)
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Disqualified,
                             CONVERT (
	                            VARCHAR (100),
	                            t.CreateDate,
	                            120
                            ) AS CreateTime,
                             ISNULL(t.SuccessDate, '') AS SuccessDate,
                             (
	                            SELECT
		                            ISNULL(
			                            t.[Count] - SUM (
				                            YT_OrderUser.Success + YT_OrderUser.Error
			                            ),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Unfinished,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (
				                            YT_OrderUser.Error + YT_OrderUser.Success
			                            ),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
	                            AND IsDelete = 0
                            ) AS Finish,
                             ISNULL(
	                            YT_MaintenanceTypes.Name,
	                            ''
                            ) AS MaintainType
                            FROM
	                            YT_Order t
                            LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                            LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                            LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                            WHERE
	                            t.CreateDate BETWEEN '" + dtStart + @"'
                            AND '" + dtEnd + @"'
                            AND t.IsDelete = 0
                            AND t.Order_Status != 3
                            AND t.Order_Status != 0
                            ORDER BY YT_Supplier.Name");
                    DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        int cou = dt.Compute("sum(cou)", "TRUE").ToInt();
                        int sum = dt.Compute("sum(Sum)", "TRUE").ToInt();
                        model.finish = dt.Compute("sum(Finish)", "TRUE").ToInt();
                        model.unfinished = dt.Compute("sum(Unfinished)", "TRUE").ToInt();
                        model.qualified = dt.Compute("sum(Qualified)", "TRUE").ToInt();
                        model.disqualified = dt.Compute("sum(Disqualified)", "TRUE").ToInt();

                        #region 生成EXCEL + 发送邮件

                        // 文件名称
                        string fileName = DateTime.Now.ToString("yyMMddHHmm") + "亚泰新推送吉利日报.xlsx";
                        // 文件路径
                        string filePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data\\" + "ExcelFiles/" + fileName);

                        //判断是否有目录        
                        string directoryName = Path.GetDirectoryName(filePath);
                        if (!Directory.Exists(directoryName))
                        {
                            Directory.CreateDirectory(directoryName);
                        }

                        // 定义 Workbook 对象
                        XSSFWorkbook myWorkBook;
                        // 定义 Worksheet 对象
                        XSSFSheet myWorkSheet;
                        try
                        {
                            // 模板文件
                            string templetFilePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data/亚泰新推送吉利日报模板.xlsx");
                            // 用 FileStream 打开 Excel 模板文件
                            using (FileStream file = new FileStream(templetFilePath, FileMode.Open, FileAccess.ReadWrite))
                            {
                                // 获取 Workbook 对象
                                myWorkBook = new XSSFWorkbook(file);
                                // 获取 Worksheet 对象
                                myWorkSheet = myWorkBook.GetSheet("日报") as XSSFSheet;

                                ICellStyle style = myWorkBook.CreateCellStyle() as XSSFCellStyle;

                                style.Alignment = HorizontalAlignment.Center;  // 设置单元格居中
                                myWorkSheet.GetRow(2).CreateCell(2).SetCellValue(userEntity.F_RealName);
                                myWorkSheet.GetRow(2).CreateCell(5).SetCellValue(dtStart.ToString("f").ToString() + "至" + dtEnd.ToString("f").ToString());
                                int starRow = 4;
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    DataRow en = dt.Rows[i];
                                    int rowNumber = starRow + i;
                                    int nums = i + 1;
                                    myWorkSheet.CreateRow(rowNumber).CreateCell(0).SetCellValue(nums);  //序号
                                    myWorkSheet.GetRow(rowNumber).CreateCell(1).SetCellValue(en[12].ToString());  //开始时间
                                    myWorkSheet.GetRow(rowNumber).CreateCell(2).SetCellValue(en[13].ToString());  //完成时间
                                    myWorkSheet.GetRow(rowNumber).CreateCell(3).SetCellValue(en[0].ToString());  //订单号
                                    myWorkSheet.GetRow(rowNumber).CreateCell(4).SetCellValue(en[1].ToString());  //图纸版本号
                                    myWorkSheet.GetRow(rowNumber).CreateCell(5).SetCellValue(en[2].ToString());  //供应商名称
                                    myWorkSheet.GetRow(rowNumber).CreateCell(6).SetCellValue(en[3].ToString());  //零件号
                                    myWorkSheet.GetRow(rowNumber).CreateCell(7).SetCellValue(en[4].ToString());  //零件名称
                                    myWorkSheet.GetRow(rowNumber).CreateCell(8).SetCellValue(en[5].ToString());  //批次号
                                    myWorkSheet.GetRow(rowNumber).CreateCell(9).SetCellValue(en[16].ToString());  //维修类型
                                    myWorkSheet.GetRow(rowNumber).CreateCell(10).SetCellValue(en[9].ToString());  //实际缺陷
                                    myWorkSheet.GetRow(rowNumber).CreateCell(11).SetCellValue(en[8].ToString());  //临时措施
                                    myWorkSheet.GetRow(rowNumber).CreateCell(12).SetCellValue(en[6].ToString());  //下单数量
                                    myWorkSheet.GetRow(rowNumber).CreateCell(13).SetCellValue(en[7].ToString());  //挑选零件数
                                    myWorkSheet.GetRow(rowNumber).CreateCell(14).SetCellValue(en[10].ToString());  //良品数量
                                    myWorkSheet.GetRow(rowNumber).CreateCell(15).SetCellValue(en[11].ToString());  //不良品数量
                                    if (en[14].ToInt() < 0)
                                    {
                                        string d = en[14].ToString().Replace("-", string.Empty);
                                        myWorkSheet.GetRow(rowNumber).CreateCell(16).SetCellValue("异常数量" + d);  //待挑选数量
                                    }
                                    else
                                    {
                                        myWorkSheet.GetRow(rowNumber).CreateCell(16).SetCellValue(en[14].ToString());  //待挑选数量
                                    }
                                }
                                int endRow = dt.Rows.Count + 4;
                                myWorkSheet.CreateRow(endRow).CreateCell(0).SetCellValue("合计");
                                myWorkSheet.GetRow(endRow).CreateCell(1).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(2).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(3).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(4).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(5).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(6).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(7).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(8).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(9).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(10).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(11).SetCellValue("");
                                myWorkSheet.GetRow(endRow).CreateCell(12).SetCellValue(cou);
                                myWorkSheet.GetRow(endRow).CreateCell(13).SetCellValue(sum);
                                myWorkSheet.GetRow(endRow).CreateCell(14).SetCellValue(model.qualified);
                                myWorkSheet.GetRow(endRow).CreateCell(15).SetCellValue(model.disqualified);
                                if (model.unfinished < 0)
                                {
                                    string d = model.unfinished.ToString().Replace("-", string.Empty);
                                    myWorkSheet.GetRow(endRow).CreateCell(16).SetCellValue("异常数量" + model.unfinished);
                                }
                                else
                                {
                                    myWorkSheet.GetRow(endRow).CreateCell(16).SetCellValue(model.unfinished);
                                }

                                //生成文件
                                FileStream newfile = new FileStream(filePath, FileMode.Create);
                                myWorkBook.Write(newfile);
                                newfile.Close();
                                file.Close();
                            }

                            #region 发送邮件
                            MailMessage mymail = new MailMessage();
                            //为该电子邮件添加附件   附件的路径     
                            mymail.Attachments.Add(new Attachment(filePath));
                            //发件人地址 
                            mymail.From = new MailAddress("it-service@yataixin.com.cn");
                            //收件人地址
                            mymail.To.Add(new MailAddress("yunfei.li@geely.com"));
                            mymail.To.Add(new MailAddress("Shuo.Fan@geely.com"));
                            mymail.To.Add(new MailAddress("Jinye.Zhao@geely.com"));
                            mymail.To.Add(new MailAddress("hulibin@geely.com"));
                            mymail.To.Add(new MailAddress("xiang.jin@geely.com"));
                            mymail.To.Add(new MailAddress("Zhiyong.Tu@geely.com"));
                            mymail.To.Add(new MailAddress("Zhen.Xu01@geely.com"));
                            mymail.To.Add(new MailAddress("jun.yang05@geely.com"));
                            mymail.To.Add(new MailAddress("Wanshun.Zhao@geely.com"));
                            mymail.To.Add(new MailAddress("Ling.li08@geely.com"));
                            //邮件主题
                            mymail.Subject = "亚泰新推送吉利日报";
                            //邮件标题编码
                            mymail.SubjectEncoding = System.Text.Encoding.UTF8;
                            //发送邮件的内容
                            mymail.Body = "";
                            //邮件内容编码
                            mymail.BodyEncoding = System.Text.Encoding.UTF8;
                            //是否是HTML邮件
                            mymail.IsBodyHtml = false;
                            //邮件优先级
                            mymail.Priority = MailPriority.High;
                            //创建一个邮件服务器类  
                            SmtpClient myclient = new SmtpClient();
                            //邮箱服务器地址，不同的邮箱不同
                            myclient.Host = "smtp.263.net";
                            //SMTP服务端口s
                            myclient.Port = 587;
                            myclient.EnableSsl = true;
                            //验证登录   @"输入有效的邮箱名, "*"输入有效的密码
                            myclient.Credentials = new NetworkCredential("it-service@yataixin.com.cn", "pass@word");
                            myclient.Send(mymail);
                            #endregion
                        }
                        catch (Exception e)
                        {
                            throw new Exception("邮件发送失败。");
                        }
                        #endregion

                        YT_ManagerReportEntity managerReportEntity = new YT_ManagerReportEntity();
                        managerReportEntity.Create();
                        managerReportEntity.Time = DateTime.Now;
                        managerReportEntity.CreateUser = UserId;
                        this.BaseRepository().Insert(managerReportEntity);
                    }
                }

                return model;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        ///  交班日志详情中的列表+详情
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public ReportDetailModel getReportDetail(string reportId)
        {
            ReportDetailModel model = new ReportDetailModel();
            List<ReportOrderInfoList> list = new List<ReportOrderInfoList>();
            List<YT_ReportInfoEntity> reportInfos = new RepositoryFactory().BaseRepository().FindList<YT_ReportInfoEntity>(t => t.ParentId == reportId).ToList();
            if (reportInfos.Count > 0)
            {
                model.mattersAttention = reportInfos[0].MattersAttention == null ? "" : reportInfos[0].MattersAttention;
                foreach (var item in reportInfos)
                {
                    ReportOrderInfoList info = new ReportOrderInfoList();
                    info.id = item.Id == null ? "" : item.Id;
                    info.startDate = item.StartDate == null ? "" : item.StartDate.Substring(0, item.StartDate.Length - 3);
                    info.successDate = item.SuccessDate == null ? "" : item.SuccessDate.Substring(0, item.SuccessDate.Length - 3);
                    info.supplier = item.Supplier == null ? "" : item.Supplier;
                    info.partsType = item.PartsType == null ? "" : item.PartsType;
                    info.partsNumber = item.PartsNumber == null ? "" : item.PartsNumber;
                    info.batchNumber = item.BatchNumber == null ? "" : item.BatchNumber;
                    info.allotCount = item.AllotCount == null ? "" : item.AllotCount;
                    info.maintainType = item.MaintainType == null ? "" : item.MaintainType;
                    info.repairType = item.RepairType == null ? "" : item.RepairType;
                    info.count = item.Count == null ? "" : item.Count;
                    info.qualified = item.Qualified.ToString() == null ? "" : item.Qualified.ToString();
                    info.disqualified = item.Disqualified.ToString() == null ? "" : item.Disqualified.ToString();
                    info.unfinished = item.Unfinished.ToString() == null ? "" : item.Unfinished.ToString();
                    info.person = item.Person == null ? "" : item.Person;
                    info.workHours = item.WorkHours == null ? "" : item.WorkHours;
                    info.badProductDescribe = item.BadProductDescribe == null ? "" : item.BadProductDescribe;
                    list.Add(info);
                }
                model.orders = list;
            }
            else
            {
                model.mattersAttention = "";
                model.orders = new List<ReportOrderInfoList>();
            }

            return model;
        }

        /// <summary>
        /// 日志交接
        /// </summary>
        /// <param name="keyValue"></param>
        public void ExchangeReport(string keyValue)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    YT_LogReportEntity logEntity = new YT_LogReportEntity();
                    logEntity.Modify(keyValue);
                    logEntity.HeirStatus = 1;
                    this.BaseRepository().Update(logEntity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 日志交接
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<ReportListModel> GetReportList(GetOrderListParam param)
        {
            try
            {
                string where = "";
                if (!string.IsNullOrEmpty(param.startTime) && !string.IsNullOrEmpty(param.endTime))
                {
                    if (Convert.ToDateTime(param.startTime) > Convert.ToDateTime(param.endTime))
                    {
                        throw new Exception("请检查时间选择是否正确");
                    }

                    DateTime start = Convert.ToDateTime(Convert.ToDateTime(param.startTime).ToString("yyyy-MM-dd 00:00:00"));
                    DateTime end = Convert.ToDateTime(Convert.ToDateTime(param.endTime).ToString("yyyy-MM-dd 23:59:59"));

                    where = " WHERE t.CreateDate BETWEEN '" + start + @"' AND '" + end + @"' ";
                }
                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT
	                    t.Id AS id,
	                    t.Name AS name,
	                    h.F_RealName AS heiruser,
	                    c.F_RealName AS createuser,
	                    t.HeirStatus AS heirstatus,
	                    ISNULL(CONVERT (VARCHAR(100), t.HeirTime, 23),'') AS heirtime,
	                    CONVERT (
		                    VARCHAR (100),
		                    t.CreateDate,
		                    23
	                    ) AS createtime,
	                    CASE
                    WHEN HeirUser = '" + param.userId + @"'
                    AND HeirStatus = '0' THEN
	                    1
                    ELSE
	                    0
                    END everconfirmed,
                    t.CreateDate AS createdate
                    FROM
	                    YT_LogReport t
                    LEFT JOIN Base_User h ON h.F_UserId = t.HeirUser
                    LEFT JOIN Base_User c ON c.F_UserId = t.CreateUser
                    " + where + @"");
                param.pagination.sidx = "t.CreateDate";
                param.pagination.sord = "DESC";
                List<ReportListModel> list = new RepositoryFactory().BaseRepository().FindList<ReportListModel>(sql.ToString(),param.pagination).AsList();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        StringBuilder strsql = new StringBuilder();
                        strsql.Append(@"SELECT
	                            OrderNumber AS ordernumber,
	                            Unfinished AS surplus
                            FROM
	                            YT_ReportInfo
                            WHERE
	                            ParentId = '" + item.id + @"'
                            AND Unfinished != '0'
                            ORDER BY
	                            CreateDate DESC");
                        List<OrderList> lists = new RepositoryFactory().BaseRepository().FindList<OrderList>(strsql.ToString()).AsList();
                        item.orderlist = lists;
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region 导出订单统计
        /// <summary>
        /// 导出订单统计
        /// </summary>
        /// <param name="context"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public string ExportOrderList(HttpContext context, string startTime, string endTime, string start, string end, string name, string supplier, string orderNumber, string partsNumber)
        {
            #region
            List<ATLRLAttachment> excelList = new List<ATLRLAttachment>();
            try
            {
                string fileurl = "";
                string startDate = "";
                string endDate = "";
                if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                {
                    startDate = Convert.ToDateTime(startTime).ToString("yyyy-MM-dd " + start);
                    endDate = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd " + end);
                }
                else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                {
                    startDate = Convert.ToDateTime(startTime).ToString("yyyy-MM-dd 00:00:00");
                    endDate = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd " + end);
                }
                else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                {
                    startDate = Convert.ToDateTime(startTime).ToString("yyyy-MM-dd " + start);
                    endDate = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd 23:59:59");
                }
                else
                {
                    startDate = Convert.ToDateTime(startTime).ToString("yyyy-MM-dd 00:00:00");
                    endDate = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd 23:59:59");
                }
                string where = " AND (CONVERT(varchar(100), t.SuccessDate, 120) >= '" + startDate + @"' AND CONVERT(varchar(100), t.SuccessDate, 120) <= '" + endDate + @"') AND t.SuccessDate is NOT NULL AND t.SuccessDate != '' ";
                if (!string.IsNullOrEmpty(name))
                {
                    where += "  AND t.Name  LIKE '%"+name+@"%'  ";
                }
                if (!string.IsNullOrEmpty(supplier))
                {
                    where += "  AND YT_Supplier.Name  LIKE '%" + supplier + @"%'  ";
                }
                if (!string.IsNullOrEmpty(orderNumber))
                {
                    where += "  AND t.OrderNumber  LIKE '%" + orderNumber + @"%'  ";
                }
                if (!string.IsNullOrEmpty(partsNumber))
                {
                    where += "  AND t.PartsNumber  LIKE '%" + partsNumber + @"%'  ";
                }

                StringBuilder str = new StringBuilder();
                str.Append(@"SELECT
                 t.Supplier_Id
                FROM
                 YT_Order t
                LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                WHERE
                 t.IsDelete = 0
                 "+where+@"
                AND t.Order_Status != 3
                GROUP BY
                 t.Supplier_Id");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(str.ToString());
                if (dt.Rows.Count > 0)
                {
                    int aa = dt.Rows.Count;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        YT_SupplierEntity supplierEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(dt.Rows[i]["Supplier_Id"].ToString());
                        if (supplierEntity != null)
                        {
                            StringBuilder strSql = new StringBuilder();
                            strSql.Append(@"SELECT
                                 ISNULL(t.StartDate, '') AS CreateTime,
                                 ISNULL(t.SuccessDate, '') AS SuccessDate,
                                 YT_Supplier.Name AS Supplier,
                                 t.PartsNumber AS PartsNumber,
                                 YT_PartsTypes.Parts_Name AS PartsType,
                                 t.Batch_Number AS BatchNumber,
                                 ISNULL(
                                  YT_Instructor.IssueDescription,
                                  '无标识'
                                 ) AS IssueDescription,
                                 ISNULL(t.SortingWay, '') AS SortingWay,
                                 ISNULL(t.Frequency, '0') AS Frequency,
                                 (
                                  SELECT
                                   ISNULL(
                                    SUM (YT_OrderUser.Success),
                                    0
                                   )
                                  FROM
                                   YT_OrderUser
                                  WHERE
                                   Order_Id = t.Id
                                  AND IsDelete = 0
                                 ) AS Qualified,
                                 (
                                  SELECT
                                   ISNULL(SUM(YT_OrderUser.Error), 0)
                                  FROM
                                   YT_OrderUser
                                  WHERE
                                   Order_Id = t.Id
                                  AND IsDelete = 0
                                 ) AS Disqualified,
                                 (
                                  SELECT
                                   ISNULL(
                                    SUM (
                                     YT_OrderUser.Success + YT_OrderUser.Error
                                    ),
                                    0
                                   )
                                  FROM
                                   YT_OrderUser
                                  WHERE
                                   Order_Id = t.Id
                                  AND IsDelete = 0
                                 ) AS [Sum],
                                 ISNULL(t.Workers, '0') AS Workers,
                                 ISNULL(t.Workday, '0') AS Workday,
                                 ISNULL(t.Weekend, '0') AS Weekend,
                                 ISNULL(t.StatutoryHoliday, '0') AS StatutoryHoliday,
                                 ISNULL(t.MealTimes, '0') AS MealTimes,
                                 CASE
                                WHEN t.RepairType = '0' THEN
                                 '挑选'
                                WHEN t.RepairType = '1' THEN
                                 '返修'
                                ELSE
                                 ''
                                END AS RepairType,
                                 t.OrderNumber AS OrderNumber,
                                 (
                                 SELECT
                                  ISNULL(
                                   t.[Count] - SUM (
                                    YT_OrderUser.Success + YT_OrderUser.Error
                                   ),
                                   0
                                  )
                                 FROM
                                  YT_OrderUser
                                 WHERE
                                  Order_Id = t.Id
                                 AND IsDelete = 0
                                ) AS Unfinished,
                                 (
                                 SELECT
                                  ISNULL(
                                   SUM (
                                    YT_OrderUser.Error + YT_OrderUser.Success
                                   ),
                                   0
                                  )
                                 FROM
                                  YT_OrderUser
                                 WHERE
                                  Order_Id = t.Id
                                 AND IsDelete = 0
                                ) AS Finish,
                                ISNULL(YT_Instructor.IssuePic, '') AS IssuePic,
	                            ISNULL(t.EstimateDate, '0') AS estim,
	                            ISNULL(t.ThreadElapsedTime, '0') AS thtim,
	                            ISNULL(t.LineEdgeElapsedTime, '0') AS litime,
                                ISNULL(t.Thread, 0) AS Thread,
                                ISNULL(t.Area, 0) AS Area,
                                ISNULL(t.LineEdge, 0) AS LineEdge
                                FROM
                                 YT_Order t
                                LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                                LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                                LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                                LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                                WHERE
	                                t.Order_Status  != 3
                                AND t.IsDelete = 0
                                AND t.Supplier_Id = '" + supplierEntity.Id + @"'
                                " + where + @"
                                ORDER BY
                                 t.CreateDate ASC");
                            DataTable dts = new RepositoryFactory().BaseRepository().FindTable(strSql.ToString());
                            if (dts.Rows.Count > 0)
                            {
                                int sum = dts.Compute("sum(Sum)", "TRUE").ToInt();
                                decimal workday = 0;
                                decimal weekend = 0;
                                decimal statutoryHoliday = 0;
                                decimal estim = 0;
                                decimal thtim = 0;
                                decimal litime = 0;
                                foreach (DataRow item in dts.Rows)
                                {
                                    workday += decimal.Parse(item["Workday"].ToString());
                                    weekend += decimal.Parse(item["Weekend"].ToString());
                                    statutoryHoliday += decimal.Parse(item["StatutoryHoliday"].ToString());
                                    string esti = item["estim"].ToString();
                                    estim += decimal.Parse(item["estim"].ToString());
                                    thtim += decimal.Parse(item["thtim"].ToString());
                                    litime += decimal.Parse(item["litime"].ToString());
                                }
                                int finish = dts.Compute("sum(Finish)", "TRUE").ToInt();
                                int unfinished = dts.Compute("sum(Unfinished)", "TRUE").ToInt();
                                int qualified = dts.Compute("sum(Qualified)", "TRUE").ToInt();
                                int disqualified = dts.Compute("sum(Disqualified)", "TRUE").ToInt();

                                string filename = DateTime.Now.ToString("yyMMddHHmmss") + "-" + supplierEntity.Name + "挑选返修技术服务汇总.xls";
                                // 文件路径
                                string filePath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data\\" + "ExcelFiles/" + filename);
                                //Excel模版

                                string masterPath = System.Web.HttpContext.Current.Server.MapPath("../../App_Data/亚泰新工业技术服务汇总报表模板.xls");

                                try
                                {
                                    //复制Excel模版
                                    File.Copy(masterPath, filePath);

                                    // 先把文件的属性读取出来 
                                    FileAttributes attrs = File.GetAttributes(filePath);

                                    // 下面表达式中的 1 是 FileAttributes.ReadOnly 的值 
                                    // 此表达式是把 ReadOnly 所在的位改成 0, 
                                    attrs = (FileAttributes)((int)attrs & ~(1));

                                    FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);

                                    #region sheet
                                    ISheet sheet = hssfworkbook.GetSheet("工时明细");
                                    int sheetStarRow = 4;
                                    for (int a = 0; a < dts.Rows.Count; a++)
                                    {
                                        DataRow en = dts.Rows[a];
                                        int rowNumber = sheetStarRow + a;
                                        int nums = a + 1;
                                        HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowNumber);
                                        dataRow.HeightInPoints = 60;
                                        dataRow.CreateCell(0).SetCellValue(nums);  //序号
                                        dataRow.CreateCell(1).SetCellValue(en[0].ToString());  //开始时间
                                        string ti = en[1].ToString();
                                        string edt = "";
                                        if (ti.Length == 20)
                                        {
                                            edt = ti.Substring(0, ti.Length - 1);
                                        }
                                        else if (ti.Length == 19)
                                        {
                                            edt = ti;
                                        }
                                        dataRow.CreateCell(2).SetCellValue(edt);  //完成时间
                                        dataRow.CreateCell(3).SetCellValue(en[2].ToString());  //供应商
                                        dataRow.CreateCell(4).SetCellValue(en[3].ToString());  //零件号
                                        dataRow.CreateCell(5).SetCellValue(en[4].ToString());  //零件名称
                                        dataRow.CreateCell(6).SetCellValue(en[5].ToString());  //批次号
                                        dataRow.CreateCell(7).SetCellValue(en[6].ToString());  //质检原因
                                        dataRow.CreateCell(8).SetCellValue(en[7].ToString());  //检测方法
                                        dataRow.CreateCell(9).SetCellValue(en[8].ToString());  //频次
                                        dataRow.CreateCell(10).SetCellValue(en[9].ToInt());  //良品数
                                        dataRow.CreateCell(11).SetCellValue(en[10].ToInt());  //不良品数量
                                        dataRow.CreateCell(12).SetCellValue(en[11].ToInt());  //总数
                                        if (en[11].ToString() == "0")
                                        {
                                            dataRow.CreateCell(13).SetCellValue("0%");  //合格率
                                            dataRow.CreateCell(14).SetCellValue("0%");  //不合格率
                                        }
                                        else
                                        {
                                            decimal h = (en[9].ToDecimal() / en[11].ToDecimal()) * 100;
                                            decimal b = (en[10].ToDecimal() / en[11].ToDecimal()) * 100;
                                            string hh = Math.Round(h, 2, MidpointRounding.AwayFromZero) + "%";
                                            string bb = Math.Round(b, 2, MidpointRounding.AwayFromZero) + "%";
                                            dataRow.CreateCell(13).SetCellValue(hh);  //合格率
                                            dataRow.CreateCell(14).SetCellValue(bb);  //不合格率
                                        }
                                        dataRow.CreateCell(15).SetCellValue(en[12].ToInt());  //作业人数
                                        dataRow.CreateCell(16).SetCellValue(en[13].ToDouble());  //工作日
                                        dataRow.CreateCell(17).SetCellValue(en[14].ToDouble());  //周末
                                        dataRow.CreateCell(18).SetCellValue(en[15].ToDouble());  //法定
                                        decimal workTimes = en[22].ToDecimal() + en[23].ToDecimal() + en[24].ToDecimal();
                                        dataRow.CreateCell(19).SetCellValue(workTimes.ToDouble());  //共计工时
                                        dataRow.CreateCell(20).SetCellValue(en[16].ToInt());  //用餐次数
                                        string remak = "";
                                        int thread = en[25].ToInt(); //根线
                                        int area = en[26].ToInt();   //库检
                                        int lineEdge = en[27].ToInt();  //线边
                                        if (thread == 1 && area == 1 && lineEdge == 1)
                                        {
                                            remak = "跟线；库检；线边";
                                        }
                                        else if (thread == 1 && area == 1 && lineEdge == 0)
                                        {
                                            remak = "跟线；库检";
                                        }
                                        else if (thread == 1 && area == 0 && lineEdge == 1)
                                        {
                                            remak = "跟线；线边";
                                        }
                                        else if (thread == 0 && area == 1 && lineEdge == 1)
                                        {
                                            remak = "库检；线边";
                                        }
                                        else if (thread == 0 && area == 0 && lineEdge == 1)
                                        {
                                            remak = "线边";
                                        }
                                        else if (thread == 0 && area == 1 && lineEdge == 0)
                                        {
                                            remak = "库检";
                                        }
                                        else if (thread == 1 && area == 0 && lineEdge == 0)
                                        {
                                            remak = "跟线";
                                        }
                                        dataRow.CreateCell(21).SetCellValue(remak);  //备注
                                        dataRow.CreateCell(22).SetCellValue(en[18].ToString());  //订单编号
                                    }
                                    int endRow = dts.Rows.Count + 4;
                                    HSSFRow dataEnd = (HSSFRow)sheet.CreateRow(endRow);
                                    dataEnd.HeightInPoints = 60;
                                    ICellStyle style = hssfworkbook.CreateCellStyle();
                                    //设置单元格的样式：水平对齐居中
                                    style.Alignment = HorizontalAlignment.Center;
                                    style.VerticalAlignment = VerticalAlignment.Center;
                                    //新建一个字体样式对象
                                    IFont font = hssfworkbook.CreateFont();
                                    font.FontHeightInPoints = 12;
                                    dataEnd.CreateCell(0).SetCellValue("合计");
                                    dataEnd.CreateCell(1).SetCellValue("");
                                    dataEnd.CreateCell(2).SetCellValue("");
                                    dataEnd.CreateCell(3).SetCellValue("");
                                    dataEnd.CreateCell(4).SetCellValue("");
                                    dataEnd.CreateCell(5).SetCellValue("");
                                    dataEnd.CreateCell(6).SetCellValue("");
                                    dataEnd.CreateCell(7).SetCellValue("");
                                    dataEnd.CreateCell(8).SetCellValue("");
                                    dataEnd.CreateCell(9).SetCellValue("");
                                    dataEnd.CreateCell(10).SetCellValue(qualified);
                                    dataEnd.CreateCell(11).SetCellValue(disqualified);
                                    dataEnd.CreateCell(12).SetCellValue(sum);
                                    if (sum == 0)
                                    {
                                        dataEnd.CreateCell(13).SetCellValue("0%");
                                        dataEnd.CreateCell(14).SetCellValue("0%");
                                    }
                                    else
                                    {
                                        decimal lp = (qualified.ToDecimal() / sum) * 100;
                                        decimal bl = (disqualified.ToDecimal() / sum) * 100;
                                        string lps = Math.Round(lp, 2, MidpointRounding.AwayFromZero) + "%";
                                        string blp = Math.Round(bl, 2, MidpointRounding.AwayFromZero) + "%";
                                        dataEnd.CreateCell(13).SetCellValue(lps);
                                        dataEnd.CreateCell(14).SetCellValue(blp);
                                    }
                                    dataEnd.CreateCell(15).SetCellValue("");
                                    dataEnd.CreateCell(16).SetCellValue(workday.ToString());
                                    dataEnd.CreateCell(17).SetCellValue(weekend.ToString());
                                    dataEnd.CreateCell(18).SetCellValue(statutoryHoliday.ToString());
                                    decimal zs = estim + thtim + litime;
                                    dataEnd.CreateCell(19).SetCellValue(zs.ToString());
                                    dataEnd.CreateCell(20).SetCellValue("");
                                    dataEnd.CreateCell(21).SetCellValue("");
                                    dataEnd.CreateCell(22).SetCellValue("");
                                    sheet.AddMergedRegion(new CellRangeAddress(endRow, endRow, 0, 9));
                                    //将新的样式赋给单元格
                                    style.SetFont(font);
                                    dataEnd.GetCell(0).CellStyle = style;
                                    #endregion

                                    #region sheet1
                                    ISheet sheet1 = hssfworkbook.GetSheet("图表");
                                    StringBuilder chartSql = new StringBuilder();
                                    chartSql.Append(@"SELECT
	                                                (
		                                                SELECT
			                                                ISNULL(
				                                                SUM (YT_OrderUser.Success),
				                                                0
			                                                )
		                                                FROM
			                                                YT_OrderUser
		                                                WHERE
			                                                Order_Id = t.Id
		                                                AND IsDelete = 0
	                                                ) AS Qualified,
	                                                (
		                                                SELECT
			                                                ISNULL(SUM(YT_OrderUser.Error), 0)
		                                                FROM
			                                                YT_OrderUser
		                                                WHERE
			                                                Order_Id = t.Id
		                                                AND IsDelete = 0
	                                                ) AS Disqualified,
	                                                (
		                                                SELECT
			                                                ISNULL(
				                                                SUM (
					                                                YT_OrderUser.Success + YT_OrderUser.Error
				                                                ),
				                                                0
			                                                )
		                                                FROM
			                                                YT_OrderUser
		                                                WHERE
			                                                Order_Id = t.Id
		                                                AND IsDelete = 0
	                                                ) AS [Sum],
	                                                ISNULL(t.Workday, '0') AS Workday,
	                                                ISNULL(t.Weekend, '0') AS Weekend,
	                                                ISNULL(t.StatutoryHoliday, '0') AS StatutoryHoliday,
	                                                ISNULL(t.EstimateDate, '0') AS estim,
	                                                ISNULL(t.ThreadElapsedTime, '0') AS thtim,
	                                                ISNULL(t.LineEdgeElapsedTime, '0') AS litime
                                                FROM
	                                                YT_Order t
                                                LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                                                LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                                                WHERE
	                                                t.Order_Status != 3
                                                AND t.IsDelete = 0
                                                AND t.Supplier_Id = '" + supplierEntity.Id + @"'
                                                " + where + @" ");
                                    DataTable chartdt = this.BaseRepository().FindTable(chartSql.ToString());
                                    if (chartdt.Rows.Count > 0)
                                    {
                                        int cSum = chartdt.Compute("sum(Sum)", "TRUE").ToInt();
                                        decimal cWorkday = 0;
                                        decimal cWeekend = 0;
                                        decimal cStatutoryHoliday = 0;
                                        decimal cestim = 0;
                                        decimal cthtim = 0;
                                        decimal clitime = 0;
                                        foreach (DataRow item in chartdt.Rows)
                                        {
                                            cWorkday += decimal.Parse(item["Workday"].ToString());
                                            cWeekend += decimal.Parse(item["Weekend"].ToString());
                                            cStatutoryHoliday += decimal.Parse(item["StatutoryHoliday"].ToString());
                                            cestim += decimal.Parse(item["estim"].ToString());
                                            cthtim += decimal.Parse(item["thtim"].ToString());
                                            clitime += decimal.Parse(item["litime"].ToString());
                                        }
                                        int cQualified = chartdt.Compute("sum(Qualified)", "TRUE").ToInt();
                                        int cDisqualified = chartdt.Compute("sum(Disqualified)", "TRUE").ToInt();

                                        string supplierName = supplierEntity.Name;
                                        string compellation = supplierEntity.Compellation;
                                        string phone = supplierEntity.Phone;
                                        HSSFRow dataRow = (HSSFRow)sheet1.GetRow(6);
                                        HSSFRow dataRow8 = (HSSFRow)sheet1.GetRow(8);
                                        HSSFRow dataRow9 = (HSSFRow)sheet1.GetRow(9);
                                        HSSFRow dataRow10 = (HSSFRow)sheet1.GetRow(10);
                                        dataRow.GetCell(1).SetCellValue(cSum);  //总数
                                        dataRow.GetCell(2).SetCellValue(cQualified); //合格数
                                        dataRow.GetCell(3).SetCellValue(cDisqualified); //不合格数
                                        if (cSum == 0)
                                        {
                                            dataRow.GetCell(4).SetCellValue(0); //合格率
                                            dataRow.GetCell(5).SetCellValue(0); //不合格率

                                            dataRow10.GetCell(8).SetCellValue(0);
                                            dataRow10.GetCell(9).SetCellValue(0);
                                        }
                                        else
                                        {
                                            decimal plp = (cQualified.ToDecimal() / cSum);
                                            decimal pbl = (cDisqualified.ToDecimal() / cSum);
                                            double plps = Math.Round(plp, 4, MidpointRounding.AwayFromZero).ToDouble();
                                            double pblp = Math.Round(pbl, 4, MidpointRounding.AwayFromZero).ToDouble();
                                            dataRow.GetCell(4).SetCellValue(plps); //合格率
                                            dataRow.GetCell(5).SetCellValue(pblp); //不合格率

                                            dataRow10.GetCell(8).SetCellValue(plps);
                                            dataRow10.GetCell(9).SetCellValue(pblp);
                                        }
                                        decimal pzs = cestim + cthtim + clitime;
                                        dataRow.GetCell(6).SetCellValue(pzs.ToDouble()); //总工时
                                        dataRow.GetCell(7).SetCellValue(cWorkday.ToDouble()); //平日工时
                                        dataRow.GetCell(8).SetCellValue(cWeekend.ToDouble()); //周末工时
                                        dataRow.GetCell(9).SetCellValue(cStatutoryHoliday.ToDouble()); //法定工时


                                        dataRow8.GetCell(3).SetCellValue(supplierName);
                                        dataRow9.GetCell(3).SetCellValue(compellation);
                                        dataRow9.GetCell(8).SetCellValue(cQualified);
                                        dataRow9.GetCell(9).SetCellValue(cDisqualified);            
                                        dataRow10.GetCell(3).SetCellValue(phone);
                                    }

                                    #endregion

                                    #region sheet2
                                    ISheet sheet2 = hssfworkbook.GetSheet("不良品图片");
                                    //int sheet2StarRow = 3;
                                    //for (int c = 0; c < dts.Rows.Count; c++)
                                    //{
                                    //    DataRow en = dts.Rows[c];
                                    //    int rowNumber = sheet2StarRow + c;
                                    //    int nums = c + 1;
                                    //    HSSFRow dataRow = (HSSFRow)sheet2.CreateRow(rowNumber);
                                    //    dataRow.CreateCell(0).SetCellValue(nums);  //序号
                                    //    dataRow.CreateCell(1).SetCellValue(en[3].ToString());  //零件号
                                    //    dataRow.CreateCell(2).SetCellValue(en[4].ToString());  //零件名称
                                    //    dataRow.CreateCell(3).SetCellValue(en[5].ToString());  //批次号
                                    //    dataRow.CreateCell(4).SetCellValue(en[6].ToString());  //质检原因
                                    //    HSSFPatriarch patriarch = (HSSFPatriarch)sheet2.CreateDrawingPatriarch();
                                    //    string imgId = en[21].ToString();
                                    //    if (!string.IsNullOrEmpty(imgId))
                                    //    {
                                    //        List<AnnexesFileEntity> fileList = this.BaseRepository().FindList<AnnexesFileEntity>().Where(t => t.F_FolderId == imgId).ToList();
                                    //        if (fileList.Count > 0)
                                    //        {
                                    //            for (int d = 0; d < fileList.Count; d++)
                                    //            {
                                    //                String a = fileList[d].F_FilePath.Substring(fileList[d].F_FilePath.LastIndexOf("yt_new/") + 7);
                                    //                string Img_Url = "http://ytxqf.api.ttonservice.com/image/" + a;
                                    //                setPic(hssfworkbook, patriarch, Img_Url, sheet2, rowNumber, 6 + d);
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                    sheet2.ForceFormulaRecalculation = true;
                                    #endregion

                                    #region sheet3
                                    StringBuilder sql = new StringBuilder();
                                    sql.Append(@"SELECT DISTINCT
	                                        t.PartsType
                                        FROM
	                                        YT_Order t
                                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                                        WHERE
	                                        t.Order_Status != 3
                                        AND t.IsDelete = 0
                                        AND t.Supplier_Id = '" + supplierEntity.Id + @"'
                                         " + where + @"");
                                    DataTable dtPart = this.BaseRepository().FindTable(sql.ToString());
                                    if (dtPart.Rows.Count > 0)
                                    {
                                        ISheet sheet3 = hssfworkbook.GetSheet("零件汇总");
                                        int sheet3StarRow = 1;
                                        for (int e = 0; e < dtPart.Rows.Count; e++)
                                        {
                                            StringBuilder sumsql = new StringBuilder();
                                            sumsql.Append(@"SELECT
	                                                YT_PartsTypes.Parts_Name AS name,
	                                                YT_PartsTypes.Parts_Number AS number,
	                                                (
		                                                SELECT
			                                                ISNULL(
				                                                SUM (YT_OrderUser.Success),
				                                                0
			                                                )
		                                                FROM
			                                                YT_OrderUser
		                                                WHERE
			                                                Order_Id = t.Id
		                                                AND IsDelete = 0
	                                                ) AS Qualified,
	                                                (
		                                                SELECT
			                                                ISNULL(SUM(YT_OrderUser.Error), 0)
		                                                FROM
			                                                YT_OrderUser
		                                                WHERE
			                                                Order_Id = t.Id
		                                                AND IsDelete = 0
	                                                ) AS Disqualified,
	                                                (
		                                                SELECT
			                                                ISNULL(
				                                                SUM (
					                                                YT_OrderUser.Success + YT_OrderUser.Error
				                                                ),
				                                                0
			                                                )
		                                                FROM
			                                                YT_OrderUser
		                                                WHERE
			                                                Order_Id = t.Id
		                                                AND IsDelete = 0
	                                                ) AS [Sum],
	                                                ISNULL(t.Workday, '0') AS Workday,
	                                                ISNULL(t.Weekend, '0') AS Weekend,
	                                                ISNULL(t.StatutoryHoliday, '0') AS StatutoryHoliday,
	                                                ISNULL(t.EstimateDate, '0') AS estim,
	                                                ISNULL(t.ThreadElapsedTime, '0') AS thtim,
	                                                ISNULL(t.LineEdgeElapsedTime, '0') AS litime
                                                FROM
	                                                YT_Order t
                                                LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                                                LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                                                WHERE
	                                                t.Order_Status != 3
                                                AND t.IsDelete = 0
                                                AND t.PartsType = '" + dtPart .Rows[e]["PartsType"].ToString() + @"'
                                                AND t.Supplier_Id = '" + supplierEntity.Id + @"'
                                                 " + where + @" ");
                                            DataTable padt = this.BaseRepository().FindTable(sumsql.ToString());
                                            if (padt.Rows.Count > 0)
                                            {
                                                string partName = padt.Rows[0]["name"].ToString();
                                                string partNumber = padt.Rows[0]["number"].ToString();
                                                int pSum = padt.Compute("sum(Sum)", "TRUE").ToInt();
                                                decimal pWorkday = 0;
                                                decimal pWeekend = 0;
                                                decimal pStatutoryHoliday = 0;
                                                decimal pestim = 0;
                                                decimal pthtim = 0;
                                                decimal plitime = 0;
                                                foreach (DataRow item in padt.Rows)
                                                {
                                                    pWorkday += decimal.Parse(item["Workday"].ToString());
                                                    pWeekend += decimal.Parse(item["Weekend"].ToString());
                                                    pStatutoryHoliday += decimal.Parse(item["StatutoryHoliday"].ToString());
                                                    pestim += decimal.Parse(item["estim"].ToString());
                                                    pthtim += decimal.Parse(item["thtim"].ToString());
                                                    plitime += decimal.Parse(item["litime"].ToString());
                                                }
                                                int pQualified = padt.Compute("sum(Qualified)", "TRUE").ToInt();
                                                int pDisqualified = padt.Compute("sum(Disqualified)", "TRUE").ToInt();

                                                IDataFormat dataformat = hssfworkbook.CreateDataFormat();
                                                HSSFRow dataRow = (HSSFRow)sheet3.CreateRow(sheet3StarRow + e);

                                                dataRow.CreateCell(0).SetCellValue(partName);  //零件名
                                                if (pSum == 0)
                                                {
                                                    dataRow.CreateCell(1).SetCellValue(0); //合格率
                                                    dataRow.CreateCell(2).SetCellValue(0); //不合格率
                                                }
                                                else
                                                {
                                                    decimal plp = (pQualified.ToDecimal() / pSum) * 100;
                                                    decimal pbl = (pDisqualified.ToDecimal() / pSum) * 100;
                                                    double plps = Math.Round(plp, 2, MidpointRounding.AwayFromZero).ToDouble();
                                                    double pblp = Math.Round(pbl, 2, MidpointRounding.AwayFromZero).ToDouble();
                                                    dataRow.CreateCell(1).SetCellValue(plps); //合格率
                                                    dataRow.CreateCell(2).SetCellValue(pblp); //不合格率
                                                }
                                                dataRow.CreateCell(3).SetCellValue(pSum);  //零件数量
                                                dataRow.CreateCell(4).SetCellValue(pQualified);  //良品数量
                                                dataRow.CreateCell(5).SetCellValue(pDisqualified);  //不良品数量
                                                decimal pzs = pestim + pthtim + plitime;
                                                dataRow.CreateCell(6).SetCellValue(pzs.ToDouble());  //总工时
                                                dataRow.CreateCell(7).SetCellValue(pWeekend.ToDouble());  //周末工时
                                                dataRow.CreateCell(8).SetCellValue(pWorkday.ToDouble());  //工作日工时
                                                dataRow.CreateCell(9).SetCellValue(pStatutoryHoliday.ToDouble());  //法定工时
                                                if (sum == 0 || pzs.ToDecimal() == 0)
                                                {
                                                    dataRow.CreateCell(10).SetCellValue(0); //平均耗时
                                                }
                                                else
                                                {
                                                    decimal average = (pzs.ToDecimal() * 60) / sum.ToDecimal();
                                                    double averageTime = Math.Round(average, 2, MidpointRounding.AwayFromZero).ToDouble();
                                                    dataRow.CreateCell(10).SetCellValue(averageTime); //平均耗时
                                                }
                                                dataRow.CreateCell(11).SetCellValue(partNumber); //零件编号
                                            }
                                        }
                                    }
                                    #endregion

                                    #region sheet4 
                                    StringBuilder qsql = new StringBuilder();
                                    qsql.Append(@"SELECT DISTINCT
	                                        t.MaintainType
                                        FROM
	                                        YT_Order t
                                        LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                                        WHERE
	                                        t.Order_Status != 3
                                        AND t.IsDelete = 0
                                        AND t.Supplier_Id = '" + supplierEntity.Id + @"'
                                        " + where + @"");
                                    DataTable qdt = this.BaseRepository().FindTable(qsql.ToString());
                                    if (qdt.Rows.Count > 0)
                                    {
                                        ISheet sheet4 = hssfworkbook.GetSheet("缺陷汇总");
                                        int sheet4StarRow = 1;
                                        for (int f = 0; f < qdt.Rows.Count; f++)
                                        {
                                            StringBuilder sumsql = new StringBuilder();
                                            sumsql.Append(@"SELECT
	                                                YT_MaintenanceTypes.Name AS name,
	                                                t.[Count]
                                                FROM
	                                                YT_Order t
                                                LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = t.MaintainType
                                                LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                                                WHERE
	                                                t.Order_Status != 3
                                                AND t.IsDelete = 0
                                                AND t.MaintainType = '" + qdt.Rows[f]["MaintainType"].ToString() + @"'
                                                AND t.Supplier_Id = '" + supplierEntity.Id + @"'
                                                 " + where + @" ");
                                            DataTable qxdt = this.BaseRepository().FindTable(sumsql.ToString());
                                            if (qxdt.Rows.Count > 0)
                                            {
                                                string matName = qxdt.Rows[0]["name"].ToString();
                                                int qcount = qxdt.Compute("sum(Count)", "TRUE").ToInt();

                                                HSSFRow dataRow = (HSSFRow)sheet4.CreateRow(sheet4StarRow + f);
                                                dataRow.CreateCell(0).SetCellValue(matName);  //缺陷类型
                                                dataRow.CreateCell(1).SetCellValue(qcount);  //零件个数
                                            }
                                        }
                                    }
                                    #endregion

                                    using (FileStream filess = File.OpenWrite(filePath))
                                    {
                                        hssfworkbook.Write(filess);
                                   
                                    }
                                    sheet2.ForceFormulaRecalculation = true;
                                    sheet.ForceFormulaRecalculation = true;
                                    sheet1.ForceFormulaRecalculation = true;

                                }
                                catch (Exception e)
                                {
                                    throw new Exception("订单导出失败。");
                                }


                                ATLRLAttachment model = new ATLRLAttachment();
                                model.FileContent = GetImageByte(filePath);
                                model.FileExt = ".xls";
                                model.FileName = "" + filename + ".xls";
                                excelList.Add(model);
                            }
                        }
                    }
                    fileurl = ImageDown(context, excelList, 0, aa);

                    //删除临时文件
                    //foreach (var item in excelList)
                    //{
                    //    String url = System.Web.HttpContext.Current.Server.MapPath("../../App_Data\\" + "ExcelFiles/" + item.FileName);
                    //    DeleteFile(url);
                    //}
                }
                return fileurl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="patriarch"></param>
        /// <param name="path"></param>
        /// <param name="sheet"></param>
        /// <param name="rowline"></param>
        /// <param name="col"></param>
        private void setPic(HSSFWorkbook workbook, HSSFPatriarch patriarch, string path, ISheet sheet, int rowline, int col)
        {
            if (string.IsNullOrEmpty(path)) return;
            byte[] bytes = System.IO.File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath(path));
            int pictureIdx = workbook.AddPicture(bytes, PictureType.JPEG);
            // 插图片的位置  HSSFClientAnchor（dx1,dy1,dx2,dy2,col1,row1,col2,row2) 后面再作解释
            HSSFClientAnchor anchor = new HSSFClientAnchor(70, 10, 0, 0, col, rowline, col + 1, rowline + 1);
            //把图片插到相应的位置
            HSSFPicture pict = (HSSFPicture)patriarch.CreatePicture(anchor, pictureIdx);
        }

        /// <summary>
        /// 根据路径删除文件
        /// </summary>
        /// <param name="path"></param>
        public void DeleteFile(string path)
        {
            FileAttributes attr = File.GetAttributes(path);
            if (attr == FileAttributes.Directory)
            {
                Directory.Delete(path, true);
            }
            else
            {
                File.Delete(path);
            }
        }

        public class ATLRLAttachment
        {
            /// <summary>
            /// 字节流
            /// </summary>
            public byte[] FileContent { get; set; }

            /// <summary>
            /// 类型
            /// </summary>
            public string FileExt { get; set; }

            /// <summary>
            /// 名称
            /// </summary>
            public string FileName { get; set; }
        }

        /// <summary>
        /// 通过图片地址将图片转换成字节流
        /// add by chenxy 16.3.10
        /// </summary>
        /// <param name="url">图片地址</param>
        /// <returns>字节流</returns>
        public byte[] GetImageByte(string url)
        {
            FileStream fs = new FileStream(url, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return byData;
        }

        /// <summary>
        /// 批量下载
        /// add by chenxy 16.3.10
        /// </summary>
        /// <param name="context">HTTP请求</param>
        /// <param name="models">下载集合</param>
        public string ImageDown(HttpContext context, IEnumerable<ATLRLAttachment> models, int save, int size)
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            string FileToZip = Convert.ToInt64(ts.TotalSeconds).ToString() + ".zip";
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string url = string.Format(@"{0}Temporary", path);
            path = string.Format(@"{0}\{1}", url, FileToZip);
            BatchDeleteZip(url);

            using (FileStream ms = File.Create(path))
            {
                using (ZipOutputStream zip = new ZipOutputStream(ms))
                {
                    zip.SetLevel(9);
                    int itFileNameNum = 0;
                    foreach (ATLRLAttachment m in models)
                    {
                        #region 修改重名文件
                        if (models.Where(p => p.FileName == m.FileName).Count() > 1)
                        {
                            itFileNameNum++;
                            try
                            {
                                m.FileName = m.FileName.Insert(m.FileName.IndexOf("."), string.Format("_{0}", itFileNameNum));
                            }
                            catch (ArgumentNullException)
                            {
                                ErrorMessage(context, string.Format("文件名：{0}，格式错误，未包含后缀连接符.", m.FileName));
                            }
                        }
                        #endregion
                        if (m.FileContent == null || m.FileContent.Length < 1)
                        {
                            continue;
                        }
                        ZipEntry entry = new ZipEntry(m.FileName);
                        zip.PutNextEntry(entry);
                        zip.Write(m.FileContent, 0, m.FileContent.Length);
                    }
                    zip.Finish();
                    zip.Close();
                }
            }
            return FileToZip;//客户端保存的文件名         
        }

        /// <summary>
        /// 批量删除压缩包
        /// add by chenxy 16.3.15
        /// </summary>
        /// <param name="strUrl">压缩包路径</param>
        public void BatchDeleteZip(string strUrl)
        {
            //判断路径是否为空
            if (string.IsNullOrEmpty(strUrl))
            {
                throw new ArgumentNullException("文件目录为空");
            }
            //判断是否有目录
            if (!Directory.Exists(strUrl))
            {
                Directory.CreateDirectory(strUrl);
                return;
            }

            //获取目录下所有文件
            string[] strFiles = Directory.GetFiles(strUrl);
            //判断文件时间长短
            foreach (string strfile in strFiles)
            {
                FileInfo info = new FileInfo(strfile);
                if (info.CreationTime.Day >= 24)
                {
                    File.Delete(info.FullName);
                }
            }
        }

        public void ErrorMessage(HttpContext context, string strMsg)
        {
            context.Response.Write(string.Format("<script language=javascript>alert('{0}');</" + "script>", strMsg));
            context.Response.Write("<script language=javascript>history.go(-1);</script>");
            context.Response.End();
        }
        #endregion

        #region 首页图表
        public OrderEchartModel GetOrderEchartsToAnalysis(string startTime, string endTime, string supplier)
        {
            OrderEchartModel model = new OrderEchartModel();
            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT
	                SUM (QCount) AS QCount,
	                SUM (OCount) AS OCount,
	                SUM (SCount) AS SCount
                FROM
	                YT_OrdersAnalysis
                WHERE
	                Supplier = '" + supplier + @"'
                AND [Time] BETWEEN '" + startTime + @"'
                AND '" + endTime + "'");
            List<OrderEchartModel> lists = new RepositoryFactory().BaseRepository().FindList<OrderEchartModel>(sql.ToString()).AsList();
            model = lists[0];
            return model;
        }

        public List<OrderEchartsToAModel> GetOrderEchartsToA(string startTime, string endTime, string supplier)
        {
            List<OrderEchartsToAModel> list = new List<OrderEchartsToAModel>();

            DateTime startDate = Convert.ToDateTime(startTime);
            DateTime endDate = Convert.ToDateTime(endTime);

            #region 岗位筛选
            List<string> OUserId = new List<string>();
            List<string> SUserId = new List<string>();
            List<string> QUserId = new List<string>();

            #region 本部
            string Oobjects1 = Config.GetValue("OrderAndTaskManage");
            string Oobjects2 = Config.GetValue("OrderManage");
            string Oobjects3 = Config.GetValue("SquadLeader");
            StringBuilder OUserRelationSql = new StringBuilder();
            OUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Oobjects1 + "','" + Oobjects2 + "','" + Oobjects3 + "')  ");
            List<UserRelationEntity> OrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(OUserRelationSql.ToString()).AsList();
            if (OrelationEntity.Count != 0)
            {
                for (int i = 0; i < OrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(OrelationEntity[i].F_UserId);
                    OUserId.Add(userEntity.F_UserId);
                }
            }
            string ouserid = "";
            foreach (var ostruid in OUserId)
            {
                ouserid += "'" + ostruid + "'" + ",";
            }
            if (ouserid != "")
            {
                ouserid = ouserid.Substring(0, ouserid.Length - 1);
            }
            #endregion

            #region 供应商
            string Sobjects = Config.GetValue("Supplier");
            StringBuilder SUserRelationSql = new StringBuilder();
            SUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + Sobjects + "'  ");
            List<UserRelationEntity> SrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(SUserRelationSql.ToString()).AsList();
            if (SrelationEntity.Count != 0)
            {
                for (int i = 0; i < SrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(SrelationEntity[i].F_UserId);
                    SUserId.Add(userEntity.F_UserId);
                }
            }
            string suserid = "";
            foreach (var sstruid in SUserId)
            {
                suserid += "'" + sstruid + "'" + ",";
            }
            if (suserid != "")
            {
                suserid = suserid.Substring(0, suserid.Length - 1);
            }
            #endregion

            #region SQE
            string Qobjects1 = Config.GetValue("Quality");
            string Qobjects2 = Config.GetValue("QualityManager");
            StringBuilder QUserRelationSql = new StringBuilder();
            QUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Qobjects1 + "','" + Qobjects2 + "')  ");
            List<UserRelationEntity> QrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(QUserRelationSql.ToString()).AsList();
            if (QrelationEntity.Count != 0)
            {
                for (int i = 0; i < QrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(QrelationEntity[i].F_UserId);
                    QUserId.Add(userEntity.F_UserId);
                }
            }
            string quserid = "";
            foreach (var qstruid in QUserId)
            {
                quserid += "'" + qstruid + "'" + ",";
            }
            if (quserid != "")
            {
                quserid = quserid.Substring(0, quserid.Length - 1);
            }
            #endregion
            #endregion

            int Month = (startDate.Year - endDate.Year) * 12 + (startDate.Month - endDate.Month);
            if (Month == 0)
            {
                OrderEchartsToAModel model = new OrderEchartsToAModel();
                string startR = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endR = endDate.ToString("yyyy-MM-dd HH:mm:ss");

                model.time = startDate.ToString("yyyy-MM");

                StringBuilder OsqlF = new StringBuilder();
                OsqlF.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startR + @"'
                    AND '" + endR + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + ouserid.Trim() + ")");
                DataTable OdtF = this.BaseRepository().FindTable(OsqlF.ToString());
                model.oCount = OdtF.Rows[0]["cou"].ToInt();

                StringBuilder QsqlF = new StringBuilder();
                QsqlF.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startR + @"'
                    AND '" + endR + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + quserid.Trim() + ")");
                DataTable QdtF = this.BaseRepository().FindTable(QsqlF.ToString());
                model.qCount = QdtF.Rows[0]["cou"].ToInt();

                StringBuilder SsqlF = new StringBuilder();
                SsqlF.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startR + @"'
                    AND '" + endR + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + suserid.Trim() + ")");
                DataTable SdtF = this.BaseRepository().FindTable(SsqlF.ToString());
                model.sCount = SdtF.Rows[0]["cou"].ToInt();

                list.Add(model);
            }
            else
            {
                OrderEchartsToAModel modelF = new OrderEchartsToAModel();
                string endF = endDate.ToString("yyyy-MM-01 00:00:00");
                string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");

                modelF.time = endDate.ToString("yyyy-MM");

                StringBuilder OsqlF = new StringBuilder();
                OsqlF.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + endF + @"'
                    AND '" + endD + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + ouserid.Trim() + ")");
                DataTable OdtF = this.BaseRepository().FindTable(OsqlF.ToString());
                modelF.oCount = OdtF.Rows[0]["cou"].ToInt();

                StringBuilder QsqlF = new StringBuilder();
                QsqlF.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + endF + @"'
                    AND '" + endD + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + quserid.Trim() + ")");
                DataTable QdtF = this.BaseRepository().FindTable(QsqlF.ToString());
                modelF.qCount = QdtF.Rows[0]["cou"].ToInt();

                StringBuilder SsqlF = new StringBuilder();
                SsqlF.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + endF + @"'
                    AND '" + endD + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + suserid.Trim() + ")");
                DataTable SdtF = this.BaseRepository().FindTable(SsqlF.ToString());
                modelF.sCount = SdtF.Rows[0]["cou"].ToInt();

                list.Add(modelF);

                for (int i = -1; i > Month; i--)
                {
                    string start = endDate.AddMonths(i).ToString("yyyy-MM-01 00:00:00");
                    string end = endDate.AddMonths(i).AddDays(1 - endDate.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
                    OrderEchartsToAModel modelS = new OrderEchartsToAModel();

                    modelS.time = Convert.ToDateTime(start).ToString("yyyy-MM");

                    StringBuilder OsqlS = new StringBuilder();
                    OsqlS.Append(@"SELECT 
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + start + @"'
                    AND '" + end + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + ouserid.Trim() + ")");
                    DataTable OdtS = this.BaseRepository().FindTable(OsqlS.ToString());
                    modelS.oCount = OdtS.Rows[0]["cou"].ToInt();

                    StringBuilder QsqlS = new StringBuilder();
                    QsqlS.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + start + @"'
                    AND '" + end + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + quserid.Trim() + ")");
                    DataTable QdtS = this.BaseRepository().FindTable(QsqlS.ToString());
                    modelS.qCount = QdtS.Rows[0]["cou"].ToInt();

                    StringBuilder SsqlS = new StringBuilder();
                    SsqlS.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + start + @"'
                    AND '" + end + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + suserid.Trim() + ")");
                    DataTable SdtS = this.BaseRepository().FindTable(SsqlS.ToString());
                    modelS.sCount = SdtS.Rows[0]["cou"].ToInt();

                    list.Add(modelS);
                }

                OrderEchartsToAModel modelH = new OrderEchartsToAModel();
                string startH = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endH = startDate.AddDays(1 - startDate.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");

                modelH.time = Convert.ToDateTime(startH).ToString("yyyy-MM");

                StringBuilder OsqlH = new StringBuilder();
                OsqlH.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startH + @"'
                    AND '" + endH + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + ouserid.Trim() + ")");
                DataTable OdtH = this.BaseRepository().FindTable(OsqlH.ToString());
                modelH.oCount = OdtH.Rows[0]["cou"].ToInt();

                StringBuilder QsqlH = new StringBuilder();
                QsqlH.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startH + @"'
                    AND '" + endH + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + quserid.Trim() + ")");
                DataTable QdtH = this.BaseRepository().FindTable(QsqlH.ToString());
                modelH.qCount = QdtH.Rows[0]["cou"].ToInt();

                StringBuilder SsqlH = new StringBuilder();
                SsqlH.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startH + @"'
                    AND '" + endH + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + suserid.Trim() + ")");
                DataTable SdtH = this.BaseRepository().FindTable(SsqlH.ToString());
                modelH.sCount = SdtH.Rows[0]["cou"].ToInt();

                list.Add(modelH);
            }
       
            return list;
        }

        public OrderEchartModel OrderEchartsToAnalysis(string startTime, string endTime, string supplier)
        {
            OrderEchartModel model = new OrderEchartModel();

            #region 时间筛选
            DateTime now = DateTime.Now;

            DateTime startDate = new DateTime();

            DateTime endDate = new DateTime();

            if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                startDate = now.AddMonths(-1);
                endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            }
            else if (string.IsNullOrEmpty(startTime))
            {
                endDate = Convert.ToDateTime(endTime);
                startDate = endDate.AddDays(-30);
            }
            else if (string.IsNullOrEmpty(endTime))
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = DateTime.Now;
            }
            else
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = Convert.ToDateTime(endTime);
            }
            #endregion

            #region 岗位筛选
            List<string> OUserId = new List<string>();
            List<string> SUserId = new List<string>();
            List<string> QUserId = new List<string>();

            #region 本部
            string Oobjects1 = Config.GetValue("OrderAndTaskManage");
            string Oobjects2 = Config.GetValue("OrderManage");
            string Oobjects3 = Config.GetValue("SquadLeader");
            StringBuilder OUserRelationSql = new StringBuilder();
            OUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Oobjects1 + "','" + Oobjects2 + "','" + Oobjects3 + "')  ");
            List<UserRelationEntity> OrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(OUserRelationSql.ToString()).AsList();
            if (OrelationEntity.Count != 0)
            {
                for (int i = 0; i < OrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(OrelationEntity[i].F_UserId);
                    OUserId.Add(userEntity.F_UserId);
                }
            }
            string ouserid = "";
            foreach (var ostruid in OUserId)
            {
                ouserid += "'" + ostruid + "'" + ",";
            }
            if (ouserid != "")
            {
                ouserid = ouserid.Substring(0, ouserid.Length - 1);
            }
            #endregion

            #region 供应商
            string Sobjects = Config.GetValue("Supplier");
            StringBuilder SUserRelationSql = new StringBuilder();
            SUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + Sobjects + "'  ");
            List<UserRelationEntity> SrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(SUserRelationSql.ToString()).AsList();
            if (SrelationEntity.Count != 0)
            {
                for (int i = 0; i < SrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(SrelationEntity[i].F_UserId);
                    SUserId.Add(userEntity.F_UserId);
                }
            }
            string suserid = "";
            foreach (var sstruid in SUserId)
            {
                suserid += "'" + sstruid + "'" + ",";
            }
            if (suserid != "")
            {
                suserid = suserid.Substring(0, suserid.Length - 1);
            }
            #endregion

            #region SQE
            string Qobjects1 = Config.GetValue("Quality");
            string Qobjects2 = Config.GetValue("QualityManager");
            StringBuilder QUserRelationSql = new StringBuilder();
            QUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Qobjects1 + "','" + Qobjects2 + "')  ");
            List<UserRelationEntity> QrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(QUserRelationSql.ToString()).AsList();
            if (QrelationEntity.Count != 0)
            {
                for (int i = 0; i < QrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(QrelationEntity[i].F_UserId);
                    QUserId.Add(userEntity.F_UserId);
                }
            }
            string quserid = "";
            foreach (var qstruid in QUserId)
            {
                quserid += "'" + qstruid + "'" + ",";
            }
            if (quserid != "")
            {
                quserid = quserid.Substring(0, quserid.Length - 1);
            }
            #endregion
            #endregion

            StringBuilder Osql = new StringBuilder();
            Osql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    AND Supplier_Id = '"+ supplier + @"'
                    AND CreateUserId IN (" + ouserid.Trim() + ")");
            DataTable Odt = this.BaseRepository().FindTable(Osql.ToString());
            model.OCount = Odt.Rows[0]["cou"].ToInt();

            StringBuilder Qsql = new StringBuilder();
            Qsql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + quserid.Trim() + ")");
            DataTable Qdt = this.BaseRepository().FindTable(Qsql.ToString());
            model.QCount = Qdt.Rows[0]["cou"].ToInt();

            StringBuilder Ssql = new StringBuilder();
            Ssql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    AND Supplier_Id = '" + supplier + @"'
                    AND CreateUserId IN (" + suserid.Trim() + ")");
            DataTable Sdt = this.BaseRepository().FindTable(Ssql.ToString());
            model.SCount = Sdt.Rows[0]["cou"].ToInt();

            return model;
        }

        public OrderEchartModel GetOrderEcharts(string startTime, string endTime)
        {
            OrderEchartModel model = new OrderEchartModel();

            #region 时间筛选
            DateTime now = DateTime.Now;

            DateTime startDate = new DateTime();

            DateTime endDate = new DateTime();

            if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                startDate = now.AddMonths(-1);
                endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            }
            else if (string.IsNullOrEmpty(startTime))
            {
                endDate = Convert.ToDateTime(endTime);
                startDate = endDate.AddDays(-30);
            }
            else if (string.IsNullOrEmpty(endTime))
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = DateTime.Now;
            }
            else
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = Convert.ToDateTime(endTime);
            }
            #endregion

            #region 岗位筛选
            List<string> OUserId = new List<string>();
            List<string> SUserId = new List<string>();
            List<string> QUserId = new List<string>();

            #region 本部
            string Oobjects1 = Config.GetValue("OrderAndTaskManage");
            string Oobjects2 = Config.GetValue("OrderManage");
            string Oobjects3 = Config.GetValue("SquadLeader");
            StringBuilder OUserRelationSql = new StringBuilder();
            OUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Oobjects1 + "','" + Oobjects2 + "','" + Oobjects3 + "')  ");
            List<UserRelationEntity> OrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(OUserRelationSql.ToString()).AsList();
            if (OrelationEntity.Count != 0)
            {
                for (int i = 0; i < OrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(OrelationEntity[i].F_UserId);
                    OUserId.Add(userEntity.F_UserId);
                }
            }
            string ouserid = "";
            foreach (var ostruid in OUserId)
            {
                ouserid += "'" + ostruid + "'" + ",";
            }
            if (ouserid != "")
            {
                ouserid = ouserid.Substring(0, ouserid.Length - 1);
            }
            #endregion

            #region 供应商
            string Sobjects = Config.GetValue("Supplier");
            StringBuilder SUserRelationSql = new StringBuilder();
            SUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + Sobjects + "'  ");
            List<UserRelationEntity> SrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(SUserRelationSql.ToString()).AsList();
            if (SrelationEntity.Count != 0)
            {
                for (int i = 0; i < SrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(SrelationEntity[i].F_UserId);
                    SUserId.Add(userEntity.F_UserId);
                }
            }
            string suserid = "";
            foreach (var sstruid in SUserId)
            {
                suserid += "'" + sstruid + "'" + ",";
            }
            if (suserid != "")
            {
                suserid = suserid.Substring(0, suserid.Length - 1);
            }
            #endregion

            #region SQE
            string Qobjects1 = Config.GetValue("Quality");
            string Qobjects2 = Config.GetValue("QualityManager");
            StringBuilder QUserRelationSql = new StringBuilder();
            QUserRelationSql.Append("SELECT Base_UserRelation.* FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Qobjects1 + "','" + Qobjects2 + "')  ");
            List<UserRelationEntity> QrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(QUserRelationSql.ToString()).AsList();
            if (QrelationEntity.Count != 0)
            {
                for (int i = 0; i < QrelationEntity.Count; i++)
                {
                    UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(QrelationEntity[i].F_UserId);
                    QUserId.Add(userEntity.F_UserId);
                }
            }
            string quserid = "";
            foreach (var qstruid in QUserId)
            {
                quserid += "'" + qstruid + "'" + ",";
            }
            if (quserid != "")
            {
                quserid = quserid.Substring(0, quserid.Length - 1);
            }
            #endregion
            #endregion

            StringBuilder Osql = new StringBuilder();
            Osql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    AND CreateUserId IN (" + ouserid.Trim() + ")");
            DataTable Odt = this.BaseRepository().FindTable(Osql.ToString());
            model.OCount = Odt.Rows[0]["cou"].ToInt();

            StringBuilder Qsql = new StringBuilder();
            Qsql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    AND CreateUserId IN (" + quserid.Trim() + ")");
            DataTable Qdt = this.BaseRepository().FindTable(Qsql.ToString());
            model.QCount = Qdt.Rows[0]["cou"].ToInt();

            StringBuilder Ssql = new StringBuilder();
            Ssql.Append(@"SELECT
	                    COUNT (Id) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    AND CreateUserId IN (" + suserid.Trim() + ")");
            DataTable Sdt = this.BaseRepository().FindTable(Ssql.ToString());
            model.SCount = Sdt.Rows[0]["cou"].ToInt();

            return model;
        }

        public List<UserFinishEchartsModel> GetOrderEchartsToIdentity(string startTime, string endTime, int type)
        {
            try
            {
                List<UserFinishEchartsModel> list = new List<UserFinishEchartsModel>();

                #region 时间筛选
                DateTime now = DateTime.Now;

                DateTime startDate = new DateTime();

                DateTime endDate = new DateTime();

                if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    startDate = now.AddMonths(-1);
                    endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                }
                else if (string.IsNullOrEmpty(startTime))
                {
                    endDate = Convert.ToDateTime(endTime);
                    startDate = endDate.AddDays(-30);
                }
                else if (string.IsNullOrEmpty(endTime))
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = DateTime.Now;
                }
                else
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = Convert.ToDateTime(endTime);
                }
                #endregion

                //本部
                if (type == 1)
                {
                    string Oobjects1 = Config.GetValue("OrderAndTaskManage");
                    string Oobjects2 = Config.GetValue("OrderManage");
                    string Oobjects3 = Config.GetValue("SquadLeader");
                    StringBuilder OUserRelationSql = new StringBuilder();
                    OUserRelationSql.Append("SELECT DISTINCT Base_UserRelation.F_UserId FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Oobjects1 + "','" + Oobjects2 + "','" + Oobjects3 + "')  ");
                    List<UserRelationEntity> OrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(OUserRelationSql.ToString()).AsList();
                    if (OrelationEntity.Count != 0)
                    {
                        for (int i = 0; i < OrelationEntity.Count; i++)
                        {
                            UserFinishEchartsModel model = new UserFinishEchartsModel();
                            UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(OrelationEntity[i].F_UserId);
                            model.name = userEntity.F_RealName;

                            StringBuilder Osql = new StringBuilder();
                            Osql.Append(@"SELECT
	                                COUNT (Id) AS cou
                                FROM
	                                YT_Order
                                WHERE
	                                IsDelete = 0
                                AND Order_Status != 3
                                AND CreateDate BETWEEN '" + startDate + @"'
                                AND '" + endDate + @"'
                                AND CreateUserId = '" + userEntity.F_UserId + "'");
                            DataTable Odt = this.BaseRepository().FindTable(Osql.ToString());
                            model.count = Odt.Rows[0]["cou"].ToInt();

                            if (model.count > 0)
                            {
                                list.Add(model);
                            }
                        }
                    }
                }
                //供应商
                if (type == 2)
                {
                    string Sobjects = Config.GetValue("Supplier");
                    StringBuilder SUserRelationSql = new StringBuilder();
                    SUserRelationSql.Append("SELECT DISTINCT Base_UserRelation.F_UserId FROM Base_UserRelation where  Base_UserRelation.F_ObjectId = '" + Sobjects + "'  ");
                    List<UserRelationEntity> SrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(SUserRelationSql.ToString()).AsList();
                    if (SrelationEntity.Count != 0)
                    {
                        for (int i = 0; i < SrelationEntity.Count; i++)
                        {
                            UserFinishEchartsModel model = new UserFinishEchartsModel();
                            UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(SrelationEntity[i].F_UserId);
                            model.name = userEntity.F_RealName;

                            StringBuilder Qsql = new StringBuilder();
                            Qsql.Append(@"SELECT
	                                COUNT (Id) AS cou
                                FROM
	                                YT_Order
                                WHERE
	                                IsDelete = 0
                                AND Order_Status != 3
                                AND CreateDate BETWEEN '" + startDate + @"'
                                AND '" + endDate + @"'
                                AND CreateUserId = '" + userEntity.F_UserId + "'");
                            DataTable Qdt = this.BaseRepository().FindTable(Qsql.ToString());
                            model.count = Qdt.Rows[0]["cou"].ToInt();

                            if (model.count > 0)
                            {
                                list.Add(model);
                            }
                        }
                    }
                }
                //SQE
                if (type == 3)
                {
                    string Qobjects1 = Config.GetValue("Quality");
                    string Qobjects2 = Config.GetValue("QualityManager");
                    StringBuilder QUserRelationSql = new StringBuilder();
                    QUserRelationSql.Append("SELECT DISTINCT Base_UserRelation.F_UserId FROM Base_UserRelation where  Base_UserRelation.F_ObjectId IN ('" + Qobjects1 + "','" + Qobjects2 + "')  ");
                    List<UserRelationEntity> QrelationEntity = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(QUserRelationSql.ToString()).AsList();
                    if (QrelationEntity.Count != 0)
                    {
                        for (int i = 0; i < QrelationEntity.Count; i++)
                        {
                            UserFinishEchartsModel model = new UserFinishEchartsModel();
                            UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(QrelationEntity[i].F_UserId);
                            model.name = userEntity.F_RealName;

                            StringBuilder Ssql = new StringBuilder();
                            Ssql.Append(@"SELECT
	                                COUNT (Id) AS cou
                                FROM
	                                YT_Order
                                WHERE
	                                IsDelete = 0
                                AND Order_Status != 3
                                AND CreateDate BETWEEN '" + startDate + @"'
                                AND '" + endDate + @"'
                                AND CreateUserId = '" + userEntity.F_UserId + "'");
                            DataTable Sdt = this.BaseRepository().FindTable(Ssql.ToString());
                            model.count = Sdt.Rows[0]["cou"].ToInt();

                            if (model.count > 0)
                            {
                                list.Add(model);
                            }
                        }
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public StatisticsInfoModel GetStatisticsInfo()
        {
            StatisticsInfoModel model = new StatisticsInfoModel();

            string startTime = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            string endTime = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");

            StringBuilder sql1 = new StringBuilder();
            sql1.Append(@"SELECT
	                    ISNULL(COUNT(Id), 0) AS cou
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    AND Order_Status != 3
                    AND CreateDate BETWEEN '" + startTime + @"'
                    AND '" + endTime + "'");
            DataTable dt1 = this.BaseRepository().FindTable(sql1.ToString());
            model.orderCount = dt1.Rows[0]["cou"].ToString() + "单";

            StringBuilder sql2 = new StringBuilder();
            sql2.Append(@"SELECT
	                    ISNULL(SUM (
		                    YT_OrderUser.Success + YT_OrderUser.Error
	                    ),0) AS cou
                    FROM
	                    YT_OrderUser
                    LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                    WHERE
	                    YT_Order.IsDelete = 0
                    AND YT_OrderUser.Accomplish BETWEEN  '" + startTime + @"'
                    AND '" + endTime + "'");
            DataTable dt2 = this.BaseRepository().FindTable(sql2.ToString());
            model.successCount = dt2.Rows[0]["cou"].ToString() + "件";

            StringBuilder sql3 = new StringBuilder();
            sql3.Append(@"SELECT
	                    ISNULL(SUM (
		                    YT_OrderUser.[All] - YT_OrderUser.Success - YT_OrderUser.Error
	                    ),0)  AS cou
                    FROM
	                    YT_OrderUser
                    LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                    WHERE
	                    YT_Order.IsDelete = 0
                    AND YT_OrderUser.Accomplish BETWEEN  '" + startTime + @"'
                    AND '" + endTime + "'");
            DataTable dt3 = this.BaseRepository().FindTable(sql3.ToString());
            model.unfinishedCount = dt3.Rows[0]["cou"].ToString() + "件";

            StringBuilder sql4 = new StringBuilder();
            sql4.Append(@"SELECT
	                    TOP 1 CONVERT (VARCHAR(100), PushTime, 120) AS tim
                    FROM
	                    YT_PushMessage
                    ORDER BY
	                    PushTime DESC");
            DataTable dt4 = this.BaseRepository().FindTable(sql4.ToString());
            model.lastTime = dt4.Rows[0]["tim"].ToString();

            return model;
        }
        #endregion

        #region 短信方法
        public void PushNote(string mobiles)
        {
            String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
            String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
            String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
            String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            //初始化ascClient,暂时不支持多region（请勿修改）
            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            try
            {
                request.PhoneNumbers = mobiles;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = "亚泰新";
                //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                request.TemplateCode = "SMS_144150409";
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                request.TemplateParam = "{\"supplier\":\"aa\",\"name\":\"aa\",\"phone\":\"aa\"}";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
            }
            catch (Exception e)
            {
                if (e is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(e);
                }
            }
        }
        #endregion
    }
}
