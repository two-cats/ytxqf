﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier
{
    public class GetSupplierInfoModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Remark { get; set; }

        public string Compellation { get; set; }

        public string Phone { get; set; }

        public string Code { get; set; }

        public string CreateDate { get; set; }

        public string CreateUserId { get; set; }

        public string ModifyDate { get; set; }

        public string Check { get; set; }
    }
}
