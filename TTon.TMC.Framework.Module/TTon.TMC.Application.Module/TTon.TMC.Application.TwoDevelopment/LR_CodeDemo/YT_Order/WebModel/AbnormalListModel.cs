﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class AbnormalListModel
    {
        public string id { get; set; }

        public string orderNumber { get; set; }

        public string partName { get; set; }

        public string userName { get; set; }

        public string createTime { get; set; }

        public int status { get; set; }
    }
}
