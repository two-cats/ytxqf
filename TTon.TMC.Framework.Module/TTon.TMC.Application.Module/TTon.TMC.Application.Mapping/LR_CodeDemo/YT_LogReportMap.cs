﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_LogReport;

namespace TTon.TMC.Application.Mapping.LR_CodeDemo
{
    public class YT_LogReportMap : EntityTypeConfiguration<YT_LogReportEntity>
    {
        public YT_LogReportMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_LOGREPORT");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
