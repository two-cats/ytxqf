﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.VX
{
    public  class WeChatUserBLL : WeChatUserIBLL
    {
        private WeChatUserService service = new WeChatUserService();


        #region 微信授权登陆

        /// <summary>
        /// 通过code换取网页授权access_token
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="secret"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public WxAccessTokenModel getWxAccessToken(string appid, string secret, string code)
        {
            try
            {
                return service.getWxAccessToken(appid, secret, code);
            }
            catch (Exception)
            {
                throw new Exception("");
            }
        }
        /// <summary>
        /// 拉取用户信息
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public WxUserInfoModel getWxUserInfo(string access_token, string openid)
        {
            {
                try
                {
                    return service.getWxUserInfo(access_token, openid);
                }
                catch (Exception)
                {
                    throw new Exception("");
                }
            }
        }
        #endregion
    }
}
