﻿using Dos.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESteward.QuartzService
{
    public class DB
    {
        public DbSession Context { get; set; }

        public DB(DbSession Context)
        {
            this.Context = Context;
            Context.RegisterSqlLogger(delegate(string sql) 
            {
                //在此可以记录sql日志
                //写日志会影响性能，建议开发版本记录sql以便调试，发布正式版本不要记录
                Dos.Common.LogHelper.Debug(sql, "SQL日志");
            });
        }

    }
}
