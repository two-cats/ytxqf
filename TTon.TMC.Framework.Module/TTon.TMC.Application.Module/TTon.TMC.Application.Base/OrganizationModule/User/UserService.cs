﻿using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TTon.TMC.Application.BaseModule.OrganizationModule.User;
using TTon.TMC.Application.BaseModule.OrganizationModule.User.Model;
using TTon.TMC.Application.Base.AuthorizeModule;

namespace TTon.TMC.Application.Base.OrganizationModule
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.03.04
    /// 描 述：用户模块数据操作服务类
    /// </summary>
    public class UserService : RepositoryFactory
    {
        #region 属性 构造函数
        private string fieldSql;
        public  UserService()
        {
            fieldSql = @" 
                        t.F_UserId,
                        t.F_EnCode,
                        t.F_Account,
                        t.F_Password,
                        t.F_Secretkey,
                        t.F_RealName,
                        t.F_NickName,
                        t.F_HeadIcon,
                        t.F_QuickQuery,
                        t.F_SimpleSpelling,
                        t.F_Gender,
                        t.F_Birthday,
                        t.F_Mobile,
                        t.F_Telephone,
                        t.F_Email,
                        t.F_OICQ,
                        t.F_WeChat,
                        t.F_MSN,
                        t.F_CompanyId,
                        t.F_DepartmentId,
                        t.F_SecurityLevel,
                        t.F_OpenId,
                        t.F_Question,
                        t.F_AnswerQuestion,
                        t.F_CheckOnLine,
                        t.F_AllowStartTime,
                        t.F_AllowEndTime,
                        t.F_LockStartDate,
                        t.F_LockEndDate,
                        t.F_SortCode,
                        t.F_DeleteMark,
                        t.F_EnabledMark,
                        t.F_Description,
                        t.F_CreateDate,
                        t.F_CreateUserId,
                        t.F_CreateUserName,
                        t.F_ModifyDate,
                        t.F_ModifyUserId,
                        t.F_ModifyUserName
                        ";
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取实体,通过用户账号
        /// </summary>
        /// <param name="account">用户账号</param>
        /// <returns></returns>
        public UserEntity GetEntityByAccount(string account)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM Base_User t ");
                strSql.Append(" WHERE t.F_Account = @account AND t.F_DeleteMark = 0  ");
                return this.BaseRepository().FindEntity<UserEntity>(strSql.ToString(), new { account = account });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 用户列表(根据人名查询)
        /// </summary>
        /// <param name="RealName">真实姓名</param>
        /// <returns></returns>
        public IEnumerable<UserEntity> GetListbyRealName(string RealName)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql.Replace("t.F_Password,", "").Replace("t.F_Secretkey,", ""));
                strSql.Append(" FROM Base_User t WHERE t.F_DeleteMark = 0 AND t.F_RealName like  @RealName ORDER BY t.F_DepartmentId,t.F_Account ");
                return this.BaseRepository().FindList<UserEntity>(strSql.ToString(), new { RealName ="%"+ RealName+"%" });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 用户列表(根据公司主键)
        /// </summary>
        /// <param name="companyId">公司主键</param>
        /// <returns></returns>
        public IEnumerable<UserEntity> GetList(string companyId)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql.Replace("t.F_Password,", "").Replace("t.F_Secretkey,", ""));
                strSql.Append(" FROM Base_User t WHERE t.F_DeleteMark = 0 AND t.F_CompanyId = @companyId ORDER BY t.F_DepartmentId,t.F_Account ");
                return this.BaseRepository().FindList<UserEntity>(strSql.ToString(), new { companyId = companyId });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 用户列表（导出Excel）
        /// </summary>
        /// <returns></returns>
        public DataTable GetExportList()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT u.F_Account
                                  ,u.F_RealName
                                  ,CASE WHEN u.F_Gender=1 THEN '男' ELSE '女' END AS F_Gender
                                  ,u.F_Birthday
                                  ,u.F_Mobile
                                  ,u.F_Telephone
                                  ,u.F_Email
                                  ,u.F_WeChat
                                  ,u.F_MSN
                                  ,o.F_FullName AS F_Company
                                  ,d.F_FullName AS F_Department
                                  ,u.F_Description
                                  ,u.F_CreateDate
                                  ,u.F_CreateUserName
                              FROM Base_User u
                              INNER JOIN Base_Department d ON u.F_DepartmentId=d.F_DepartmentId
                              INNER JOIN Base_Company o ON u.F_CompanyId=o.F_CompanyId WHERE u.F_DeleteMark = 0 ");
                return this.BaseRepository().FindTable(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public List<ManagerListModel> GetManagerList()
        {
            try
            {
                string objects = Config.GetValue("SquadLeader");
                StringBuilder UserRelationSql = new StringBuilder();
                UserRelationSql.Append(@"SELECT
                            Base_User.F_UserId AS user_id,
                            Base_User.F_RealName AS user_name
                        FROM
                            Base_User
                        LEFT JOIN Base_UserRelation ON Base_UserRelation.F_UserId = Base_User.F_UserId
                        WHERE
                            Base_UserRelation.F_ObjectId = '"+objects+ @"'
                        ORDER BY Base_User.F_CreateDate DESC");
                List<ManagerListModel> relationList = (List<ManagerListModel>) new RepositoryFactory().BaseRepository().FindList<ManagerListModel>(UserRelationSql.ToString());
                return relationList;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 验证是否为经理
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int VerificationManager(string userId)
        {
            try
            {
                int a = 0;
                string order = Config.GetValue("OrderAndTaskManage");
                StringBuilder sql = new StringBuilder();
                sql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + order + "'");
                List<UserEntity> users = (List<UserEntity>)new RepositoryFactory().BaseRepository().FindList<UserEntity>(sql.ToString());
                if(users.Count > 0)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        if (users[i].F_UserId == userId)
                        {
                            a = 1;
                        }
                    }
                }
                return a;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 用户实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public UserEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<UserEntity>(t => (t.F_UserId == keyValue || t.F_Mobile ==keyValue) && t.F_DeleteMark == 0);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        public List<UserEntity> GetUserList() {

            return this.BaseRepository().FindList<UserEntity>().ToList();
        }
        public void RefreshUserPost(ref UserEntity userEntity)
        {
            try
            {
                string sql = "SELECT F_ObjectId FROM Base_UserRelation WHERE F_UserId = '" + userEntity.F_UserId + "' AND F_Category=2 ";
                DataTable dataTable = BaseRepository().FindTable(sql);
                string post = "";
                if (dataTable != null)
                {
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        string id = dataRow["F_ObjectId"].ToString();
                        PostEntity postEntity = BaseRepository().FindEntity<PostEntity>(d => d.F_PostId == id);
                        post += postEntity.F_Name + ',';
                    }
                }
                if (post != "")
                {
                    if (post.Remove(0, post.Length - 1) == ",")
                    {
                        post = post.Substring(0, post.Length - 1);
                    }
                }
                userEntity.PostName = post;
                //string postId = "";
                //if(dataTable != null)
                //{
                //    foreach(DataRow dataRow in dataTable.Rows)
                //    {
                //        postId += dataRow["F_ObjectId"].ToString() + ',';
                //    }
                //}
                //if (!String.IsNullOrEmpty(postId))
                //{
                //    PostEntity postEntity1 = BaseRepository().FindEntity<PostEntity>(d => d.F_PostId == postId);
                //    if(postEntity1.F_ParentId == "0")
                //    {
                //        userEntity.PostLevel = "1";
                //    }
                //    else
                //    {
                //        IList<PostEntity> subList = (IList<PostEntity>)BaseRepository().FindList<PostEntity>(d => d.F_ParentId == postId);
                //        if(subList != null)
                //        {
                //            if(subList.Count > 0)
                //            {
                //                userEntity.PostLevel = "2";
                //            }
                //            else
                //            {
                //                userEntity.PostLevel = "3";
                //            }
                //        }
                //        else
                //        {
                //            userEntity.PostLevel = "3";
                //        }
                //    }
                //    userEntity.PostId = postId;
                //    userEntity.PostName = postEntity1.F_Name;
                //}
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 验证数据
        /// <summary>
        /// 账户不能重复
        /// </summary>
        /// <param name="account">账户值</param>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        public bool ExistAccount(string account, string keyValue)
        {
            try
            {
                var expression = LinqExtensions.True<UserEntity>();
                expression = expression.And(t => t.F_Account == account);
                if (!string.IsNullOrEmpty(keyValue))
                {
                    expression = expression.And(t => t.F_UserId != keyValue);
                }
                return this.BaseRepository().IQueryable(expression).Count() == 0 ? true : false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 虚拟删除
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void VirtualDelete(string keyValue)
        {
            try
            {
                UserEntity entity = new UserEntity()
                {
                    F_UserId = keyValue,
                    F_DeleteMark = 1
                };
                this.BaseRepository().Update(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 保存用户表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="userEntity">用户实体</param>
        /// <returns></returns>
        public void SaveEntity(string keyValue, UserEntity userEntity)
        {
            try
            {
                if (string.IsNullOrEmpty(keyValue))
                {
                    userEntity.Create();
                    userEntity.F_Secretkey = Md5Helper.Encrypt(CommonHelper.CreateNo(), 16).ToLower();
                    userEntity.F_Password = Md5Helper.Encrypt(DESEncrypt.Encrypt(userEntity.F_Password, userEntity.F_Secretkey).ToLower(), 32).ToLower();
                    this.BaseRepository().Insert(userEntity);
                }
                else
                {
                    userEntity.Modify(keyValue);
                    userEntity.F_Secretkey = null;
                    userEntity.F_Password = null;
                    this.BaseRepository().Update(userEntity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 修改用户登录密码
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="password">新密码（MD5 小写）</param>
        public void RevisePassword(string keyValue, string password)
        {
            try
            {
                UserEntity userEntity = new UserEntity();
                userEntity.Modify(keyValue);
                userEntity.F_Secretkey = Md5Helper.Encrypt(CommonHelper.CreateNo(), 16).ToLower();
                userEntity.F_Password = Md5Helper.Encrypt(DESEncrypt.Encrypt(password, userEntity.F_Secretkey).ToLower(), 32).ToLower();
                this.BaseRepository().Update(userEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="state">状态：1-启动；0-禁用</param>
        public void UpdateState(string keyValue, int state)
        {
            try
            {
                UserEntity userEntity = new UserEntity();
                userEntity.Modify(keyValue);
                userEntity.F_EnabledMark = state;
                this.BaseRepository().Update(userEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="userEntity">实体对象</param>
        public void UpdateEntity(UserEntity userEntity)
        {
            try
            {
                this.BaseRepository().Update(userEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 我的任务列表
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="type"></param>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public List<UserTaskResponseModel> GetUserTaskList(string keyValue,int type, Pagination pagination)
        {
            DateTime dateTime = DateTime.Now.AddDays(6);
            pagination.sidx = "CreateDate";
            pagination.sord = "desc";
             StringBuilder strsql = new StringBuilder();
            if (type==0)
            {
                strsql.Append(@"SELECT 0 AS type,WO_Order.W_Id AS Id,CONVERT(VARCHAR(16),WO_Order.W_CreateDate,20) AS CreateDate,
			                                  '位置:'+Base_DataItemDetail.F_ItemName+',描述:'+  CASE WHEN WO_Order.W_Content IS NULL THEN '' ELSE WO_Order.W_Content END AS Content
                                FROM WO_Order
                                LEFT JOIN WO_Order_Designate ON WO_Order_Designate.OrderId = WO_Order.W_Id
                                LEFT JOIN WO_Order_DesignateUser ON WO_Order_DesignateUser.Designate = WO_Order_Designate.Id
                                LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue=WO_Order.W_Place
                                WHERE WO_Order_DesignateUser.UserId = '" + keyValue+ @"'  AND (WO_Order.W_Status = 3 or WO_Order.W_Status = 8)
                                UNION ALL
                                SELECT 1 AS type, OA_Schedule.F_ScheduleId AS Id, CONVERT(VARCHAR(16), OA_Schedule.F_CreateDate, 20) AS CreateDate,
			                                   '会议室:'+Base_DataItemDetail.F_ItemName+',描述:' +CASE WHEN 	OA_Schedule.F_ScheduleContent IS NULL THEN '' ELSE OA_Schedule.F_ScheduleContent END  AS Content
                                 FROM  OA_Schedule
                               LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue=OA_Schedule.F_RoomId
                                WHERE OA_Schedule.F_Assign = '" + keyValue+"' AND  OA_Schedule.F_StartDate > '"+DateTime.Now+ @"'  
                                 UNION ALL
                                SELECT 2 AS type,Maintenance_Manage.Id, CONVERT(VARCHAR(16), Maintenance_Manage.StartTime, 20) AS CreateDate,
                                         '资产编码:'+ Property_Asset.A_Code+',设备:'+Property_Asset.A_Name  AS Content
                                FROM Maintenance_Manage
                                LEFT JOIN Property_Asset ON Property_Asset.A_Code=Maintenance_Manage.Property_Id
                                WHERE   Maintenance_Manage.UserId='" + keyValue + "' and Maintenance_Manage.StartTime > '" + dateTime + "' and  Maintenance_Manage.Start_Pic is null ");
            }
            else if (type==1)
            {
                strsql.Append(@"SELECT 0 AS type,WO_Order.W_Id AS Id,CONVERT(VARCHAR(16),WO_Order.W_CreateDate,20) AS CreateDate,
			                                  '位置:'+Base_DataItemDetail.F_ItemName+',描述:'+  CASE WHEN WO_Order.W_Content IS NULL THEN '' ELSE WO_Order.W_Content END AS Content
                                FROM WO_Order
                                LEFT JOIN WO_Order_Designate ON WO_Order_Designate.OrderId = WO_Order.W_Id
                                LEFT JOIN WO_Order_DesignateUser ON WO_Order_DesignateUser.Designate = WO_Order_Designate.Id
                                LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue=WO_Order.W_Place
                                WHERE WO_Order_DesignateUser.UserId = '" + keyValue + @"'  AND (WO_Order.W_Status = 4 or WO_Order.W_Status = 5 or WO_Order.W_Status = 6)
                                UNION ALL
                                SELECT 1 AS type, OA_Schedule.F_ScheduleId AS Id, CONVERT(VARCHAR(16), OA_Schedule.F_CreateDate, 20) AS CreateDate,
			                                   '会议室:'+Base_DataItemDetail.F_ItemName+',描述:' +CASE WHEN 	OA_Schedule.F_ScheduleContent IS NULL THEN '' ELSE OA_Schedule.F_ScheduleContent END  AS Content
                                 FROM  OA_Schedule
                                LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue=OA_Schedule.F_RoomId
                                WHERE OA_Schedule.F_Assign = '" + keyValue + "' AND '" + DateTime.Now + @"' between OA_Schedule.F_StartDate and OA_Schedule.F_EndDate
                                UNION ALL
                                SELECT 2 AS type, Maintenance_Manage.Id, CONVERT(VARCHAR(16), Maintenance_Manage.StartTime, 20) AS CreateDate,
                                         '资产编码:' + Property_Asset.A_Code + ',设备:' + Property_Asset.A_Name  AS Content
                                FROM Maintenance_Manage
                                LEFT JOIN Property_Asset ON Property_Asset.A_Code = Maintenance_Manage.Property_Id
                                WHERE   Maintenance_Manage.UserId = '" + keyValue + "' and Maintenance_Manage.StartTime < '" + dateTime + "' and  Maintenance_Manage.Start_Pic is null ");
            }
            else if (type==2)
            {
                strsql.Append(@"SELECT 0 AS type,WO_Order.W_Id AS Id,CONVERT(VARCHAR(16),WO_Order.W_CreateDate,20) AS CreateDate,
			                                  '位置:'+Base_DataItemDetail.F_ItemName+',描述:'+  CASE WHEN WO_Order.W_Content IS NULL THEN '' ELSE WO_Order.W_Content END AS Content
                                FROM WO_Order
                                LEFT JOIN WO_Order_Designate ON WO_Order_Designate.OrderId = WO_Order.W_Id
                                LEFT JOIN WO_Order_DesignateUser ON WO_Order_DesignateUser.Designate = WO_Order_Designate.Id
                                LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue=WO_Order.W_Place
                                WHERE WO_Order_DesignateUser.UserId = '" + keyValue + @"'  AND WO_Order.W_Status = 7
                                UNION ALL
                                 SELECT 1 AS type, OA_Schedule.F_ScheduleId AS Id, CONVERT(VARCHAR(16), OA_Schedule.F_CreateDate, 20) AS CreateDate,
			                                   '会议室:'+Base_DataItemDetail.F_ItemName+',描述:' +CASE WHEN 	OA_Schedule.F_ScheduleContent IS NULL THEN '' ELSE OA_Schedule.F_ScheduleContent END  AS Content
                                 FROM  OA_Schedule
                                LEFT JOIN Base_DataItemDetail ON Base_DataItemDetail.F_ItemValue=OA_Schedule.F_RoomId
                                WHERE OA_Schedule.F_Assign = '" + keyValue + "' AND  OA_Schedule.F_EndDate < '" + DateTime.Now + @"'
                                UNION ALL
                                SELECT 2 AS type, Maintenance_Manage.Id, CONVERT(VARCHAR(16), Maintenance_Manage.StartTime, 20) AS CreateDate,
                                         '资产编码:' + Property_Asset.A_Code + ',设备:' + Property_Asset.A_Name  AS Content
                                FROM Maintenance_Manage
                                LEFT JOIN Property_Asset ON Property_Asset.A_Code = Maintenance_Manage.Property_Id
                                WHERE   Maintenance_Manage.UserId = '" + keyValue + "' and  Maintenance_Manage.Start_Pic is not null ");
            }

            return new RepositoryFactory().BaseRepository().FindList<UserTaskResponseModel>(strsql.ToString(), pagination).ToList();

        }
        #endregion
    }
}
