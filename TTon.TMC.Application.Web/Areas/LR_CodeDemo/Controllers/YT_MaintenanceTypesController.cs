﻿using TTon.TMC.Util;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace TTon.TMC.Application.Web.Areas.LR_CodeDemo.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 11:48
    /// 描 述：配置维修类型
    /// </summary>
    public class YT_MaintenanceTypesController : MvcControllerBase
    {
        private YT_MaintenanceTypesIBLL yT_MaintenanceTypesIBLL = new YT_MaintenanceTypesBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaintenanceTypesTree(string keyValue)
        {
            var list = yT_MaintenanceTypesIBLL.GetMaintenanceTypesTree(keyValue);

            return Success(list);
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = yT_MaintenanceTypesIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var YT_MaintenanceTypesData = yT_MaintenanceTypesIBLL.GetYT_MaintenanceTypesEntity( keyValue );
            var jsonData = new {
                YT_MaintenanceTypesData = YT_MaintenanceTypesData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetEntity(string keyValue)
        {
            var YT_MaintenanceTypesData = yT_MaintenanceTypesIBLL.GetYT_MaintenanceTypesEntity(keyValue);
            return Success(YT_MaintenanceTypesData);
        }
        /// <summary>
        /// 获取类型列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList() {
            var data = yT_MaintenanceTypesIBLL.GetList();
            return Success(data);
        }
        /// <summary>
        /// 获取类型列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetListTree()
        {
            var data = yT_MaintenanceTypesIBLL.GetListTree();
            return Success(data);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            yT_MaintenanceTypesIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            YT_MaintenanceTypesEntity entity = strEntity.ToObject<YT_MaintenanceTypesEntity>();
            yT_MaintenanceTypesIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }
        #endregion

        #region 首页图表
        /// <summary>
        /// 零件平均消耗
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPartsTypesEcharts(string supplier)
        {
            var data = yT_MaintenanceTypesIBLL.GetPartsTypesEcharts(supplier);
            return Success(data);
        }

        /// <summary>
        /// 获取零件维修耗时
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="supplier"></param>
        /// <param name="part"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaintenanceTypesTime(string startTime,string endTime,string supplier,string part)
        {
            var data = yT_MaintenanceTypesIBLL.GetMaintenanceTypesTime(startTime, endTime,supplier, part);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetMaintenanceTypesTimeToAnalysis(string startTime, string endTime, string supplier)
        {
            var data = yT_MaintenanceTypesIBLL.GetMaintenanceTypesTimeToAnalysis(startTime, endTime, supplier);
            return Success(data);
        }
        #endregion
    }
}
