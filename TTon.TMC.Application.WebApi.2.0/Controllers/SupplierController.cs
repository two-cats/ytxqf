﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/supplier")]
    public class SupplierController : BaseApiController
    {
        private YT_SupplierIBLL supplierBLL = new YT_SupplierBLL();
        private YT_PartsTypesIBLL partsTypesBLL = new YT_PartsTypesBLL();
        private YT_MaintenanceTypesIBLL maintenanceTypesBLL = new YT_MaintenanceTypesBLL();
        private YT_InstructorIBLL instructorBLL = new YT_InstructorBLL();

        /// <summary>
        /// 获取供应商列表
        /// </summary>
        /// <returns></returns>
        [Route("list")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetSupplierList()
        {
            try
            {
                var data = supplierBLL.GetList();

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 根据供应商获取零件列表
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [Route("part/list")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetPartsList(string parentId)
        {
            try
            {
                var data = partsTypesBLL.GetListTree(parentId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 根据供应商和零件获取维修类型
        /// </summary>
        /// <param name="SupplierId"></param>
        /// <param name="PartId"></param>
        /// <returns></returns>
        [Route("part/maintenance/list")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetMaintenanceList()
        {
            try
            {
                var data = maintenanceTypesBLL.GetListTree();

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 根据供应商和零件和维修类型获取指导书
        /// </summary>
        /// <param name="SupplierId"></param>
        /// <param name="PartId"></param>
        /// <param name="MaintenanceId"></param>
        /// <returns></returns>
        [Route("part/maintenance/Instructor/list")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetInstructorList(string SupplierId, string PartId, string MaintenanceId)
        {
            try
            {
                var data = instructorBLL.GetInstructorList(SupplierId, PartId, MaintenanceId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 根据供应商编号获取供应商
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [Route("supplierentity")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage SupplierEntityByid(string Id)
        {
            try
            {
                var data = supplierBLL.SupplierEntityByid(Id);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 根据零件编号获取零件
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [Route("partentity")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage PartEntityByid(string Id)
        {
            try
            {
                var data = partsTypesBLL.PartsEntityByid(Id);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }
    }
}