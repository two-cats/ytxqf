﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.TaskManager
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class Task_UserInfoEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 指派人员
        /// </summary>
        [Column("USERID")]
        public string UserId { get; set; }
        /// <summary>
        /// 是否是工单负责人
        /// </summary>
        [Column("ISMASTER")]
        public int? IsMaster { get; set; }
        /// <summary>
        /// 任务角色
        /// </summary>
        [Column("TASKROLE")]
        public string TaskRole { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATUS")]
        public int? Status { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        [Column("TASKID")]
        public string TaskId { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion

        #region 扩展字段

        /// <summary>
        /// 用户名
        /// </summary>
        [NotMapped]
        public string UserName { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        public string UserAvatar { get; set; }

        #endregion
    }
}

