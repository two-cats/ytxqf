﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 验证身份
    /// </summary>
    public class IdentityModel
    {
        public int Manager { get; set; }

        public int Supplier { get; set; }

        public string SupplierId { get; set; }

        public string SupplierName { get; set; }

        public int SQE { get; set; }
    }
}
