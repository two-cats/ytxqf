﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-04-17 13:42
 * 描  述：审批理由
 */
var acceptClick;
var keyValue = request('keyValue');
var status = request('status');
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.bind();
        },
        bind: function () {
        },
    };
    acceptClick = function () {
        if (!$('body').lrValidform()) {
            return false;
        };
        var postData = {
            "ID": keyValue,
            "Approval": status,
            "Opinion": $('#F_Opinion').val()
        };
        $.ajax({
            url: "http://ytxwy.api.ttonservice.com/v1.0/Schedule/Approval",
            dataType: "json",
            data: postData,
            type: "POST",
            success: function (data) {
                var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                parent.layer.close(index);  // 关闭当前layer  
                parent.layer.close(index-1);  // 关闭layer  
            },
            error: function () {
                alert('审批失败，请重试！');
            }
        });
    };
    page.init();
}
