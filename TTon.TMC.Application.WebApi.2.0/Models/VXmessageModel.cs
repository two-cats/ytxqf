﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class VXmessageModel
   {
        /// <summary>
        /// 接收者openid
        /// </summary>
        public string touser { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string template_id { get; set; }
        /// <summary>
        /// 模板跳转链接
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 跳小程序所需数据，不需跳小程序可不用传该数据
        /// </summary>
        /// /// <summary>
        /// 模板ID
        /// </summary>
        public string topcolor { get; set; }
     
        public Datalist data { get; set; }
    }

   
    /// <summary>
    /// 模板数据
    /// </summary>r
    public class Datalist
    {
        public item first { get; set; }
        public item keyword1 { get; set; }
        public item keyword2 { get; set; }
        public item keyword3 { get; set; }
        public item keyword4 { get; set; }
        public item keyword5 { get; set; }
        public item remark { get; set; }

    }
    public class item
    {
         public string value { get; set; }
        /// <summary>
        /// 模板内容字体颜色，不填默认为黑色
        /// </summary>
        public string color { get; set; }

}

}