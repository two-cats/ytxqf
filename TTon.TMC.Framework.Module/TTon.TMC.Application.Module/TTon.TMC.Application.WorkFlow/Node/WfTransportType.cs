﻿
namespace TTon.TMC.Application.WorkFlow
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.17
    /// 描 述：工作流流转类型
    /// </summary>
    public enum WfTransportType
    {
        /// <summary>
        /// 同意
        /// </summary>
        Agree = 1,
        /// <summary>
        /// 不同意
        /// </summary>
        Disagree,
        /// <summary>
        /// 超时
        /// </summary>
        Overtime
    }
}
