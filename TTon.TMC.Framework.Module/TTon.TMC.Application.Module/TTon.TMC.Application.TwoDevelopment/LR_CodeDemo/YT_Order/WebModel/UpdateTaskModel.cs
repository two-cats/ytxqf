﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 更改任务零件数量
    /// </summary>
    public class UpdateTaskModel
    {
        /// <summary>
        /// 任务主键
        /// </summary>
        public string task_id { get; set; }

        /// <summary>
        /// 用户主键
        /// </summary>
        public string user_id { get; set; }

        /// <summary>
        /// 合格数量
        /// </summary>
        public int success { get; set; }
        /// <summary>
        /// 不合格数量
        /// </summary>
        public int error { get; set; }
    }
}
