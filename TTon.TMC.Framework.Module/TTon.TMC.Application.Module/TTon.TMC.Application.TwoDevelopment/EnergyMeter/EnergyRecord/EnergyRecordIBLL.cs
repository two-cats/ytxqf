﻿using TTon.TMC.Util;
using System.Collections.Generic;

namespace TTon.TMC.Application.TwoDevelopment.EnergyMeter
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-04-25 19:58
    /// 描 述：抄表
    /// </summary>
    public interface EnergyRecordIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Energy_ClockValueInfoEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取Energy_ClockValueInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Energy_ClockValueInfoEntity GetEnergy_ClockValueInfoEntity(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Energy_ClockValueInfoEntity entity);
        #endregion

    }
}
