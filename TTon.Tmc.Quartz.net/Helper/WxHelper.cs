﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TTon.Tmc.Util;
using TTon.TMC.Quartz.net.Model;

namespace ESteward.QuartzService.Helper
{
    public class WxHelper
    {
        /// <summary>
        /// WX
        /// </summary>
        /// <returns></returns>
        public static string getAccessToken()
        {
            string access_token = string.Empty;
            string appid = Config.GetValue("WeChatAppId");
            string secret = Config.GetValue("WeChatSecret");
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&APPID=" + appid + "&secret=" + secret;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxAccessTokenModel jsonResult = JsonConvert.DeserializeObject<WxAccessTokenModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            access_token = jsonResult.access_token;
            return access_token;
        }

        public static void WriteLog(string strLog)
        {
            string sFilePath = "C:\\" + DateTime.Now.ToString("yyyyMM");
            string sFileName = "rizhi" + DateTime.Now.ToString("dd") + ".log";
            sFileName = sFilePath + "\\" + sFileName; //文件的绝对路径
            if (!Directory.Exists(sFilePath))//验证路径是否存在
            {
                Directory.CreateDirectory(sFilePath);
                //不存在则创建
            }
            FileStream fs;
            StreamWriter sw;
            if (File.Exists(sFileName))
            //验证文件是否存在，有则追加，无则创建
            {
                fs = new FileStream(sFileName, FileMode.Append, FileAccess.Write);
            }
            else
            {
                fs = new FileStream(sFileName, FileMode.Create, FileAccess.Write);
            }
            sw = new StreamWriter(fs);
            sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "   ---   " + strLog);
            sw.Close();
            fs.Close();
        }


        /// <summary>
        /// WX
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="body"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        public static string pushMassage(string openid, string body, string access_token)
        {
            //第二步组装推送数进行推送
            string url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.ContentType = "application/json";

            byte[] buffer = encoding.GetBytes(body);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
