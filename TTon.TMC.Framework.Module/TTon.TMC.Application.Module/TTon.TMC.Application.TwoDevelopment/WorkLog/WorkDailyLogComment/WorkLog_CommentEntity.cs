﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.WorkLog

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:31
    /// 描 述：日志评论
    /// </summary>
    public class WorkLog_CommentEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// LogId
        /// </summary>
        /// <returns></returns>
        [Column("LOGID")]
        public string LogId { get; set; }
        /// <summary>
        /// Comment
        /// </summary>
        /// <returns></returns>
        [Column("COMMENT")]
        public string Comment { get; set; }
        /// <summary>
        /// CommentStar
        /// </summary>
        /// <returns></returns>
        [Column("COMMENTSTAR")]
        public int? CommentStar { get; set; }
        /// <summary>
        /// CommentType
        /// </summary>
        /// <returns></returns>
        [Column("COMMENTTYPE")]
        public int? CommentType { get; set; }
        /// <summary>
        /// CommentUserId
        /// </summary>
        /// <returns></returns>
        [Column("COMMENTUSERID")]
        public string CommentUserId { get; set; }
        /// <summary>
        /// CommentTime
        /// </summary>
        /// <returns></returns>
        [Column("COMMENTTIME")]
        public DateTime? CommentTime { get; set; }
        /// <summary>
        /// Flag
        /// </summary>
        /// <returns></returns>
        [Column("FLAG")]
        public int? Flag { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        /// <returns></returns>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion


        #region 扩展字段

        [NotMapped]
        public string CommentUserName { get; set; }

        [NotMapped]
        public string CommentUserAvatar { get; set; }

        #endregion
    }
}

