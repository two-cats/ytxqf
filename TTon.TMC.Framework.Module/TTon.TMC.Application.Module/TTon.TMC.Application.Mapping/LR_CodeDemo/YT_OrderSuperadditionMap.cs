﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_OrderSuperaddition;

namespace TTon.TMC.Application.Mapping.LR_CodeDemo
{
    public class YT_OrderSuperadditionMap : EntityTypeConfiguration<YT_OrderSuperadditionEntity>
    {
        public YT_OrderSuperadditionMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_OrderSuperaddition");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
