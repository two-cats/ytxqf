﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.TaskManager

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 16:20
    /// 描 述：任务操作日志
    /// </summary>
    public class Task_OperationLogEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        /// <returns></returns>
        [Column("TASKID")]
        public string TaskId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        /// <returns></returns>
        [Column("USERID")]
        public string UserId { get; set; }
        /// <summary>
        /// 任务操作（大写）Start->Confirm->Forward->Finish->Comment->Callback
        /// </summary>
        /// <returns></returns>
        [Column("OPERATIONMODE")]
        public string OperationMode { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        /// <returns></returns>
        [Column("OPERATIONTIME")]
        public DateTime? OperationTime { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        /// <returns></returns>
        [Column("SUMMARY")]
        public string Summary { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion

        #region 扩展字段

        /// <summary>
        /// 用户名称
        /// </summary>
        [NotMapped]
        public string UserName { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        [NotMapped]
        public string UserAvatar { get; set; }

        #endregion
    }
}

