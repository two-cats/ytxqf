﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-05-01 00:21
 * 描  述：会议列表
 */
var refreshGirdData;
var startTime = request('startTime');
var endTime = request('endTime');
var status = request('status');
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').jfGrid({
                url: top.$.rootUrl + '/OAModule/Schedule/GetEChartsPageList?startTime=' + startTime + '&endTime=' + endTime + '&Status=' + status,
                headData: [
                    { label: "会议名称", name: "scheduleName", width: 150, align: "center" },
                    { label: "预定房间", name: "roomName", width: 150, align: "center" },
                    { label: "与会人数", name: "peopleCount", width: 150, align: "center" },
                    { label: "开始时间", name: "startTime", width: 150, align: "center" },
                    { label: "结束时间", name: "endTime", width: 150, align: "center" },
                    { label: "会议服务", name: "services", width: 150, align: "center" },
                    { label: "会议设备", name: "equipment", width: 150, align: "center" },
                    { label: "创建人", name: "createUser", width: 150, align: "center" },
                    { label: "创建时间", name: "createTime", width: 150, align: "center" },
                ],
                mainId: 'id',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    page.init();
}
