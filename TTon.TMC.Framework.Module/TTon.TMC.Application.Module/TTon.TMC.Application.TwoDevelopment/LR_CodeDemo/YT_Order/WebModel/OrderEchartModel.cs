﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class OrderEchartModel
    {
        /// <summary>
        /// 本部
        /// </summary>
        public int OCount { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public int SCount { get; set; }
        /// <summary>
        /// SQE
        /// </summary>
        public int QCount { get; set; }
    }
}
