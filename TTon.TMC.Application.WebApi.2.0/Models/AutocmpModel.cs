﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class AutocmpModel
    {
        /// <summary>
        /// 提示内容
        /// </summary>
        public string hex { get; set; }
        /// <summary>
        /// 选择内容
        /// </summary>
        public string label { get; set; }
      
    }
}