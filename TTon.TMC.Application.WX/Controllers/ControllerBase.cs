﻿using Dos.WeChat;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TTon.TMC.Application.WX.Entities;

namespace TTon.TMC.Application.WX.Controllers
{
    public class ControllerBase : Controller
    {

        protected WechatUserInfo wechatUser;

        protected Base_User user;

        /// <summary>
        /// 校验是否是微信用户
        /// </summary>
        /// <returns></returns>
        protected bool HasLinkToWechatUser()
        {
            bool result = false;

            if (Request.QueryString["code"] == null)
            {
                //打开任意页面时，进行页面重定向
                string url = Request.Url.ToString();
                string appId = ConfigurationManager.AppSettings["WeChatAppId"];
                string scope = "snsapi_userinfo";
                string state = "WechatAuth";
                url = HttpUtility.UrlEncode(url);
                string wechatUrl = String.Format("https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope={2}&state={3}#wechat_redirect",
                                                    appId,
                                                    url,
                                                    scope,
                                                    state);
                Response.Redirect(wechatUrl);
            }
            else
            {
                string code = Request.QueryString["code"].ToString();

                var dic = UserHelper.GetOauth2AccessToken(new WeChatParam()
                {
                    Code = code
                });
                //获取用户OpenId
                var openId = dic["openid"];
                //根据用户OpenId以及access_token获取用户昵称、头像
                //前提是你的自定义菜单链接中的scope=snsapi_userinfo
                var userInfo = UserHelper.GetSnsUserInfo(new WeChatParam()
                {
                    OpenId = openId,
                    AccessToken = dic["access_token"]
                });
                string userAvatar = userInfo.HeadImgUrl;
                string userNickName = userInfo.NickName;

                ViewData["OpenId"] = userInfo.OpenId;
                wechatUser = new WechatUserInfo()
                {
                    OpenId = openId,
                    UserAvatar = userAvatar,
                    NickName = userNickName
                };

                user = DB.Context.From<Base_User>()
                               .Where(d => d.F_WeChat == openId)
                               .ToFirst();
                if (user != null)
                {
                    ViewData["UserId"] = user.F_UserId;
                    ViewData["UserName"] = user.F_RealName;
                    ViewData["Phone"] = user.F_Mobile;
                    result = true;
                }
                else
                {
                    //如果用户不存在，则跳转注册页面。
                    string url = ConfigurationManager.AppSettings["RegUrl"];
                    string appId = ConfigurationManager.AppSettings["WeChatAppId"];
                    string scope = "snsapi_userinfo";
                    string state = "WechatAuth";
                    url = HttpUtility.UrlEncode(url);
                    string wechatUrl = String.Format("https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope={2}&state={3}#wechat_redirect",
                                                        appId,
                                                        url,
                                                        scope,
                                                        state);
                    Response.Redirect(wechatUrl);

                }
            }

            return result;
        }


       
        public void GetWxSign(string param)
        {
            WxSignModel model = new WxSignModel();
            try
            {
                string token = GetAccessToken();
                string url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + token + "&type=jsapi";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "text/html;charset=UTF-8";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream);
                string retString = myStreamReader.ReadToEnd();
                WxTicketModel jsonResult = JsonConvert.DeserializeObject<WxTicketModel>(retString);
                myStreamReader.Close();
                myResponseStream.Close();
                string jsapi_ticket = jsonResult.ticket;

                string chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                Random randrom = new Random((int)DateTime.Now.Ticks);
                string str = "";
                for (int i = 0; i < 16; i++)
                {
                    str += chars[randrom.Next(chars.Length)];
                }
                string noncestr = str;

                TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                string timestamp = Convert.ToInt64(ts.TotalSeconds).ToString();

                string strparam = "jsapi_ticket=" + jsapi_ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + param;

                ViewData["signature"] = SHA1(strparam, Encoding.UTF8);
                ViewData["timestamp"] = timestamp;
                ViewData["nonceStr"] = noncestr;
                ViewData["appId"] = ConfigurationManager.AppSettings["WeChatAppId"];

                //Config.GetValue("WeChatAppId");

               
            }
            catch (Exception ex)
            {
              
            }
        }

        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        public static string GetAccessToken()
        {
            string access_token = string.Empty;
            string appid = ConfigurationManager.AppSettings["WeChatAppId"];
            string secret = ConfigurationManager.AppSettings["WeChatSecret"];
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&APPID=" + appid + "&secret=" + secret;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxAccessTokenModel jsonResult = JsonConvert.DeserializeObject<WxAccessTokenModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            access_token = jsonResult.access_token;
            //Log.Info("token", access_token);
            return access_token;
        }

        /// <summary>
        /// SHA1加密
        /// </summary>
        /// <param name="content"></param>
        /// <param name="encode"></param>
        /// <returns></returns>
        public static string SHA1(string content, Encoding encode)
        {
            try
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();
                byte[] bytes_in = encode.GetBytes(content);
                byte[] bytes_out = sha1.ComputeHash(bytes_in);
                sha1.Dispose();
                string result = BitConverter.ToString(bytes_out);
                result = result.Replace("-", "");
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("SHA1加密出错：" + ex.Message);
            }
        }

    }

    public class WxAccessTokenModel
    {
        /// <summary>
        /// 获取到的凭证
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// 凭证有效时间，单位：秒
        /// </summary>
        public int expires_in { get; set; }

        /// <summary>
        /// 用户刷新access_token
        /// </summary>
        public string refresh_token { get; set; }

        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public string openid { get; set; }

        /// <summary>
        /// 用户授权的作用域
        /// </summary>
        public string scope { get; set; }
    }


    public class WxTicketModel
    {
        public int errcode { get; set; }

        public string errmsg { get; set; }

        public string ticket { get; set; }

        public string expires_in { get; set; }
    }


    public class WxSignModel
    {
        /// <summary>
        /// AppId
        /// </summary>
        public string appId { get; set; }
        /// <summary>
        /// 时间戳
        /// </summary>
        public string timestamp { get; set; }
        /// <summary>
        /// 生成签名的随机串
        /// </summary>
        public string nonceStr { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string signature { get; set; }
    }


    public class WechatUserInfo
    { 

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        public string UserAvatar { get; set; }

        /// <summary>
        /// 用户OpenId
        /// </summary>
        public string OpenId { get; set; }
    }
}