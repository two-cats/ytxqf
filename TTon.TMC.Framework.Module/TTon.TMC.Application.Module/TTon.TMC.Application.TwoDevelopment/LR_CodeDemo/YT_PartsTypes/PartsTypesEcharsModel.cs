﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes
{
    /// <summary>
    ///  本月（时段）各零件挑选数量折线图
    /// </summary>
    public class PartsTypesEcharsModel
    {
        /// <summary>
        /// 日期
        /// </summary>
        public List<string> date { get; set; }
        /// <summary>
        /// 名称
        /// </summary>

        public string name { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public List<int> value { get; set; }
    }

    /// <summary>
    /// 本月（时段）零件挑选原因占比
    /// </summary>
    public class MaintenanceTypesModel
    {
        public string name { get; set; }

        public string value { get; set; }
    }

    /// <summary>
    /// 供应商累计合格率（合格数量/总数）
    /// </summary>
    public class PercentOfPassModel
    {
        public int success { get; set; }

        public int error { get; set; }
    }


    public class MaintenanceTypesAnalysis
    {
        public string maxname { get; set; }

        public string percent { get; set; }

        public List<MaintenanceTypesModel> list { get; set; }
    }
}
