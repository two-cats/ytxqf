﻿using ESteward.QuartzService.Helper;
using ESteward.QuartzService.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TTon.Tmc.Util;
using TTon.Tmc.Util.Extension;
using TTon.TMC.Quartz.net.Model;

namespace ESteward.QuartzService.BizLogic
{
    public class PushToSupplierOrderStatisticsLogic
    {
        public void pushToSupplierOrderStatistics()
        {
            DateTime endTime = DateTime.Now;
            DateTime startTime = endTime.AddHours(-12);

            StringBuilder str = new StringBuilder();
            str.Append(@"SELECT
	                Supplier_Id
                FROM
	                YT_Order
                WHERE
	                IsDelete = 0
                AND CreateDate BETWEEN '"+ startTime + @"'
                AND '"+endTime+@"'
                GROUP BY
	                Supplier_Id");
            DataTable supplierdt = DB_Pla.Context.FromSql(str.ToString()).ToDataTable();
            if (supplierdt.Rows.Count > 0)
            {
                for (int i = 0; i < supplierdt.Rows.Count; i++)
                {
                    YT_Supplier supplier = DB_Pla.Context.From<YT_Supplier>().Where(d => d.Id == supplierdt.Rows[i]["Supplier_Id"].ToString()).ToFirst();
                    if(supplier != null)
                    {
                        StringBuilder strSql = new StringBuilder();
                        strSql.Append(@"SELECT
	                            t.OrderNumber AS OrderNumber,
	                            ISNULL(t.DrawingNumber, '') AS DrawingNumber,
	                            YT_Supplier.Name AS Supplier,
	                            t.PartsNumber AS PartsNumber,
	                            YT_PartsTypes.Parts_Name AS PartsType,
	                            t.Batch_Number AS BatchNumber,
	                            (
		                            SELECT
			                            ISNULL(
				                            SUM (Error + Success + Unfinished),
				                            0
			                            )
		                            FROM
			                            YT_OrderUser
		                            WHERE
			                            Order_Id = t.Id
	                            ) AS [Sum],
	                            CASE
                            WHEN t.RepairType = '0' THEN
	                            '挑选'
                            WHEN t.RepairType = '1' THEN
	                            '返修'
                            ELSE
	                            ''
                            END AS RepairType,
                             ISNULL(
	                            YT_Instructor.IssueDescription,
	                            ''
                            ) AS IssueDescription,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (YT_OrderUser.Success),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
                            ) AS Qualified,
                             (
	                            SELECT
		                            ISNULL(SUM(YT_OrderUser.Error), 0)
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
                            ) AS Disqualified,
                             CONVERT (
	                            VARCHAR (100),
	                            t.CreateDate,
	                            120
                            ) AS CreateTime,
                             ISNULL(t.SuccessDate, '') AS SuccessDate,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM ([All] - Error - Success),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
                            ) AS Unfinished,
                             (
	                            SELECT
		                            ISNULL(
			                            SUM (
				                            YT_OrderUser.Error + YT_OrderUser.Success
			                            ),
			                            0
		                            )
	                            FROM
		                            YT_OrderUser
	                            WHERE
		                            Order_Id = t.Id
                            ) AS Finish
                            FROM
	                            YT_Order t
                            LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                            LEFT JOIN YT_Instructor ON YT_Instructor.Id = t.Instructor
                            WHERE t.Supplier_Id = '" + supplier.Id+@"'
                            AND t.CreateDate BETWEEN '"+startTime+"' AND '"+endTime+@"'
                            ORDER BY
	                            t.CreateDate DESC");
                        DataTable orderdt = DB_Pla.Context.FromSql(strSql.ToString()).ToDataTable();
                        if(orderdt.Rows.Count > 0)
                        {
                            int sum = orderdt.Compute("sum(Sum)", "TRUE").ToInt();
                            int finish = orderdt.Compute("sum(Finish)", "TRUE").ToInt();
                            int unfinished = orderdt.Compute("sum(Unfinished)", "TRUE").ToInt();
                            int qualified = orderdt.Compute("sum(Qualified)", "TRUE").ToInt();
                            int disqualified = orderdt.Compute("sum(Disqualified)", "TRUE").ToInt();

                            string start = startTime.ToString("yyyy-MM-dd HH:mm");
                            string end = endTime.ToString("yyyy-MM-dd HH:mm");

                            YT_OrderStatistics pushMessageEntity = new YT_OrderStatistics
                            {
                                Id = Guid.NewGuid().ToString(),
                                Amount = sum,
                                Finish = finish,
                                UnFinished = unfinished,
                                Qualified = qualified,
                                Unqualified = disqualified,
                                CreateDate = DateTime.Now,
                                SupplierId = supplier.Id,
                                TimeBucket = start + "至" + end
                            };
                            DB_Pla.Context.Insert<YT_OrderStatistics>(pushMessageEntity);

                            for (int a = 0; a < orderdt.Rows.Count; a++)
                            {
                                YT_Statistics statistics = new YT_Statistics
                                {
                                    Id = Guid.NewGuid().ToString(),
                                    OrderNumber = orderdt.Rows[a]["OrderNumber"].ToString(),
                                    DrawingNumber = orderdt.Rows[a]["DrawingNumber"].ToString(),
                                    Supplier = orderdt.Rows[a]["Supplier"].ToString(),
                                    PartsNumber = orderdt.Rows[a]["PartsNumber"].ToString(),
                                    PartsType = orderdt.Rows[a]["PartsType"].ToString(),
                                    BatchNumber = orderdt.Rows[a]["BatchNumber"].ToString(),
                                    IssueDescription = orderdt.Rows[a]["IssueDescription"].ToString(),
                                    RepairType = orderdt.Rows[a]["RepairType"].ToString(),
                                    Sum = orderdt.Rows[a]["Sum"].ToInt(),
                                    Qualified = orderdt.Rows[a]["Qualified"].ToInt(),
                                    Disqualified = orderdt.Rows[a]["Disqualified"].ToInt(),
                                    Unfinished = orderdt.Rows[a]["Unfinished"].ToInt(),
                                    CreateTime = orderdt.Rows[a]["CreateTime"].ToString(),
                                    SuccessDate = orderdt.Rows[a]["SuccessDate"].ToString(),
                                    ParentId = pushMessageEntity.Id
                                };
                                DB_Pla.Context.Insert<YT_Statistics>(statistics);
                            }

                            string[] array = supplier.Compellation.Split(',');
                            if (array.Length != 0)
                            {
                                foreach (var item in array)
                                {
                                    Base_User userEntity = DB_Pla.Context.From<Base_User>().Where(d => d.F_RealName == item).ToFirst();
                                    if (!string.IsNullOrEmpty(userEntity.F_WeChat))
                                    {
                                        string openid = userEntity.F_WeChat;
                                        //填写推送内容
                                        WX_MessageModel vxmessage = new WX_MessageModel();
                                        List<Datalist> ldl = new List<Datalist>();
                                        vxmessage.touser = openid;
                                        vxmessage.template_id = "saTtC5DmVcsD9xlqLANs1XDijlp2FHoUA0SSunwFk2g";
                                        vxmessage.url = "http://ytxqf.vx.ttonservice.com/Report/DayReport?id=" + pushMessageEntity.Id;
                                        vxmessage.topcolor = "#FF0000";
                                        vxmessage.data = new Datalist()
                                        {
                                            first = new item()
                                            {
                                                value = "您有一条新的报告已生成",
                                                color = ""
                                            },

                                            keyword1 = new item()
                                            {
                                                value = "今日工作汇总(" + DateTime.Now.ToString("yyyy-MM-dd") + ")",
                                                color = ""
                                            },
                                            keyword2 = new item()
                                            {
                                                value = supplier.Name,
                                                color = ""
                                            },
                                            keyword3 = new item()
                                            {
                                                value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                color = ""
                                            },
                                            remark = new item()
                                            {
                                                value = "点击详情查看更多",
                                                color = ""
                                            }
                                        };
                                        string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
                                        //获取access_token
                                        string access_token = WxHelper.getAccessToken();
                                        WxHelper.pushMassage(openid, jsonvxmessage, access_token);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
