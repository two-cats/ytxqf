﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class BizImageModel
    {
        /// <summary>
        /// 图片的id
        /// </summary>
        public string imgid { get; set; }
    }
}