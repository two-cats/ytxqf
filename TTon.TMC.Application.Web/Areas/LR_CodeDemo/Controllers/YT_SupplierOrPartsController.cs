﻿using TTon.TMC.Util;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Web.Mvc;
using System.Collections.Generic;
using NPOI.XSSF.UserModel;
using System.IO;
using System;
using System.Data;
using NPOI.SS.UserModel;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_SupplierOrParts.ImportExcel;
using NPOI.HSSF.UserModel;
using TTon.TMC.Application.Base.SystemModule;
using TTon.TMC.Application.Excel;
using TTon.TMC.Cache.Base;
using TTon.TMC.Cache.Factory;

namespace TTon.TMC.Application.Web.Areas.LR_CodeDemo.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 13:20
    /// 描 述：配置供应商零件
    /// </summary>
    public class YT_SupplierOrPartsController : MvcControllerBase
    {
        private YT_SupplierOrPartsIBLL yT_SupplierOrPartsIBLL = new YT_SupplierOrPartsBLL();
        private ExcelImportIBLL excelImportIBLL = new ExcelImportBLL();
        private AnnexesFileIBLL annexesFileIBLL = new AnnexesFileBLL();
        private ICache cache = CacheFactory.CaChe();
        private string cacheKey = "learun_adms_annexes_";
        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        /// <summary>
        /// 上传
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ImportForm() {
            return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = yT_SupplierOrPartsIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var YT_SupplierOrPartsData = yT_SupplierOrPartsIBLL.GetYT_SupplierOrPartsEntity( keyValue );
            var jsonData = new {
                YT_SupplierOrPartsData = YT_SupplierOrPartsData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 信息导入
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="Filedata"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExecuteImportExcel(string templateId, string fileId, int chunks, string suffix)
        {
            string path = SaveAnnexes(fileId, fileId, chunks, suffix);
            if (!string.IsNullOrEmpty(path))
            {
                List<ImportModel> list = GetDataFromExcel(path);
                int suess = 0;
                int err = 0;
                foreach (var item in list)
                {
                    try
                    {
                         yT_SupplierOrPartsIBLL.InsertImportExcel(item);
                         suess++;
                    }
                    catch (Exception)
                    {
                        err++;
                    }

                }
                var data = new
                {
                    Success = suess,
                    Fail = err
                };
                return Success(data);
            }
            else
            {
                return Fail("导入数据失败!");
            }
        }
        /// <summary>
        /// 获取数据集合
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private List<ImportModel> GetDataFromExcel(string FilePath)
        {
            XSSFWorkbook myWorkbook;
            XSSFSheet myWorksheet;
            try
            {
                using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    myWorkbook = new XSSFWorkbook(fs);
                    myWorksheet = myWorkbook.GetSheet("信息导入") as XSSFSheet;
                    string[] arrHeards = { "供应商名称","供应商联系人", "供应商电话", "零件名称", "零件编号", "维修类型"};
                    DataTable ds = ReadDataFromExcel(myWorksheet, false, arrHeards, 0);
                    List<ImportModel> listEntity = DetailListToModel(ds);
                    return listEntity;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        /// <summary>
        /// 转换对象
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<ImportModel> DetailListToModel(DataTable ds)
        {
            if (ds == null)
            {
                return null;
            }
            List<ImportModel> result = new List<ImportModel>();

            foreach (DataRow row in ds.Rows)
            {
                ImportModel model = new ImportModel();
                model = DataRowToModel(row);
                if (model != null)
                {
                    result.Add(model);
                }
            }
            return result;
        }

        /// <summary>
        /// 转换
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public ImportModel DataRowToModel(DataRow row)
        {
            ImportModel model = new ImportModel();
            if (row != null)
            {
                if (row["供应商名称"] != null && row["供应商名称"].ToString() != "")
                {
                    model.Supplier_Name = row["供应商名称"].ToString();
                }
                if (row["供应商联系人"] != null && row["供应商联系人"].ToString() != "")
                {
                    model.Supplier_user = row["供应商联系人"].ToString();
                }
                if (row["供应商电话"] != null && row["供应商电话"].ToString() != "")
                {
                    model.Supplier_phone = row["供应商电话"].ToString();
                }

                if (row["零件名称"] != null && row["零件名称"].ToString() != "")
                {
                    model.Parts_Name = row["零件名称"].ToString();
                }

                if (row["零件编号"] != null && row["零件编号"].ToString() != "")
                {
                    model.Parts_Number = row["零件编号"].ToString();
                }

                if (row["维修类型"] != null && row["维修类型"].ToString() != "")
                {
                    model.MaintenanceTypes = row["维修类型"].ToString();
                }




            }
            return model;
        }
        /// <summary>
        /// 读取行列
        /// </summary>
        /// <param name="mySheet"></param>
        /// <param name="isColumnName"></param>
        /// <param name="arrHeads"></param>
        /// <param name="startRow"></param>
        /// <returns></returns>
        public static DataTable ReadDataFromExcel(ISheet mySheet, bool isColumnName, string[] arrHeads, int startRow)
        {
            DataTable dt = new DataTable();
            DataColumn column;
            DataRow dataRow;
            IRow row;
            ICell cell;

            try
            {
                if (mySheet != null)
                {
                    //dt = new DataTable();
                    // 当前 Sheet 页数据的总行数  
                    int rowCount = mySheet.LastRowNum;

                    if (rowCount > startRow)
                    {
                        // 第五行  
                        IRow firstRow = mySheet.GetRow(startRow);
                        // 列数
                        int columnCount = firstRow.LastCellNum;

                        //构建datatable的列  
                        if (isColumnName)
                        {
                            //startRow = 4;//如果第一行是列名，则从第二行开始读取  
                            for (int i = firstRow.FirstCellNum; i < columnCount; ++i)
                            {
                                cell = firstRow.GetCell(i);
                                if (cell != null)
                                {
                                    if (cell.StringCellValue != null)
                                    {
                                        column = new DataColumn(cell.StringCellValue);
                                        dt.Columns.Add(column);
                                    }
                                }
                            }
                        }
                        else
                        {

                            for (int i = firstRow.FirstCellNum; i < columnCount; ++i)
                            {
                                column = new DataColumn(arrHeads[i]);
                                dt.Columns.Add(column);
                            }
                        }
                        //填充行  
                        for (int i = startRow + 1; i <= rowCount; ++i)
                        {
                            row = mySheet.GetRow(i);
                            if (row == null) continue;
                            dataRow = dt.NewRow();
                            for (int j = row.FirstCellNum; j < columnCount; ++j)
                            {
                                cell = row.GetCell(j);
                                if (cell == null)
                                {
                                    dataRow[j] = "";
                                }
                                else
                                {
                                    switch (cell.CellType)
                                    {
                                        case CellType.Blank:
                                            dataRow[j] = "";
                                            break;
                                        case CellType.Numeric:

                                            if (HSSFDateUtil.IsCellDateFormatted(cell))
                                            {
                                                dataRow[j] = cell.DateCellValue;
                                            }
                                            else
                                            {
                                                dataRow[j] = cell.NumericCellValue.ToString();
                                            }
                                            break;
                                        case CellType.String:
                                            dataRow[j] = cell.StringCellValue;
                                            break;
                                        case CellType.Formula:
                                            dataRow[j] = cell.NumericCellValue.ToString();


                                            break;

                                    }
                                }
                            }
                            dt.Rows.Add(dataRow);
                        }
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        /// <summary>
        /// 保存附件（支持大文件分片传输）
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名称</param>
        /// <param name="chunks">文件总共分多少片</param>
        /// <param name="fileStream">文件二进制流</param>
        /// <returns></returns>
        public string SaveAnnexes(string fileGuid, string fileName, int chunks, string suffix)
        {
            try
            {
                UserInfo userInfo = LoginUserInfo.Get();
                //获取文件完整文件名(包含绝对路径)
                //文件存放路径格式：/Resource/ResourceFile/{userId}/{date}/{guid}.{后缀名}
                string filePath = Config.GetValue("AnnexesFile");
                string uploadDate = DateTime.Now.ToString("yyyyMMdd");
                string virtualPath = string.Format("{0}/{1}/{2}/{3}.{4}", filePath, userInfo.userId, uploadDate, fileGuid, suffix);
                //创建文件夹
                string path = Path.GetDirectoryName(virtualPath);
                Directory.CreateDirectory(path);
                AnnexesFileEntity fileAnnexesEntity = new AnnexesFileEntity();
                if (!System.IO.File.Exists(virtualPath))
                {
                    long filesize = SaveAnnexesToFile(fileGuid, virtualPath, chunks);
                    if (filesize == -1)// 表示保存失败
                    {
                        new AnnexesFileBLL().RemoveChunkAnnexes(fileGuid, chunks);
                        return "";
                    }
                }
                return virtualPath;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 保存附件到文件中
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="filePath">文件路径</param>
        /// <param name="chunks">总共分片数</param>
        /// <param name="buffer">文件二进制流</param>
        /// <returns>-1:表示保存失败</returns>
        public long SaveAnnexesToFile(string fileGuid, string filePath, int chunks)
        {
            try
            {
                long filesize = 0;
                //创建一个FileInfo对象
                FileInfo file = new FileInfo(filePath);
                //创建文件
                FileStream fs = file.Create();
                for (int i = 0; i < chunks; i++)
                {
                    byte[] bufferByRedis = cache.Read<byte[]>(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                    if (bufferByRedis == null)
                    {
                        return -1;
                    }
                    //写入二进制流
                    fs.Write(bufferByRedis, 0, bufferByRedis.Length);
                    filesize += bufferByRedis.Length;
                    cache.Remove(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                }
                //关闭文件流
                fs.Close();

                return filesize;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            yT_SupplierOrPartsIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            YT_SupplierOrPartsEntity entity = strEntity.ToObject<YT_SupplierOrPartsEntity>();
            yT_SupplierOrPartsIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }
        #endregion

    }
}
