﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.EnergyMeter
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-04-23 21:45
    /// 描 述：能源表基本信息
    /// </summary>
    public class Energy_ClockBaseInfoEntity 
    {
        #region 实体成员
        /// <summary>
        /// 能源表ID
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 关联设备ID
        /// </summary>
        [Column("ASSETID")]
        public string AssetID { get; set; }
        /// <summary>
        /// 能源表名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 能源表状态
        /// </summary>
        [Column("STATUS")]
        public int? Status { get; set; }
        /// <summary>
        /// 能源表所在位置
        /// </summary>
        [Column("LOCATION")]
        public string Location { get; set; }
        /// <summary>
        /// 能源表描述
        /// </summary>
        [Column("SUMMARY")]
        public string Summary { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

