﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using ESteward.QuartzService.Helper;
using ESteward.QuartzService.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.Tmc.Util;
using TTon.TMC.Quartz.net.Model;

namespace ESteward.QuartzService.BizLogic
{
    public class PushToSupplierLogic
    {
        public void pushToSupplierLogic()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
	                    t.Id AS Id,
	                    t.Supplier_Id AS SupplierId,
	                    ISNULL(t.WxPushCount, 0) AS WxPushCount,
	                    ISNULL(t.SmsPushCount, 0) AS SmsPushCount,
	                    t.PartsType AS PartsType,
	                    t.RepairType AS RepairType,
	                    t.Remark AS Remark,
	                    t.Name AS Name,
	                    YT_PartsTypes.Parts_Name AS Parts_Name,
	                    YT_Supplier.Compellation AS Compellation,
	                    YT_Supplier.Phone AS Phone,
	                    t.CreateUserId AS UserId
                    FROM
	                    YT_Order t
                    LEFT JOIN YT_PushMessage ON YT_PushMessage.ParentId = t.Id
                    LEFT JOIN YT_Supplier ON YT_Supplier.Id = t.Supplier_Id
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = t.PartsType
                    WHERE
	                    (
		                    t.Instructor = ''
		                    OR t.Instructor IS NULL
	                    )
                    AND t.IsDelete = 0
                    AND t.Order_Status != 3
                    AND t.SupplierStatus != 2");
            DataTable dts = DB_Pla.Context.FromSql(strSql.ToString()).ToDataTable();
            if(dts.Rows.Count > 0)
            {
                for (int i = 0; i < dts.Rows.Count; i++)
                {
                    int aa = Convert.ToInt32(dts.Rows[i]["WxPushCount"].ToString());
                    int bb = Convert.ToInt32(dts.Rows[i]["SmsPushCount"].ToString());
                    YT_PushMessage pushMessage = DB_Pla.Context.From<YT_PushMessage>().Where(d => d.ParentId == dts.Rows[i]["Id"].ToString()).ToFirst();
                    if (pushMessage == null)
                    {
                        //获取质检部经理
                        string Quality = Config.GetValue("QualityManager");
                        StringBuilder QualitySql = new StringBuilder();
                        QualitySql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + Quality + "' and Base_UserRelation.F_UserId='" + dts.Rows[i]["UserId"].ToString() + "'  ");
                        List<Base_User> QualityList = DB_Pla.Context.FromSql(QualitySql.ToString()).ToList<Base_User>();
                        if(QualityList.Count > 0)
                        {
                            #region 微信
                            if (!string.IsNullOrEmpty(dts.Rows[i]["Compellation"].ToString()))
                            {
                                string[] array = dts.Rows[i]["Compellation"].ToString().Split(',');
                                if (array.Length != 0)
                                {
                                    foreach (var item in array)
                                    {
                                        Base_User userEntity = DB_Pla.Context.From<Base_User>().Where(d => d.F_RealName == item).ToFirst();
                                        if (!string.IsNullOrEmpty(userEntity.F_WeChat))
                                        {
                                            string openid = userEntity.F_WeChat;
                                            //填写推送内容
                                            WX_MessageModel vxmessage = new WX_MessageModel();
                                            List<Datalist> ldl = new List<Datalist>();
                                            vxmessage.touser = openid;
                                            vxmessage.template_id = "7_91rKwiCRYZ7xjIXPhalkV4FYRz3nSy1-_BHSLm0WY";
                                            vxmessage.url = "http://ytxqf.vx.ttonservice.com/guidebook/edit?id=" + dts.Rows[i]["Id"].ToString();
                                            vxmessage.topcolor = "#FF0000";
                                            vxmessage.data = new Datalist()
                                            {
                                                first = new item()
                                                {
                                                    value = "您好，您有一条新的零件异常信息需要处理",
                                                    color = ""
                                                },

                                                keyword1 = new item()
                                                {
                                                    value = dts.Rows[i]["RepairType"].ToString().Equals("0") ? "挑选" : "返修",
                                                    color = ""
                                                },
                                                keyword2 = new item()
                                                {
                                                    value = dts.Rows[i]["Parts_Name"].ToString(),
                                                    color = ""
                                                },
                                                keyword3 = new item()
                                                {
                                                    value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                    color = ""
                                                },
                                                keyword4 = new item()
                                                {
                                                    value = dts.Rows[i]["Remark"].ToString(),
                                                    color = ""
                                                },
                                                remark = new item()
                                                {
                                                    value = dts.Rows[i]["Name"].ToString() + "对" + dts.Rows[i]["Parts_Name"].ToString() + "产生的问题进行了描述，请完善订单。",
                                                    color = ""
                                                }
                                            };
                                            string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
                                            //获取access_token
                                            string access_token = WxHelper.getAccessToken();
                                            WxHelper.pushMassage(openid, jsonvxmessage, access_token);
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region 短信
                            String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
                            String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
                            String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
                            String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
                            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
                            //初始化ascClient,暂时不支持多region（请勿修改）
                            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
                            IAcsClient acsClient = new DefaultAcsClient(profile);
                            SendSmsRequest request = new SendSmsRequest();
                            //手机号
                            request.PhoneNumbers = dts.Rows[i]["Phone"].ToString();
                            //必填:短信签名-可在短信控制台中找到
                            request.SignName = "亚泰新";
                            //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                            request.TemplateCode = "SMS_143560574";
                            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                            request.TemplateParam = "";
                            //请求失败这里会抛ClientException异常
                            SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                            #endregion

                            #region 更新订单表 + 创建推送信息
                            YT_Order orderEntity = DB_Pla.Context.From<YT_Order>().Where(d => d.Id == dts.Rows[i]["Id"].ToString()).ToFirst();
                            orderEntity.WxPushStatus = 1;
                            orderEntity.SmsPushStatus = 1;
                            orderEntity.WxPushCount = aa + 1;
                            orderEntity.SmsPushCount = bb + 1;
                            DB_Pla.Context.Update<YT_Order>(orderEntity);

                            YT_PushMessage pushMessageEntity = new YT_PushMessage
                            {
                                Id = Guid.NewGuid().ToString(),
                                ParentId = dts.Rows[i]["Id"].ToString(),
                                PushTime = DateTime.Now
                            };
                            DB_Pla.Context.Insert<YT_PushMessage>(pushMessageEntity);
                            #endregion
                        }
                        else
                        {
                            //获取供应商
                            string Supplier = Config.GetValue("Supplier");
                            StringBuilder SupplierSql = new StringBuilder();
                            SupplierSql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + Supplier + "' and Base_UserRelation.F_UserId='" + dts.Rows[i]["UserId"].ToString() + "'  ");
                            List<Base_User> SupplierList = DB_Pla.Context.FromSql(SupplierSql.ToString()).ToList<Base_User>();
                            if(SupplierList.Count > 0)
                            {
                                //获取值班经理
                                StringBuilder strsql = new StringBuilder();
                                strsql.Append(@"select Base_User.* from Base_User
                                          WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
                                List<Base_User> userList = DB_Pla.Context.FromSql(strsql.ToString()).ToList<Base_User>();
                                Base_User model = userList[0];
                                if (model != null)
                                {
                                    #region 短信
                                    String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
                                    String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
                                    String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
                                    String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
                                    IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
                                    //初始化ascClient,暂时不支持多region（请勿修改）
                                    DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
                                    IAcsClient acsClient = new DefaultAcsClient(profile);
                                    SendSmsRequest request = new SendSmsRequest();
                                    //手机号
                                    request.PhoneNumbers = model.F_Mobile;
                                    //必填:短信签名-可在短信控制台中找到
                                    request.SignName = "亚泰新";
                                    //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                                    request.TemplateCode = "SMS_143560574";
                                    //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                                    request.TemplateParam = "";
                                    //请求失败这里会抛ClientException异常
                                    SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                                    #endregion

                                    #region 更新订单表 + 创建推送信息
                                    YT_Order orderEntity = DB_Pla.Context.From<YT_Order>().Where(d => d.Id == dts.Rows[i]["Id"].ToString()).ToFirst();
                                    orderEntity.WxPushStatus = 0;
                                    orderEntity.SmsPushStatus = 1;
                                    orderEntity.WxPushCount = aa;
                                    orderEntity.SmsPushCount = bb + 1;
                                    DB_Pla.Context.Update<YT_Order>(orderEntity);

                                    YT_PushMessage pushMessageEntity = new YT_PushMessage
                                    {
                                        Id = Guid.NewGuid().ToString(),
                                        ParentId = dts.Rows[i]["Id"].ToString(),
                                        PushTime = DateTime.Now
                                    };
                                    DB_Pla.Context.Insert<YT_PushMessage>(pushMessageEntity);
                                    #endregion
                                }
                            }
                        }
                    }
                    else
                    {
                        TimeSpan ts = (TimeSpan) (DateTime.Now - pushMessage.PushTime);
                        if(ts.Minutes >= 30)
                        {
                            //获取质检部经理
                            string Quality = Config.GetValue("QualityManager");
                            StringBuilder QualitySql = new StringBuilder();
                            QualitySql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + Quality + "' and Base_UserRelation.F_UserId='" + dts.Rows[i]["UserId"].ToString() + "'  ");
                            List<Base_User> QualityList = DB_Pla.Context.FromSql(QualitySql.ToString()).ToList<Base_User>();
                            if (QualityList.Count > 0)
                            {
                                #region 微信
                                if (aa < 6)
                                {
                                    if (!string.IsNullOrEmpty(dts.Rows[i]["Compellation"].ToString()))
                                    {
                                        string[] array = dts.Rows[i]["Compellation"].ToString().Split(',');
                                        if (array.Length != 0)
                                        {
                                            foreach (var item in array)
                                            {
                                                Base_User userEntity = DB_Pla.Context.From<Base_User>().Where(d => d.F_RealName == item).ToFirst();
                                                if (!string.IsNullOrEmpty(userEntity.F_WeChat))
                                                {
                                                    string openid = userEntity.F_WeChat;
                                                    //填写推送内容
                                                    WX_MessageModel vxmessage = new WX_MessageModel();
                                                    List<Datalist> ldl = new List<Datalist>();
                                                    vxmessage.touser = openid;
                                                    vxmessage.template_id = "7_91rKwiCRYZ7xjIXPhalkV4FYRz3nSy1-_BHSLm0WY";
                                                    vxmessage.url = "http://ytxqf.vx.ttonservice.com/guidebook/edit?id=" + dts.Rows[i]["Id"].ToString();
                                                    vxmessage.topcolor = "#FF0000";
                                                    vxmessage.data = new Datalist()
                                                    {
                                                        first = new item()
                                                        {
                                                            value = "您好，您有一条新的零件异常信息需要处理",
                                                            color = ""
                                                        },

                                                        keyword1 = new item()
                                                        {
                                                            value = dts.Rows[i]["RepairType"].ToString().Equals("0") ? "挑选" : "返修",
                                                            color = ""
                                                        },
                                                        keyword2 = new item()
                                                        {
                                                            value = dts.Rows[i]["Parts_Name"].ToString(),
                                                            color = ""
                                                        },
                                                        keyword3 = new item()
                                                        {
                                                            value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                            color = ""
                                                        },
                                                        keyword4 = new item()
                                                        {
                                                            value = dts.Rows[i]["Remark"].ToString(),
                                                            color = ""
                                                        },
                                                        remark = new item()
                                                        {
                                                            value = dts.Rows[i]["Name"].ToString() + "对" + dts.Rows[i]["Parts_Name"].ToString() + "产生的问题进行了描述，请完善订单。",
                                                            color = ""
                                                        }
                                                    };
                                                    string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
                                                    //获取access_token
                                                    string access_token = WxHelper.getAccessToken();
                                                    WxHelper.pushMassage(openid, jsonvxmessage, access_token);
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region 短信
                                if (bb < 6)
                                {
                                    String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
                                    String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
                                    String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
                                    String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
                                    IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
                                    //初始化ascClient,暂时不支持多region（请勿修改）
                                    DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
                                    IAcsClient acsClient = new DefaultAcsClient(profile);
                                    SendSmsRequest request = new SendSmsRequest();
                                    //手机号
                                    request.PhoneNumbers = dts.Rows[i]["Phone"].ToString();
                                    //必填:短信签名-可在短信控制台中找到
                                    request.SignName = "亚泰新";
                                    //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                                    request.TemplateCode = "SMS_143560574";
                                    //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                                    request.TemplateParam = "";
                                    //请求失败这里会抛ClientException异常
                                    SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                                }
                                #endregion

                                #region 更新订单表 + 创建推送信息
                                YT_Order orderEntity = DB_Pla.Context.From<YT_Order>().Where(d => d.Id == dts.Rows[i]["Id"].ToString()).ToFirst();
                                orderEntity.WxPushStatus = 1;
                                orderEntity.SmsPushStatus = 1;
                                if (aa < 6)
                                {
                                    orderEntity.WxPushCount = aa + 1;
                                }
                                if (bb < 6)
                                {
                                    orderEntity.SmsPushCount = bb + 1;
                                }
                                DB_Pla.Context.Update<YT_Order>(orderEntity);

                                if (aa < 6 && bb < 6)
                                {
                                    pushMessage.PushTime = DateTime.Now;
                                    DB_Pla.Context.Update<YT_PushMessage>(pushMessage);
                                }
                                #endregion
                            }
                            else
                            {
                                //获取供应商
                                string Supplier = Config.GetValue("Supplier");
                                StringBuilder SupplierSql = new StringBuilder();
                                SupplierSql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + Supplier + "' and Base_UserRelation.F_UserId='" + dts.Rows[i]["UserId"].ToString() + "'  ");
                                List<Base_User> SupplierList = DB_Pla.Context.FromSql(SupplierSql.ToString()).ToList<Base_User>();
                                if (SupplierList.Count > 0)
                                {
                                    //获取值班经理
                                    StringBuilder strsql = new StringBuilder();
                                    strsql.Append(@"select Base_User.* from Base_User
                                          WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
                                    List<Base_User> userList = DB_Pla.Context.FromSql(strsql.ToString()).ToList<Base_User>();
                                    Base_User model = userList[0];
                                    if (model != null)
                                    {
                                        #region 短信
                                        if (bb < 6)
                                        {
                                            String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
                                            String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
                                            String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
                                            String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
                                            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
                                            //初始化ascClient,暂时不支持多region（请勿修改）
                                            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
                                            IAcsClient acsClient = new DefaultAcsClient(profile);
                                            SendSmsRequest request = new SendSmsRequest();
                                            //手机号
                                            request.PhoneNumbers = model.F_Mobile;
                                            //必填:短信签名-可在短信控制台中找到
                                            request.SignName = "亚泰新";
                                            //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                                            request.TemplateCode = "SMS_143560574";
                                            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                                            request.TemplateParam = "";
                                            //请求失败这里会抛ClientException异常
                                            SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                                        }
                                        #endregion

                                        #region 更新订单表 + 创建推送信息
                                        if (bb < 6)
                                        {
                                            YT_Order orderEntity = DB_Pla.Context.From<YT_Order>().Where(d => d.Id == dts.Rows[i]["Id"].ToString()).ToFirst();
                                            orderEntity.WxPushStatus = 0;
                                            orderEntity.SmsPushStatus = 1;
                                            orderEntity.WxPushCount = aa;
                                            orderEntity.SmsPushCount = bb + 1;
                                            DB_Pla.Context.Update<YT_Order>(orderEntity);

                                            pushMessage.PushTime = DateTime.Now;
                                            DB_Pla.Context.Update<YT_PushMessage>(pushMessage);
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
