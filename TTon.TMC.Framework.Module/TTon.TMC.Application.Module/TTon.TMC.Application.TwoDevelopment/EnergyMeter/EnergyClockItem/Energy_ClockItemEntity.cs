﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.EnergyMeter
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-04-24 11:38
    /// 描 述：能源表参数
    /// </summary>
    public class Energy_ClockItemEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 能源表Id
        /// </summary>
        [Column("CLOCKID")]
        public string ClockId { get; set; }
        /// <summary>
        /// 能源表参数名称
        /// </summary>
        [Column("ITEMNAME")]
        public string ItemName { get; set; }
        /// <summary>
        /// 能源表参数单位
        /// </summary>
        [Column("ITEMUNIT")]
        public string ItemUnit { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column("STATUS")]
        public int? Status { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 能源表参数类型
        /// </summary>
        [Column("ITEMTYPE")]
        public string ItemType { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

