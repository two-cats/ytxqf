﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace TTon.TMC.Application.TwoDevelopment.TaskManager
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class TaskBillInfoService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Task_BaseBillInfoEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Id,
                t.Subject,
                t.Category,
                t.PlaceId,
                t.PlaceExtend,
                t.Level,
                t.Summary,
                t.Status,
                t.SubmitTime,
                t.ConfirmTime,
                t.ForwardTime,
                t.FinishTime,
                t.CommentTime,
                t.CallbackTime,
                t.PhysicalStatus,
                t.DisplayStatus,
                t1.FilePath,
                t1.FileType,
                t2.MaterialId,
                t2.Count,
                t2.Remark,
                t3.UserId,
                t3.IsMaster,
                t3.TaskRole
                ");
                strSql.Append("  FROM Task_BaseBillInfo t ");
                strSql.Append("  LEFT JOIN Task_FileInfo t1 ON t1.TaskId = t.Id ");
                strSql.Append("  LEFT JOIN Task_MaterialInfo t2 ON t2.TaskId = t.Id ");
                strSql.Append("  LEFT JOIN Task_UserInfo t3 ON t3.TaskId = t.Id ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<Task_BaseBillInfoEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_BaseBillInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_BaseBillInfoEntity GetTask_BaseBillInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Task_BaseBillInfoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #region Web API

        public IList<Task_BaseBillInfoEntity> GetCurrentTaskByUser(string userId)
        {
            try
            {
                //var dp = new DynamicParameters(new { });
                string sql = String.Format(@"SELECT * FROM Task_BaseBillInfo WHERE ID IN (
                            SELECT TaskId FROM Task_UserInfo WHERE UserId = '{0}' AND IsMaster=1 AND Status=1)
                            AND DisplayStatus='PROCESS' ORDER BY SubmitTime DESC ", userId);
                IList<Task_BaseBillInfoEntity> list = (IList<Task_BaseBillInfoEntity>)BaseRepository().FindList<Task_BaseBillInfoEntity>(sql);
                return list;
            }
            catch (Exception ex)
            {
                if(ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public IList<Task_BaseBillInfoEntity> GetTaskByUser(string userId,string category)
        {
            try
            {
                string cate = category.ToUpper();
                string sql = String.Format(@"SELECT * FROM Task_BaseBillInfo WHERE ID IN (
                            SELECT TaskId FROM Task_UserInfo WHERE UserId = '{0}' AND IsMaster=1 AND Status=1)
                            AND DisplayStatus='{1}' ORDER BY SubmitTime DESC ", userId, cate);
                IList<Task_BaseBillInfoEntity> list = (IList<Task_BaseBillInfoEntity>)BaseRepository().FindList<Task_BaseBillInfoEntity>(sql);
                return list;
            }
            catch (Exception ex )
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        public dynamic GetTaskDetail(string taskId)
        {
            try
            {
                dynamic taskDetail = null;

                Task_BaseBillInfoEntity baseBillInfoEntity = BaseRepository().FindEntity<Task_BaseBillInfoEntity>(taskId);

                //任务附件
                IList<Task_FileInfoEntity> fileList = (IList<Task_FileInfoEntity>)BaseRepository().FindList<Task_FileInfoEntity>(d => d.TaskId == taskId && d.Status == 1);

                //任务人员
                string userSql = @"SELECT t1.Id,
                                    t1.UserId,
                                    t1.IsMaster,
                                    t1.TaskRole,
                                    t1.Status,
                                    t1.UpdateTime,
                                    t1.TaskId,
                                    t2.F_RealName as UserName,
                                    t2.F_HeadIcon as UserAvatar
                                    FROM Task_UserInfo t1
                                    LEFT JOIN Base_User t2 ON t1.UserId = t2.F_UserId
                                    WHERE t1.TaskId = '{0}' AND t1.Status=1";
                IList<Task_UserInfoEntity> userList = (IList<Task_UserInfoEntity>)BaseRepository().FindList<Task_UserInfoEntity>(String.Format(userSql, taskId));

                //任务物料
                //TODO:

                //任务时间线
                string logSql = @"SELECT t1.Id,
                                    t1.TaskId,
                                    t1.UserId,
                                    t1.OperationMode,
                                    t1.OperationTime,
                                    t1.Summary,
                                    t2.F_RealName as UserName,
                                    t2.F_HeadIcon as UserAvatar
                                    FROM Task_OperationLog t1
                                    LEFT JOIN Base_User t2 ON t1.UserId = t2.F_UserId
                                    WHERE t1.TaskId = '{0}'";
                IList<Task_OperationLogEntity> logList = (IList<Task_OperationLogEntity>)BaseRepository().FindList<Task_OperationLogEntity>(String.Format(logSql, taskId));

                taskDetail.BaseBillInfo = baseBillInfoEntity;     
                taskDetail.FileList = fileList;
                taskDetail.UserList = userList;
                taskDetail.LogList = logList;

                return taskDetail;

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存任务信息
        /// </summary>
        /// <param name="id">任务Id</param>
        /// <param name="baseInfo">任务基础信息</param>
        /// <param name="fileList">任务附件列表</param>
        /// <returns></returns>
        /// TODO:缺少物料信息
        /// 暂时不做修改操作
        public bool SubmitTaskInfo(string id,Task_BaseBillInfoEntity baseInfo,IList<Task_FileInfoEntity> fileList)
        {
            bool result = false;
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if(String.IsNullOrEmpty(id))
                {
                    //如果Id为空，则表示数据为插入
                    baseInfo.Create();
                    db.Insert<Task_BaseBillInfoEntity>(baseInfo);

                    //任务附件
                    foreach(var e in fileList)
                    {
                        e.TaskId = baseInfo.Id;
                        db.Insert<Task_FileInfoEntity>(e);
                    }

                    //任务物料信息
                }
                //else
                //{
                //    //修改数据
                //    db.Update<Task_BaseBillInfoEntity>(baseInfo);
                //    IList<Task_FileInfoEntity> oldFileList = (IList<Task_FileInfoEntity>)db.FindList<Task_FileInfoEntity>(d => d.TaskId == id);
                    
                //}
                //提交事务
                db.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                //任务回滚
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }

        /// <summary>
        /// 任务评价
        /// </summary>
        /// <param name="taskId">任务Id</param>
        /// <param name="userId">甲方用户id</param>
        /// <param name="content">评价内容</param>
        /// <returns></returns>
        public bool AppraiseTask(string taskId,string userId,string content)
        {
            bool result = false;
            var db = this.BaseRepository().BeginTrans();
            try
            {
                Task_BaseBillInfoEntity entity = BaseRepository().FindEntity<Task_BaseBillInfoEntity>(taskId);
                if(entity != null)
                {
                    entity.FPUserId = userId;
                    entity.FPUserName = "";
                    entity.AppraiseContent = content;
                    entity.CommentTime = DateTime.Now;
                    db.Update<Task_BaseBillInfoEntity>(entity);
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                //任务回滚
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }

        #endregion

        /// <summary>
        /// 获取Task_FileInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_FileInfoEntity GetTask_FileInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Task_FileInfoEntity>(t=>t.TaskId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_MaterialInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_MaterialInfoEntity GetTask_MaterialInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Task_MaterialInfoEntity>(t=>t.TaskId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_UserInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_UserInfoEntity GetTask_UserInfoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Task_UserInfoEntity>(t=>t.TaskId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var task_BaseBillInfoEntity = GetTask_BaseBillInfoEntity(keyValue); 
                db.Delete<Task_BaseBillInfoEntity>(t=>t.Id == keyValue);
                db.Delete<Task_FileInfoEntity>(t=>t.TaskId == task_BaseBillInfoEntity.Id);
                db.Delete<Task_MaterialInfoEntity>(t=>t.TaskId == task_BaseBillInfoEntity.Id);
                db.Delete<Task_UserInfoEntity>(t=>t.TaskId == task_BaseBillInfoEntity.Id);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Task_BaseBillInfoEntity entity,Task_FileInfoEntity task_FileInfoEntity,Task_MaterialInfoEntity task_MaterialInfoEntity,Task_UserInfoEntity task_UserInfoEntity)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    var task_BaseBillInfoEntityTmp = GetTask_BaseBillInfoEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    db.Delete<Task_FileInfoEntity>(t=>t.TaskId == task_BaseBillInfoEntityTmp.Id);
                    task_FileInfoEntity.Create();
                    task_FileInfoEntity.TaskId = task_BaseBillInfoEntityTmp.Id;
                    db.Insert(task_FileInfoEntity);
                    db.Delete<Task_MaterialInfoEntity>(t=>t.TaskId == task_BaseBillInfoEntityTmp.Id);
                    task_MaterialInfoEntity.Create();
                    task_MaterialInfoEntity.TaskId = task_BaseBillInfoEntityTmp.Id;
                    db.Insert(task_MaterialInfoEntity);
                    db.Delete<Task_UserInfoEntity>(t=>t.TaskId == task_BaseBillInfoEntityTmp.Id);
                    task_UserInfoEntity.Create();
                    task_UserInfoEntity.TaskId = task_BaseBillInfoEntityTmp.Id;
                    db.Insert(task_UserInfoEntity);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    task_FileInfoEntity.Create();
                    task_FileInfoEntity.TaskId = entity.Id;
                    db.Insert(task_FileInfoEntity);
                    task_MaterialInfoEntity.Create();
                    task_MaterialInfoEntity.TaskId = entity.Id;
                    db.Insert(task_MaterialInfoEntity);
                    task_UserInfoEntity.Create();
                    task_UserInfoEntity.TaskId = entity.Id;
                    db.Insert(task_UserInfoEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
