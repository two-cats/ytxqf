﻿using TTon.TMC.Application.TwoDevelopment.TaskManager;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class Task_FileInfoMap : EntityTypeConfiguration<Task_FileInfoEntity>
    {
        public Task_FileInfoMap()
        {
            #region 表、主键
            //表
            this.ToTable("TASK_FILEINFO");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

