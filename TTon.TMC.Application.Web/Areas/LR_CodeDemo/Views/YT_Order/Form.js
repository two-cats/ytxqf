﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-08-22 09:53
 * 描  述：订单报修
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, learun) {
    "use strict";
    var selectedRow = learun.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                $.lrSetForm(top.$.rootUrl + '/LR_CodeDemo/YT_Order/GetSqlFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                        }
                        else {
                            $('[data-table="' + id + '"]').lrSetFormData(data[id]);
                            for (var i in data[id]) {
                                if (i == "Order_Status") {
                                    if (data[id][i]==0) {
                                        $("#Order_Status").val("已提交");
                                    } else if (data[id][i] == 1) {
                                        $("#Order_Status").val("已预估");
                                    } else if (data[id][i] == 2) {
                                        $("#Order_Status").val("已提交");
                                    } else if (data[id][i] == 3) {
                                        $("#Order_Status").val("已拒绝");
                                    } else if (data[id][i] == 4) {
                                        $("#Order_Status").val("已指派");
                                    } else if (data[id][i] == 5) {
                                        $("#Order_Status").val("已完成");
                                    }
                                }
                                if (i == "pic") {
                                    //if (data[id][i] != "" && data[id][i] != null) {
                                    //    $("#Start_Pic").html("<img src='" + data[id][i] + "' style='width:280px;height:250px;'/>");
                                    //}
                                   
                                    var html = "";
                                    var img = data[id][i];
                                    for (var j = 0; j < img.length; j++) {
                                        html += "<img src='" + img[j].Img_Url + "' style='width:280px;height:250px;margin-left:10px;'/>";
                                    }
                                    $("#pic").html(html);
                                }
                                if (i == "task_pic") {
                                  
                                    var html = "";
                                    for (var j = 0; j < data[id][i].length; j++) {
                                        html += "<img src='" + data[id][i][j].Img_Url + "' style='width:280px;height:250px;margin-left:10px;'/>";
                                    }
                                    $("#task_pic").html(html);
                                }
                            }
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').lrValidform()) {
            return false;
        }
        var postData = {
            strEntity: JSON.stringify($('body').lrGetFormData())
        };
        $.lrSaveForm(top.$.rootUrl + '/LR_CodeDemo/YT_Order/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
