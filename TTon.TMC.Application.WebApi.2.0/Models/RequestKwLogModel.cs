﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class RequestKwLogModel
    {
        public string UserId { get; set; }

        /// <summary>
        /// 二维码图片主键
        /// </summary>
        public string codeImageId { get; set; }
    }
}