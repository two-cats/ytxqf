﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-08-21 13:20
 * 描  述：配置供应商零件
 */
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            $('#multiple_condition_query').lrMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 120, 400);
            $('#Supplier_Id').lrYT_SupplierSelect();
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                learun.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/LR_CodeDemo/YT_SupplierOrParts/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/LR_CodeDemo/YT_SupplierOrParts/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 导入
            $('#lr_import').on('click', function () {
                learun.layerForm({
                    id: 'form',
                    title: '上传文件',
                    url: top.$.rootUrl + '/LR_CodeDemo/YT_SupplierOrParts/ImportForm',
                    width: 600,
                    height: 400,
                    btn: null
                });

            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('Id');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/LR_CodeDemo/YT_SupplierOrParts/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').jfGrid({
                url: top.$.rootUrl + '/LR_CodeDemo/YT_SupplierOrParts/GetPageList',
                headData: [
                    { label: "供应商", name: "Supplier_Id", width: 160, align: "left"},
                    { label: "零件", name: "Parts_Id", width: 160, align: "left" },
                    { label: "备注", name: "Remark", width: 160, align: "left" },
                    { label: "创建人", name: "CreateUserId", width: 250, align: "left" },
                    { label: "创建时间", name: "CreateDate", width: 250, align: "left" },
                    { label: "修改时间", name: "ModifyDate", width: 250, align: "left" },
                ],
                mainId:'Id',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
