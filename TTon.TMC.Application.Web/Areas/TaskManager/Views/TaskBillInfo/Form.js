﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-05-06 15:11
 * 描  述：任务单
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, learun) {
    "use strict";
    var selectedRow = learun.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                $.lrSetForm(top.$.rootUrl + '/TaskManager/TaskBillInfo/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                        }
                        else {
                            $('[data-table="' + id + '"]').lrSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').lrValidform()) {
            return false;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Task_BaseBillInfo"]').lrGetFormData());
        postData.strtask_FileInfoEntity = JSON.stringify($('[data-table="Task_FileInfo"]').lrGetFormData());
        postData.strtask_MaterialInfoEntity = JSON.stringify($('[data-table="Task_MaterialInfo"]').lrGetFormData());
        postData.strtask_UserInfoEntity = JSON.stringify($('[data-table="Task_UserInfo"]').lrGetFormData());
        $.lrSaveForm(top.$.rootUrl + '/TaskManager/TaskBillInfo/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
