﻿using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 10:22
    /// 描 述：配置零件
    /// </summary>
    public class YT_PartsTypesBLL : YT_PartsTypesIBLL
    {
        private YT_PartsTypesService yT_PartsTypesService = new YT_PartsTypesService();

        #region 获取数据
        public List<SupplierListToTree> GetPartsTypesTree(string keyValue)
        {
            try
            {
                return yT_PartsTypesService.GetPartsTypesTree(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_PartsTypesEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_PartsTypesService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_PartsTypesEntity> GetList()
        {
            try
            {
                return yT_PartsTypesService.GetList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 根据供应商获取零件
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_PartsTypesEntity> GetListTree(string parentId)
        {
            try
            {
                return yT_PartsTypesService.GetListTree(parentId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取YT_PartsTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_PartsTypesEntity GetYT_PartsTypesEntity(string keyValue)
        {
            try
            {
                return yT_PartsTypesService.GetYT_PartsTypesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取YT_PartsTypes表实体数据
        /// <param name="keyValue">编号</param>
        /// <summary>
        /// <returns></returns>
        public YT_PartsTypesEntity PartsEntityByid(string keyValue)
        {
            try
            {
                return yT_PartsTypesService.PartsEntityByid(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<UserFinishEchartsModel> GetUserFinishEcharts(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetUserFinishEcharts(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        public IEnumerable<AutocmpModel> GetAParts()
        {
            try
            {
                return yT_PartsTypesService.GetAParts();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<UserFinishEchartsModel> GetUserFinishEchartsToSu(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetUserFinishEchartsToSu(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                yT_PartsTypesService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_PartsTypesEntity entity)
        {
            try
            {
                yT_PartsTypesService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 本月（时段）各零件挑选数量折线图
        /// </summary>
        /// <returns></returns>
        public List<PartsTypesEcharsModel> GetPartsTypesEcharsModel(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetPartsTypesEcharsModel(startTime,endTime,supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<NewPartsTypesAnalysisModel> GetNewPartsTypesToAnalysis(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetNewPartsTypesToAnalysis(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<PartsTypesAnalysisModel> GetPartsTypesEcharsToAnalysis(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetPartsTypesEcharsToAnalysis(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 本月（时段）零件挑选原因占比
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<MaintenanceTypesModel> GetMaintenanceTypesModel(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetMaintenanceTypesModel(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public MaintenanceTypesAnalysis GetMaintenanceTypesToAnalysis(string startTime, string endTime,string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetMaintenanceTypesToAnalysis(startTime, endTime,supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 供应商累计合格率（合格数量/总数）
        /// </summary>
        /// <returns></returns>
        public PercentOfPassModel GetPercentOfPass(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetPercentOfPass(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public PercentOfPassModel GetPercentToAnalysis(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetPercentToAnalysis(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<PartsFinishAndNumEcharsModel> GetPartsFinishAndNumEcharts(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_PartsTypesService.GetPartsFinishAndNumEcharts(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
