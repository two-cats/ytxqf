﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class SaveAbnormalParams
    {
        public string orderId { get; set; }

        public string userId { get; set; }

        public string content { get; set; }

        public string img { get; set; }
    }
}