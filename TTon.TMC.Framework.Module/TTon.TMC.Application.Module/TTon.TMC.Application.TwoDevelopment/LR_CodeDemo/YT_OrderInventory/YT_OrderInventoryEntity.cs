﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-10-31 10:29
    /// 描 述：库管流程
    /// </summary>
    public class YT_OrderInventoryEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// Order_Id
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_ID")]
        public string Order_Id { get; set; }
        /// <summary>
        /// User_Id
        /// </summary>
        /// <returns></returns>
        [Column("USER_ID")]
        public string User_Id { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        /// <returns></returns>
        [Column("COUNT")]
        public int? Count { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// CreateUserId
        /// </summary>
        /// <returns></returns>
        [Column("CREATEUSERID")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// Batch_Number
        /// </summary>
        [Column("BATCH_NUMBER")]
        public string Batch_Number { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

