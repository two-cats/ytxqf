﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class ExchangeReportParams
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime { get; set; }
        /// <summary>
        /// 交接人
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 被交接人
        /// </summary>
        public string HeirUser { get; set; }
        /// <summary>
        /// 注意事项
        /// </summary>
        public string MattersAttention { get; set; }
    }
}