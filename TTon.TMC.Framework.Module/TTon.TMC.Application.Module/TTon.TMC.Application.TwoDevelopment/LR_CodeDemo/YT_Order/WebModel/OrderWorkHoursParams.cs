﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class OrderWorkHoursParams
    {
        /// <summary>
        /// 订单主键
        /// </summary>
        public string orderId { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string startDate { get; set; }
        /// <summary>
        /// 效率
        /// </summary>
        public string workpieceRatio { get; set; }
        /// <summary>
        /// 零件总数
        /// </summary>
        public string count { get; set; }
        /// <summary>
        /// 人数
        /// </summary>
        public string workers { get; set; }
    }
}
