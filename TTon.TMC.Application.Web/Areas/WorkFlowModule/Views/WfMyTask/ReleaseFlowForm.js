﻿/*
 * TTon.TMC()
 * Copyright (c) 2013-2017 TTON
 * 创建人：TTON-WEB
 * 日 期：2017.04.18
 * 描 述：提交发起流程	
 */
var acceptClick;
var bootstrap = function ($, learun) {
    "use strict";
    var type = learun.frameTab.currentIframe().type;


    var page = {
        init: function () {
            if (type == 2)
            {
                $('#processName').parent().remove();
            }
        },
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').lrValidform()) {
            return false;
        }
        var formData = $('#form').lrGetFormData();

        callBack(formData);
        return true;
    };
    page.init();
}