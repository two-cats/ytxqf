﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTon.TMC.Application.Base.OrganizationModule;
using TTon.TMC.Application.Base.SystemModule;
using TTon.TMC.Util;
using TTon.TMC.Util.Operat;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/Login")]
    public class LoginController : BaseApiController
    {
        #region 模块对象
        private UserIBLL userBll = new UserBLL();

        #endregion
       
        /// <summary>
        /// 验证登录
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [Route("CheckLogin")]
        [AcceptVerbs("Get")]
        public HttpResponseMessage CheckLogin(string username, string password,string app,string clientid)
        {
            try
            {
                //进行第一次加密处理
                password = Md5Helper.Encrypt(password, 32);

                #region 内部账户验证
                
                UserEntity userEntity = userBll.CheckLogin(username,password,app);
                LogEntity logEntity = new LogEntity();
                if (userEntity.LoginOk)
                {
                    if (!string.IsNullOrEmpty(clientid))
                    { 
                        userEntity.F_ClientId = clientid;
                        userBll.SaveEntity(userEntity.F_UserId, userEntity);
                    }
                    #region 写入日志
                  
                    logEntity.F_CategoryId = 1;
                    logEntity.F_OperateTypeId = ((int)OperationType.Login).ToString();
                    logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Login);
                    logEntity.F_OperateAccount = username + "(" + userEntity.F_RealName + ")";
                    logEntity.F_OperateUserId = !string.IsNullOrEmpty(userEntity.F_UserId) ? userEntity.F_UserId : username;
                    logEntity.F_Module = app;
                    #endregion
                    //写入日志
                    logEntity.F_ExecuteResult = 1;
                    logEntity.F_ExecuteResultJson = "登录成功";
                    logEntity.WriteLog();
                    OperatorHelper.Instance.ClearCurrentErrorNum();
                    return Render(HttpStatusCode.OK, "success", userEntity);
                }
                else //登录失败
                {
                    //写入日志
                    logEntity.F_ExecuteResult = 0;
                    logEntity.F_ExecuteResultJson = "登录失败:" + userEntity.LoginMsg;
                    logEntity.WriteLog();
                    return Render(HttpStatusCode.InternalServerError,userEntity.LoginMsg);
                }
               
             
                   
               
                #endregion  

            }
            catch (Exception)
            {

                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }

    }
}
