﻿using System;
namespace TTon.TMC.Application.IM
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.01
    /// 描 述：日志打印函数
    /// </summary>
    public class ConsoleEx
    {
        /// <summary>
        /// 控制台屏幕输出
        /// </summary>
        /// <param name="msg">消息</param>
        public static void WriteLine(string msg)
        {
            try
            {
                Console.WriteLine("【{0}】{1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), msg);
            }
            catch
            {
            }
        }
    }
}
