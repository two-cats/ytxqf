﻿using TTon.TMC.Application.TwoDevelopment.WorkLog;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:30
    /// 描 述：工作日志附件
    /// </summary>
    public class WorkLog_AttachmentMap : EntityTypeConfiguration<WorkLog_AttachmentEntity>
    {
        public WorkLog_AttachmentMap()
        {
            #region 表、主键
            //表
            this.ToTable("WORKLOG_ATTACHMENT");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

