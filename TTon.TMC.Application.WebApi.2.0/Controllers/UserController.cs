﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTon.TMC.Application.Base.OrganizationModule;
using TTon.TMC.Application.WebApi._2._0.Models;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/User")]
    public class UserController : BaseApiController
    {
        #region 属性
        private UserService userService = new UserService();
        private DepartmentService departmentService = new DepartmentService();
        private UserIBLL userIBLL = new UserBLL();
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="companyId">公司主键</param>
        /// <param name="departmentId">部门主键</param>
        /// <param name="pagination">分页参数</param>
        /// <param name="keyword">查询关键词</param>
        /// <returns></returns>  
        [Route("GetUserList")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage GetPageList(queryJson queryjson)
        {
            try
            {
                List<UserEntity> list = (List<UserEntity>)userService.GetListbyRealName(queryjson.querystr);
                list = list.FindPage<UserEntity>(queryjson.pagination);
                List<UserEntity> listAndDname = new List<UserEntity>();
                foreach (UserEntity ue in list) {
                    ue.F_DepartmentId = GetDname(ue.F_DepartmentId);
                    listAndDname.Add(ue);
                }
               
                return Render(HttpStatusCode.OK, "success", listAndDname);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取部门数据实体
        /// </summary>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        public string GetDname(string keyValue)
        {
            try
            {
                DepartmentEntity d = departmentService.GetEntity(keyValue);
                return d.F_FullName.ToString();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    return "部门获取异常";
                }
                else
                {
                    return "部门获取异常";
                }
            }
        }

        /// <summary>
        /// 获取用户信息列表
        /// </summary>
        /// <param name="userIds">用户主键串</param>
        /// <returns></returns>
      
        public UserEntity GetListByUserIds(string keyValue)
        {
            return userIBLL.GetListByUserIds(keyValue).ToList().FirstOrDefault();
        }

        /// <summary>
        /// 获取部门列表信息(根据公司Id)
        /// </summary>
        /// <param name="companyId">公司Id</param>
        /// <returns></returns>
        [Route("GetDepList")]
        [AcceptVerbs("Get")]
        public HttpResponseMessage GetList(string companyId)
        {
            try
            {
                List<DepartmentEntity> list = (List<DepartmentEntity>)departmentService.GetList(companyId);

                return Render(HttpStatusCode.OK, "success", list);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        #endregion

        [Route("listGrpByDept/{projectid}")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetUserByDepartment(string projectid)
        {
            HttpResponseMessage message = default(HttpResponseMessage);
            try
            {
                IList<RespDepartmentModel> list = new List<RespDepartmentModel>();
                IList<UserEntity> userList = (IList<UserEntity>)userService.GetList(projectid);
                IList<DepartmentEntity> departmentList = (IList<DepartmentEntity>)departmentService.GetList(projectid);
                foreach(DepartmentEntity dept in departmentList)
                {
                    var ndept = new RespDepartmentModel()
                    {
                        Id = dept.F_DepartmentId,
                        Name = dept.F_FullName
                    };

                    var l = (from d in userList
                             where d.F_DepartmentId == dept.F_DepartmentId
                             select new RespUserModel() { Id = d.F_UserId,
                                 LoginName = d.F_Account,
                                 RealName = d.F_RealName })
                             .ToList<RespUserModel>();

                    ndept.Users = l;
                    ndept.Count = l.Count;
                    list.Add(ndept);
                }
                message = Render(HttpStatusCode.OK, "获取部门用户信息", list);
            }
            catch (Exception ex)
            {
                message = Render(HttpStatusCode.InternalServerError, ex.Message);
            }
            return message;
        }


        [Route("manager/list")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetManagerList()
        {
            try
            {
                var data = userService.GetManagerList();

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        [Route("manager/verify")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage VerificationManager(string userId)
        {
            try
            {
                var data = userService.VerificationManager(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        #region 绑定微信openid

        [Route("BindOpenID")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage bindopenid(string mobile, string openid,string departmentId,string userName)
        { 
            try
            {
                UserEntity ue = GetListByUserIds(mobile);
                ue.F_WeChat = openid;
                userIBLL.SaveEntity(ue.F_UserId, ue);
                return Render(HttpStatusCode.OK, "success", ue.F_UserId);
            }
            catch (Exception)
            {
                //用户不存在即创建用户,创建为加密用户不可登录
                //UserEntity ue = new UserEntity();
                //ue.F_RealName = userName;
                //ue.F_UserId = Guid.NewGuid().ToString();
                //ue.F_CompanyId = "53298b7a-404c-4337-aa7f-80b2a4ca6681"; //此处为中良用，注册业务流程尚未完善。
                //ue.F_DepartmentId = departmentId;
                //ue.F_Telephone = mobile;
                //ue.F_Mobile = mobile;
                //ue.F_Password = Guid.NewGuid().ToString();
                //ue.F_Account = Guid.NewGuid().ToString();
                //ue.F_WeChat = openid;
                //userIBLL.SaveEntity("", ue);
                //return Render(HttpStatusCode.OK, "success", ue.F_UserId);
                return Render(HttpStatusCode.InternalServerError, "您无权使用本系统，请联系管理员");
            }
          

        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <param name="oldPassword"></param>
        /// <returns></returns>
        [Route("ResetPassword")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage ResetPassword(string id, string password, string oldPassword)
        {
            try
            {

               
                bool res = userIBLL.mobileRevisePwd(password, oldPassword,id);

                if (res)
                {
                    return Render(HttpStatusCode.OK, "success", "密码修改成功，请牢记新密码。");
                }
                else {
                    return Render(HttpStatusCode.InternalServerError, "原密码错误，请重新输入");
                }

              
            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "原密码错误，请重新输入");
            }


        }

        /// <summary>
        /// 我的任务列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <param name="oldPassword"></param>
        /// <returns></returns>
        [Route("task/user")]
        [AcceptVerbs("post")]
        public HttpResponseMessage UserTaskList(queryJson query,int type)
        {
            try
            {


                var  res = new UserService().GetUserTaskList(query.querystr, type, query.pagination);

                return Render(HttpStatusCode.OK, "success", res);
                

            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "获取列表失败！。");
            }


        }
        #endregion

    }
}
