﻿using TTon.TMC.Util;
using System;
using System.Collections.Generic;

namespace TTon.TMC.Application.TwoDevelopment.TaskManager
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class TaskBillInfoBLL : TaskBillInfoIBLL
    {
        private TaskBillInfoService taskBillInfoService = new TaskBillInfoService();

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Task_BaseBillInfoEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return taskBillInfoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_BaseBillInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_BaseBillInfoEntity GetTask_BaseBillInfoEntity(string keyValue)
        {
            try
            {
                return taskBillInfoService.GetTask_BaseBillInfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_FileInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_FileInfoEntity GetTask_FileInfoEntity(string keyValue)
        {
            try
            {
                return taskBillInfoService.GetTask_FileInfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_MaterialInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_MaterialInfoEntity GetTask_MaterialInfoEntity(string keyValue)
        {
            try
            {
                return taskBillInfoService.GetTask_MaterialInfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Task_UserInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Task_UserInfoEntity GetTask_UserInfoEntity(string keyValue)
        {
            try
            {
                return taskBillInfoService.GetTask_UserInfoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                taskBillInfoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Task_BaseBillInfoEntity entity,Task_FileInfoEntity task_FileInfoEntity,Task_MaterialInfoEntity task_MaterialInfoEntity,Task_UserInfoEntity task_UserInfoEntity)
        {
            try
            {
                taskBillInfoService.SaveEntity(keyValue, entity,task_FileInfoEntity,task_MaterialInfoEntity,task_UserInfoEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
