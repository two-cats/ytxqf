﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class UnfinishedOrderStatisticsModel
    {
        public int Unfinished_Task_Count { get; set; }

        public List<OrderTaskListModel> list { get; set; }
    }
}
