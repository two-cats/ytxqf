﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTon.TMC.Application.Web.Areas.EnergyMeter
{
    public class EnergyMeterAreaRegistration :  AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EnergyMeter";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EnergyMeter_default",
                "EnergyMeter/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}