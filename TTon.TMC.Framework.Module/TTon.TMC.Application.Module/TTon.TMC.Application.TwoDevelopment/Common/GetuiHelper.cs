﻿using com.igetui.api.openservice;
using com.igetui.api.openservice.igetui;
using com.igetui.api.openservice.igetui.template;
using com.igetui.api.openservice.payload;
using System;
using System.Collections.Generic;

using TTon.TMC.Application.Base.SystemModule;
using TTon.TMC.Util;

namespace TTon.TMC.Application.TwoDevelopment.Common
{
    public class GeTuiHelper
    {
        private static string APPID = string.Empty;
        private static string HOST = string.Empty;
        private static string APPKEY = string.Empty;
        private static string MASTERSECRET = string.Empty;

        static GeTuiHelper()
        {
            APPID = Config.GetValue("APPID");
            HOST = Config.GetValue("HOST");
            APPKEY = Config.GetValue("APPKEY");
            MASTERSECRET = Config.GetValue("MASTERSECRET");
        }



        //PushMessageToList接口测试代码
        public static String PushMessageToList(List<string> clientId, PushModel pushmodel)
        {
            // 推送主类（方式1，不可与方式2共存）
            IGtPush push = new IGtPush(HOST, APPKEY, MASTERSECRET);
            // 推送主类（方式2，不可与方式1共存）此方式可通过获取服务端地址列表判断最快域名后进行消息推送，每10分钟检查一次最快域名
            //IGtPush push = new IGtPush("",APPKEY,MASTERSECRET);
            ListMessage message = new ListMessage();

            TransmissionTemplate template = TransmissionTemplate(pushmodel);
            // 用户当前不在线时，是否离线存储,可选
            message.IsOffline = true;
            // 离线有效时间，单位为毫秒，可选
            message.OfflineExpireTime = 1000 * 3600 * 12;
            message.Data = template;
            //message.PushNetWorkType = 0;        //判断是否客户端是否wifi环境下推送，1为在WIFI环境下，0为不限制网络环境。
            //设置接收者
            List<com.igetui.api.openservice.igetui.Target> targetList = new List<com.igetui.api.openservice.igetui.Target>();

            foreach (string s in clientId)
            {
                com.igetui.api.openservice.igetui.Target target = new com.igetui.api.openservice.igetui.Target();
                target.appId = APPID;
                target.clientId = s;

                targetList.Add(target);

            }
            String contentId = push.getContentId(message);
            String pushResult = push.pushMessageToList(contentId, targetList);


            LogEntity logEntity = new LogEntity();
            logEntity.F_ExecuteResult = 0;
            logEntity.F_ExecuteResultJson = "消息推送:" + pushResult;
            logEntity.WriteLog();

            return pushResult;
        }


        //通知透传模板动作内容
        public static NotificationTemplate NotificationTemplate(PushModel push)
        {
            NotificationTemplate template = new NotificationTemplate();
            template.AppId = APPID;
            template.AppKey = APPKEY;
            //通知栏标题
            template.Title = push.title;
            //通知栏内容     
            template.Text = push.content;
            //通知栏显示本地图片
            template.Logo = "";
            //通知栏显示网络图标
            template.LogoURL = "";
            //应用启动类型，1：强制应用启动  2：等待应用启动
            template.TransmissionType = "1";
            //透传内容  
            template.TransmissionContent = push.ToJson();
            //接收到消息是否响铃，true：响铃 false：不响铃   
            template.IsRing = true;
            //接收到消息是否震动，true：震动 false：不震动   
            template.IsVibrate = true;
            //接收到消息是否可清除，true：可清除 false：不可清除    
            template.IsClearable = true;
            //设置通知定时展示时间，结束时间与开始时间相差需大于6分钟，消息推送后，客户端将在指定时间差内展示消息（误差6分钟）
            String begin = DateTime.Now.ToString();
            String end = DateTime.Now.AddMinutes(6).ToString();
            template.setDuration(begin, end);

            return template;
        }

        //透传模板动作内容
        public static TransmissionTemplate TransmissionTemplate(PushModel message)
        {
            TransmissionTemplate template = new TransmissionTemplate();
            template.AppId = APPID;
            template.AppKey = APPKEY;
            //应用启动类型，1：强制应用启动 2：等待应用启动
            template.TransmissionType = "2";
            //透传内容  
            template.TransmissionContent = message.ToJson();
            //设置通知定时展示时间，结束时间与开始时间相差需大于6分钟，消息推送后，客户端将在指定时间差内展示消息（误差6分钟）
            DateTime begin = DateTime.Now;
            DateTime end = DateTime.Now.AddMinutes(6);
            template.setDuration(begin.ToString(), end.ToString());

            APNPayload apnpayload = new APNPayload();
            DictionaryAlertMsg alertMsg = new DictionaryAlertMsg();
            alertMsg.Body = message.content;
            alertMsg.ActionLocKey = "ActionLocKey";
            alertMsg.LocKey = message.content;
            alertMsg.addLocArg("LocArg");
            alertMsg.LaunchImage = "LaunchImage";
            //IOS8.2支持字段
            alertMsg.Title = message.title;
            alertMsg.TitleLocKey = message.title;
            alertMsg.addTitleLocArg("TitleLocArg");

            apnpayload.AlertMsg = alertMsg;
            apnpayload.Badge = 1;
            apnpayload.ContentAvailable = 1;
            //apnpayload.Category = "";
            apnpayload.Sound = "test1.wav";
            apnpayload.addCustomMsg("payload", "payload");
            template.setAPNInfo(apnpayload);

            return template;
        }
    }
}