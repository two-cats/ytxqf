﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 16:01
    /// 描 述：配置指导书
    /// </summary>
    public class YT_InstructorEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// Supplier_Id
        /// </summary>
        [Column("SUPPLIER_ID")]
        public string Supplier_Id { get; set; }
        /// <summary>
        /// Parts_Id
        /// </summary>
        [Column("PARTS_ID")]
        public string Parts_Id { get; set; }
        /// <summary>
        /// MaintenanceType
        /// </summary>
        [Column("MAINTENANCETYPE")]
        public string MaintenanceType { get; set; }
        /// <summary>
        /// SketchDate
        /// </summary>
        [Column("SKETCHDATE")]
        public string SketchDate { get; set; }
        /// <summary>
        /// Number
        /// </summary>
        [Column("NUMBER")]
        public string Number { get; set; }
        /// <summary>
        /// WorkHours
        /// </summary>
        [Column("WORKHOURS")]
        public string WorkHours { get; set; }
        /// <summary>
        /// IssueDescription
        /// </summary>
        [Column("ISSUEDESCRIPTION")]
        public string IssueDescription { get; set; }
        /// <summary>
        /// IssuePic
        /// </summary>
        [Column("ISSUEPIC")]
        public string IssuePic { get; set; }
        /// <summary>
        /// Pick_Scope
        /// </summary>
        [Column("PICK_SCOPE")]
        public string Pick_Scope { get; set; }
        /// <summary>
        /// Pick_Prepare
        /// </summary>
        [Column("PICK_PREPARE")]
        public string Pick_Prepare { get; set; }
        /// <summary>
        /// Pick_Step
        /// </summary>
        [Column("PICK_STEP")]
        public string Pick_Step { get; set; }
        /// <summary>
        /// Pick_Pic
        /// </summary>
        [Column("PICK_PIC")]
        public string Pick_Pic { get; set; }
        /// <summary>
        /// Pick_Demand
        /// </summary>
        [Column("PICK_DEMAND")]
        public string Pick_Demand { get; set; }
        /// <summary>
        /// Pick_Project
        /// </summary>
        [Column("PICK_PROJECT")]
        public string Pick_Project { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// Remark_Pic
        /// </summary>
        [Column("REMARK_PIC")]
        public string Remark_Pic { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// CreateUserId
        /// </summary>
        [Column("CREATEUSERID")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// ModifyDate
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// ModifyUserId
        /// </summary>
        [Column("MODIFYUSERID")]
        public string ModifyUserId { get; set; }
        /// <summary>
        /// IsDelete
        /// </summary>
        [Column("ISDELETE")]
        public int? IsDelete { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// Parts_Num
        /// </summary>
        [Column("PARTS_NUM")]
        public string Parts_Num { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreateDate = DateTime.Now;
            this.IsDelete = 0;
            this.CreateUserId = LoginUserInfo.Get().userId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
            this.ModifyDate = DateTime.Now;
            this.ModifyUserId = LoginUserInfo.Get().userId;
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

