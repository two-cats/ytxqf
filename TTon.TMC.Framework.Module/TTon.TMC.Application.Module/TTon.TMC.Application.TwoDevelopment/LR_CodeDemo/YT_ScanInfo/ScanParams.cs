﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ScanInfo
{
    public class ScanParams
    {
        /// <summary>
        /// 原始
        /// </summary>
        public string Primitive { get; set; }
        /// <summary>
        /// 截取后
        /// </summary>
        public string Equipment { get; set; }
        /// <summary>
        /// 当前用户
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 扫描时间
        /// </summary>
        public string ScanTime { get; set; }
    }
}
