﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class UserFinishPartTypesModel
    {
        public string name { get; set; }

        public int count { get; set; }

        public string percentage { get; set; }
    }
}
