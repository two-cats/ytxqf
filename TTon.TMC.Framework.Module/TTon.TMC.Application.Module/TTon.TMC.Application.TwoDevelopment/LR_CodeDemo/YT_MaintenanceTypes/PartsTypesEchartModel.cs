﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes
{
    public class PartsTypesEchartModel
    {
        public string partsTypes { get; set; }

        public double lastMonth { get; set; }

        public double nowMonth { get; set; }
    }
}
