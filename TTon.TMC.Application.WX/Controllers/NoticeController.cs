﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTon.TMC.Application.WX.Controllers
{
    public class NoticeController : ControllerBase
    {
        // GET: Notice
        public ActionResult Index()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "Notice/Index", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        // GET: Notice
        public ActionResult Add()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "Notice/Add", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        // GET: Notice
        public ActionResult Scan()
        {
            //var result = HasLinkToWechatUser();

            //if (!result)
            //{
            //    return RedirectToAction("Register", "", new { rel = "Notice/Scan", wechatUser = wechatUser });
            //}
            //else
            //{
                return View();
            //}
        }
    }
}