﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_SupplierOrParts.ImportExcel
{
    /// <summary>
    /// 导入实体
    /// </summary>
    public class ImportModel
    {
        public string Supplier_Name { get; set; }

        public string Supplier_user { get; set; }

        public string Supplier_phone { get; set; }

        public string Parts_Name { get; set; }

        public string Parts_Number { get; set; }

        public string MaintenanceTypes { get; set; }
    }
}
