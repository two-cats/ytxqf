﻿namespace TTon.TMC.Util
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.07.10
    /// 描 述：表格属性模型
    /// </summary>
    public class jfGridModel
    {
        public string name { get; set; }
        public string label { get; set; }
        public string width { get; set; }
        public string align { get; set; }
        public string height { get; set; }
        public string hidden { get; set; }
    }
}
