﻿using TTon.TMC.Application.Base.OrganizationModule;
using System.Data.Entity.ModelConfiguration;

namespace TTon.TMC.Application.Mapping
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.03.04
    /// 描 述：机构管理
    /// </summary>
    public class CompanyMap : EntityTypeConfiguration<CompanyEntity>
    {
        public CompanyMap()
        {
            #region 表、主键
            //表
            this.ToTable("BASE_COMPANY");
            //主键
            this.HasKey(t => t.F_CompanyId);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
