﻿using TTon.TMC.Util;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 09:50
    /// 描 述：供应商配置
    /// </summary>
    public interface YT_SupplierIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_SupplierEntity> GetPageList(Pagination pagination, string queryJson);

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_SupplierEntity> GetList();
        /// <summary>
        /// 获取YT_Supplier表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        YT_SupplierEntity GetYT_SupplierEntity(string keyValue);


        /// <summary>
        /// 获取YT_Supplier表实体数据
        /// <param name="keyValue">编号</param>
        /// <summary>
        /// <returns></returns>
        IEnumerable<YT_SupplierEntity> SupplierEntityByid(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, YT_SupplierEntity entity);
        List<SupplierListToTree> GetSupplierTree();
        List<GetSupplierInfoModel> GetSupplierPageList(Pagination paginationobj, string queryJson);
        List<SupplierListToTree> GetSupplierList(string startTime, string endTime);
        #endregion

    }
}
