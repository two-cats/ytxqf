﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-22 09:53
    /// 描 述：订单报修
    /// </summary>
    public class YT_OrderEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// Phone
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary>
        /// Date
        /// </summary>
        [Column("DATE")]
        public string Date { get; set; }
        /// <summary>
        /// OrderNumber
        /// </summary>
        [Column("ORDERNUMBER")]
        public string OrderNumber { get; set; }
        /// <summary>
        /// PartsNumber
        /// </summary>
        [Column("PARTSNUMBER")]
        public string PartsNumber { get; set; }
        /// <summary>
        /// Supplier_Id
        /// </summary>
        [Column("SUPPLIER_ID")]
        public string Supplier_Id { get; set; }
        /// <summary>
        /// PartsType
        /// </summary>
        [Column("PARTSTYPE")]
        public string PartsType { get; set; }
        /// <summary>
        /// MaintainType
        /// </summary>
        [Column("MAINTAINTYPE")]
        public string MaintainType { get; set; }
        /// <summary>
        /// Instructor
        /// </summary>
        [Column("INSTRUCTOR")]
        public string Instructor { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// Order_Status
        /// </summary>
        [Column("ORDER_STATUS")]
        public int? Order_Status { get; set; }
        /// <summary>
        /// 区域耗时
        /// </summary>
        [Column("ESTIMATEDATE")]
        public string EstimateDate { get; set; }
        /// <summary>
        /// CreateUserId
        /// </summary>
        [Column("CREATEUSERID")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// ModifyUserId
        /// </summary>
        [Column("MODIFYUSERID")]
        public string ModifyUserId { get; set; }
        /// <summary>
        /// ModifyDate
        /// </summary>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// IsDelete
        /// </summary>
        [Column("ISDELETE")]
        public int? IsDelete { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [Column("SUCCESSDATE")]
        public string SuccessDate { get; set; }
        /// <summary>
        /// 订单图片
        /// </summary>
        [Column("PIC")]
        public string Pic { get; set; }
        /// <summary>
        /// 任务图片
        /// </summary>
        [Column("TASK_PIC")]
        public string Task_Pic { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Column("COUNT")]
        public int? Count { get; set; }
        /// <summary>
        /// 完成备注
        /// </summary>
        [Column("SUCCESS_REMARK")]
        public string Success_Remark { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        [Column("BATCH_NUMBER")]
        public string Batch_Number { get; set; }

        /// <summary>
        /// 跟线挑选
        /// </summary>
        [Column("THREAD")]
        public int? Thread { get; set; }

        /// <summary>
        /// 区域挑选
        /// </summary>
        [Column("AREA")]
        public int? Area { get; set; }

        /// <summary>
        /// 人数
        /// </summary>
        [Column("PERSON")]
        public int? Person { get; set; }
        /// <summary>
        /// 图纸编号
        /// </summary>
        [Column("DRAWINGNUMBER")]
        public string DrawingNumber { get; set; }
        /// <summary>
        /// 临时措施
        /// </summary>
        [Column("REPAIRTYPE")]
        public string RepairType { get; set; }

        /// <summary>
        /// 责任人
        /// </summary>
        [Column("PESPONSIBLE")]
        public string Pesponsible { get; set; }
        /// <summary>
        /// 微信推送状态（0 未推送，1 已推送）
        /// </summary>
        [Column("WXPUSHSTATUS")]
        public int? WxPushStatus { get; set; }
        /// <summary>
        /// 推送次数
        /// </summary>
        [Column("WXPUSHCOUNT")]
        public int? WxPushCount { get; set; }
        /// <summary>
        /// 短信通知状态（0 未通知，1 已通知）
        /// </summary>
        [Column("SMSPUSHSTATUS")]
        public int? SmsPushStatus { get; set; }
        /// <summary>
        /// 通知次数
        /// </summary>
        [Column("SMSPUSHCOUNT")]
        public int? SmsPushCount { get; set; }
        /// <summary>
        /// 供应商状态（0创建，1同意，2拒绝）
        /// </summary>
        [Column("SUPPLIERSTATUS")]
        public int SupplierStatus { get; set; }
        /// <summary>
        /// 线边挑选
        /// </summary>
        [Column("LINEEDGE")]
        public int? LineEdge { get; set; }

        /// <summary>
        /// 跟线耗时
        /// </summary>
        [Column("THREADELAPSEDTIME")]
        public string ThreadElapsedTime { get; set; }

        /// <summary>
        /// 线边耗时
        /// </summary>
        [Column("LINEEDGEELAPSEDTIME")]
        public string LineEdgeElapsedTime { get; set; }

        /// <summary>
        /// 检测方法
        /// </summary>
        [Column("SORTINGWAY")]
        public string SortingWay { get; set; }

        /// <summary>
        /// 频次
        /// </summary>
        [Column("FREQUENCY")]
        public string Frequency { get; set; }

        /// <summary>
        /// 作业人数
        /// </summary>
        [Column("WORKERS")]
        public string Workers { get; set; }

        /// <summary>
        /// 工作日耗时
        /// </summary>
        [Column("WORKDAY")]
        public string Workday { get; set; }

        /// <summary>
        /// 周末耗时
        /// </summary>
        [Column("WEEKEND")]
        public string Weekend { get; set; }

        /// <summary>
        /// 法定假日耗时
        /// </summary>
        [Column("STATUTORYHOLIDAY")]
        public string StatutoryHoliday { get; set; }

        /// <summary>
        /// 用餐次数
        /// </summary>
        [Column("MEALTIMES")]
        public string MealTimes { get; set; }
        /// <summary>
        /// StartDate
        /// </summary>
        [Column("STARTDATE")]
        public string StartDate { get; set; }
        /// <summary>
        /// 分配总数
        /// </summary>
        [Column("ALLOTCOUNT")]
        public string AllotCount { get; set; }
        /// <summary>
        /// 不良品原因描述
        /// </summary>
        [Column("BADPRODUCTDESCRIBE")]
        public string BadProductDescribe { get; set; }
        /// <summary>
        /// 效率
        /// </summary>
        [Column("WORKPIECERATIO")]
        public string WorkpieceRatio { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreateDate = DateTime.Now;
            this.IsDelete = 0;
            this.SupplierStatus = 0;


        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
            this.ModifyDate = DateTime.Now;
          
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

