﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes
{
    public class PartsFinishAndNumEcharsModel
    {
        public string partsTypes { get; set; }

        public double aTime { get; set; }

        public int aCount { get; set; }

        public double tTime { get; set; }

        public int tCount { get; set; }

        public double lTime { get; set; }

        public int lCount { get; set; }
    }
}
