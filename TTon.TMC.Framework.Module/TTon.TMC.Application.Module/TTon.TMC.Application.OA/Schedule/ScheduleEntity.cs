﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.OA.Schedule
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.07.11
    /// 描 述：日程管理
    /// </summary>
    public class ScheduleEntity
    {
        #region 实体成员
        /// <summary>
        /// 日程主键
        /// </summary>
        /// <returns></returns>
        [Column("F_SCHEDULEID")]
        public string F_ScheduleId { get; set; }
        /// <summary>
        /// 日程名称
        /// </summary>
        /// <returns></returns>
        [Column("F_SCHEDULENAME")]
        public string F_ScheduleName { get; set; }

       
        /// <summary>
        /// 日程内容
        /// </summary>
        /// <returns></returns>
        [Column("F_SCHEDULECONTENT")]
        public string F_ScheduleContent { get; set; }
        /// <summary>
        /// 类别
        /// </summary>
        /// <returns></returns>
        [Column("F_CATEGORY")]
        public string F_Category { get; set; }
        /// <summary>
        /// 开始日期
        /// </summary>
        /// <returns></returns>
        [Column("F_STARTDATE")]
        public DateTime F_StartDate { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        /// <returns></returns>
        [Column("F_STARTTIME")]
        public string F_StartTime { get; set; }

      
        /// <summary>
        /// 结束日期
        /// </summary>
        /// <returns></returns>
        [Column("F_ENDDATE")]
        public DateTime F_EndDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        /// <returns></returns>
        [Column("F_ENDTIME")]
        public string F_EndTime { get; set; }

       
        /// <summary>
        /// 提前提醒
        /// </summary>
        /// <returns></returns>
        [Column("F_EARLY")]
        public int? F_Early { get; set; }
        /// <summary>
        /// 邮件提醒
        /// </summary>
        /// <returns></returns>
        [Column("F_ISMAILALERT")]
        public int? F_IsMailAlert { get; set; }
        /// <summary>
        /// 手机提醒
        /// </summary>
        /// <returns></returns>
        [Column("F_ISMOBILEALERT")]
        public int? F_IsMobileAlert { get; set; }
        /// <summary>
        /// 微信提醒
        /// </summary>
        /// <returns></returns>
        [Column("F_ISWECHATALERT")]
        public int? F_IsWeChatAlert { get; set; }
        /// <summary>
        /// 日程状态
        /// </summary>
        /// <returns></returns>
        [Column("F_SCHEDULESTATE")]
        public int? F_ScheduleState { get; set; }
        /// <summary>
        /// 排序码
        /// </summary>
        /// <returns></returns>
        [Column("F_SORTCODE")]
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        /// <returns></returns>
        [Column("F_DELETEMARK")]
        public int? F_DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// </summary>
        /// <returns></returns>
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        /// <returns></returns>
        [Column("F_CREATEDATE")]
        public DateTime? F_CreateDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        /// <returns></returns>
        [Column("F_CREATEUSERID")]
        public string F_CreateUserId { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <returns></returns>
        [Column("F_CREATEUSERNAME")]
        public string F_CreateUserName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        /// <returns></returns>
        [Column("F_MODIFYDATE")]
        public DateTime? F_ModifyDate { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        /// <returns></returns>
        [Column("F_MODIFYUSERID")]
        public string F_ModifyUserId { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        /// <returns></returns>
        [Column("F_MODIFYUSERNAME")]
        public string F_ModifyUserName { get; set; }
        #endregion



        #region 扩展字段
        /// <summary>
        /// 所属项目
        /// </summary>
        /// <returns></returns>
        [Column("F_PROJECTID")]
        public string F_ProjectId { get; set; }

        /// <summary>
        /// 会议室
        /// </summary>
        /// <returns></returns>
        [Column("F_ROOMID")]
        public string F_RoomId { get; set; }

        /// <summary>
        /// 参会人员
        /// </summary>
        /// <returns></returns>
        [Column("F_ATTENDEES")]
        public string F_Attendees { get; set; }

        /// <summary>
        /// 服务内容
        /// </summary>
        /// <returns></returns>
        [Column("F_SERVICES")]
        public string F_Services { get; set; }

        /// <summary>
        /// 指派人
        /// </summary>
        /// <returns></returns>
        [Column("F_ASSIGN")]
        public string F_Assign { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        /// <returns></returns>
        [Column("F_PIC")]
        public string F_PIC { get; set; }

        /// <summary>
        /// 审批人
        /// </summary>
        /// <returns></returns>
        [Column("F_ApprovalUserID")]
        public string F_ApprovalUserID { get; set; }

        /// <summary>
        /// 审批
        /// 0：同意，1：拒绝
        /// </summary>
        /// <returns></returns>
        [Column("F_Approval")]
        public string F_Approval { get; set; }

        /// <summary>
        /// 审批意见
        /// </summary>
        /// <returns></returns>
        [Column("F_Opinion")]
        public string F_Opinion { get; set; }


        /// <summary>
        /// 评分
        /// </summary>
        /// <returns></returns>
        [Column("F_Score")]
        public string F_Score { get; set; }

        /// <summary>
        /// 评价意见
        /// </summary>
        /// <returns></returns>
        [Column("F_Evaluation")]
        public string F_Evaluation { get; set; }

        /// <summary>
        /// 参会人数
        /// </summary>
        /// <returns></returns>
        [Column("F_PeopleCount")]
        public string F_PeopleCount { get; set; }

        /// <summary>
        /// 设备
        /// </summary>
        /// <returns></returns>
        [Column("F_Equipment")]
        public string F_Equipment { get; set; }

        #endregion
        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            try
            {
                if (string.IsNullOrEmpty(this.F_CreateUserId))
                { 
                    UserInfo userInfo = LoginUserInfo.Get();
                    this.F_CreateUserId = userInfo.userId;
                    this.F_CreateUserName = userInfo.realName;
                }
            }
            catch (Exception)
            {

                
            }
            if (string.IsNullOrEmpty(this.F_ScheduleId))
            {
                this.F_ScheduleId = Guid.NewGuid().ToString();
            }
           
            this.F_CreateDate = DateTime.Now;
            this.F_StartTime = this.F_StartDate.ToTimeString().Replace(":", string.Empty).Substring(0, 4);
            this.F_EndTime = this.F_EndDate.ToTimeString().Replace(":", string.Empty).Substring(0, 4);
            this.F_DeleteMark = 0;


        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            try
            {
                UserInfo userInfo = LoginUserInfo.Get();
                this.F_ModifyUserId = userInfo.userId;
                this.F_ModifyUserName = userInfo.realName;
            }
            catch (Exception)
            {

               
            }
           

            this.F_ScheduleId = keyValue;
            this.F_ModifyDate = DateTime.Now;
            if (this.F_StartDate != null)
            {
                this.F_StartTime = this.F_StartDate.ToTimeString().Replace(":", string.Empty).Substring(0, 4);
            }
            if (this.F_EndDate != null)
            {
                this.F_EndTime = this.F_EndDate.ToTimeString().Replace(":", string.Empty).Substring(0, 4);
            }
        }
        #endregion
    }
}
