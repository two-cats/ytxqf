﻿$(function () {
    var vs = "";
    $.ajax({
        url: "/LR_CodeDemo/YT_Supplier/GetSupplierTree",
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "<option value=''>==请选择==</option>";
            for (var i = 0; i < result.data.length; i++) {
                if (result.data[i].number == 0) {
                    html += "<option value='" + result.data[i].Id + "' selected = 'selected'>" + result.data[i].Name + "</opeion>";
                    vs = result.data[i].Id;
                }
                if (result.data[i].number != 0) {
                    html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Name + "</opeion>";
                }
            }
            $("#Supplier3").html(html);
        }
    })


    $.ajax({
        url: "/LR_CodeDemo/YT_Supplier/GetSupplierTree",
        type: "GET",
        async: false,
        dataType: "JSON",
        success: function (result) {
            var html = "";
            for (var i = 0; i < result.data.length; i++) {
                if (result.data[i].number == 0) {
                    html += "<option value='" + result.data[i].Id + "' selected = 'selected'>" + result.data[i].Name + "</opeion>";
                    vs = result.data[i].Id;
                }
                if (result.data[i].number != 0) {
                    html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Name + "</opeion>";
                }
            }
            $("#Supplier2").html(html);
            $("#Supplier4").html(html);
        }
    })
    //$.ajax({
    //    url: "/LR_CodeDemo/YT_Supplier/GetSupplierTree",
    //    type: "GET",
    //    async: false,
    //    dataType: "JSON",
    //    success: function (result) {
    //        var html = "<option value=''>==请选择==</option>";
    //        for (var i = 0; i < result.data.length; i++) {
    //            html += "<option value='" + result.data[i].Id + "'>" + result.data[i].Name + "</opeion>";
    //            vs = result.data[i].Id;
    //        }
    //        $("#Supplier4").html(html);
    //    }
    //})


    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPercentOfPass?startTime=&endTime=&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.success == 0 && data.error == 0) {
                var newdiv = document.getElementById("PercentOfPass");
                newdiv.innerHTML = "<font size='5'>当前时间段内该供应商未维修零件</font>";
            }
            else {
                var PercentOfPass = echarts.init(document.getElementById('PercentOfPass'));
                var k = [];

                var j = {};
                j.value = data.success;
                j.name = '合格';
                k.push(j);
                var p = {};
                p.value = data.error;
                p.name = '不合格';
                k.push(p);

                option = {
                    title: {
                        text: '供应商累计合格率',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data: ['合格', '不合格']
                    },
                    color: ['#91c7ae', '#ca8622'],
                    series: [
                        {
                            name: '占总数百分比',
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            data: k,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                PercentOfPass.setOption(option);
            }
        }
    })

    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsTypesEcharsModel?startTime=&endTime=&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.length == 0) {
                var newdiv = document.getElementById("Supplier_PartsType");
                newdiv.innerHTML = "<font size='5'>当前时间段内该供应商未完成订单</font>";
            }
            else {
                var PartsTypesEchars = echarts.init(document.getElementById('Supplier_PartsType'));
                PartsTypesEchars.clear();
                var date = [];
                var name = [];
                var value = [];
                for (var i = 0; i < data.length; i++) {
                    if (i == 0) {
                        for (var k = 0; k < data[i].date.length; k++) {
                            date.push(data[i].date[k]);
                        }
                    }
                    name.push(data[i].name);
                    var p = {};
                    p.name = data[i].name;
                    p.type = 'line';
                    p.itemStyle = { normal: { label: { show: true } } };
                    var array = [];
                    for (var j = 0; j < data[i].value.length; j++) {
                        array.push(data[i].value[j]);
                    }
                    p.data = array;
                    value.push(p);
                }
                option = {
                    title: {
                        text: '各零件挑选数量'
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: name
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    toolbox: {
                        feature: {
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: date
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: value
                };
                PartsTypesEchars.setOption(option);
            }

        }
    })


    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetMaintenanceTypesModel?startTime=&endTime=&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            if (data.length == 0) {
                var newdiv = document.getElementById("MaintenanceTypesModel");
                newdiv.innerHTML = "<font size='5'>当前时间段内该供应商未完成订单</font>";
            }
            else {
                var MaintenanceTypesModel = echarts.init(document.getElementById('MaintenanceTypesModel'));
                var name = [];
                var value = [];
                for (var i = 0; i < data.length; i++) {
                    name.push(data[i].name);
                    var j = {};
                    j.value = data[i].value;
                    j.name = data[i].name;
                    value.push(j);
                }

                option = {
                    title: {
                        text: '本月零件挑选原因占比',
                        x: 'center',
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'horizontal',
                        x: 'center',
                        y: 'bottom',
                        data: name
                    },
                    series: [
                        {
                            name: '本月零件挑选原因占比',
                            //type: 'pie',
                            //radius: '20%',
                            //center: ['50%', '40%'],
                            //data: value,
                            //itemStyle: {
                            //    emphasis: {
                            //        shadowBlur: 10,
                            //        shadowOffsetX: 0,
                            //        shadowColor: 'rgba(0, 0, 0, 0.5)'
                            //    }
                            //}
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            data: value,
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                MaintenanceTypesModel.setOption(option);
            }
        }
    })
})


function GetPartsTypesEcharsModel() {
    var vs = $('#Supplier2  option:selected').val();
    var start = $("#Supplier_PartsType_start").val();
    var end = $("#Supplier_PartsType_end").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPartsTypesEcharsModel?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var PartsTypesEchars = echarts.init(document.getElementById('Supplier_PartsType'));
            PartsTypesEchars.clear();
            var date = [];
            var name = [];
            var value = [];
            for (var i = 0; i < data.length; i++) {
                if (i == 0) {
                    for (var k = 0; k < data[i].date.length; k++) {
                        date.push(data[i].date[k]);
                    }
                }
                name.push(data[i].name);
                var p = {};
                p.name = data[i].name;
                p.type = 'line';
                p.itemStyle = { normal: { label: { show: true } } };
                var array = [];
                for (var j = 0; j < data[i].value.length; j++) {
                    array.push(data[i].value[j]);
                }
                p.data = array;
                value.push(p);
            }
            option = {
                title: {
                    text: '各零件挑选数量'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: name
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: date
                },
                yAxis: {
                    type: 'value'
                },
                series: value
            };
            PartsTypesEchars.setOption(option);
           
        }
    })
}


function GetMaintenanceTypesModel_start() {
    var vs = $('#Supplier4  option:selected').val();
    var start = $("#MaintenanceTypesModel_start").val();
    var end = $("#MaintenanceTypesModel_end").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }

    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetMaintenanceTypesModel?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var MaintenanceTypesModel = echarts.init(document.getElementById('MaintenanceTypesModel'));
            var name = [];
            var value = [];
            for (var i = 0; i < data.length; i++) {
                name.push(data[i].name);
                var j = {};
                j.value = data[i].value;
                j.name = data[i].name;
                value.push(j);
            }
            //alert(JSON.stringify(name));
            //alert(JSON.stringify(value));

            option = {
                title: {
                    text: '本月零件挑选原因占比',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'horizontal',
                    x: 'center',
                    y: 'bottom',
                    data: name
                },
                series: [
                    {
                        name: '本月零件挑选原因占比',
                        //type: 'pie',
                        //radius: '20%',
                        //center: ['50%', '40%'],
                        //data: value,
                        //itemStyle: {
                        //    emphasis: {
                        //        shadowBlur: 10,
                        //        shadowOffsetX: 0,
                        //        shadowColor: 'rgba(0, 0, 0, 0.5)'
                        //    }
                        //}
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: value,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };

            MaintenanceTypesModel.setOption(option);

        }
    })
}


function GetPercentOfPass() {
    var vs = $('#Supplier3  option:selected').val();
    var start = $("#PercentOfPass_StartTime").val();
    var end = $("#PercentOfPass_EndTime").val();
    if (start != "" && end != "") {
        if (start > end) {
            return top.learun.alert.warning('请选择正确的时间段！');
        }
    }
    $.ajax({
        url: "/LR_CodeDemo/YT_PartsTypes/GetPercentOfPass?startTime=" + start + "&endTime=" + end + "&supplier=" + vs,
        async: false,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            var PercentOfPass = echarts.init(document.getElementById('PercentOfPass'));
            var k = [];

            var j = {};
            j.value = data.success;
            j.name = '合格';
            k.push(j);
            var p = {};
            p.value = data.error;
            p.name = '不合格';
            k.push(p);

            option = {
                title: {
                    text: '供应商累计合格率',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['合格', '不合格']
                },
                color: ['#91c7ae', '#ca8622'],
                series: [
                    {
                        name: '占总数百分比',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: k,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };

            PercentOfPass.setOption(option);

        }
    })
}

