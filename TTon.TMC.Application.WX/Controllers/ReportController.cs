﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTon.TMC.Application.WX.Controllers
{
    public class ReportController : ControllerBase
    {
        // GET: Report
        public ActionResult Index()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Report", "", new { rel = "Report/Index", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        public ActionResult DayReport()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Report", "", new { rel = "Report/DayReport", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}