/**
 * Crayola colors in JSON format
 * from: https://gist.github.com/jjdelc/1868136
 */

var parts
var maintenance
$(function () {
    $.ajax({
        type: "Get",
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        //url: 'http://ytxqf.api.ttonservice.com/v1.0/Autocompleter/maintenance',
        url: 'http://ytxqf.api.ttonservice.com/v1.0/Autocompleter/parts',
        dataType: "json",
        success: function (response) {
            if (response.data != null) {
                parts = response.data;
                $('#PartsNumber').autocompleter({
                    // marker for autocomplete matches
                    highlightMatches: true,

                    // object to local or url to remote search
                    source: parts,

                    // custom template
                    template: '{{ label }} <span>({{ hex }})</span>',

                    // show hint
                    hint: false,

                    // abort source if empty field
                    empty: false,

                    // max results
                    limit: 10,

                    callback: function (value, index, selected) {
                    
                        if (selected) {
                            $('#partsName').val(selected.hex);
                        }
                    }
                });
            }
        }
    })

    //$.ajax({
    //    type: "Get",
    //    contentType: "application/x-www-form-urlencoded;charset=utf-8",
    //    url: 'http://localhost:49345/v1.0/Autocompleter/maintenance',
    //    dataType: "json",
    //    success: function (response) {
    //        if (response.data != null) {
    //            maintenance = response.data;
    //            $('#nope2').autocompleter({
    //                // marker for autocomplete matches
    //                highlightMatches: true,

    //                // object to local or url to remote search
    //                source: maintenance,

    //                // custom template
    //                template: '{{ label }} <span>({{ hex }})</span>',

    //                // show hint
    //                hint: true,

    //                // abort source if empty field
    //                empty: false,

    //                // max results
    //                limit: 5,

    //                callback: function (value, index, selected) {
    //                    if (selected) {

    //                    }
    //                }
    //            });
    //        }
    //    }
    //})
  
});
