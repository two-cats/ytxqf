﻿using TTon.TMC.Application.WorkFlow;
using System.Data.Entity.ModelConfiguration;

namespace TTon.TMC.Application.Mapping
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.17
    /// 描 述：工作流模板信息
    /// </summary>
    public class WfSchemeInfoMap:EntityTypeConfiguration<WfSchemeInfoEntity>
    {
        public WfSchemeInfoMap() {
            #region 表、主键
            //表
            this.ToTable("WORKFLOW_SCHEMEINFO");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
