﻿using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 09:50
    /// 描 述：供应商配置
    /// </summary>
    public class YT_SupplierMap : EntityTypeConfiguration<YT_SupplierEntity>
    {
        public YT_SupplierMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_SUPPLIER");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

