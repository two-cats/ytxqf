﻿using TTon.TMC.Util;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 10:22
    /// 描 述：配置零件
    /// </summary>
    public interface YT_PartsTypesIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_PartsTypesEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_PartsTypesEntity> GetList();
        /// <summary>
        /// 根据供应商获取零件
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<YT_PartsTypesEntity> GetListTree(string parentId);

        /// <summary>
        /// 获取YT_PartsTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        YT_PartsTypesEntity GetYT_PartsTypesEntity(string keyValue);
        List<SupplierListToTree> GetPartsTypesTree(string keyValue);

        /// <summary>
        /// 获取YT_PartsTypes表实体数据
        /// <param name="keyValue">编号</param>
        /// <summary>
        /// <returns></returns>
        YT_PartsTypesEntity PartsEntityByid(string keyValue);

        /// <summary>
        /// 获取零件自补充数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<AutocmpModel> GetAParts();
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, YT_PartsTypesEntity entity);
        List<UserFinishEchartsModel> GetUserFinishEcharts(string startTime, string endTime, string supplier);

        /// <summary>
        /// 本月（时段）各零件挑选数量折线图
        /// </summary>
        /// <returns></returns>
        List<PartsTypesEcharsModel> GetPartsTypesEcharsModel(string startTime, string endTime, string supplier);

        /// <summary>
        /// 本月（时段）零件挑选原因占比
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        List<MaintenanceTypesModel> GetMaintenanceTypesModel(string startTime, string endTime, string supplier);

        /// <summary>
        /// 供应商累计合格率（合格数量/总数）
        /// </summary>
        /// <returns></returns>
        PercentOfPassModel GetPercentOfPass(string startTime, string endTime, string supplier);
        PercentOfPassModel GetPercentToAnalysis(string startTime, string endTime, string supplier);
        List<PartsTypesAnalysisModel> GetPartsTypesEcharsToAnalysis(string startTime, string endTime, string supplier);
        MaintenanceTypesAnalysis GetMaintenanceTypesToAnalysis(string startTime, string endTime, string supplier);
        List<PartsFinishAndNumEcharsModel> GetPartsFinishAndNumEcharts(string startTime, string endTime, string supplier);
        List<NewPartsTypesAnalysisModel> GetNewPartsTypesToAnalysis(string startTime, string endTime, string supplier);
        List<UserFinishEchartsModel> GetUserFinishEchartsToSu(string startTime, string endTime, string supplier);
        #endregion

    }
}
