﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 订单人员
    /// </summary>
    public class OrderUserModel
    {
        /// <summary>
        /// 人员主键
        /// </summary>
        public string user_id { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string company { get; set; }
        /// <summary>
        /// 人名
        /// </summary>
        public string user_name { get; set; }
        /// <summary>
        /// 擅长
        /// </summary>
        public string good { get; set; }
        /// <summary>
        /// 0 空闲  1 忙碌
        /// </summary>
        public int status { get; set; }
    }
}
