﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ScanInfo;
using ZXing;
using System.Drawing;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-09-26 09:14
    /// 描 述：检测记录
    /// </summary>
    public class YT_ScanInfoService : RepositoryFactory
    {
        #region 构造函数和属性

        private string fieldSql;
        public YT_ScanInfoService()
        {
            fieldSql= @"
         	            t.Id,
	            t.Parts_Id,
	            t.PrimitiveCode,
	            t.EquipmentCode,
	            c.F_RealName AS CreateUserId,
	            CONVERT (
		            VARCHAR (100),
		            t.CreateDate,
		            120
	            ) AS CreateDate,
	            m.F_RealName AS ModifyUserId,
	            CONVERT (
		            VARCHAR (100),
		            t.ModifyDate,
		            120
	            ) AS ModifyDate,
	            t.IsDelete
            ";
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<YT_ScanInfoEntity> GetList( string queryJson )
        {
            try
            {
                //参考写法
                //var queryParam = queryJson.ToJObject();
                // 虚拟参数
                //var dp = new DynamicParameters(new { });
                //dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM YT_ScanInfo t ");
                strSql.Append(" LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId ");
                strSql.Append(" LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId ");
                return this.BaseRepository().FindList<YT_ScanInfoEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取列表分页数据
        /// <param name="pagination">分页参数</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<YT_ScanInfoEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(fieldSql);
                strSql.Append(" FROM YT_ScanInfo t "); 
                strSql.Append(" LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId ");
                strSql.Append(" LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId ");
                strSql.Append(" WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreateDate >= @startTime AND t.CreateDate <= @endTime ) ");
                }
                pagination.sidx = "t.CreateDate";
                pagination.sord = "desc";
                return this.BaseRepository().FindList<YT_ScanInfoEntity>(strSql.ToString(),dp,pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_ScanInfoEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_ScanInfoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 验证扫描是否是第一次
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public bool GetScanCount(string keyValue)
        {
            try
            {
                //ScanModel model = new ScanModel();
                //StringBuilder ImgSql = new StringBuilder();
                //ImgSql.Append(@"SELECT F_FilePath  as Img_Url
                //            FROM Base_AnnexesFile
                //            WHERE F_FolderId = '" + keyValue + @"'
                //            ORDER BY F_CreateDate desc");
                //List<ImageUrlModel> ImgModel = new RepositoryFactory().BaseRepository().FindList<ImageUrlModel>(ImgSql.ToString()).AsList();
                //if (ImgModel.Count > 0)
                //{
                //    string img = ImgModel[0].Img_Url;

                //    #region 解析二维码
                //    string result = "";
                //    if (System.IO.File.Exists(img))
                //    {
                //        try
                //        {
                //            BarcodeReader reader = new BarcodeReader();
                //            reader.Options.CharacterSet = "UTF-8";
                //            Bitmap map = new Bitmap(img);
                //            Result res = reader.Decode(map);
                //            if (res != null)
                //            {
                //                result = res.Text;
                //                model.code = result;
                //            }
                //            else
                //            {
                //                model.code = "";
                //            }
                //        }
                //        catch (Exception e)
                //        {
                //            throw new Exception("识别二维码出现错误，请尝试重新扫描。");
                //        }
                //    }
                //    #endregion

                //    YT_ScanInfoEntity entity = this.BaseRepository().FindEntity<YT_ScanInfoEntity>(t => t.PrimitiveCode == result);
                //    if (entity == null)
                //    {
                //        model.verify = true;
                //    }
                //    else
                //    {
                //        model.verify = false;
                //    }
                //}
                //return model;

                YT_ScanInfoEntity entity = this.BaseRepository().FindEntity<YT_ScanInfoEntity>(t => t.PrimitiveCode == keyValue);
                if (entity == null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<YT_ScanInfoEntity>(t=>t.Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_ScanInfoEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 扫描
        /// </summary>
        /// <param name="param"></param>
        public void AddScan(ScanParams param)
        {
            try
            {
                YT_ScanInfoEntity entity = this.BaseRepository().FindEntity<YT_ScanInfoEntity>(t => t.PrimitiveCode == param.Primitive);
                if(entity == null)
                {
                    entity = new YT_ScanInfoEntity()
                    {
                        Id = Guid.NewGuid().ToString(),
                        PrimitiveCode = param.Primitive,
                        EquipmentCode = param.Equipment,
                        CreateUserId = param.UserId,
                        CreateDate = Convert.ToDateTime(param.ScanTime),
                        IsDelete = 0
                    };
                    this.BaseRepository().Insert(entity);
                }
                else
                {
                    entity.EquipmentCode = param.Equipment;
                    entity.ModifyDate = Convert.ToDateTime(param.ScanTime);
                    entity.ModifyUserId = param.UserId;
                    this.BaseRepository().Update(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion


        #region 导出
        public DataTable ExportScanList(string startTime, string endTime)
        {
            try
            {
                DateTime start = Convert.ToDateTime(startTime);
                DateTime end = Convert.ToDateTime(endTime);
                var strSql = new StringBuilder();
                strSql.Append(@"SELECT    
	                t.PrimitiveCode,
	                t.EquipmentCode,
	                c.F_RealName AS CreateUserId,
	                CONVERT (
		                VARCHAR (100),
		                t.CreateDate,
		                120
	                ) AS CreateDate,
	                m.F_RealName AS ModifyUserId,
	                CONVERT (
		                VARCHAR (100),
		                t.ModifyDate,
		                120
	                ) AS ModifyDate
                   FROM YT_ScanInfo t
                   LEFT JOIN Base_User c ON c.F_UserId = t.CreateUserId 
                   LEFT JOIN Base_User m ON m.F_UserId = t.ModifyUserId
                   WHERE t.CreateDate BETWEEN '"+start+ "' AND '" + end + "'");
                return this.BaseRepository().FindTable(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion
    }
}
