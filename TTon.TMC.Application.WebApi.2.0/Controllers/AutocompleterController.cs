﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{

    [RoutePrefix("v1.0/Autocompleter")]
    public class AutocompleterController : BaseApiController
    {
        
        private YT_PartsTypesIBLL partsTypesBLL = new YT_PartsTypesBLL();
        private YT_MaintenanceTypesIBLL maintenanceTypesBLL = new YT_MaintenanceTypesBLL();

        //获取零件自动缓存数据
        [Route("parts")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetAParts()
        {
            try
            {
                var data = partsTypesBLL.GetAParts();
                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

       


        /// <summary>
        /// 获取类型自动缓存数据
        /// </summary>
        /// <param name="SupplierId"></param>
        /// <param name="PartId"></param>
        /// <returns></returns>
        [Route("maintenance")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetACmaintenance()
        {
            try
            {
                var data = maintenanceTypesBLL.GetACmaintenance();
                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }



    }
}
