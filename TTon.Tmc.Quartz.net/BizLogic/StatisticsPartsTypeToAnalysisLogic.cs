﻿using ESteward.QuartzService.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.Tmc.Util.Extension;
using TTon.TMC.Quartz.net.Model;

namespace ESteward.QuartzService.BizLogic
{
    public class StatisticsPartsTypeToAnalysisLogic
    {
        public void statisticsPartsType()
        {
            #region 供应商筛选
            DateTime now = DateTime.Now;
            StringBuilder supplier = new StringBuilder();
            supplier.Append(@"SELECT
	                    *
                    FROM
	                    YT_Supplier
                    WHERE
	                    IsDelete = 0");
            List<YT_Supplier> supplierList = DB_Pla.Context.FromSql(supplier.ToString()).ToList<YT_Supplier>();
            #endregion

            StringBuilder sqlTime = new StringBuilder();
            sqlTime.Append(@"SELECT
	                    TOP 1 CreateDate
                    FROM
	                    YT_Order
                    WHERE
	                    IsDelete = 0
                    ORDER BY
	                    CreateDate ASC");
            DataTable stdt = DB_Pla.Context.FromSql(sqlTime.ToString()).ToDataTable();
            DateTime sTime = Convert.ToDateTime(stdt.Rows[0]["CreateDate"]);
            int ms = (now - sTime).Days + 1;

            foreach (var supplierItem in supplierList)
            {
                StringBuilder partstype_sql = new StringBuilder();
                partstype_sql.Append(@"SELECT YT_PartsTypes.Parts_Name,YT_PartsTypes.Parts_Number FROM YT_SupplierOrParts
                                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_SupplierOrParts.Parts_Id
                                    WHERE YT_SupplierOrParts.Supplier_Id ='" + supplierItem.Id + @"'
                                    GROUP BY YT_PartsTypes.Parts_Name,YT_PartsTypes.Parts_Number");
                List<YT_PartsTypes> YT_PartsTypesEntitys = DB_Pla.Context.FromSql(partstype_sql.ToString()).ToList<YT_PartsTypes>();
                if (YT_PartsTypesEntitys.Count > 0)
                {
                    foreach (var item in YT_PartsTypesEntitys)
                    {
                        for (int i = 0; i < ms + 1; i++)
                        {
                            string st = now.AddDays(-i).ToString("yyyy-MM-dd 00:00:00");
                            string et = now.AddDays(-i).ToString("yyyy-MM-dd 23:59:59");

                            StringBuilder strsql = new StringBuilder();
                            strsql.Append(@"SELECT (SELECT sum(YT_OrderUser.Success+YT_OrderUser.Error) FROM YT_OrderUser WHERE YT_OrderUser.Order_Id=YT_Order.Id) AS value 
                                    FROM YT_Order
                                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                                    LEFT JOIN YT_OrderUser ON YT_OrderUser.Order_Id=YT_Order.Id
                                    WHERE YT_Order.IsDelete = 0 AND YT_Order.Supplier_Id='" + supplierItem.Id + "' AND YT_Order.PartsNumber='" + item.Parts_Number + "' AND YT_PartsTypes.Parts_Name='" + item.Parts_Name + "' AND YT_Order.CreateDate BETWEEN '" + st + "' AND '" + et + "'");
                            DataTable dtlist = DB_Pla.Context.FromSql(strsql.ToString()).ToDataTable();

                            YT_PartsTypeAnalysis partsTypeAnalysis = new YT_PartsTypeAnalysis
                            {
                                Id = Guid.NewGuid().ToString(),
                                Supplier = supplierItem.Id,
                                Time = Convert.ToDateTime(st).ToString("yyyy-MM"),
                                Day = Convert.ToDateTime(st).Day.ToString(),
                                Name = item.Parts_Name + "-" + item.Parts_Number,
                                Count = dtlist.Rows.Count > 0 ? dtlist.Rows[0]["value"].ToInt() : 0,
                                CreateTime = DateTime.Now
                            };
                            DB_Pla.Context.Insert<YT_PartsTypeAnalysis>(partsTypeAnalysis);
                        }
                    }
                }
            }
        }
    }
}
