﻿using ESteward.QuartzService.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ESteward.QuartzService
{
    public sealed class ServiceRunner : ServiceControl, ServiceSuspend
    {
        // 生命作业调度器，
        private readonly IScheduler scheduler;

        public ServiceRunner()
        {
            // 调度器实例化
            scheduler = StdSchedulerFactory.GetDefaultScheduler();
        }

        /// <summary>
        /// 重新启动所有作业
        /// </summary>
        /// <param name="hostControl"></param>
        /// <returns></returns>
        public bool Continue(HostControl hostControl)
        {
            scheduler.ResumeAll();
            return true;
        }

        /// <summary>
        /// 暂停所有作业
        /// </summary>
        /// <param name="hostControl"></param>
        /// <returns></returns>
        public bool Pause(HostControl hostControl)
        {
            scheduler.PauseAll();
            return true;
        }

        /// <summary>
        /// 开始执行绑定的作业
        /// </summary>
        /// <param name="hostControl"></param>
        /// <returns></returns>
        public bool Start(HostControl hostControl)
        {
            scheduler.Start();

            // 绑定要执行的作业
            BindJob();
            return true;
        }

        /// <summary>
        /// 关闭作业调度器
        /// </summary>
        /// <param name="hostControl"></param>
        /// <returns></returns>
        public bool Stop(HostControl hostControl)
        {
            scheduler.Shutdown(false);
            return true;
        }

        /// <summary>
        /// 绑定要制定的作业列表
        /// </summary>
        private void BindJob()
        {
            PushToSupplier();
            PushToSupplierOrderStatistics();
            PushSmsToUser();
            GetStatisticsOrdersToAnalysis();
            //GetPartsTypeToAnalysisLogic();
        }

    

   

        /// <summary>
        ///发送邮件
        /// </summary>
        public void Test()
        {
            // 任务触发频率设置参数
            string cron = ConfigurationManager.AppSettings["TestJobCron"].ToString();

            IJobDetail job = JobBuilder.Create<Test>()
                             .WithIdentity("TestJob", "Test")
                             .Build();
            ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity("Test")
                               .StartNow()
                               .WithCronSchedule(cron)
                               .Build();

            scheduler.ScheduleJob(job, trigger);     
        }

        /// <summary>
        /// 推送
        /// </summary>
        public void PushToSupplier()
        {
            // 任务触发频率设置参数
            string cron = ConfigurationManager.AppSettings["TestJobCron"].ToString();

            IJobDetail job = JobBuilder.Create<PushToSupplierJob>()
                             .WithDescription("PushToSupplierJob")
                             .Build();
            ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity("PushToSupplierJobTrigger")
                               .StartNow()
                               .WithCronSchedule(cron)
                               .Build();
            scheduler.ScheduleJob(job, trigger);
        }

        /// <summary>
        /// 推送OrderJobCron
        /// </summary>
        public void PushToSupplierOrderStatistics()
        {
            // 任务触发频率设置参数
            string cron = ConfigurationManager.AppSettings["OrderJobCron"].ToString();

            IJobDetail job = JobBuilder.Create<PushToSupplierOrderStatisticsJob>()
                             .WithDescription("PushToSupplierOrderStatisticsJob")
                             .Build();
            ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity("PushToSupplierOrderStatisticsJobTrigger")
                               .StartNow()
                               .WithCronSchedule(cron)
                               .Build();
            scheduler.ScheduleJob(job, trigger);
        }


        /// <summary>
        /// 推送
        /// </summary>
        public void PushSmsToUser()
        {
            // 任务触发频率设置参数
            string cron = ConfigurationManager.AppSettings["OrderJobCron"].ToString();

            IJobDetail job = JobBuilder.Create<PushSmsToUserJob>()
                             .WithDescription("PushSmsToUserJob")
                             .Build();
            ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity("PushSmsToUserJobTrigger")
                               .StartNow()
                               .WithCronSchedule(cron)
                               .Build();
            scheduler.ScheduleJob(job, trigger);
        }


        public void GetStatisticsOrdersToAnalysis()
        {
            // 任务触发频率设置参数
            string cron = ConfigurationManager.AppSettings["AnalysisCron"].ToString();

            IJobDetail job = JobBuilder.Create<StatisticsOrdersToAnalysisJob>()
                             .WithDescription("StatisticsOrdersToAnalysisJob")
                             .Build();
            ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity("StatisticsOrdersToAnalysisJobTrigger")
                               .StartNow()
                               .WithCronSchedule(cron)
                               .Build();
            scheduler.ScheduleJob(job, trigger);
        }


        public void GetPartsTypeToAnalysisLogic()
        {
            // 任务触发频率设置参数
            string cron = ConfigurationManager.AppSettings["AnalysisCron"].ToString();

            IJobDetail job = JobBuilder.Create<StatisticsPartsTypeToAnalysisJob>()
                             .WithDescription("StatisticsPartsTypeToAnalysisJob")
                             .Build();
            ITrigger trigger = TriggerBuilder.Create()
                               .WithIdentity("StatisticsPartsTypeToAnalysisJobTrigger")
                               .StartNow()
                               .WithCronSchedule(cron)
                               .Build();
            scheduler.ScheduleJob(job, trigger);
        }
    }
}
