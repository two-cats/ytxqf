﻿using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-09-26 09:14
    /// 描 述：检测记录
    /// </summary>
    public class YT_ScanInfoMap : EntityTypeConfiguration<YT_ScanInfoEntity>
    {
        public YT_ScanInfoMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_SCANINFO");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

