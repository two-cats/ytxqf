﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Quartz.net.Model
{
    /// <summary>
    /// WxTokenCommenModel
    /// </summary>
    public class WxTokenCommenModel
    {
        /// <summary>
        /// 错误码  0=请求成功
        /// </summary>
        public int errcode;

        /// <summary>
        /// 错误信息
        /// </summary>
        public string errmsg;
    }
}