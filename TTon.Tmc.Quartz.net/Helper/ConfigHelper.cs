﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Quartz.net.Helper
{
    public class ConfigHelper
    {
        public string appKeyValue;
        public string masterSecretValue;

        public ConfigHelper()
        {
            this.appKeyValue = ConfigurationManager.AppSettings["APPKEY"].ToString();
            this.masterSecretValue = ConfigurationManager.AppSettings["MASTERSECRET"].ToString();
        }
    }
}
