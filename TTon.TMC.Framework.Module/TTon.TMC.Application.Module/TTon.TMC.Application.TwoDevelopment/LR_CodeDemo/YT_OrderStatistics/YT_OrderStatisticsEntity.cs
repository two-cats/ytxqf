﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-09-29 15:39
    /// 描 述：订单统计
    /// </summary>
    public class YT_OrderStatisticsEntity 
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 统计时间段
        /// </summary>
        /// <returns></returns>
        [Column("TIMEBUCKET")]
        public string TimeBucket { get; set; }
        /// <summary>
        /// 总数
        /// </summary>
        /// <returns></returns>
        [Column("AMOUNT")]
        public int? Amount { get; set; }
        /// <summary>
        /// 完成数量
        /// </summary>
        /// <returns></returns>
        [Column("FINISH")]
        public int? Finish { get; set; }
        /// <summary>
        /// 未完成数量
        /// </summary>
        /// <returns></returns>
        [Column("UNFINISHED")]
        public int? UnFinished { get; set; }
        /// <summary>
        /// 合格数量
        /// </summary>
        /// <returns></returns>
        [Column("QUALIFIED")]
        public int? Qualified { get; set; }
        /// <summary>
        /// 不合格数量
        /// </summary>
        /// <returns></returns>
        [Column("UNQUALIFIED")]
        public int? Unqualified { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        /// <returns></returns>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// CreateUserId
        /// </summary>
        /// <returns></returns>
        [Column("CREATEUSERID")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// SupplierId
        /// </summary>
        /// <returns></returns>
        [Column("SUPPLIERID")]
        public string SupplierId { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

