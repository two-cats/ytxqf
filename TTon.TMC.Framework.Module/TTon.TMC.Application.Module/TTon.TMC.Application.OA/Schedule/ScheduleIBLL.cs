﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Util;

namespace TTon.TMC.Application.OA.Schedule
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.07.11
    /// 描 述：日程管理
    /// </summary>
    public interface ScheduleIBLL
    {

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>返回列表</returns>
        IEnumerable<ScheduleEntity> GetList();
        /// <summary>
        /// 根据状态查询列表
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson"></param>
        /// <returns></returns>
        IEnumerable<ScheduleEntity> GetPageList(Pagination pagination, string queryJson);
     
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        ScheduleEntity GetEntity(string keyValue);
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        void SaveForm(string keyValue, ScheduleEntity entity);
        IEnumerable<ScheduleEntity> GetMeetingPageList(string userid,Pagination pagination, string querystr);
        void DeleteMeeting(string approvalid);
        MeetingStatusChartsModel GetMeetingStatusEcharts(string startTime, string endTime);
        IEnumerable<ScheduleEChartsList> GetEChartsPageList(Pagination paginationobj, string queryJson, string startTime, string endTime, string status);
        #endregion
    }
}
