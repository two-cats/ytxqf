﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Instructor.Model
{
    public class InstructorListModel
    {
        public string id { get; set; }

        public string name { get; set; }
    }
}
