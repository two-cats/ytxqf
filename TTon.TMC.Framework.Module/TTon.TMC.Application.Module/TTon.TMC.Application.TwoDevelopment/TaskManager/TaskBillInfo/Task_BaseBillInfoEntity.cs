﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.TaskManager
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class Task_BaseBillInfoEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// 任务标题
        /// </summary>
        [Column("SUBJECT")]
        public string Subject { get; set; }
        /// <summary>
        /// 任务分类
        /// </summary>
        [Column("CATEGORY")]
        public string Category { get; set; }
        /// <summary>
        /// 报修位置
        /// </summary>
        [Column("PLACEID")]
        public string PlaceId { get; set; }
        /// <summary>
        /// 位置补充
        /// </summary>
        [Column("PLACEEXTEND")]
        public string PlaceExtend { get; set; }
        /// <summary>
        /// 任务级别
        /// </summary>
        [Column("LEVEL")]
        public int? Level { get; set; }
        /// <summary>
        /// 任务描述
        /// </summary>
        [Column("SUMMARY")]
        public string Summary { get; set; }
        /// <summary>
        /// 任务状态、发起、确认、派发、完成、评论、回访
        /// </summary>
        [Column("STATUS")]
        public string Status { get; set; }
        /// <summary>
        /// 发起时间
        /// </summary>
        [Column("SUBMITTIME")]
        public DateTime? SubmitTime { get; set; }
        /// <summary>
        /// 确认时间
        /// </summary>
        [Column("CONFIRMTIME")]
        public DateTime? ConfirmTime { get; set; }
        /// <summary>
        /// 派发时间
        /// </summary>
        [Column("FORWARDTIME")]
        public DateTime? ForwardTime { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
        [Column("FINISHTIME")]
        public DateTime? FinishTime { get; set; }
        /// <summary>
        /// 评论时间
        /// </summary>
        [Column("COMMENTTIME")]
        public DateTime? CommentTime { get; set; }
        /// <summary>
        /// 回访时间
        /// </summary>
        [Column("CALLBACKTIME")]
        public DateTime? CallbackTime { get; set; }
        /// <summary>
        /// 工单物理状态，有效，无效，删除。
        /// </summary>
        [Column("PHYSICALSTATUS")]
        public string PhysicalStatus { get; set; }
        /// <summary>
        /// 显示状态，进行中、已完成。
        /// </summary>
        [Column("DISPLAYSTATUS")]
        public string DisplayStatus { get; set; }

        /// <summary>
        /// 甲方评价
        /// </summary>
        [Column("APPRAISECONTENT")]
        public string AppraiseContent { get; set; }

        /// <summary>
        /// 甲方用户Id
        /// </summary>
        [Column("FPUSERID")]
        public string FPUserId { get; set; }

        /// <summary>
        /// 甲方用户名
        /// </summary>
        [Column("FPUSERNAME")]
        public string FPUserName { get; set; }

        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
        #region 扩展字段
        /// <summary>
        /// 工单文件地址
        /// </summary>
        [NotMapped]
        public string FilePath { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
        [NotMapped]
        public string FileType { get; set; }
        /// <summary>
        /// 物料Id
        /// </summary>
        [NotMapped]
        public string MaterialId { get; set; }
        /// <summary>
        /// 物料数量
        /// </summary>
        [NotMapped]
        public string Count { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [NotMapped]
        public string Remark { get; set; }
        /// <summary>
        /// 指派人员
        /// </summary>
        [NotMapped]
        public string UserId { get; set; }
        /// <summary>
        /// 是否是工单负责人
        /// </summary>
        [NotMapped]
        public string IsMaster { get; set; }
        /// <summary>
        /// 任务角色
        /// </summary>
        [NotMapped]
        public string TaskRole { get; set; }
        #endregion
    }
}

