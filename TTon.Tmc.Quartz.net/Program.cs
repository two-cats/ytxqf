﻿using ESteward.QuartzService.Job;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ESteward.QuartzService
{
    class Program
    {

        static void Main(string[] args)
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.config");
            FileInfo fi = new FileInfo(filePath);
            log4net.Config.XmlConfigurator.ConfigureAndWatch(fi);
            HostFactory.Run(x =>
            {
                x.UseLog4Net();
                x.Service<ServiceRunner>();
                x.SetDescription("TTon.Tmc.Quartz.net");
                x.SetServiceName("TTon.Tmc.Quartz.net");
                x.SetDisplayName("TTon.Tmc.Quartz.net");
                x.EnablePauseAndContinue();
            });

        }
    }
}
