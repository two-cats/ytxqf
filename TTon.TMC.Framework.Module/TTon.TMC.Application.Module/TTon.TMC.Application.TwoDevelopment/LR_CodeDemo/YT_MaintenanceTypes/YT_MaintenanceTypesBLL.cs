﻿using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 11:48
    /// 描 述：配置维修类型
    /// </summary>
    public class YT_MaintenanceTypesBLL : YT_MaintenanceTypesIBLL
    {
        private YT_MaintenanceTypesService yT_MaintenanceTypesService = new YT_MaintenanceTypesService();

        #region 获取数据
        public List<SupplierListToTree> GetMaintenanceTypesTree(string keyValue)
        {
            try
            {
                return yT_MaintenanceTypesService.GetMaintenanceTypesTree(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_MaintenanceTypesEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return yT_MaintenanceTypesService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_MaintenanceTypesEntity> GetList()
        {
            try
            {
                return yT_MaintenanceTypesService.GetList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }

        }
        /// <summary>
        /// 获取YT_MaintenanceTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_MaintenanceTypesEntity GetYT_MaintenanceTypesEntity(string keyValue)
        {
            try
            {
                return yT_MaintenanceTypesService.GetYT_MaintenanceTypesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <param name="Parts_Id"></param>
        /// <returns></returns>
        public IEnumerable<YT_MaintenanceTypesEntity> GetListTree()
        {
            try
            {
                return yT_MaintenanceTypesService.GetListTree();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Supplier_Id"></param>
        /// <param name="Parts_Id"></param>
        /// <returns></returns>
        public IEnumerable<AutocmpModel> GetACmaintenance()
        {
            try
            {
                return yT_MaintenanceTypesService.GetACmaintenance();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }




        public List<PartsTypesEchartModel> GetPartsTypesEcharts(string supplier)
        {
            try
            {
                return yT_MaintenanceTypesService.GetPartsTypesEcharts(supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<MaintenanceTypeTimeModel> GetMaintenanceTypesTime(string startTime, string endTime, string supplier, string part)
        {
            try
            {
                return yT_MaintenanceTypesService.GetMaintenanceTypesTime(startTime, endTime, supplier, part);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public List<MaintenanceTypeTimeToAModel> GetMaintenanceTypesTimeToAnalysis(string startTime, string endTime, string supplier)
        {
            try
            {
                return yT_MaintenanceTypesService.GetMaintenanceTypesTimeToAnalysis(startTime, endTime, supplier);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                yT_MaintenanceTypesService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_MaintenanceTypesEntity entity)
        {
            try
            {
                yT_MaintenanceTypesService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
