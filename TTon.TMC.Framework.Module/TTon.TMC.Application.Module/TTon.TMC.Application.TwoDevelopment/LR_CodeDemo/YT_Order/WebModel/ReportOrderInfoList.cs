﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class ReportOrderInfoList
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string startDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string successDate { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string supplier { get; set; }
        /// <summary>
        /// 零件名称
        /// </summary>
        public string partsType { get; set; }
        /// <summary>
        /// 零件号
        /// </summary>
        public string partsNumber { get; set; }
        /// <summary>
        /// 供应商批次
        /// </summary>
        public string batchNumber { get; set; }
        /// <summary>
        /// 零件领回数量
        /// </summary>
        public string allotCount { get; set; }
        /// <summary>
        /// 挑选/返工原因
        /// </summary>
        public string maintainType { get; set; }
        /// <summary>
        /// 挑选方法
        /// </summary>
        public string repairType { get; set; }
        /// <summary>
        /// 挑选总数
        /// </summary>
        public string count { get; set; }
        /// <summary>
        /// 良品数
        /// </summary>
        public string qualified { get; set; }
        /// <summary>
        /// 不良品数
        /// </summary>
        public string disqualified { get; set; }
        /// <summary>
        /// 剩余数量
        /// </summary>
        public string unfinished { get; set; }
        /// <summary>
        /// 人数
        /// </summary>
        public string person { get; set; }
        /// <summary>
        /// 总工时
        /// </summary>
        public string workHours { get; set; }
        /// <summary>
        /// 不良品数量分原因具体描述
        /// </summary>
        public string badProductDescribe { get; set; }
    }
}
