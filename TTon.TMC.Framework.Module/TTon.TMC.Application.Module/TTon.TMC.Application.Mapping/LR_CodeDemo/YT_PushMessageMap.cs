﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PushMessage;

namespace TTon.TMC.Application.Mapping.LR_CodeDemo
{
    public class YT_PushMessageMap : EntityTypeConfiguration<YT_PushMessageEntity>
    {
        public YT_PushMessageMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_PUSHMESSAGE");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
