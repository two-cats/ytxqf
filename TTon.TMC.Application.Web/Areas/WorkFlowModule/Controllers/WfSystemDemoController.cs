﻿using TTon.TMC.Application.TwoDevelopment.SystemDemo;
using System.Web.Mvc;

namespace TTon.TMC.Application.Web.Areas.WorkFlowModule.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.04.17
    /// 描 述：系统表单-请假单
    /// </summary>
    public class WfSystemDemoController : MvcControllerBase
    {
        #region 请假表单
        // 请假表单后台方法
        DemoleaveIBLL demoleaveIBLL = new DemoleaveBLL();

        /// <summary>
        /// 系统请假单(视图)
        /// </summary>
        /// <returns></returns>
        public ActionResult DemoLeaveForm()
        {
            return View();
        }
        /// <summary>
        /// 系统请假单(保存数据)
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ActionResult DemoLeaveSaveForm(string keyValue, DemoleaveEntity entity)
        {
            demoleaveIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功");
        }
        /// <summary>
        /// 系统请假单(获取数据)
        /// </summary>
        /// <param name="processId">流程实例主键</param>
        /// <returns></returns>
        public ActionResult DemoLeaveGetFormData(string processId)
        {
            var data = demoleaveIBLL.GetEntity(processId);
            return Success(data);
        }
        #endregion
    }
}