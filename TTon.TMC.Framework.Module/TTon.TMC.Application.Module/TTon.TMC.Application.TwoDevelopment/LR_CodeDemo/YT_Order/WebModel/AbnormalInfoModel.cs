﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class AbnormalInfoModel
    {
        public string id { get; set; }

        public string content { get; set; }

        public List<ImageUrl> pic { get; set; }
    }
}
