﻿/* *     /// 版本 TTon.TMC开发框架()
 * Copyright (c) 2013-2017 TTON
 * 创建人：超级管理员
 * 日  期：2018-04-17 13:42
 * 描  述：资产管理
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, learun) {
    "use strict";
    var selectedRow = learun.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#F_ProjectId').lrCompanySelect({ maxHeight: 180 });
            $('#F_RoomId').lrDataItemSelect({ code: 'hys' });
            //开始时间
            $('#F_StartTime').lrselect({
                maxHeight: 150
            });
            //结束时间
            $('#F_EndTime').lrselect({
                maxHeight: 150
            });
        },
        initData: function () {
            if (!!keyValue) {
                $.lrSetForm(top.$.rootUrl + '/OAModule/Schedule/GetFormEntity?keyValue=' + keyValue, function (data) {
                    if (data.F_Approval == "0" || data.F_Approval == "1") {
                        var currentBtn1 = document.getElementById("but1");
                        var currentBtn2 = document.getElementById("but2");
                        currentBtn1.style.display = "none";
                        currentBtn2.style.display = "none";
                        document.getElementById("but3").style.cssText = "margin-left:50%"
                    };
                    $('[data-table="' + data + '"]').lrSetFormData(data);
                });
            }
        }
    };

    $("#but1").click(function () {
        var postData = {
            "ID": keyValue,
            "Approval": $('#but1').val()
        };

        $.ajax({
            url: "http://ytxwy.api.ttonservice.com/v1.0/Schedule/Approval",
            dataType: "json",
            data: postData,
            type: "POST",
            success: function (data) {
            // 保存成功后才回调
                var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                parent.layer.close(index);  // 关闭layer
            },
            error: function () {
                alert('审批失败，请重试！');
            }
        });
    });

    $("#but2").click(function () {
        var status = $('#but2').val();
        learun.layerForm({
            id: 'CauseForm',
            title: '审批意见',
            url: top.$.rootUrl + '/OAModule/Schedule/CauseForm?keyValue=' + keyValue + '&status=' + status,
            width: 300,
            height: 200,
            callBack: function (id) {
                return top[id].acceptClick();
            }
        });
    });


    $("#but3").click(function () {
        var postData = {
            "approvalid": keyValue
        };

        $.ajax({
            url: "http://ytxwy.api.ttonservice.com/v1.0/Schedule/delete/approval",
            dataType: "json",
            data: postData,
            type: "POST",
            success: function (data) {
                if (data.data == "请在会议开始前三十分钟取消") {
                    alert("请在会议开始前三十分钟取消");
                }
                if (data.data == "取消成功") {
                    // 保存成功后才回调
                    learun.frameTab.currentIframe().location.reload();
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);  // 关闭layer
                }
            },
            error: function () {
                alert('审批失败，请重试！');
            }
        });
    });
    // 保存数据
    acceptClick = function (callBack) {
    
    };
    page.init();
}
