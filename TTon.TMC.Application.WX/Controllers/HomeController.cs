﻿using Dos.WeChat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTon.TMC.Application.WX.Entities;

namespace TTon.TMC.Application.WX.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Register()
        {
            string code = Request.QueryString["code"].ToString();

            var dic = UserHelper.GetOauth2AccessToken(new WeChatParam()
            {
                Code = code
            });
            //获取用户OpenId
            var openId = dic["openid"];
            //根据用户OpenId以及access_token获取用户昵称、头像
            //前提是你的自定义菜单链接中的scope=snsapi_userinfo
            var userInfo = UserHelper.GetSnsUserInfo(new WeChatParam()
            {
                OpenId = openId,
                AccessToken = dic["access_token"]
            });
            ViewData["OpenId"] = openId;
            return View();
        }
    }
}