﻿using TTon.TMC.Util;
using TTon.TMC.Application.TwoDevelopment.TaskManager;
using System.Web.Mvc;
using System.Collections.Generic;

namespace TTon.TMC.Application.Web.Areas.TaskManager.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public class TaskBillInfoController : MvcControllerBase
    {
        private TaskBillInfoIBLL taskBillInfoIBLL = new TaskBillInfoBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = taskBillInfoIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var Task_BaseBillInfoData = taskBillInfoIBLL.GetTask_BaseBillInfoEntity( keyValue );
            var Task_FileInfoData = taskBillInfoIBLL.GetTask_FileInfoEntity( Task_BaseBillInfoData.Id );
            var Task_MaterialInfoData = taskBillInfoIBLL.GetTask_MaterialInfoEntity( Task_BaseBillInfoData.Id );
            var Task_UserInfoData = taskBillInfoIBLL.GetTask_UserInfoEntity( Task_BaseBillInfoData.Id );
            var jsonData = new {
                Task_BaseBillInfoData = Task_BaseBillInfoData,
                Task_FileInfoData = Task_FileInfoData,
                Task_MaterialInfoData = Task_MaterialInfoData,
                Task_UserInfoData = Task_UserInfoData,
            };
            return Success(jsonData);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            taskBillInfoIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string strtask_FileInfoEntity, string strtask_MaterialInfoEntity, string strtask_UserInfoEntity)
        {
            Task_BaseBillInfoEntity entity = strEntity.ToObject<Task_BaseBillInfoEntity>();
            Task_FileInfoEntity task_FileInfoEntity = strtask_FileInfoEntity.ToObject<Task_FileInfoEntity>();
            Task_MaterialInfoEntity task_MaterialInfoEntity = strtask_MaterialInfoEntity.ToObject<Task_MaterialInfoEntity>();
            Task_UserInfoEntity task_UserInfoEntity = strtask_UserInfoEntity.ToObject<Task_UserInfoEntity>();
            taskBillInfoIBLL.SaveEntity(keyValue,entity,task_FileInfoEntity,task_MaterialInfoEntity,task_UserInfoEntity);
            return Success("保存成功！");
        }
        #endregion

    }
}
