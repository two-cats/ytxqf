﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class ReportDetailModel
    {
        /// <summary>
        /// 注意事项
        /// </summary>
        public string mattersAttention { get; set; }
        /// <summary>
        /// 订单列表
        /// </summary>
        public List<ReportOrderInfoList> orders { get; set; }
    }
}
