﻿using TTon.TMC.Util;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using System.Web.Mvc;
using System.Collections.Generic;
using System;
using System.IO;
using TTon.TMC.Application.Base.SystemModule;
using TTon.TMC.Cache.Factory;
using TTon.TMC.Cache.Base;
using NPOI.XSSF.UserModel;
using System.Data;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Web;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_OrderSuperaddition;

namespace TTon.TMC.Application.Web.Areas.LR_CodeDemo.Controllers
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-22 09:53
    /// 描 述：订单报修
    /// </summary>
    public class YT_OrderController : MvcControllerBase
    {
        private YT_OrderIBLL yT_OrderIBLL = new YT_OrderBLL();
        private YT_InstructorIBLL InstructorBll = new YT_InstructorBLL();
        private HttpContext context;
        private ICache cache = CacheFactory.CaChe();
        private string cacheKey = "learun_adms_annexes_";

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SuperadditionForm()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ImportForm()
        {
            return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = yT_OrderIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = yT_OrderIBLL.GetList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var YT_OrderData = yT_OrderIBLL.GetYT_OrderEntity( keyValue );
            var jsonData = new {
                YT_OrderData = YT_OrderData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetSqlFormData(string keyValue)
        {
            var YT_OrderData = yT_OrderIBLL.GetYT_OrderSqlEntity(keyValue);
            var jsonData = new
            {
                YT_OrderData = YT_OrderData,
            };
            return Success(jsonData);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            yT_OrderIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity)
        {
            YT_OrderEntity entity = strEntity.ToObject<YT_OrderEntity>();
            yT_OrderIBLL.SaveEntity(keyValue,entity);
            return Success("保存成功！");
        }

        /// <summary>
        /// 验证是否追加
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult VerifyOrder(string keyValue)
        {
            var data = yT_OrderIBLL.VerifyOrder(keyValue);
            if (data)
            {
                return Success("未追加");
            }
            return Success("该订单已追加");
        }

        /// <summary>VerifyOrder
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveSuperadditionForm(string keyValue, string strEntity)
        {
            YT_OrderSuperadditionEntity entity = strEntity.ToObject<YT_OrderSuperadditionEntity>();
            var data = yT_OrderIBLL.SaveSuperadditionForm(keyValue, entity);
            if (data)
            {
                return Success("保存成功！");
            }
            return Success("该订单已追加");
        }
        #endregion

        #region 首页图表
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderEchartsToAnalysis(string startTime, string endTime,string supplier)
        {
            var data = yT_OrderIBLL.GetOrderEchartsToAnalysis(startTime, endTime, supplier);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderEchartsToA(string startTime, string endTime,string supplier)
        {
            var data = yT_OrderIBLL.GetOrderEchartsToA(startTime, endTime, supplier);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult OrderEchartsToAnalysis(string startTime, string endTime, string supplier)
        {
            var data = yT_OrderIBLL.OrderEchartsToAnalysis(startTime, endTime, supplier);
            return Success(data);
        }

        /// <summary>
        /// 下单统计
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderEcharts(string startTime, string endTime)
        {
            var data = yT_OrderIBLL.GetOrderEcharts(startTime, endTime);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderEchartsToIdentity(string startTime, string endTime,int type)
        {
            var data = yT_OrderIBLL.GetOrderEchartsToIdentity(startTime, endTime,type);
            return Success(data);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult GetStatisticsInfo()
        {
            var data = yT_OrderIBLL.GetStatisticsInfo();
            return Success(data);
        }
        #endregion

        #region 导入
        /// <summary>
        /// 订单导入
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="fileId"></param>
        /// <param name="chunks"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExecuteImportExcel(string templateId, string fileId, int chunks, string suffix)
        {
            string path = SaveAnnexes(fileId, fileId, chunks, suffix);
            if (!string.IsNullOrEmpty(path))
            {
                List<SupplierOrOrderImportModel> list = GetDataFromExcel(path);
                int suess = 0;
                int err = 0;
                foreach (var item in list)
                {
                    try
                    {
                        InstructorBll.ImportOrder(item.instructorEntity, item.supplierEntity, item.orderUserEntity, item.partsTypesEntity, item.orderEntity, item.maintenanceTypesEntity);
                        suess++;
                    }
                    catch (Exception e)
                    {
                        err++;
                    }
                }
                var data = new
                {
                    Success = suess,
                    Fail = err
                };
                return Success(data);
            }
            else
            {
                return Fail("导入数据失败!");
            }
        }

        /// <summary>
        /// 保存附件（支持大文件分片传输）
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名称</param>
        /// <param name="chunks">文件总共分多少片</param>
        /// <param name="fileStream">文件二进制流</param>
        /// <returns></returns>
        public string SaveAnnexes(string fileGuid, string fileName, int chunks, string suffix)
        {
            try
            {
                UserInfo userInfo = LoginUserInfo.Get();
                //获取文件完整文件名(包含绝对路径)
                //文件存放路径格式：/Resource/ResourceFile/{userId}/{date}/{guid}.{后缀名}
                string filePath = Config.GetValue("AnnexesFile");
                string uploadDate = DateTime.Now.ToString("yyyyMMdd");
                string virtualPath = string.Format("{0}/{1}/{2}/{3}.{4}", filePath, userInfo.userId, uploadDate, fileGuid, suffix);
                //创建文件夹
                string path = Path.GetDirectoryName(virtualPath);
                Directory.CreateDirectory(path);
                AnnexesFileEntity fileAnnexesEntity = new AnnexesFileEntity();
                if (!System.IO.File.Exists(virtualPath))
                {
                    long filesize = SaveAnnexesToFile(fileGuid, virtualPath, chunks);
                    if (filesize == -1)// 表示保存失败
                    {
                        new AnnexesFileBLL().RemoveChunkAnnexes(fileGuid, chunks);
                        return "";
                    }
                }
                return virtualPath;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存附件到文件中
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="filePath">文件路径</param>
        /// <param name="chunks">总共分片数</param>
        /// <param name="buffer">文件二进制流</param>
        /// <returns>-1:表示保存失败</returns>
        public long SaveAnnexesToFile(string fileGuid, string filePath, int chunks)
        {
            try
            {
                long filesize = 0;
                //创建一个FileInfo对象
                FileInfo file = new FileInfo(filePath);
                //创建文件
                FileStream fs = file.Create();
                for (int i = 0; i < chunks; i++)
                {
                    byte[] bufferByRedis = cache.Read<byte[]>(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                    if (bufferByRedis == null)
                    {
                        return -1;
                    }
                    //写入二进制流
                    fs.Write(bufferByRedis, 0, bufferByRedis.Length);
                    filesize += bufferByRedis.Length;
                    cache.Remove(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                }
                //关闭文件流
                fs.Close();

                return filesize;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取数据集合
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private List<SupplierOrOrderImportModel> GetDataFromExcel(string FilePath)
        {
            XSSFWorkbook myWorkbook;
            XSSFSheet myWorksheet;
            try
            {
                using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    myWorkbook = new XSSFWorkbook(fs);
                    myWorksheet = myWorkbook.GetSheet("Sheet1") as XSSFSheet;
                    string[] arrHeards = { "序号", "创建人", "创建人手机", "订单编号", "下单日期", "结束日期", "UMD-SQE", "临时措施", "供应商","零件号", "零件名称", "零件批次号", "图纸版本号","挑选区域", "维修类型", "缺陷描述", "挑选/返工步骤", "零件数量", "合格数量", "不合格数量", "区域挑选消耗工时", "根线挑选消耗工时", "线边挑选消耗工时", "跟线人数" };
                    DataTable ds = ReadDataFromExcel(myWorksheet, true, arrHeards, 0);
                    List<SupplierOrOrderImportModel> listEntity = DetailListToModel(ds);
                    return listEntity;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// 读取行列
        /// </summary>
        /// <param name="mySheet"></param>
        /// <param name="isColumnName"></param>
        /// <param name="arrHeads"></param>
        /// <param name="startRow"></param>
        /// <returns></returns>
        public static DataTable ReadDataFromExcel(ISheet mySheet, bool isColumnName, string[] arrHeads, int startRow)
        {
            DataTable dt = new DataTable();
            DataColumn column;
            DataRow dataRow;
            IRow row;
            ICell cell;

            try
            {
                if (mySheet != null)
                {
                    //dt = new DataTable();
                    // 当前 Sheet 页数据的总行数  
                    int rowCount = mySheet.LastRowNum;

                    if (rowCount > 0)
                    {
                        IRow firstRow = mySheet.GetRow(0);//第一行
                        int cellCount = firstRow.LastCellNum;//列数

                        //构建datatable的列
                        if (isColumnName)
                        {
                            startRow = 1;//如果第一行是列名，则从第二行开始读取
                            for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                            {
                                cell = firstRow.GetCell(i);
                                if (cell != null)
                                {
                                    if (cell.StringCellValue != null)
                                    {
                                        column = new DataColumn(cell.StringCellValue);
                                        dt.Columns.Add(column);
                                    }
                                }
                            }
                        }
                        else
                        {
                            for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                            {
                                column = new DataColumn("column" + (i + 1));
                                dt.Columns.Add(column);
                            }
                        }

                        //填充行
                        for (int i = startRow; i <= rowCount; ++i)
                        {
                            row = mySheet.GetRow(i);
                            if (row == null) continue;

                            dataRow = dt.NewRow();
                            for (int j = row.FirstCellNum; j < cellCount; ++j)
                            {
                                cell = row.GetCell(j);
                                if (cell == null)
                                {
                                    dataRow[j] = "";
                                }
                                else
                                {
                                    switch (cell.CellType)
                                    {
                                        case CellType.Blank:
                                            dataRow[j] = "";
                                            break;
                                        case CellType.Numeric:

                                            if (HSSFDateUtil.IsCellDateFormatted(cell))
                                            {
                                                dataRow[j] = cell.DateCellValue;
                                            }
                                            else
                                            {
                                                dataRow[j] = cell.NumericCellValue.ToString();
                                            }
                                            break;
                                        case CellType.String:
                                            dataRow[j] = cell.StringCellValue;
                                            break;
                                        case CellType.Formula:
                                            dataRow[j] = cell.NumericCellValue.ToString();
                                            break;
                                    }
                                }
                            }
                            dt.Rows.Add(dataRow);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 转换对象
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<SupplierOrOrderImportModel> DetailListToModel(DataTable ds)
        {
            if (ds == null)
            {
                return null;
            }
            List<SupplierOrOrderImportModel> result = new List<SupplierOrOrderImportModel>();

            foreach (DataRow row in ds.Rows)
            {
                SupplierOrOrderImportModel model = new SupplierOrOrderImportModel();
                model = DataRowToModel(row);
                if (model != null)
                {
                    result.Add(model);
                }
            }
            return result;
        }

        /// <summary>
        /// 转换
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public SupplierOrOrderImportModel DataRowToModel(DataRow row)
        {
            SupplierOrOrderImportModel model = new SupplierOrOrderImportModel();
            YT_InstructorEntity instructorEntity = new YT_InstructorEntity();
            YT_SupplierEntity supplierEntity = new YT_SupplierEntity();
            YT_OrderUserEntity orderUserEntity = new YT_OrderUserEntity();
            YT_PartsTypesEntity partsTypesEntity = new YT_PartsTypesEntity();
            YT_OrderEntity orderEntity = new YT_OrderEntity();
            YT_MaintenanceTypesEntity maintenanceTypesEntity = new YT_MaintenanceTypesEntity();
            if (row != null)
            {
                if (row["创建人"] != null && row["创建人"].ToString() != "")
                {
                    orderEntity.Name = row["创建人"].ToString();
                }
                if (row["创建人手机"] != null && row["创建人手机"].ToString() != "")
                {
                    orderEntity.Phone = row["创建人手机"].ToString();
                }
                if (row["订单编号"] != null && row["订单编号"].ToString() != "")
                {
                    orderEntity.OrderNumber = row["订单编号"].ToString();
                }
                if (row["下单日期"] != null && row["下单日期"].ToString() != "")
                {
                    orderEntity.Date = row["下单日期"].ToString();
                }
                if (row["结束日期"] != null && row["结束日期"].ToString() != "")
                {
                    orderEntity.SuccessDate = row["结束日期"].ToString();
                }
                if (row["UMD-SQE"] != null && row["UMD-SQE"].ToString() != "")
                {
                    supplierEntity.Compellation = row["UMD-SQE"].ToString();
                }
                if (row["临时措施"] != null && row["临时措施"].ToString() != "")
                {
                    if (row["临时措施"].ToString() == "挑选")
                    {
                        orderEntity.RepairType = "0";
                    }
                    if (row["临时措施"].ToString() == "返修")
                    {
                        orderEntity.RepairType = "1";
                    }
                }
                if (row["供应商"] != null && row["供应商"].ToString() != "")
                {
                    supplierEntity.Name = row["供应商"].ToString();
                }
                if (row["零件号"] != null && row["零件号"].ToString() != "")
                {
                    orderEntity.PartsNumber = row["零件号"].ToString();
                    partsTypesEntity.Parts_Number = row["零件号"].ToString();
                }
                if (row["零件名称"] != null && row["零件名称"].ToString() != "")
                {
                    partsTypesEntity.Parts_Name = row["零件名称"].ToString();
                }
                if (row["零件批次号"] != null && row["零件批次号"].ToString() != "")
                {
                    orderEntity.Batch_Number = row["零件批次号"].ToString();
                }
                if (row["图纸版本号"] != null && row["图纸版本号"].ToString() != "")
                {
                    orderEntity.DrawingNumber = row["图纸版本号"].ToString();
                }
                if (row["零件数量"] != null && row["零件数量"].ToString() != "")
                {
                    orderEntity.Count = row["零件数量"].ToInt();
                }
                if (row["挑选区域"] != null && row["挑选区域"].ToString() != "")
                {
                    if(row["挑选区域"].ToString() == "跟线挑选")
                    {
                        orderEntity.Area =0;
                        orderEntity.Thread = 1;
                    }
                    if (row["挑选区域"].ToString() == "区域挑选")
                    {
                        orderEntity.Area = 1;
                        orderEntity.Thread = 0;
                    }
                }
                if (row["维修类型"] != null && row["维修类型"].ToString() != "")
                {
                    maintenanceTypesEntity.Name = row["维修类型"].ToString();
                }
                if (row["缺陷描述"] != null && row["缺陷描述"].ToString() != "")
                {
                    instructorEntity.IssueDescription = row["缺陷描述"].ToString();
                }
                if (row["挑选/返工步骤"] != null && row["挑选/返工步骤"].ToString() != "")
                {
                    instructorEntity.Pick_Step = row["挑选/返工步骤"].ToString();
                }
                if (row["合格数量"] != null && row["合格数量"].ToString() != "")
                {
                    orderUserEntity.Success = row["合格数量"].ToInt();
                }
                if (row["不合格数量"] != null && row["不合格数量"].ToString() != "")
                {
                    orderUserEntity.Error = row["不合格数量"].ToInt();
                }
                if (row["区域挑选消耗工时"] != null && row["区域挑选消耗工时"].ToString() != "")
                {
                    orderEntity.EstimateDate = row["区域挑选消耗工时"].ToString();
                }
                if (row["根线挑选消耗工时"] != null && row["根线挑选消耗工时"].ToString() != "")
                {
                    orderEntity.ThreadElapsedTime = row["根线挑选消耗工时"].ToString();
                }
                if (row["线边挑选消耗工时"] != null && row["线边挑选消耗工时"].ToString() != "")
                {
                    orderEntity.LineEdgeElapsedTime = row["线边挑选消耗工时"].ToString();
                }
                if (row["跟线人数"] != null && row["跟线人数"].ToString() != "")
                {
                    orderEntity.Person = row["跟线人数"].ToInt();
                }
            }
            model.supplierEntity = supplierEntity;
            model.partsTypesEntity = partsTypesEntity;
            model.orderUserEntity = orderUserEntity;
            model.orderEntity = orderEntity;
            model.maintenanceTypesEntity = maintenanceTypesEntity;
            model.instructorEntity = instructorEntity;
           
            return model;
        }
        #endregion

        #region 导出
        [HttpGet]
        public ActionResult ExportOrderList(string startTime, string endTime, string start,string end,string name,string supplier,string orderNumber,string partsNumber)
        {
            string fileurl = yT_OrderIBLL.ExportOrderList(context, startTime, endTime,start,end,name,supplier,orderNumber,partsNumber);
            var jsonData = new
            {
                fileurl = fileurl,
            };
            return Success(jsonData);
        }
        #endregion
    }
}
