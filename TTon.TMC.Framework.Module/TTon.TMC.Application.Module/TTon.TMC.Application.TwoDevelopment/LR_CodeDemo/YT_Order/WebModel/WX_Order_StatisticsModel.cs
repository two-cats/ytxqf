﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// WX 订单统计Model
    /// </summary>
    public class WX_Order_StatisticsModel
    {
        /// <summary>
        /// 订单数量
        /// </summary>
        public int order_count { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public int order_accomplish { get; set; }
        /// <summary>
        /// 进行中
        /// </summary>
        public int order_underway { get; set; }
        /// <summary>
        /// 订单列表
        /// </summary>
        public List<WX_Order> order_list { get; set; }
    }

    public class WX_Order {
        /// <summary>
        /// 订单主键
        /// </summary>
        public string order_id { get; set; }
        /// <summary>
        /// 零件名称
        /// </summary>
        public string parts_name { get; set; }
        /// <summary>
        /// 零件数量
        /// </summary>
        public int parts_count { get; set; } 
        /// <summary>
        /// 创建人
        /// </summary>
        public string create_user { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public int order_status { get; set; }

    }

}
