﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTon.TMC.Application.OA.Schedule;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class ResponseSchedule
    {
        //是否审核
        public int IsApproval { get; set; }
        //是否可继续派
        public int IsAssign { get; set; }
        //日程内容
        public ScheduleEntity Schedule { get; set; }
        //日程图片
        public List<ImageModel> Images { get; set; }
        //申请人部门
        public string Department { get; set; }
        //申请人名字
        public string Username { get; set; }
        //申请人电话
        public string Mobile { get; set; }


       
    }
}