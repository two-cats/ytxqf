﻿using System.Web.Mvc;

namespace TTon.TMC.Application.Web.Areas.WebChatModule
{
    public class WebChatModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WebChatModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WebChatModule_default",
                "WebChatModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}