﻿using TTon.TMC.Util;
using System.Collections.Generic;

namespace TTon.TMC.Application.TwoDevelopment.TaskManager
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-06 15:11
    /// 描 述：任务单
    /// </summary>
    public interface TaskBillInfoIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Task_BaseBillInfoEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取Task_BaseBillInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Task_BaseBillInfoEntity GetTask_BaseBillInfoEntity(string keyValue);
        /// <summary>
        /// 获取Task_FileInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Task_FileInfoEntity GetTask_FileInfoEntity(string keyValue);
        /// <summary>
        /// 获取Task_MaterialInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Task_MaterialInfoEntity GetTask_MaterialInfoEntity(string keyValue);
        /// <summary>
        /// 获取Task_UserInfo表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Task_UserInfoEntity GetTask_UserInfoEntity(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Task_BaseBillInfoEntity entity,Task_FileInfoEntity task_FileInfoEntity,Task_MaterialInfoEntity task_MaterialInfoEntity,Task_UserInfoEntity task_UserInfoEntity);
        #endregion

    }
}
