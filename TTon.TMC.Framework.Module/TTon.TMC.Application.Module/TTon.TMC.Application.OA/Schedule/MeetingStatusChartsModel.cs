﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.OA.Schedule
{
    public class MeetingStatusChartsModel
    {
        /// <summary>
        /// 已同意
        /// </summary>
        public int agreeCount { get; set; }
        /// <summary>
        /// 已拒绝
        /// </summary>
        public int repulseCount { get; set; }
        /// <summary>
        /// 已取消
        /// </summary>
        public int cancelCount { get; set; }
        /// <summary>
        /// 已完成
        /// </summary>
        public int finishCount { get; set; }
    }
}
