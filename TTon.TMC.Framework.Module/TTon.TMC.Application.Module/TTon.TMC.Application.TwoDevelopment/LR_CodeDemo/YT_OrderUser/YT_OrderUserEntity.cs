﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo

{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-22 16:06
    /// 描 述：订单人员信息
    /// </summary>
    public class YT_OrderUserEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// Order_Id
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_ID")]
        public string Order_Id { get; set; }
        /// <summary>
        /// Order_UserId
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_USERID")]
        public string Order_UserId { get; set; }
        /// <summary>
        /// PartsType
        /// </summary>
        /// <returns></returns>
        [Column("PARTSTYPE")]
        public string PartsType { get; set; }
        /// <summary>
        /// IsApprover
        /// </summary>
        /// <returns></returns>
        [Column("ISAPPROVER")]
        public int? IsApprover { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        /// <returns></returns>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// CreateUserId
        /// </summary>
        /// <returns></returns>
        [Column("CREATEUSERID")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// ModifyDate
        /// </summary>
        /// <returns></returns>
        [Column("MODIFYDATE")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// ModifyUserId
        /// </summary>
        /// <returns></returns>
        [Column("MODIFYUSERID")]
        public string ModifyUserId { get; set; }
        /// <summary>
        /// IsDelete
        /// </summary>
        /// <returns></returns>
        [Column("ISDELETE")]
        public int? IsDelete { get; set; }

        /// <summary>
        /// 一共
        /// </summary>
        [Column("ALL")]
        public int? All { get; set; }

        /// <summary>
        /// 完成
        /// </summary>
        [Column("SUCCESS")]
        public int? Success { get; set; }
        /// <summary>
        /// 失败
        /// </summary>
        [Column("ERROR")]
        public int? Error { get; set; }

        /// <summary>
        /// 未完成
        /// </summary>
        [Column("UNFINISHED")]
        public int? Unfinished { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        [Column("ACCOMPLISH")]
        public string Accomplish { get; set; }
        /// <summary>
        /// 类别（0根线，1区域，2线边）
        /// </summary>
        [Column("CATEGORY")]
        public int? Category { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
    }
}

