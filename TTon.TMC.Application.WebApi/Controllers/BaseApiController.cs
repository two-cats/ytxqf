﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TTon.TMC.Application.WebApi.Helper;
using TTon.TMC.Application.WebApi.Modules;

namespace TTon.TMC.Application.WebApi.Controllers
{
    [Authorize]
    public class BaseApiController : ApiController
    {
        protected HttpResponseMessage Render(HttpStatusCode code, string message, object data)
        {
            HttpResponseMessage msg = Request.CreateResponse();
            ResponseModel model = new ResponseModel()
            {
                Message = message,
                Code = ((int)code),
                Data = data
            };
            msg.Content = new StringContent(JsonConvert.SerializeObject(model, JsonHelper.settings));
            return msg;
        }

        protected HttpResponseMessage Render(HttpStatusCode code, string message)
        {
            return Render(code, message, new object());
        }

        protected HttpResponseMessage Render(bool status, string message)
        {
            return Render(String.Empty, message);
        }

        protected HttpResponseMessage Render(string message, object data)
        {
            return Render(HttpStatusCode.OK, message, data);
        }
    }
}