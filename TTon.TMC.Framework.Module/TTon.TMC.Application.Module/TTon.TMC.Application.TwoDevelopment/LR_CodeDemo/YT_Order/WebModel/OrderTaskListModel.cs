﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class OrderTaskListModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Date { get; set; }

        public string OrderNumber { get; set; }

        public string PartsNumber { get; set; }

        public string SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string PartsTypeId { get; set; }

        public string PartsTypeName { get; set; }

        public string PartsCount { get; set; }

        public string MaintainType { get; set; }

        public string InstructorId { get; set; }

        public string InstructorName { get; set; }

        public int Thread { get; set; }

        public int Area { get; set; }

        public int LineEdge { get; set; }

        public int person { get; set; }

        public string BatchNumber { get; set; }

        public string TaskId { get; set; }

        public string Remark { get; set; }

        public int? Order_Status { get; set; }

        public string EstimateDate { get; set; }

        public string ThreadElapsedTime { get; set; }

        public string LineEdgeElapsedTime { get; set; }

        public string ElapsedTime { get; set; }

        public string CreateUser { get; set; }

        public string CreateUserPhone { get; set; }

        public string ModifyUser { get; set; }

        public string ModifyUserPhone { get; set; }

        public string CreateDate { get; set; }

        public string StartDate { get; set; }

        public string SuccessDate { get; set; }

        public string SuccessRemark { get; set; }

        public int All { get; set; }

        public int Success { get; set; }

        public int Error { get; set; }

        public string Unfinished { get; set; }

        public string Accomplish { get; set; }

        public int IsApprover { get; set; }

        public string DrawingNumber { get; set; }

        public string Pesponsible { get; set; }

        public string RepairType { get; set; }

        public string Notifier { get; set; }

        public int WxPushStatus { get; set; }

        public int WxPushCount { get; set; }

        public int SmsPushStatus { get; set; }

        public int SmsPushCount { get; set; }

        public string UserName { get; set; }

        public int SupplierStatus { get; set; }
        /// <summary>
        /// 0 一次完成   1 更新完成
        /// </summary>
        public int Category { get; set; }

        public string QualifiedCount { get; set; }

        public string UnqualifiedCount { get; set; }

        public string SRemark { get; set; }

        public string SCreateDate { get; set; }
        /// <summary>
        /// 分配总数量
        /// </summary>
        public string AllotCount { get; set; }

        public List<ImageUrl> Pic { get; set; }

        public List<ImageUrl> TaskPic { get; set; }

        public List<AbnormalInfoModel> abnormalList { get; set; }
    }

    public class ImageUrl
    {
        public string Img_Url { get; set; }
    }
}
