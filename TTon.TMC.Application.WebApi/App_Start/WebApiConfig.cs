﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TTon.TMC.Application.WebApi.Helper;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services,用 CORS 解决 Ajax 跨域问题
            EnableCrossSiteRequests(config);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new WebApiValidateModelAttribute());

            // Remove the XML formatter
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new LowercaseContractResolver();
        }

        /// <summary>
        /// Author:shown
        /// 设置接口的 Header 等信息, “*” 所有的域都可访问api的资源;如果只允许一个域可以访问，则可以设置单独域名。
        /// </summary>
        /// <param name="config"></param>
        private static void EnableCrossSiteRequests(HttpConfiguration config)
        {
            var allowedOrigin = Config.GetValue("cors:allowedOrigin");
            var allowedHeaders = Config.GetValue("cors:allowedHeaders");
            var allowedMethods = Config.GetValue("cors:allowedMethods");

            var geduCors = new EnableCorsAttribute(allowedOrigin, allowedHeaders, allowedMethods)
            {
                SupportsCredentials = true
            };

            config.EnableCors(geduCors);
        }


        //还可以直接在 Action 上设置属性：
        //[EnableCors("http://example.com","*","*")]
    }
}