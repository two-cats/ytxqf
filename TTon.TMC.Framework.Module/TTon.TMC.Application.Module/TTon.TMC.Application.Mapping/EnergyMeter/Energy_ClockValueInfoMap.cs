﻿using TTon.TMC.Application.TwoDevelopment.EnergyMeter;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-04-25 19:58
    /// 描 述：抄表
    /// </summary>
    public class Energy_ClockValueInfoMap : EntityTypeConfiguration<Energy_ClockValueInfoEntity>
    {
        public Energy_ClockValueInfoMap()
        {
            #region 表、主键
            //表
            this.ToTable("ENERGY_CLOCKVALUEINFO");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

