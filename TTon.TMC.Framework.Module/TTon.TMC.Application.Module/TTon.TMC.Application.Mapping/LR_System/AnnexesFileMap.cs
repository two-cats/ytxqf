﻿using TTon.TMC.Application.Base.SystemModule;
using System.Data.Entity.ModelConfiguration;

namespace TTon.TMC.Application.Mapping
{
    /// <summary>
    /// TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创建人：TTON
    /// 日 期：2017.03.08
    /// 描 述：附件管理
    /// </summary>
    public class AnnexesFileMap : EntityTypeConfiguration<AnnexesFileEntity>
    {
        public AnnexesFileMap()
        {
            #region 表、主键
            //表
            this.ToTable("BASE_ANNEXESFILE");
            //主键
            this.HasKey(t => t.F_Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
