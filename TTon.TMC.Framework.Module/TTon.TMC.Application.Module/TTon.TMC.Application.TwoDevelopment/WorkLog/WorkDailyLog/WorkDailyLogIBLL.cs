﻿using TTon.TMC.Util;
using System.Collections.Generic;

namespace TTon.TMC.Application.TwoDevelopment.WorkLog
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:25
    /// 描 述：工作日志
    /// </summary>
    public interface WorkDailyLogIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<WorkLog_DetailEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取WorkLog_Detail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        WorkLog_DetailEntity GetWorkLog_DetailEntity(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, WorkLog_DetailEntity entity);
        #endregion

    }
}
