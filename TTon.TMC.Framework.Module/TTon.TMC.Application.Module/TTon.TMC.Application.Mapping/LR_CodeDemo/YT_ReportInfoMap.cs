﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ReportInfo;

namespace TTon.TMC.Application.Mapping.LR_CodeDemo
{
    public class YT_ReportInfoMap : EntityTypeConfiguration<YT_ReportInfoEntity>
    {
        public YT_ReportInfoMap()
        {
            #region 表、主键
            //表
            this.ToTable("YT_REPORTINFO");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
