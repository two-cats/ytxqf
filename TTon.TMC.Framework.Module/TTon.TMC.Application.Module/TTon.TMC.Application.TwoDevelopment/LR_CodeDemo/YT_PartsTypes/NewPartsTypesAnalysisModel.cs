﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes
{
    public class NewPartsTypesAnalysisModel
    {
        public string time { get; set; }

        public string name { get; set; }

        public string count { get; set; }

        public string percent { get; set; }
    }
}
