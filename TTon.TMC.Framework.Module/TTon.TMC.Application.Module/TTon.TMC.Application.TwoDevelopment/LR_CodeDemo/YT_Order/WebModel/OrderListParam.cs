﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Util;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class OrderListParam
    {
        public string userId { get; set; }

        public string type { get; set; }

        public string querystr { get; set; }

        public Pagination pagination { get; set; }
    }
}
