﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 检查菜单目录
    /// </summary>
    public class CheckMenuModel
    {
        /// <summary>
        /// 我的任务
        /// </summary>
        public int task { get; set; }
        /// <summary>
        /// 我的订单
        /// </summary>
        public int order { get; set; }
        /// <summary>
        /// 库存分配
        /// </summary>
        public int allot { get; set; }
        /// <summary>
        /// 异常记录
        /// </summary>
        public int abnormal { get; set; }
        /// <summary>
        /// 交班日志
        /// </summary>
        public int journal { get; set; }
    }
}
