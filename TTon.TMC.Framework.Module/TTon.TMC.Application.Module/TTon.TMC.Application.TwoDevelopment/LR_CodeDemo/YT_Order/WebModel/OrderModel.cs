﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class OrderModel
    {
        #region 实体成员

        public string Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Date { get; set; }

        public string OrderNumber { get; set; }

        public string PartsNumber { get; set; }

        public string Supplier_Id { get; set; }
 
        public string PartsType { get; set; }

        public string MaintainType { get; set; }

        public string Instructor { get; set; }
 
        public string Remark { get; set; }

        public int? Order_Status { get; set; }

        public string ThreadElapsedTime { get; set; }

        public string LineEdgeElapsedTime { get; set; }

        public string EstimateDate { get; set; }

        public string ElapsedTime { get; set; }

        public string CreateUserId { get; set; }
 
        public DateTime? CreateDate { get; set; }

        public string ModifyUserId { get; set; }

        public DateTime? ModifyDate { get; set; }
   
        public int? IsDelete { get; set; }

        public string Designated { get; set; }

        public string SuccessDate { get; set; }

        public int Count { get; set; }

        public string Success_Remark { get; set; }

        public string QualifiedCount { get; set; }

        public string Batch_Number { get; set; }

        public string UnqualifiedCount { get; set; }

        public string SRemark { get; set; }

        public string SCreateDate { get; set; }

        public List<ImageUrl> pic { get; set; }

        public List<ImageUrl> task_pic { get; set; }
        #endregion
    }
}
