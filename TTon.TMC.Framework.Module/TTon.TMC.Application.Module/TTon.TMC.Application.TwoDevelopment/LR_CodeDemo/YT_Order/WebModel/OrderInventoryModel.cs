﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 库管提交零件数量
    /// </summary>
    public class OrderInventoryModel
    {
        /// <summary>
        /// 用户主键
        /// </summary>
        public string user_id { get; set; }
        /// <summary>
        /// 订单主键
        /// </summary>
       public string order_id { get; set; }
        /// <summary>
        /// 零件数量
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        public string batch_number { get; set; }
    }
}
