﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_PartsTypes;
using TTon.TMC.Application.Base.OrganizationModule;
using System.Linq;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Supplier;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 10:22
    /// 描 述：配置零件
    /// </summary>
    public class YT_PartsTypesService : RepositoryFactory
    {
        #region 获取数据
        public List<SupplierListToTree> GetPartsTypesTree(string keyValue)
        {
            List<SupplierListToTree> list = new List<SupplierListToTree>();
            var strSql = new StringBuilder();
            strSql.Append(@"SELECT
	                YT_PartsTypes.Id AS id,
	                YT_PartsTypes.Parts_Name AS name
                FROM
	                YT_PartsTypes
                LEFT JOIN YT_SupplierOrParts ON YT_SupplierOrParts.Parts_Id = YT_PartsTypes.Id
                WHERE
	                YT_SupplierOrParts.Supplier_Id = '" + keyValue + @"'
                ORDER BY
	                YT_PartsTypes.CreateDate DESC ");
            DataTable dt = new RepositoryFactory().BaseRepository().FindTable(strSql.ToString());
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SupplierListToTree model = new SupplierListToTree();
                    model.number = i;
                    model.Id = dt.Rows[i]["id"].ToString();
                    model.Name = dt.Rows[i]["name"].ToString();
                    list.Add(model);
                }
            }
            return list;
        }

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_PartsTypesEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Parts_Name,
                t.Remark,
                t.Code,
                t.CreateUserId,
                t.CreateDate,
                t.ModifyDate,t.Parts_Number
                ");
                strSql.Append("  FROM YT_PartsTypes t ");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Parts_Name"].IsEmpty())
                {
                    dp.Add("Parts_Name", "%" + queryParam["Parts_Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Parts_Name Like @Parts_Name ");
                }
                if (!queryParam["Parts_Number"].IsEmpty())
                {
                    dp.Add("Parts_Number", "%" + queryParam["Parts_Number"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Parts_Number Like @Parts_Number");
                }



                return this.BaseRepository().FindList<YT_PartsTypesEntity>(strSql.ToString(), dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_PartsTypesEntity> GetList()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Parts_Name,
                t.Remark,
                t.CreateUserId,
                t.CreateDate,
                t.ModifyDate
                ");
                strSql.Append("  FROM YT_PartsTypes t ");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0  Order by t.CreateDate desc ");
                return this.BaseRepository().FindList<YT_PartsTypesEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取自动补充零零件内容
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AutocmpModel> GetAParts()
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append(@"select Parts_Name as hex,Parts_Number as label from YT_PartsTypes WHERE IsDelete = 0 ORDER BY Parts_Name" );
               
                return this.BaseRepository().FindList<AutocmpModel>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }






        /// <summary>
        /// 根据供应商获取零件
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_PartsTypesEntity> GetListTree(string parentId)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                t.Parts_Name,
                t.Remark,
                t.CreateUserId,
                t.CreateDate,
                t.ModifyDate,
                t.Parts_Number
                ");
                strSql.Append("  FROM YT_PartsTypes t ");
                strSql.Append("  WHERE 1=1 And t.IsDelete=0 And t.Id in (select YT_SupplierOrParts.Parts_Id from YT_SupplierOrParts where YT_SupplierOrParts.IsDelete=0 and YT_SupplierOrParts.Supplier_Id='" + parentId + "' And YT_SupplierOrParts.IsDelete=0 )  Order by t.CreateDate desc ");
                return this.BaseRepository().FindList<YT_PartsTypesEntity>(strSql.ToString());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取YT_PartsTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_PartsTypesEntity GetYT_PartsTypesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_PartsTypesEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取YT_PartsTypes表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_PartsTypesEntity PartsEntityByid(string keyValue)
        {

            YT_PartsTypesEntity pe = new YT_PartsTypesEntity();
            try
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT * FROM YT_PartsTypes WHERE Parts_Number = '" + keyValue + "' AND IsDelete ='0' ORDER BY CreateDate DESC");
                List<YT_PartsTypesEntity> list = new RepositoryFactory().BaseRepository().FindList<YT_PartsTypesEntity>(sql.ToString()).AsList();

                try
                {
                    pe = list[0];
                }
                catch (Exception)
                {
                }
                return pe;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                YT_PartsTypesEntity Entity = this.BaseRepository().FindEntity<YT_PartsTypesEntity>(keyValue);
                Entity.IsDelete = 1;
                this.BaseRepository().Update<YT_PartsTypesEntity>(Entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_PartsTypesEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion


        #region 首页图表
        /// <summary>
        /// 本月（时段）各零件挑选数量折线图
        /// </summary>
        /// <returns></returns>
        public List<PartsTypesEcharsModel> GetPartsTypesEcharsModel(string startTime, string endTime, string supplier)
        {
            List<PartsTypesEcharsModel> partsTypesEcharsModels = new List<PartsTypesEcharsModel>();
            DateTime now = DateTime.Now;
            string startDate;
            string endDate;
            if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                startDate = now.AddDays(-6).ToString("yyyy-MM-dd 00:00:00");
                endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59).ToString();
            }
            else if (string.IsNullOrEmpty(startTime))
            {
                endDate = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd 23:59:59");
                startDate = Convert.ToDateTime(endDate).AddDays(-6).ToString("yyyy-MM-dd 00:00:00");
            }
            else if (string.IsNullOrEmpty(endTime))
            {
                startDate = Convert.ToDateTime(startTime).ToString("yyyy-MM-dd 00:00:00");
                endDate = DateTime.Now.ToString("yyyy-MM-dd 23:59:59");
            }
            else
            {
                startDate = Convert.ToDateTime(startTime).ToString("yyyy-MM-dd 00:00:00");
                endDate = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd 23:59:59");
            }

            //相隔天数
            int iDays = (Convert.ToDateTime(endDate) - Convert.ToDateTime(startDate)).Days + 1;
            //查询一个供应商

            YT_SupplierEntity yT_SupplierEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(supplier);

            StringBuilder partstype_sql = new StringBuilder();
            partstype_sql.Append(@"SELECT
                        YT_PartsTypes.Id,
	                    YT_PartsTypes.Parts_Name,
	                    YT_PartsTypes.Parts_Number
                    FROM
	                    YT_Order
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                    WHERE
	                    YT_Order.Supplier_Id = '" + supplier + @"'
                    AND YT_Order.IsDelete = 0
                    AND YT_Order.Order_Status = 5
                    AND YT_Order.SuccessDate BETWEEN '" + startDate + @"'
                    AND '" + endDate + @"'
                    GROUP BY
                        YT_PartsTypes.Id,
	                    YT_PartsTypes.Parts_Name,
	                    YT_PartsTypes.Parts_Number");
            List<YT_PartsTypesEntity> YT_PartsTypesEntitys = new RepositoryFactory().BaseRepository().FindList<YT_PartsTypesEntity>(partstype_sql.ToString()).AsList();
            if (YT_PartsTypesEntitys.Count == 0)
            {
                return new List<PartsTypesEcharsModel>();
            }



            foreach (var item in YT_PartsTypesEntitys)
            {
                PartsTypesEcharsModel model = new PartsTypesEcharsModel();
                //model.name = item.Parts_Name + "-" + item.Parts_Number;
                model.name = item.Parts_Number;
                List<int> value = new List<int>();
                List<string> data = new List<string>();

                for (int i = 0; i < iDays; i++)
                {
                    string start = Convert.ToDateTime(startDate).AddDays(i).ToString("yyyy-MM-dd 00:00:00");
                    string end = Convert.ToDateTime(startDate).AddDays(i).ToString("yyyy-MM-dd 23:59:59");

                    data.Add(Convert.ToDateTime(start).Day.ToString());
                    StringBuilder strsql = new StringBuilder();
                    strsql.Append(@"SELECT
	                        SUM (
		                        YT_OrderUser.Success + YT_OrderUser.Error
	                        ) AS [VALUE]
                        FROM
	                        YT_Order
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                        LEFT JOIN YT_OrderUser ON YT_OrderUser.Order_Id = YT_Order.Id
                        WHERE
	                        YT_Order.IsDelete = 0
                        AND YT_Order.Order_Status = 5
                        AND YT_Order.Supplier_Id = '" + yT_SupplierEntity.Id + @"'
                        AND YT_Order.PartsNumber = '" + item.Parts_Number + @"'
                        AND YT_PartsTypes.Parts_Name = '" + item.Parts_Name + @"'
                        AND YT_Order.SuccessDate BETWEEN '" + start + @"'
                        AND '" + end + "'");
                    DataTable list = new RepositoryFactory().BaseRepository().FindTable(strsql.ToString());

                    value.Add(list.Rows.Count > 0 ? list.Rows[0]["value"].ToInt() : 0);
                }
                model.date = data;
                model.value = value;
                partsTypesEcharsModels.Add(model);
            }


            return partsTypesEcharsModels;
        }

        public List<NewPartsTypesAnalysisModel> GetNewPartsTypesToAnalysis(string startTime, string endTime, string supplier)
        {
            List<NewPartsTypesAnalysisModel> list = new List<NewPartsTypesAnalysisModel>();
            DateTime startDate = Convert.ToDateTime(startTime);
            DateTime endDate = Convert.ToDateTime(endTime);
            int Month = (startDate.Year - endDate.Year) * 12 + (startDate.Month - endDate.Month);

            if (Month == 0)
            {
                NewPartsTypesAnalysisModel model = new NewPartsTypesAnalysisModel();
                string startR = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endR = endDate.ToString("yyyy-MM-dd HH:mm:ss");
                StringBuilder sql = new StringBuilder();
                sql.Append(@"SELECT DISTINCT
	                                PartsType
                                FROM
	                                YT_Order
                                WHERE
	                                SuccessDate BETWEEN '" + startR + @"'
                                AND '" + endR + @"'
                                AND IsDelete = 0
                                AND Supplier_Id = '" + supplier + "'");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(sql.ToString());
                if (dt.Rows.Count > 0)
                {
                    model.time = startDate.ToString("yyyy-MM");
                    model.name = dt.Rows.Count.ToString();

                    StringBuilder countSql = new StringBuilder();
                    countSql.Append(@"SELECT
	                            SUM (
		                            ISNULL(
			                            YT_OrderUser.Success + YT_OrderUser.Error,
			                            0
		                            )
	                            ) AS coun,
	                        SUM (
		                        ISNULL(YT_OrderUser.Success, 0)
	                        ) AS sucount
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + startR + @"'
                            AND '" + endR + @"'
                            AND t.IsDelete = 0
                            AND t.Supplier_Id = '" + supplier + "'");
                    DataTable countdt = new RepositoryFactory().BaseRepository().FindTable(countSql.ToString());
                    if (countdt.Rows.Count > 0)
                    {
                        model.count = countdt.Rows[0]["coun"].ToString();
                        int a = countdt.Rows[0]["sucount"].ToInt();
                        int b = countdt.Rows[0]["coun"].ToInt();
                        double coun = (double)a / b;
                        model.percent = coun.ToString("0.00%");
                    }
                    list.Add(model);
                }
            }
            else
            {
                NewPartsTypesAnalysisModel modelF = new NewPartsTypesAnalysisModel();
                string endF = endDate.ToString("yyyy-MM-01 00:00:00");
                string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");
                StringBuilder sqlF = new StringBuilder();
                sqlF.Append(@"SELECT DISTINCT
	                                PartsType
                                FROM
	                                YT_Order
                                WHERE
	                                SuccessDate BETWEEN '" + endF + @"'
                                AND '" + endD + @"'
                                AND IsDelete = 0
                                AND Supplier_Id = '" + supplier + "'");
                DataTable dtF = new RepositoryFactory().BaseRepository().FindTable(sqlF.ToString());
                if (dtF.Rows.Count > 0)
                {
                    modelF.time = endDate.ToString("yyyy-MM");
                    modelF.name = dtF.Rows.Count.ToString();

                    StringBuilder countSql = new StringBuilder();
                    countSql.Append(@"SELECT
	                            SUM (
		                            ISNULL(
			                            YT_OrderUser.Success + YT_OrderUser.Error,
			                            0
		                            )
	                            ) AS coun,
	                        SUM (
		                        ISNULL(YT_OrderUser.Success, 0)
	                        ) AS sucount
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + endF + @"'
                            AND '" + endD + @"'
                            AND t.IsDelete = 0
                            AND t.Supplier_Id = '" + supplier + "'");
                    DataTable countdt = new RepositoryFactory().BaseRepository().FindTable(countSql.ToString());
                    if (countdt.Rows.Count > 0)
                    {
                        modelF.count = countdt.Rows[0]["coun"].ToString();
                        int a = countdt.Rows[0]["sucount"].ToInt();
                        int b = countdt.Rows[0]["coun"].ToInt();
                        double coun = (double)a / b;
                        modelF.percent = coun.ToString("0.00%");
                    }
                    list.Add(modelF);
                }

                for (int i = -1; i > Month; i--)
                {
                    string start = endDate.AddMonths(i).ToString("yyyy-MM-01 00:00:00");
                    string end = endDate.AddMonths(i).AddDays(1 - endDate.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");

                    NewPartsTypesAnalysisModel modelS = new NewPartsTypesAnalysisModel();
                    StringBuilder sqlS = new StringBuilder();
                    sqlS.Append(@"SELECT DISTINCT
	                                PartsType
                                FROM
	                                YT_Order
                                WHERE
	                                SuccessDate BETWEEN '" + start + @"'
                                AND '" + end + @"'
                                AND IsDelete = 0
                                AND Supplier_Id = '" + supplier + "'");
                    DataTable dtS = new RepositoryFactory().BaseRepository().FindTable(sqlS.ToString());
                    if (dtS.Rows.Count > 0)
                    {
                        modelS.time = Convert.ToDateTime(start).ToString("yyyy-MM");
                        modelS.name = dtS.Rows.Count.ToString();

                        StringBuilder countSql = new StringBuilder();
                        countSql.Append(@"SELECT
	                            SUM (
		                            ISNULL(
			                            YT_OrderUser.Success + YT_OrderUser.Error,
			                            0
		                            )
	                            ) AS coun,
	                        SUM (
		                        ISNULL(YT_OrderUser.Success, 0)
	                        ) AS sucount
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + start + @"'
                            AND '" + end + @"'
                            AND t.IsDelete = 0
                            AND t.Supplier_Id = '" + supplier + "'");
                        DataTable countdt = new RepositoryFactory().BaseRepository().FindTable(countSql.ToString());
                        if (countdt.Rows.Count > 0)
                        {
                            modelS.count = countdt.Rows[0]["coun"].ToString();
                            int a = countdt.Rows[0]["sucount"].ToInt();
                            int b = countdt.Rows[0]["coun"].ToInt();
                            double coun = (double)a / b;
                            modelS.percent = coun.ToString("0.00%");
                        }
                        list.Add(modelS);
                    }
                }

                NewPartsTypesAnalysisModel modelH = new NewPartsTypesAnalysisModel();
                string startH = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endH = startDate.AddDays(1 - startDate.Day).Date.AddMonths(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
                StringBuilder sqlH = new StringBuilder();
                sqlH.Append(@"SELECT DISTINCT
	                                PartsType
                                FROM
	                                YT_Order
                                WHERE
	                                SuccessDate BETWEEN '" + startH + @"'
                                AND '" + endH + @"'
                                AND IsDelete = 0
                                AND Supplier_Id = '" + supplier + "'");
                DataTable dtH = new RepositoryFactory().BaseRepository().FindTable(sqlH.ToString());
                if (dtH.Rows.Count > 0)
                {
                    modelH.time = Convert.ToDateTime(startH).ToString("yyyy-MM");
                    modelH.name = dtH.Rows.Count.ToString();

                    StringBuilder countSql = new StringBuilder();
                    countSql.Append(@"SELECT
	                            SUM (
		                            ISNULL(
			                            YT_OrderUser.Success + YT_OrderUser.Error,
			                            0
		                            )
	                            ) AS coun,
	                        SUM (
		                        ISNULL(YT_OrderUser.Success, 0)
	                        ) AS sucount
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + startH + @"'
                            AND '" + endH + @"'
                            AND t.IsDelete = 0
                            AND t.Supplier_Id = '" + supplier + "'");
                    DataTable countdt = new RepositoryFactory().BaseRepository().FindTable(countSql.ToString());
                    if (countdt.Rows.Count > 0)
                    {
                        modelH.count = countdt.Rows[0]["coun"].ToString();
                        int a = countdt.Rows[0]["sucount"].ToInt();
                        int b = countdt.Rows[0]["coun"].ToInt();
                        double coun = (double)a / b;
                        modelH.percent = coun.ToString("0.00%");
                    }
                    list.Add(modelH);
                }
            }     
           return list;
        }

        public List<PartsTypesAnalysisModel> GetPartsTypesEcharsToAnalysis(string startTime, string endTime, string supplier)
        {
            List<PartsTypesAnalysisModel> partsTypesEcharsModels = new List<PartsTypesAnalysisModel>();

            StringBuilder partstype_sql = new StringBuilder();
            partstype_sql.Append(@"SELECT
	                    YT_PartsTypes.Parts_Name,
	                    YT_PartsTypes.Parts_Number
                    FROM
	                    YT_Order
                    LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                    WHERE
	                    YT_Order.Supplier_Id = '" + supplier + @"'
                    AND YT_Order.IsDelete = 0
                    AND YT_Order.SuccessDate BETWEEN '" + startTime + @"'
                    AND '" + endTime + @"'
                    GROUP BY
	                    YT_PartsTypes.Parts_Name,
	                    YT_PartsTypes.Parts_Number");
            List<YT_PartsTypesEntity> YT_PartsTypesEntitys = new RepositoryFactory().BaseRepository().FindList<YT_PartsTypesEntity>(partstype_sql.ToString()).AsList();
            if (YT_PartsTypesEntitys.Count == 0)
            {
                return new List<PartsTypesAnalysisModel>();
            }

            //DateTime startDate = Convert.ToDateTime(startTime);
            //DateTime endDate = Convert.ToDateTime(endTime);

            foreach (var item in YT_PartsTypesEntitys)
            {
                PartsTypesAnalysisModel model = new PartsTypesAnalysisModel();
                model.name = item.Parts_Name;
                model.number = item.Parts_Number;
                StringBuilder strsql = new StringBuilder();
                strsql.Append(@"SELECT
	                        SUM(ISNULL(YT_OrderUser.Success,0)) AS [success],
	                        SUM(ISNULL(YT_OrderUser.Error,0)) AS [error]
                        FROM
	                        YT_OrderUser
                        LEFT JOIN YT_Order ON YT_Order.Id = YT_OrderUser.Order_Id
                        LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                        WHERE
	                        YT_Order.IsDelete = 0
                        AND YT_Order.Supplier_Id = '" + supplier + @"'
                        AND YT_Order.PartsNumber = '" + item.Parts_Number + @"'
                        AND YT_PartsTypes.Parts_Name = '" + item.Parts_Name + @"'
                        AND YT_Order.SuccessDate BETWEEN '" + startTime + @"'
                        AND '" + endTime + "'");
                DataTable list = new RepositoryFactory().BaseRepository().FindTable(strsql.ToString());
                if (list.Rows.Count > 0)
                {
                    var a = list.Rows[0]["success"].ToInt();
                    var b = list.Rows[0]["error"].ToInt();
                    model.success = a;
                    model.error = b;
                    partsTypesEcharsModels.Add(model);
                }
            }
            return partsTypesEcharsModels;
        }


        /// <summary>
        /// 员工零件完成数量统计
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<UserFinishEchartsModel> GetUserFinishEcharts(string startTime, string endTime, string supplier)
        {
            try
            {
                List<UserFinishEchartsModel> list = new List<UserFinishEchartsModel>();

                #region 时间筛选
                DateTime now = DateTime.Now;

                DateTime startDate = new DateTime();

                DateTime endDate = new DateTime();

                if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    startDate = now.AddMonths(-1);
                    endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                }
                else if (string.IsNullOrEmpty(startTime))
                {
                    endDate = Convert.ToDateTime(endTime);
                    startDate = endDate.AddDays(-30);
                }
                else if (string.IsNullOrEmpty(endTime))
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = DateTime.Now;
                }
                else
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = Convert.ToDateTime(endTime);
                }
                #endregion

                string startD = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");

                StringBuilder userSql = new StringBuilder();
                userSql.Append(@"SELECT DISTINCT
	                            YT_OrderUser.Order_UserId AS userid
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + startD + @"'
                            AND '" + endD + @"'
                            AND t.IsDelete = 0
                            AND t.Order_Status != 3
                            AND t.Supplier_Id = '" + supplier + "'");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(userSql.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string id = dt.Rows[i]["userid"].ToString();
                        UserFinishEchartsModel model = new UserFinishEchartsModel();
                        UserEntity entity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(t => t.F_UserId == id);
                        model.name = entity.F_RealName;

                        StringBuilder countSql = new StringBuilder();
                        countSql.Append(@"SELECT
	                            SUM (
		                            ISNULL(
			                            YT_OrderUser.Success + YT_OrderUser.Error,
			                            0
		                            )
	                            ) AS coun
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + startD + @"'
                            AND '" + endD + @"'
                            AND t.IsDelete = 0
                            AND t.Order_Status != 3
                            AND t.Supplier_Id = '" + supplier + @"'
                            AND YT_OrderUser.Order_UserId = '" + id + "'");
                        DataTable countdt = new RepositoryFactory().BaseRepository().FindTable(countSql.ToString());
                        model.count = countdt.Rows[0]["coun"].ToInt();
                        list.Add(model);
                    }
                }
                else
                {
                    list = new List<UserFinishEchartsModel>();
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserFinishEchartsModel> GetUserFinishEchartsToSu(string startTime, string endTime, string supplier)
        {
            try
            {
                List<UserFinishEchartsModel> list = new List<UserFinishEchartsModel>();

                #region 时间筛选
                DateTime now = DateTime.Now;

                DateTime startDate = new DateTime();

                DateTime endDate = new DateTime();

                if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    startDate = now.AddMonths(-1);
                    endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                }
                else if (string.IsNullOrEmpty(startTime))
                {
                    endDate = Convert.ToDateTime(endTime);
                    startDate = endDate.AddDays(-30);
                }
                else if (string.IsNullOrEmpty(endTime))
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = DateTime.Now;
                }
                else
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = Convert.ToDateTime(endTime);
                }
                #endregion

                YT_SupplierEntity supplierEntity = this.BaseRepository().FindEntity<YT_SupplierEntity>(supplier);
                if (supplierEntity != null)
                {
                    if (supplierEntity.Compellation != null)
                    {
                        List<string> user = new List<string>();
                        string[] username = supplierEntity.Compellation.Split(',');
                        foreach (var name in username)
                        {
                            user.Add(name);
                        }

                        if (user.Count > 0)
                        {
                            foreach (var item in user)
                            {
                                UserFinishEchartsModel model = new UserFinishEchartsModel();
                                UserEntity userEntity = this.BaseRepository().FindEntity<UserEntity>(t => t.F_RealName == item);
                                if (userEntity != null)
                                {
                                    model.name = userEntity.F_RealName;

                                    StringBuilder Qsql = new StringBuilder();
                                    Qsql.Append(@"SELECT
                                                 COUNT (Id) AS cou
                                                FROM
                                                 YT_Order
                                                WHERE
                                                 IsDelete = 0
                                                AND Order_Status != 3
                                                AND CreateDate BETWEEN '" + startDate + @"'
                                                AND '" + endDate + @"'
                                                AND CreateUserId = '" + userEntity.F_UserId + "'");
                                    DataTable Qdt = this.BaseRepository().FindTable(Qsql.ToString());
                                    model.count = Qdt.Rows[0]["cou"].ToInt();

                                    list.Add(model);
                                }
                            }
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 本月（时段）零件挑选原因占比
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<MaintenanceTypesModel> GetMaintenanceTypesModel(string startTime, string endTime, string supplier)
        {
            DateTime now = DateTime.Now;

            DateTime startDate = new DateTime();

            DateTime endDate = new DateTime();

            if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                startDate = now.AddMonths(-1);
                endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            }
            else if (string.IsNullOrEmpty(startTime))
            {
                endDate = Convert.ToDateTime(endTime);
                startDate = endDate.AddDays(-30);
            }
            else if (string.IsNullOrEmpty(endTime))
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = DateTime.Now;
            }
            else
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = Convert.ToDateTime(endTime);
            }

            string startD = startDate.ToString("yyyy-MM-dd HH:mm:ss");
            string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");

            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT YT_MaintenanceTypes.Name as name,Count(*) AS value 
                            FROM YT_Order
                            LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = YT_Order.MaintainType
                            WHERE YT_Order.IsDelete = 0 AND YT_Order.SuccessDate BETWEEN '" + startD + @"' and '" + endD + @"'");
            if (!string.IsNullOrEmpty(supplier))
            {
                strsql.Append(" AND YT_Order.Supplier_Id = '" + supplier + "' ");
            }
            strsql.Append(" GROUP BY YT_MaintenanceTypes.Name ");
            List<MaintenanceTypesModel> list = new RepositoryFactory().BaseRepository().FindList<MaintenanceTypesModel>(strsql.ToString()).AsList();
            return list;


        }

        public MaintenanceTypesAnalysis GetMaintenanceTypesToAnalysis(string startTime, string endTime, string supplier)
        {
            MaintenanceTypesAnalysis model = new MaintenanceTypesAnalysis();
            //DateTime startDate = Convert.ToDateTime(startTime);
            //DateTime endDate = Convert.ToDateTime(endTime);
            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT
	                    ISNULL(YT_MaintenanceTypes.Name,'') AS name,
	                    COUNT (*) AS
                    VALUE

                    FROM
	                    YT_Order
                    LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id = YT_Order.MaintainType
                    WHERE
	                    YT_Order.IsDelete = 0
                    AND YT_Order.Supplier_Id = '" + supplier + @"'
                    AND YT_Order.SuccessDate BETWEEN '" + startTime + @"'
                    AND '" + endTime + @"'
                    GROUP BY
	                    YT_MaintenanceTypes.Name");
            List<MaintenanceTypesModel> list = new RepositoryFactory().BaseRepository().FindList<MaintenanceTypesModel>(strsql.ToString()).AsList();
            int sum3 = list.Sum(x => x.value.ToInt());

            model.list = list;
            if (list.Count > 0)
            {
                int max = list[0].value.ToInt();
                for (int i = 1; i < list.Count; i++)
                {
                    if (list[i].value.ToInt() > max)
                    {
                        max = list[i].value.ToInt();
                    }
                }
                List<MaintenanceTypesModel> maxList = list.Where(x => x.value == max.ToString()).ToList();
                string maxname = "未知";
                string percent = "0.00%";
                if (maxList.Count > 0)
                {
                    maxname = maxList[0].name;
                    int sum = list.Sum(x => x.value.Trim().ToInt());
                    Decimal ftemp = (Decimal)max / sum;
                    percent = Convert.ToDecimal(ftemp * 100).ToString("f2") + "%";
                }
                model.maxname = maxname;
                model.percent = percent;
            }
            else
            {
                model.maxname = "未知";
                model.percent = "0.00%";
            }

            foreach (var item in list)
            {
                Decimal ftemp3 = (Decimal)item.value.ToInt() / sum3;
                string counts = Convert.ToDecimal(ftemp3 * 100).ToString("f2") + "%";
                item.name = item.name + counts + "(" + item.value + "个)";
            }

            return model;
        }

        /// <summary>
        /// 供应商累计合格率（合格数量/总数）
        /// </summary>
        /// <returns></returns>
        public PercentOfPassModel GetPercentOfPass(string startTime, string endTime, string supplier)
        {
            DateTime now = DateTime.Now;

            DateTime startDate = new DateTime();

            DateTime endDate = new DateTime();

            if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                startDate = now.AddMonths(-1);
                endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            }
            else if (string.IsNullOrEmpty(startTime))
            {
                endDate = Convert.ToDateTime(endTime);
                startDate = endDate.AddDays(-30);
            }
            else if (string.IsNullOrEmpty(endTime))
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = DateTime.Now;
            }
            else
            {
                startDate = Convert.ToDateTime(startTime);
                endDate = Convert.ToDateTime(endTime);
            }

            string startD = startDate.ToString("yyyy-MM-dd HH:mm:ss");
            string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");

            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT  CASE WHEN SUM(YT_OrderUser.Success) IS NULL THEN 0 ELSE SUM(YT_OrderUser.Success) END AS success,
                                    CASE WHEN SUM(YT_OrderUser.Error) IS NULL THEN 0 ELSE SUM(YT_OrderUser.Error) END  AS error
                            FROM YT_Order
                            LEFT JOIN YT_OrderUser ON YT_OrderUser.Order_Id = YT_Order.Id
                            WHERE YT_Order.IsDelete = 0 AND  YT_Order.Supplier_Id = '" + supplier + "' AND YT_Order.SuccessDate  BETWEEN '" + startD + @"'   AND '" + endD + "'");
            List<PercentOfPassModel> list = new RepositoryFactory().BaseRepository().FindList<PercentOfPassModel>(strsql.ToString()).AsList();
            return list[0];

        }


        public PercentOfPassModel GetPercentToAnalysis(string startTime, string endTime, string supplier)
        {
            //DateTime startDate = Convert.ToDateTime(startTime);
            //DateTime endDate = Convert.ToDateTime(endTime);

            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT
	                       SUM (
		                        ISNULL(YT_OrderUser.Error, 0)
	                        ) AS error,
	                        SUM (
		                        ISNULL(YT_OrderUser.Success, 0)
	                        ) AS success
                            FROM
	                            YT_OrderUser
                            LEFT JOIN YT_Order t ON YT_OrderUser.Order_Id = t.Id
                            WHERE
	                            t.SuccessDate BETWEEN '" + startTime + @"'
                            AND '" + endTime + @"'
                            AND t.IsDelete = 0
                            AND t.Supplier_Id = '" + supplier + "'");
            List<PercentOfPassModel> list = new RepositoryFactory().BaseRepository().FindList<PercentOfPassModel>(strsql.ToString()).AsList();
            return list[0];

        }

        /// <summary>
        /// 零件完成数量与耗时统计
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param> 
        /// <param name="supplier"></param>
        /// <returns></returns>
        public List<PartsFinishAndNumEcharsModel> GetPartsFinishAndNumEcharts(string startTime, string endTime, string supplier)
        {
            try
            {
                List<PartsFinishAndNumEcharsModel> list = new List<PartsFinishAndNumEcharsModel>();

                #region 时间筛选
                DateTime now = DateTime.Now;

                DateTime startDate = new DateTime();

                DateTime endDate = new DateTime();

                if (string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    startDate = now.AddMonths(-1);
                    endDate = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                }
                else if (string.IsNullOrEmpty(startTime))
                {
                    endDate = Convert.ToDateTime(endTime);
                    startDate = endDate.AddDays(-30);
                }
                else if (string.IsNullOrEmpty(endTime))
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = DateTime.Now;
                }
                else
                {
                    startDate = Convert.ToDateTime(startTime);
                    endDate = Convert.ToDateTime(endTime);
                }
                #endregion

                string startD = startDate.ToString("yyyy-MM-dd HH:mm:ss");
                string endD = endDate.ToString("yyyy-MM-dd HH:mm:ss");

                StringBuilder supplierSql = new StringBuilder();
                supplierSql.Append(@"SELECT DISTINCT
                             PartsType
                            FROM
                             YT_Order t
                            WHERE
                             t.SuccessDate BETWEEN '" + startD + @"'
                            AND '" + endD + @"'
                            AND t.IsDelete = 0
                            AND t.Order_Status = 5
                            AND t.Supplier_Id = '" + supplier + "'");
                DataTable dt = new RepositoryFactory().BaseRepository().FindTable(supplierSql.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string id = dt.Rows[i]["PartsType"].ToString();
                        PartsFinishAndNumEcharsModel model = new PartsFinishAndNumEcharsModel();
                        YT_PartsTypesEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(t => t.Id == id);
                        if (entity != null)
                        {
                            model.partsTypes = entity.Parts_Name;

                            StringBuilder aConsumeSql = new StringBuilder();
                            aConsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(EstimateDate, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Area = 1
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + @"'");
                            DataTable aConsumedt = new RepositoryFactory().BaseRepository().FindTable(aConsumeSql.ToString());
                            double aConsume = aConsumedt.Rows[0]["EstimateDate"].ToDouble();

                            StringBuilder bConsumeSql = new StringBuilder();
                            bConsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(ThreadElapsedTime, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Thread = 1
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + @"'");
                            DataTable bConsumedt = new RepositoryFactory().BaseRepository().FindTable(bConsumeSql.ToString());
                            double bConsume = bConsumedt.Rows[0]["EstimateDate"].ToDouble();

                            StringBuilder cConsumeSql = new StringBuilder();
                            cConsumeSql.Append(@"SELECT
                                 SUM (
                                  CONVERT (
                                   DECIMAL(10, 2),
                                   ISNULL(LineEdgeElapsedTime, 0.00)
                                  )
                                 ) AS EstimateDate
                                FROM
                                 YT_Order t
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.LineEdge = 1
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + @"'");
                            DataTable cConsumedt = new RepositoryFactory().BaseRepository().FindTable(cConsumeSql.ToString());
                            double cConsume = cConsumedt.Rows[0]["EstimateDate"].ToDouble();

                            StringBuilder aSumSql = new StringBuilder();
                            aSumSql.Append(@"SELECT
                                 ISNULL(
                                  SUM (ISNULL(Success + Error, 0)),
                                  0
                                 ) AS cou
                                FROM
                                 YT_OrderUser
                                LEFT JOIN YT_Order t ON t.Id = YT_OrderUser.Order_Id
                                WHERE
                                 t.SuccessDate BETWEEN '" + startD + @"'
                                AND '" + endD + @"'
                                AND t.IsDelete = 0
                                AND t.Order_Status = 5
                                AND t.PartsType = '" + dt.Rows[i]["PartsType"].ToString() + @"'");
                            DataTable aSumdt = new RepositoryFactory().BaseRepository().FindTable(aSumSql.ToString());
                            int aSum = aSumdt.Rows[0]["cou"].ToInt();
                            model.aCount = aSum;
                            model.aTime = aConsume + bConsume + cConsume;

                            if (aConsume > 0)
                            {
                                list.Add(model);
                            }
                        }
                    }
                }
                else
                {
                    list = new List<PartsFinishAndNumEcharsModel>();
                }

                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}
