﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTon.TMC.Util;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ReportInfo
{
    public class YT_ReportInfoEntity
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// ParentId
        /// </summary>
        [Column("PARENTID")]
        public string ParentId { get; set; }
        /// <summary>
        /// OrderNumber
        /// </summary>
        [Column("ORDERNUMBER")]
        public string OrderNumber { get; set; }
        /// <summary>
        /// Supplier
        /// </summary>
        [Column("SUPPLIER")]
        public string Supplier { get; set; }
        /// <summary>
        /// PartsNumber
        /// </summary>
        [Column("PARTSNUMBER")]
        public string PartsNumber { get; set; }
        /// <summary>
        /// PartsType
        /// </summary>
        [Column("PARTSTYPE")]
        public string PartsType { get; set; }
        /// <summary>
        /// BatchNumber
        /// </summary>
        [Column("BATCHNUMBER")]
        public string BatchNumber { get; set; }
        /// <summary>
        /// Sum
        /// </summary>
        [Column("SUM")]
        public int? Sum { get; set; }
        /// <summary>
        /// Qualified
        /// </summary>
        [Column("QUALIFIED")]
        public int? Qualified { get; set; }
        /// <summary>
        /// Disqualified
        /// </summary>
        [Column("DISQUALIFIED")]
        public int? Disqualified { get; set; }
        /// <summary>
        /// Finish
        /// </summary>
        [Column("FINISH")]
        public int? Finish { get; set; }
        /// <summary>
        /// Unfinished
        /// </summary>
        [Column("UNFINISHED")]
        public int? Unfinished { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// CreateUser
        /// </summary>
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        /// <summary>
        /// StartDate
        /// </summary>
        [Column("STARTDATE")]
        public string StartDate { get; set; }
        /// <summary>
        /// SuccessDate
        /// </summary>
        [Column("SUCCESSDATE")]
        public string SuccessDate { get; set; }
        /// <summary>
        /// AllotCount
        /// </summary>
        [Column("ALLOTCOUNT")]
        public string AllotCount { get; set; }
        /// <summary>
        /// MaintainType
        /// </summary>
        [Column("MAINTAINTYPE")]
        public string MaintainType { get; set; }
        /// <summary>
        /// RepairType
        /// </summary>
        [Column("REPAIRTYPE")]
        public string RepairType { get; set; }
        /// <summary>
        /// Person
        /// </summary>
        [Column("PERSON")]
        public string Person { get; set; }
        /// <summary>
        /// WorkHours
        /// </summary>
        [Column("WORKHOURS")]
        public string WorkHours { get; set; }
        /// <summary>
        /// BadProductDescribe
        /// </summary>
        [Column("BADPRODUCTDESCRIBE")]
        public string BadProductDescribe { get; set; }
        /// <summary>
        /// MattersAttention
        /// </summary>
        [Column("MATTERSATTENTION")]
        public string MattersAttention { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        [Column("COUNT")]
        public string Count { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
            this.CreateDate = DateTime.Now;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;

        }
        #endregion
    }
}
