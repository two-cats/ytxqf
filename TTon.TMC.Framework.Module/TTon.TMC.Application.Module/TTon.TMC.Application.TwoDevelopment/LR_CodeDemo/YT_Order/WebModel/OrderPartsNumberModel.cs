﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    /// <summary>
    /// 任务指派零件基本信息
    /// </summary>
    public class OrderPartsNumberModel
    {

        /// <summary>
        /// 已分配
        /// </summary>
        public int allocated { get; set; }

        /// <summary>
        /// 未分配
        /// </summary>
        public int unabsorbed { get; set; }

    }
}
