﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_MaintenanceTypes
{
    public class MaintenanceTypeTimeToAModel
    {
        public string maintenanceType { get; set; }

        public string average { get; set; }

        public string num { get; set; }
    }
}
