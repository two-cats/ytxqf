﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_ScanInfo;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/scan")]
    public class ScanInfoController : BaseApiController
    {
        private YT_ScanInfoIBLL scanInfoBLL = new YT_ScanInfoBLL();

        /// <summary>
        /// 验证是否是第一次扫描
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [Route("verify/count")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetScanCount(string keyValue)
        {
            try
            {
                var data = scanInfoBLL.GetScanCount(keyValue);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 扫描
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("add")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddScan(ScanParams param)
        {
            try
            {
                if (!string.IsNullOrEmpty(param.ScanTime))
                {
                    param.ScanTime = Convert.ToDateTime(param.ScanTime).ToString("yyyy-MM-dd HH:ss");
                }
                int index1 = param.Primitive.IndexOf("%23");
                if (index1 > -1)
                {
                    param.Primitive = param.Primitive.Replace("%23", "#");
                }
                int index2 = param.Equipment.IndexOf("%23");
                if (index2 > -1)
                {
                    param.Equipment = param.Equipment.Replace("%23", "#");
                }
                scanInfoBLL.AddScan(param);
                return Render(HttpStatusCode.OK, "success", new object());
            }
            catch (Exception)
            {
                return Render(HttpStatusCode.InternalServerError, "server error");
            }
        }
    }
}