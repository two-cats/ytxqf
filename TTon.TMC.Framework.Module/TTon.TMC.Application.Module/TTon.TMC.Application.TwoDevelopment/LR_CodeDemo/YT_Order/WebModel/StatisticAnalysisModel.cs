﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class StatisticAnalysisModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 时间段
        /// </summary>
        public string timeQuantum { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        public int orderCount { get; set; }
        /// <summary>
        /// SQE
        /// </summary>
        public string sqe { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public string supplier { get; set; }
        /// <summary>
        /// 本部
        /// </summary>
        public string headquarters { get; set; }
        /// <summary>
        /// 图表
        /// </summary>
        public OrderEchartModel orderEchartModel { get; set; }
    }
}
