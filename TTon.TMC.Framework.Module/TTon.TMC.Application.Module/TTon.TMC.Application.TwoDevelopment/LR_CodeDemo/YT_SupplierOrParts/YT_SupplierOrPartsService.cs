﻿using Dapper;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_SupplierOrParts.ImportExcel;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-08-21 13:20
    /// 描 述：配置供应商零件
    /// </summary>
    public class YT_SupplierOrPartsService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<YT_SupplierOrPartsEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.Id,
                YT_Supplier.Name AS Supplier_Id,YT_PartsTypes.Parts_Name AS Parts_Id,YT_MaintenanceTypes.Name AS MaintenanceType,
                t.Remark,
                t.CreateUserId,
                t.CreateDate,
                t.ModifyDate
                ");
                strSql.Append("  FROM YT_SupplierOrParts t  LEFT JOIN YT_Supplier ON YT_Supplier.Id=t.Supplier_Id  LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id=t.Parts_Id   LEFT JOIN YT_MaintenanceTypes ON YT_MaintenanceTypes.Id=t.MaintenanceType ");
                strSql.Append("  WHERE 1=1  AND t.IsDelete=0 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Supplier_Id"].IsEmpty())
                {
                    dp.Add("Supplier_Id",queryParam["Supplier_Id"].ToString(), DbType.String);
                    strSql.Append(" AND t.Supplier_Id = @Supplier_Id ");
                }
                return this.BaseRepository().FindList<YT_SupplierOrPartsEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取YT_SupplierOrParts表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public YT_SupplierOrPartsEntity GetYT_SupplierOrPartsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<YT_SupplierOrPartsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                YT_SupplierOrPartsEntity entity = GetYT_SupplierOrPartsEntity(keyValue);
                entity.IsDelete = 1;
                this.BaseRepository().Update<YT_SupplierOrPartsEntity>(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, YT_SupplierOrPartsEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 数据导入
        /// </summary>
        /// <param name="model"></param>
        public void InsertImportExcel(ImportModel model)
        {
            YT_SupplierEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(d=>d.Name==model.Supplier_Name&&d.Compellation==model.Supplier_user&&d.Phone==model.Supplier_phone);
            if (entity == null)
            {
                //供应商
                entity = new YT_SupplierEntity()
                {
                    Name = model.Supplier_Name,
                    Compellation = model.Supplier_user,
                    Phone = model.Supplier_phone
                };
                entity.Create();
                new RepositoryFactory().BaseRepository().Insert<YT_SupplierEntity>(entity);
            }
        
            if (!string.IsNullOrEmpty(model.Parts_Name))
            {
                //零件
                YT_PartsTypesEntity partsTypesEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(d=>d.Parts_Name==model.Parts_Name&&d.Parts_Number==model.Parts_Number);
                if (partsTypesEntity==null)
                {
                    partsTypesEntity = new YT_PartsTypesEntity()
                    {
                        Parts_Name = model.Parts_Name,
                        Parts_Number = model.Parts_Number
                    };
                    partsTypesEntity.Create();
                    new RepositoryFactory().BaseRepository().Insert<YT_PartsTypesEntity>(partsTypesEntity);
                }

                //维修类型

                YT_MaintenanceTypesEntity maintenanceTypesEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_MaintenanceTypesEntity>(d=>d.Name==model.MaintenanceTypes);
                if (maintenanceTypesEntity==null)
                {
                    maintenanceTypesEntity = new YT_MaintenanceTypesEntity()
                    {
                        Name = model.MaintenanceTypes
                    };
                    maintenanceTypesEntity.Create();
                    new RepositoryFactory().BaseRepository().Insert<YT_MaintenanceTypesEntity>(maintenanceTypesEntity);
                }

                YT_SupplierOrPartsEntity supplierOrPartsEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierOrPartsEntity>(d=>d.Supplier_Id==entity.Id&&d.Parts_Id==partsTypesEntity.Id&&d.MaintenanceType==maintenanceTypesEntity.Id);
                if (supplierOrPartsEntity==null)
                {
                    supplierOrPartsEntity = new YT_SupplierOrPartsEntity()
                    {
                        Supplier_Id = entity.Id,
                        Parts_Id = partsTypesEntity.Id,
                        MaintenanceType = maintenanceTypesEntity.Id
                    };
                    supplierOrPartsEntity.Create();
                    new RepositoryFactory().BaseRepository().Insert<YT_SupplierOrPartsEntity>(supplierOrPartsEntity);
                }
                

            }
            
        }
        #endregion

    }
}
