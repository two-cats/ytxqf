﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    /// <summary>
    /// 预估订单Model
    /// </summary>
    public class estimateOrderModel
    {
        /// <summary>
        /// 预估时间
        /// </summary>
        public string estimate_date { get; set; }
        /// <summary>
        /// 订单主键
        /// </summary>
        public string order_id { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        public string user_id { get; set; }
    }
}