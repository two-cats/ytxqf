﻿using TTon.TMC.Application.TwoDevelopment.WorkLog;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:31
    /// 描 述：日志评论
    /// </summary>
    public class WorkLog_CommentMap : EntityTypeConfiguration<WorkLog_CommentEntity>
    {
        public WorkLog_CommentMap()
        {
            #region 表、主键
            //表
            this.ToTable("WORKLOG_COMMENT");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

