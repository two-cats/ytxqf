﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTon.TMC.Application.WX.Controllers
{
    public class GuideBookController : ControllerBase
    {
        // GET: GuideBook
        public ActionResult Index()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "GuideBook/Index", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }
        // GET: GuideBook
        public ActionResult Add()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "GuideBook/Add", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }
        // GET: GuideBook
        public ActionResult Edit()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "GuideBook/Edit", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        // GET: GuideBook
        public ActionResult EditNext()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "GuideBook/Edit", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        // GET: GuideBook
        public ActionResult Detaile()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "GuideBook/Detaile", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }
    }
}