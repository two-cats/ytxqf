﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Profile;
using TTon.TMC.Application.Base.AuthorizeModule;
using TTon.TMC.Application.Base.OrganizationModule;
using TTon.TMC.Application.TwoDevelopment.Common;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.VX;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using TTon.TMC.DataBase.Repository;
using TTon.TMC.Util;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order
{

    public class WX_Order_StatisticsService: RepositoryFactory
    {

        /// <summary>
        /// 获取WX统计
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public WX_Order_StatisticsModel getWX_Order_Statistics(string supplier,string start,string end) {
            WX_Order_StatisticsModel model = new WX_Order_StatisticsModel();
            DateTime start_time =string.IsNullOrEmpty(start)?DateTime.Now: Convert.ToDateTime(start);
            DateTime end_time = string.IsNullOrEmpty(end) ? DateTime.Now : Convert.ToDateTime(end);
            //订单完成数量
            List< YT_OrderEntity > accomplishList= new RepositoryFactory().BaseRepository().FindList<YT_OrderEntity>(d=>d.Supplier_Id==supplier&&d.Order_Status==5&&d.CreateDate>=start_time&&d.CreateDate<=end_time).ToList();
            model.order_accomplish = accomplishList.Count;
            //订单未完成数量
            List<YT_OrderEntity> underwayList = new RepositoryFactory().BaseRepository().FindList<YT_OrderEntity>(d => d.Supplier_Id == supplier && d.Order_Status != 5 && d.CreateDate >= start_time && d.CreateDate <= end_time).ToList();
            model.order_underway = underwayList.Count;
            //订单总数量
            List<YT_OrderEntity> order_countList = new RepositoryFactory().BaseRepository().FindList<YT_OrderEntity>(d => d.Supplier_Id == supplier &&  d.CreateDate >= start_time && d.CreateDate <= end_time).ToList();
            model.order_count = order_countList.Count;

            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"SELECT YT_Order.Id AS order_id, YT_PartsTypes.Parts_Name AS parts_name,YT_Order.[Count] AS parts_count,
                                         Base_User.F_RealName AS create_user, YT_Order.Order_Status AS order_status
                            FROM YT_Order
                            LEFT JOIN YT_PartsTypes ON YT_PartsTypes.Id = YT_Order.PartsType
                            LEFT JOIN Base_User ON Base_User.F_UserId = YT_Order.CreateUserId
                            WHERE YT_Order.Supplier_Id = '"+supplier+"' AND  YT_Order.CreateDate BETWEEN '"+start_time+"' AND '"+end_time+"'");
            List<WX_Order> ordersList = new RepositoryFactory().BaseRepository().FindList<WX_Order>(strsql.ToString()).ToList();
            if (ordersList.Count!=0)
            {
                model.order_list = ordersList;
            }
            else
            {
                model.order_list = new List<WX_Order>();
            }
            return model;
        }


        /// <summary>
        /// 质检部向供应商提交订单
        /// </summary>
        public void SaveOrderEntity(YT_OrderEntity entity) {

            //查询零件
            YT_PartsTypesEntity partsTypesEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(d => d.Parts_Number == entity.PartsNumber.Trim());
            //没有就创建零件
            if (partsTypesEntity == null)
            {
                partsTypesEntity = new YT_PartsTypesEntity() {
                    Id = Guid.NewGuid().ToString(),
                    Parts_Name = entity.PartsType,
                    Parts_Number = entity.PartsNumber,
                    CreateDate = DateTime.Now,
                    IsDelete = 0,
                    CreateUserId = entity.Name
                };
                new RepositoryFactory().BaseRepository().Insert<YT_PartsTypesEntity>(partsTypesEntity);            
            }
            entity.PartsNumber = partsTypesEntity.Parts_Number;
            entity.PartsType = partsTypesEntity.Id;


            //关联表
            YT_SupplierOrPartsEntity yT_SupplierOrPartsEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierOrPartsEntity>(d => d.Supplier_Id == entity.Supplier_Id && d.Parts_Id == entity.PartsType);
            if (yT_SupplierOrPartsEntity == null)
            {
                yT_SupplierOrPartsEntity = new YT_SupplierOrPartsEntity()
                {
                    Supplier_Id = entity.Supplier_Id,
                    Parts_Id = entity.PartsType,
                    MaintenanceType = entity.MaintainType,
                    Id = Guid.NewGuid().ToString(),
                    CreateDate = DateTime.Now,
                    IsDelete = 0,
                };
                new RepositoryFactory().BaseRepository().Insert<YT_SupplierOrPartsEntity>(yT_SupplierOrPartsEntity);
            }
            //获取供应商信息
            YT_SupplierEntity supplierEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(entity.Supplier_Id);
            //添加订单

            entity.Order_Status =0;          
            entity.OrderNumber = "YTX" + supplierEntity.Name.Replace(" ", "") + DateTime.Now.ToString("yyMMddHHmm");
            entity.Create();
            new RepositoryFactory().BaseRepository().Insert<YT_OrderEntity>(entity);

            //添加流程
            YT_OrderProcessEntity orderProcessEntity = new YT_OrderProcessEntity() {
                Order_Id=entity.Id,
                Order_Status=0,
                CreateUser=entity.CreateUserId,
                CreateDate=DateTime.Now,
                Id=Guid.NewGuid().ToString()
            };
            new RepositoryFactory().BaseRepository().Insert<YT_OrderProcessEntity>(orderProcessEntity);

            //给总经理推送推送APP和短信
            string OrderAndTaskManage = Config.GetValue("OrderAndTaskManage");
            List<UserRelationEntity> userRelations = new RepositoryFactory().BaseRepository().FindList<UserRelationEntity>(d=>d.F_ObjectId== OrderAndTaskManage).ToList();
            if (userRelations.Count!=0)
            {
                List<string> clientList = new List<string>();
                foreach (var item in userRelations)
                {
                    UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d=>d.F_UserId==item.F_UserId);
                    if (!string.IsNullOrEmpty(userEntity.F_ClientId))
                    {
                        clientList.Add(userEntity.F_ClientId);
                    }
             
                }
                PushModel push = new PushModel()
                {
                    id = entity.Id,
                    title = "UMD向供应商发起了一条问题反馈,请及时关注",
                    content = entity.Name + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "提交订单",
                    status = 0,
                    type = 0

                };
                if (clientList.Count != 0)
                {
                    Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                }
                

            }

            UserEntity Pesponsible = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d=>d.F_RealName==entity.Pesponsible);
            if (!string.IsNullOrEmpty(Pesponsible.F_WeChat))
            {
                string openid = Pesponsible.F_WeChat;
                //填写推送内容
                WX_MessageModel vxmessage = new WX_MessageModel();
                List<Datalist> ldl = new List<Datalist>();
                vxmessage.touser = openid;
                vxmessage.template_id = "kle8jOEdMCq2cEY_VKiSahrd-7YDIRQWlBtpIGHhX_8";
                vxmessage.url = "http://ytxqf.vx.ttonservice.com/Order/Detaile?id=" + entity.Id;
                vxmessage.topcolor = "#FF0000";
                vxmessage.data = new Datalist()
                {
                    first = new item()
                    {
                        value = "您好，您有一条新的零件异常信息通知",
                        color = ""
                    },

                    keyword1 = new item()
                    {
                        value = entity.Name,
                        color = ""
                    },
                    keyword2 = new item()
                    {
                        value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        color = ""
                    },
                    keyword3 = new item()
                    {
                        value = entity.Name + "提交了" + partsTypesEntity.Parts_Name + "零件的异常信息。",
                        color = ""
                    },
                    remark = new item()
                    {
                        value = "请及时查看！",
                        color = ""
                    }

                };
                string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
                //获取access_token
                string access_token = getAccessToken();
                pushMassage(openid, jsonvxmessage, access_token);
            }
            //if (!string.IsNullOrEmpty(supplierEntity.Compellation))
            //{
            //    string[] array = supplierEntity.Compellation.Split(',');
            //    if (array.Length != 0)
            //    {
            //        foreach (var item in array)
            //        {
            //            UserEntity CompellationUser = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_RealName == item);
            //            YT_PartsTypesEntity partsTypesEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_PartsTypesEntity>(entity.PartsType);
            //            if (!string.IsNullOrEmpty(CompellationUser.F_WeChat))
            //            {
            //                string openid = CompellationUser.F_WeChat;
            //                //填写推送内容
            //                WX_MessageModel vxmessage = new WX_MessageModel();
            //                List<Datalist> ldl = new List<Datalist>();
            //                vxmessage.touser = openid;
            //                vxmessage.template_id = "7_91rKwiCRYZ7xjIXPhalkV4FYRz3nSy1-_BHSLm0WY";
            //                vxmessage.url = "http://ytxqf.vx.ttonservice.com/guidebook/edit?id=" + entity.Id;
            //                vxmessage.topcolor = "#FF0000";
            //                vxmessage.data = new Datalist()
            //                {
            //                    first = new item()
            //                    {
            //                        value = "您好，您有一条新的零件异常信息需要处理",
            //                        color = ""
            //                    },

            //                    keyword1 = new item()
            //                    {
            //                        value = entity.RepairType.Equals("0")? "挑选" : "返修",
            //                        color = ""
            //                    },
            //                    keyword2 = new item()
            //                    {
            //                        value = partsTypesEntity.Parts_Name,
            //                        color = ""
            //                    },
            //                    keyword3 = new item()
            //                    {
            //                        value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
            //                        color = ""
            //                    },
            //                    keyword4 = new item()
            //                    {
            //                        value = entity.Remark,
            //                        color = ""
            //                    },
            //                    remark = new item()
            //                    {
            //                        value =entity.Name+"对"+ partsTypesEntity.Parts_Name+ "产生的问题进行了描述，请完善订单。",
            //                        color = ""
            //                    }

            //                };
            //                string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
            //                //获取access_token
            //                string access_token = getAccessToken();
            //                pushMassage(openid, jsonvxmessage, access_token);
            //            }
            //        }
            //    }

            //    string[] phoneArray = supplierEntity.Phone.Split(',');
            //    if (phoneArray.Length != 0)
            //    {
            //        foreach (var item in phoneArray)
            //        {
            //            Task.Run(() => { new YT_OrderService().PushNote(item); });
            //        }
            //    }
            //}

        }


        /// <summary>
        /// WX
        /// </summary>
        /// <returns></returns>
        public string getAccessToken()
        {
            string access_token = string.Empty;
            string appid = Config.GetValue("WeChatAppId");
            string secret = Config.GetValue("WeChatSecret");
            string url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&APPID=" + appid + "&secret=" + secret;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream);
            string retString = myStreamReader.ReadToEnd();
            WxAccessTokenModel jsonResult = JsonConvert.DeserializeObject<WxAccessTokenModel>(retString);
            myStreamReader.Close();
            myResponseStream.Close();
            access_token = jsonResult.access_token;
            return access_token;
        }
        /// <summary>
        /// WX
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="body"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        static string pushMassage(string openid, string body, string access_token)
        {
            //第二步组装推送数进行推送
            string url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.ContentType = "application/json";

            byte[] buffer = encoding.GetBytes(body);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        #region 短信方法
        public void PushNote(string mobiles)
        {
            String product = "Dysmsapi"; //短信API产品名称（短信产品名固定，无需修改）
            String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
            String accessKeyId = "LTAIVBUBSmF5xENU";//你的accessKeyId
            String accessKeySecret = "2zBjUTiMhq4CHBB3EZjrMzUuIZxqzI";//你的accessKeySecret
            IClientProfile profile = Aliyun.Acs.Core.Profile.DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            //初始化ascClient,暂时不支持多region（请勿修改）
            Aliyun.Acs.Core.Profile.DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            try
            {
                request.PhoneNumbers = mobiles;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = "亚泰新";
                //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                request.TemplateCode = "SMS_143560574";
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                request.TemplateParam = "";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
            }
            catch (Exception e)
            {
                if (e is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(e);
                }
            }
        }
        #endregion

        /// <summary>
        /// 库管提交给值班经理
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool OrderInventory(OrderInventoryModel model) {

            YT_OrderEntity orderEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(model.order_id);
            if (orderEntity==null)
            {
                return false;
            }
            orderEntity.Order_Status = orderEntity.Order_Status==6? 2: orderEntity.Order_Status;
            orderEntity.Count = orderEntity.Count == null ? model.count : orderEntity.Count + model.count;
            new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(orderEntity);
            YT_OrderInventoryEntity inventoryEntity = new YT_OrderInventoryEntity() {
                Id=Guid.NewGuid().ToString(),
                User_Id=model.user_id,
                Order_Id=model.order_id,
                Count=model.count,
                Batch_Number = model.batch_number,
                CreateTime=DateTime.Now,
                CreateUserId=model.user_id
            };
            new RepositoryFactory().BaseRepository().Insert<YT_OrderInventoryEntity>(inventoryEntity);
            ///获取当天值班经理
            StringBuilder strsql = new StringBuilder();
            strsql.Append(@"select Base_User.* from Base_User
                            WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
            List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).ToList();
            if (userList.Count != 0)
            {
                List<string> clientList = new List<string>();
                foreach (var item in userList)
                {
                    if (!string.IsNullOrEmpty(item.F_ClientId))
                    {
                        clientList.Add(item.F_ClientId);
                    }
                }

                PushModel push = new PushModel()
                {
                    id = model.order_id,
                    title = "取件通知",
                    content ="仓库管理员为您分配了" + model.count + "件,请联系管理员取件。",
                    status = 2,
                    type = 0

                };
                if (clientList.Count != 0)
                {
                    Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                }


            }
            return true;
        }


        /// <summary>
        /// 更改任务提交零件数量
        /// </summary>
        /// <returns></returns>
        public bool UpdateTask(UpdateTaskModel model) {
            
            YT_OrderUserEntity orderUserEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderUserEntity>(model.task_id);

            YT_OrderEntity orderEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(orderUserEntity.Order_Id);
            if (orderEntity.Order_Status==5)
            {
                return false;
            }

            orderUserEntity.Success =model.success;
            orderUserEntity.Error = model.error;
            orderUserEntity.Unfinished = orderUserEntity.All.ToInt() - model.success - model.error;
            new RepositoryFactory().BaseRepository().Update<YT_OrderUserEntity>(orderUserEntity);



            ///获取当天值班经理
            StringBuilder strsql = new StringBuilder();

            //strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + orderManage + "'");

            strsql.Append(@"select Base_User.* from Base_User
                            WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
            List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).ToList();
            if (userList.Count != 0)
            {
                List<string> clientList = new List<string>();
                foreach (var item in userList)
                {
                    if (!string.IsNullOrEmpty(item.F_ClientId))
                    {
                        clientList.Add(item.F_ClientId);
                    }
                }
                UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(model.user_id);
                PushModel push = new PushModel()
                {
                    id = model.task_id,
                    title = "更改任务通知",
                    content = userEntity.F_RealName + "在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "更改"+orderEntity.OrderNumber+"中任务",
                    status = 1,
                    type = 3

                };
                if (clientList.Count != 0)
                {
                    Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                }
            }

            return true;
        }

        /// <summary>
        /// 库管确认订单
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public bool ManageSubmit(string order_id,string user_id) {
            YT_OrderEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(order_id);
            if (entity.Order_Status!=7)
            {
                return false;
            }
            entity.Order_Status = 5;

            #region 库管确认后更新总数
            StringBuilder sumSql = new StringBuilder();
            sumSql.Append(@"SELECT
                            ISNULL(SUM(ISNULL(Success + Error, 0)),0) AS cou 
                            FROM  YT_OrderUser 
                            where Order_Id = '"+entity.Id + "'");
            DataTable sumdt = new RepositoryFactory().BaseRepository().FindTable(sumSql.ToString());

            int sum = sumdt.Rows[0]["cou"].ToInt();
            #endregion

            entity.Count = sum;
            new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(entity);


            UserEntity userManage = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(user_id);

            #region 推送给供应商
            YT_SupplierEntity supplierEntity = new RepositoryFactory().BaseRepository().FindEntity<YT_SupplierEntity>(entity.Supplier_Id);
            if (supplierEntity != null)
            {
                string[] array = supplierEntity.Compellation.Split(',');
                if (array.Length != 0)
                {
                    foreach (var item in array)
                    {
                        UserEntity user = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(d => d.F_RealName == item);
                        if (!string.IsNullOrEmpty(user.F_WeChat))
                        {
                            string open = user.F_WeChat;
                            //填写推送内容
                            WX_MessageModel message = new WX_MessageModel();
                            List<Datalist> ldls = new List<Datalist>();
                            message.touser = open;
                            message.template_id = "tdZxgehDK_ze9WbXo_oRG66fO2-STMARy5H67QgXMLw";
                            message.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
                            message.topcolor = "#FF0000";
                            message.data = new Datalist()
                            {
                                first = new item()
                                {
                                    value = "您好，已维修完成",
                                    color = ""
                                },

                                keyword1 = new item()
                                {
                                    value = userManage.F_RealName,
                                    color = ""
                                },
                                keyword2 = new item()
                                {
                                    value = entity.SuccessDate,
                                    color = ""
                                },
                                remark = new item()
                                {
                                    value = "请查收。",
                                    color = ""
                                }

                            };
                            string jsonvx = JsonConvert.SerializeObject(message);
                            //获取access_token
                            string token = getAccessToken();
                            pushMassage(open, jsonvx, token);
                        }
                    }
                }
            }
            #endregion

            #region 推送给创建人
            if (!String.IsNullOrEmpty(entity.CreateUserId))
            {
                UserEntity vxue = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(entity.CreateUserId);
                string openid = vxue.F_WeChat;
                //填写推送内容
                WX_MessageModel vxmessage = new WX_MessageModel();
                List<Datalist> ldl = new List<Datalist>();
                vxmessage.touser = openid;
                vxmessage.template_id = "tdZxgehDK_ze9WbXo_oRG66fO2-STMARy5H67QgXMLw";
                vxmessage.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
                vxmessage.topcolor = "#FF0000";
                vxmessage.data = new Datalist()
                {
                    first = new item()
                    {
                        value = "您好，已维修完成",
                        color = ""
                    },

                    keyword1 = new item()
                    {
                        value = userManage.F_RealName,
                        color = ""
                    },
                    keyword2 = new item()
                    {
                        value = entity.SuccessDate,
                        color = ""
                    },
                    remark = new item()
                    {
                        value = "请查收。",
                        color = ""
                    }

                };
                string jsonvxmessage = JsonConvert.SerializeObject(vxmessage);
                //获取access_token
                string access_token = getAccessToken();
                pushMassage(openid, jsonvxmessage, access_token);
            }
            #endregion

            #region 推送给SQE
            if (entity.Pesponsible != null)
            {
                UserEntity pes = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(t => t.F_RealName == entity.Pesponsible);
                string openId = pes.F_WeChat;
                //填写推送内容
                WX_MessageModel vxmess = new WX_MessageModel();
                List<Datalist> ldlss = new List<Datalist>();
                vxmess.touser = openId;
                vxmess.template_id = "tdZxgehDK_ze9WbXo_oRG66fO2-STMARy5H67QgXMLw";
                vxmess.url = "http://ytxqf.vx.ttonservice.com/order/Detaile?id=" + entity.Id;
                vxmess.topcolor = "#FF0000";
                vxmess.data = new Datalist()
                {
                    first = new item()
                    {
                        value = "您好，已维修完成",
                        color = ""
                    },

                    keyword1 = new item()
                    {
                        value = userManage.F_RealName,
                        color = ""
                    },
                    keyword2 = new item()
                    {
                        value = entity.SuccessDate,
                        color = ""
                    },
                    remark = new item()
                    {
                        value = "请查收。",
                        color = ""
                    }

                };
                string json = JsonConvert.SerializeObject(vxmess);
                //获取access_token
                string accesstoken = getAccessToken();
                pushMassage(openId, json, accesstoken);
            }
            #endregion
            return true;
        }


        /// <summary>
        /// 库管拒绝订单
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="user_id"></param>
        /// <returns></returns>
        public bool UpdateStorekeeper(string order_id,string user_id) {
            YT_OrderEntity entity = new RepositoryFactory().BaseRepository().FindEntity<YT_OrderEntity>(order_id);
            entity.Order_Status = 4;
            new RepositoryFactory().BaseRepository().Update<YT_OrderEntity>(entity);

            ///获取当天值班经理
            StringBuilder strsql = new StringBuilder();

            //strsql.Append("select Base_User.* from Base_User left join Base_UserRelation on Base_UserRelation.F_UserId=Base_User.F_UserId where Base_UserRelation.F_ObjectId='" + orderManage + "'");

            strsql.Append(@"select Base_User.* from Base_User
                            WHERE Base_User.F_UserId IN(SELECT TOP 1 YT_LogReport.HeirUser  FROM YT_LogReport WHERE HeirStatus = 1 ORDER BY CreateDate DESC)");
            List<UserEntity> userList = new RepositoryFactory().BaseRepository().FindList<UserEntity>(strsql.ToString()).ToList();
            if (userList.Count != 0)
            {
                List<string> clientList = new List<string>();
                foreach (var item in userList)
                {
                    if (!string.IsNullOrEmpty(item.F_ClientId))
                    {
                        clientList.Add(item.F_ClientId);
                    }
                }
                UserEntity userEntity = new RepositoryFactory().BaseRepository().FindEntity<UserEntity>(user_id);
                PushModel push = new PushModel()
                {
                    id = order_id,
                    title = "订单被拒绝",
                    content = userEntity.F_RealName + "仓库馆里员在" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "拒绝您的" + entity.OrderNumber + "订单",
                    status = 4,
                    type = 5

                };
                if (clientList.Count != 0)
                {
                    Task.Run(() => { GeTuiHelper.PushMessageToList(clientList, push); });
                }
            }
            return true;
        }

    }
}
