﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTon.TMC.Application.WX.Controllers
{
    public class OrderController : ControllerBase
    {
        // GET: Order
        public ActionResult Index()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "Order/Index", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        public ActionResult Detaile()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "Order/Detaile", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }

        public ActionResult Add()
        {
            //var result = HasLinkToWechatUser();

            //if (!result)
            //{
            //    return RedirectToAction("Register", "", new { rel = "Order/Add", wechatUser = wechatUser });
            //}
            //else
            //{
            //    return View();
            //}
            return View();
        }

        public ActionResult Scan()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "Order/Scan", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }
        }
    }
}