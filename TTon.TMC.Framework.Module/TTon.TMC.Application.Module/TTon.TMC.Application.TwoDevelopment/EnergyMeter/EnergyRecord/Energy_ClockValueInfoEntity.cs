﻿using TTon.TMC.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TTon.TMC.Application.TwoDevelopment.EnergyMeter
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-04-25 19:58
    /// 描 述：抄表
    /// </summary>
    public class Energy_ClockValueInfoEntity 
    {
        #region 实体成员
        /// <summary>
        /// Id
        /// </summary>
        [Column("ID")]
        public string Id { get; set; }
        /// <summary>
        /// ClockId
        /// </summary>
        [Column("CLOCKID")]
        public string ClockId { get; set; }
        /// <summary>
        /// ItemId
        /// </summary>
        [Column("ITEMID")]
        public string ItemId { get; set; }
        /// <summary>
        /// ItemValue
        /// </summary>
        [Column("ITEMVALUE")]
        public string ItemValue { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Id = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Id = keyValue;
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

