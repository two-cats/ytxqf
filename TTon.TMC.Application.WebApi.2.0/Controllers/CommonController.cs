﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TTon.TMC.Application.Base.SystemModule;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_LogReport.Model;
using TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel;
using TTon.TMC.Application.WebApi._2._0.Models;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Controllers
{
    [RoutePrefix("v1.0/common")]
    public class CommonController : BaseApiController
    {
        private AnnexesFileIBLL annexesFileIBLL = new AnnexesFileBLL();
        private YT_OrderService orderService = new YT_OrderService();


        /// <summary>
        /// 上传附件分片数据
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        [Route("upload/image")]
        [AcceptVerbs("Post")]
        public HttpResponseMessage UploadAnnexesFileChunk(string folderId)
        {

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return Render(HttpStatusCode.InternalServerError, "请使用form-data上传图片");
            }

            var fileName = string.Empty;
            string savePath = Config.GetValue("ImagePath");
            var sExtension = string.Empty;
            var fileGuidName = string.Empty;
            string ImageID = string.Empty;
            //指定要将文件存入的服务器物理位置  
            string root = System.Web.HttpContext.Current.Server.MapPath("~/App_Data");

            var provider = new MultipartFormDataStreamProvider(root);
            try
            {
                Task.Run(async () => await Request.Content.ReadAsMultipartAsync(provider)).Wait();
                // 以下描述了如何获取文件
                foreach (MultipartFileData file in provider.FileData)
                {
                    try
                    {
                        //获取上传文件信息。
                        if (string.IsNullOrEmpty(folderId))
                        {
                            folderId = Guid.NewGuid().ToString();
                        }
                        string fileGuid = Guid.NewGuid().ToString();
                        fileName = file.Headers.ContentDisposition.FileName.Trim(Char.Parse("\""));
                        fileGuidName = file.LocalFileName;
                        int chunks = 1;
                        ImageID = annexesFileIBLL.SaveAnnexesToId(folderId, fileGuid, fileName, chunks, fileGuidName);
                    }
                    catch (Exception ex)
                    {
                        return Render(HttpStatusCode.InternalServerError, "failed: server error");
                    }
                }
            }
            catch (Exception ex)
            {
                return Render(HttpStatusCode.InternalServerError, "failed: server error");
            }
            return Render(HttpStatusCode.OK, "success", ImageID);
        }




        /// <summary>
        /// 发送日志
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        [Route("mail")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage SendMail(ExchangeReportParams param)
        {
            try
            {
                var data = orderService.SendMail(param.StartTime, param.EndTime, param.UserId, param.HeirUser,param.MattersAttention);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                return Render(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// 交班日志详情中的列表+详情
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [Route("report/detail")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage getReportDetail(string reportId)
        {
            try
            {
                var data = orderService.getReportDetail(reportId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 日志交接
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("exchange")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage ExchangeReport(ExchangeModel param)
        {
            try
            {
                orderService.ExchangeReport(param.reportId);

                return Render(HttpStatusCode.OK, "success", new object());
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 日志列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("list")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetReportList(GetOrderListParam param)
        {
            try
            {
                var data = orderService.GetReportList(param);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 获取报告
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("report")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetStatistics(string keyvalue)
        {
            try
            {
                var data = orderService.GetStatistics(keyvalue);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }


        /// <summary>
        /// 获取报告列表
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("report/list")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetStatisticsList(string keyvalue)
        {
            try
            {
                var data = orderService.GetStatisticsList(keyvalue);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }

        /// <summary>
        /// 检查目录小红点
        /// </summary>
        /// <returns></returns>
        [Route("check/menu")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage checkMenu(string userId)
        {
            try
            {
                var data = orderService.checkMenu(userId);

                return Render(HttpStatusCode.OK, "success", data);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    return Render(HttpStatusCode.InternalServerError, "server error");
                }
            }
        }
    }
}