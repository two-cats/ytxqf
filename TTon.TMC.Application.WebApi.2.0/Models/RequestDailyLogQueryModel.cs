﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTon.TMC.Util;

namespace TTon.TMC.Application.WebApi._2._0.Models
{
    public class RequestDailyLogQueryModel
    {
        public Pagination pagination { get; set; }

        public string userId { get; set; }

        /// <summary>
        /// Scope:send / approve
        /// </summary>
        public string scope { get; set; }
    }
}