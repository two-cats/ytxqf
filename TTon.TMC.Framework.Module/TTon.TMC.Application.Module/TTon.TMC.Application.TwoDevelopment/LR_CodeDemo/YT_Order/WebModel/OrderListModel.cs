﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTon.TMC.Application.TwoDevelopment.LR_CodeDemo.YT_Order.WebModel
{
    public class OrderListModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Date { get; set; }

        public string OrderNumber { get; set; }

        public string PartsNumber { get; set; }

        public string Supplier { get; set; }

        public string PartsType { get; set; }
   
        public string MaintainType { get; set; }

        public string InstructorId { get; set; }

        public string InstructorName { get; set; }

        public string Remark { get; set; }

        public int? Order_Status { get; set; }

        public string EstimateDate { get; set; }

        public string CreateUser { get; set; }

        public string CreateDate { get; set; }

        public string SuccessDate { get; set; }

        public string Pic { get; set; }
    }
}
