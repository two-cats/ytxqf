﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace TTon.TMC.Application.WX.Controllers
{
    public class WOController : ControllerBase
    {
        // GET: WO
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddOrder()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "WO/Addorder", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }

        }

        /// <summary>
        /// 个人用户统计
        /// </summary>
        /// <returns></returns>
        public ActionResult Statistics()
        {
            var result = HasLinkToWechatUser();
            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "WO/Statistics", wechatUser = wechatUser });
            }
            else
            {
                string btime = DateTime.Now.AddDays(-3).ToString("yyyy/MM/dd");
                string etime = DateTime.Now.Date.ToString("dd");
                ViewData["time"] = btime + "~" + etime;
                return View();
            }
        }

        public ActionResult List()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "WO/List", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }

        }

        public ActionResult Detaile()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "WO/Detaile", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }

        }


        public ActionResult Nopermissions()
        {
            var result = HasLinkToWechatUser();

            if (!result)
            {
                return RedirectToAction("Register", "", new { rel = "WO/Detaile", wechatUser = wechatUser });
            }
            else
            {
                return View();
            }

        }

    }
}