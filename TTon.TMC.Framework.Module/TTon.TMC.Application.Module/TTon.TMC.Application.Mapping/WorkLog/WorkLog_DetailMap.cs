﻿using TTon.TMC.Application.TwoDevelopment.WorkLog;
using System.Data.Entity.ModelConfiguration;

namespace  TTon.TMC.Application.Mapping
{
    /// <summary>
    /// 版本 TTon.TMC
    /// Copyright (c) 2013-2017 TTON
    /// 创 建：超级管理员
    /// 日 期：2018-05-29 14:25
    /// 描 述：工作日志
    /// </summary>
    public class WorkLog_DetailMap : EntityTypeConfiguration<WorkLog_DetailEntity>
    {
        public WorkLog_DetailMap()
        {
            #region 表、主键
            //表
            this.ToTable("WORKLOG_DETAIL");
            //主键
            this.HasKey(t => t.Id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

